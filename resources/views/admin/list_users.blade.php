@include('include.admin_header')	


<!-- summernote -->
  <link rel="stylesheet" href="{{ asset('public/adminlte/plugins/summernote/summernote-bs4.min.css')}}">
<div class="content-body pb-5 pt-5">
		
		<div class="container w-100">
				
				<!-- register form stepper -->
					
					<div class="row">
					  <div class="col-md-12">
						<div class="card card-default">
						  <div class="card-header">
							<h3 class="pl-4 mt-3 font-weight700 color-blue">Users</h3>
						  </div>
						  <div class="card-body">
							<div class="card shadow-none">
              <div class="card-header">
                <div class="row p-4 mb-4" style="background: #f3f3f3;
border-radius: 8px;">
                	<div class="col-md-12">
                		<h3>Filters:</h3>
                	</div>
									<div class="col-md-3">
												<select class="form-control" id="userType" name="userType">
												  	<option value="">Select User Type</option>
												  	<option value="1">Partner</option>
												  	<option value="2">Agency</option>
												  	<option value="3">Agent</option>
												  </select>
										</div>
										<div class="col-md-2">
												<select class="form-control" id="stateList" name="stateList">
												  	<option value="">Select State</option>
												  </select>
										</div>
										<div class="col-md-2">
												<select class="form-control" id="cityList" name="cityList">
												  	<option value="">Select City</option>
												  </select>
										</div>
										<div class="col-md-2">
												<select class="form-control" id="adminStatus" name="adminStatus">
												  	<option value="">Select Status</option>
												  	<option value="not">Not Submitted</option>
												  	<option value="submitted">Submitted</option>
												  	<option value="approved">Approved</option>
												  	<option value="rejected">Rejected</option>
												  </select>
										</div>
										<div class="col-md-1" >
												<button  id="userTblFilter" onclick="showSearchResult()" class="btn btn-primary">Search</button>
										</div>
										<div class="col-md-2" style="text-align: center;">
												<button  id="userTblFilterClr" onclick="clearFilter('user')" class="btn btn-primary">Reset</button>
										</div>
										
              </div>
              <!-- /.card-header -->
              <div class="card-body  pl-0 pr-0">
                <table id="adminUsersList" class="table table-bordered table-striped table-hover mt-4" style="width:100%">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Entity Type</th>
                    <th>Business Name</th>
                    <th>Primary Contact</th>
                    <th>State / City</th>
                    <th>Application Status</th>
                    <th>Application</th>
                    <th>Profile</th>
                  </tr>
                  </thead>
                  <tbody class="userListBody">
                 
                  
                  
                  </tbody>
                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
							  
								
							
						  </div>
						  <!-- /.card-body -->
						  
						</div>
						<!-- /.card -->
					  </div>
					</div>
				
				<!-- register form stepper -->
				
			</div>
		
	</div>

	<!-- models -->
<div class="modal fade" id="adminCommentsModal">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Add Review Comments </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <textarea id="summernote">
               
              </textarea>
              <input type="hidden" id="selectedUserId"/>
              <input type="hidden" id="selectedUserType" />
              <input type="hidden" id="selectedApplicationId"/>
            </div>
           <div class="modal-footer justify-content-between">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-primary" onclick="submitReview()">Submit</button>
</div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->



      <div class="modal fade" id="adminCommentsList">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Review Comments </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div id="accordion" class="adminReviewAccordion">
                  
                  
                  
                </div>
            </div>
           <div class="modal-footer justify-content-between">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

</div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


      <div class="modal fade" id="managerModal">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Assign Manager </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            	<div class="form-group">
            		<select class="form-control" id="managerName" name="managerName">
													<option value="">Select Manager</option>

													@foreach($managerList as $data)
													<option value="{{$data['id']}}">{{$data['name']}}</option>
													@endforeach
												  </select>
             <!--	<input type="text" class="form-control" id="managerName"  />-->
             	<input type="hidden" id="managerUserId"  />
             	<div class="error managerError"></div>
             </div>
            </div>
           <div class="modal-footer justify-content-between">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-primary" onclick="assignManager()">Submit</button>

</div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
	
	@include('include.admin_footer')
<!-- Summernote -->
<script src="{{ asset('public/adminlte/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script>
  $(function () {
  	loadUserList();

  	

    $('#summernote').summernote({
		  height: 300,
		  minHeight: 300,              
		  maxHeight: 450,
		  toolbar: [
						    // [groupName, [list of button]]
						    ['style', ['bold', 'italic', 'underline', 'clear']],
						    ['font', ['strikethrough', 'superscript', 'subscript']],
						    ['fontsize', ['fontsize']],
						    ['color', ['color']],
						    ['para', ['ul', 'ol', 'paragraph']],
						    ['height', ['height']]
						  ]
		});

		$('#adminCommentsModal').on('hidden.bs.modal', function () {
		    $('#selectedUserId').val('');
				$('#selectedUserType').val('');
				$('#selectedApplicationId').val('');
		});
    
  });
</script>
