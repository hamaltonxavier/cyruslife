<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

class HomeController extends Controller
{
    public function showLandingPage(){
        $title = 'CYRUS Life Insurance';
        return view('landing_page', compact('title'));
    }

    public function showPartnerRegister(){
       /*  if (auth()->user()) {
            return redirect('/show-profile');
        }*/
        $title = 'CYRUS | Partner Registration';
        $countryDetails = DB::table('countries')->whereIn('id', [233])->get()->toArray();
        return view('partner_registration', compact(['title','countryDetails']));
    }

    public function showAgencyRegister(){
        /* if (auth()->user()) {
            return redirect('/show-profile');
        }*/
        $title = 'CYRUS | Agency Registration';
        $countryDetails = DB::table('countries')->whereIn('id', [233])->get()->toArray();
        return view('agency_registration', compact(['title','countryDetails']));
    }


    public function showAgentRegister(){
         /*if (auth()->user()) {
            return redirect('/show-profile');
        }*/
        $title = 'CYRUS | Agent Registration';
        $countryDetails = DB::table('countries')->whereIn('id', [233])->get()->toArray();
        return view('agent_registration', compact(['title','countryDetails']));
    }

    public function showSigninPage(){
         $title = 'CYRUS | Sign In';
        return view('login', compact('title'));
    }

    public function resetNewPassword(Request $request, $token = null){
         $title = 'CYRUS | Create Password';
         $token = $token;
         $email = $request->email;
        return view('reset_new_password', compact(['title','token','email']));
    }


    public function createPassword(){
         $title = 'CYRUS | Reset Password';
        return view('new_password', compact('title'));
    }

     public function forgotPassword(){
         $title = 'CYRUS | Forgot Password';
        return view('forgotpassword', compact('title'));
    }


    public function loadStateBasedonCountry(Request $request){
        $countryId = $request->countryId;
         $stateDetails =DB::table('states')
         ->join('countries', 'countries.id', '=', 'states.country_id')
         ->select('states.id', 'states.name')
         ->where('countries.id', '=', $countryId)
         ->orderBy('states.name','ASC')
         ->get();
         return response()->json($stateDetails);
    }

    public function loadCityBasedonCountry(Request $request){
        $stateId = $request->stateId;
         $cityDetails =DB::table('cities')
         ->join('states', 'states.id', '=', 'cities.state_id')
         ->select('cities.id', 'cities.name')
         ->where('states.id', '=', $stateId)
         ->orderBy('cities.name','ASC')
         ->get();
         return response()->json($cityDetails);
    }


    public function adminLogin(){
        $title = 'CYRUS | Admin Login';
        return view('admin.login', compact('title'));
    }
    
    public function thankyou(){
        $title = 'CYRUS | Thanks You';
        return view('registration_thanks', compact('title'));
    }
    
    public function getDocDetails(Request $request){
        $userId = $request->userid;
        
         $docDetails = DB::table('document_path')->where('user_id','=', $userId)->get()->toArray();
         return json_encode($docDetails);
         //var_dump($docDetails);
    }

}
