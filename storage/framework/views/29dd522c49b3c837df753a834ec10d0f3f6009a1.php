   <?php if(Session::has('message-danger')): ?>
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-ban"></i> <?php echo e(Session::get('message-danger')); ?></h5>
</div>
<?php endif; ?>
<?php if(Session::has('message-success')): ?>
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-check"></i> <?php echo e(Session::get('message-success')); ?></h5>
</div> 
<?php endif; ?>
<?php if(Session::has('message-info')): ?>
<div class="alert alert-info alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-info"></i> <?php echo e(Session::get('message-info')); ?></h5>
</div> 
<?php endif; ?><?php /**PATH C:\xampp\htdocs\cyruslife\resources\views/include/message.blade.php ENDPATH**/ ?>