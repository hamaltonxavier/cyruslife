@include('include.admin_header')	
<!-- summernote -->
  <link rel="stylesheet" href="{{ asset('public/adminlte/plugins/summernote/summernote-bs4.min.css')}}">
<div class="content-body pb-5 pt-5">
		
		<div class="container w-100">
				
				<!-- register form stepper -->
					
					<div class="row">
					  <div class="col-md-12">
						<div class="card card-default">
						  <div class="card-header">
							<h3 class="pl-4 mt-3 font-weight700 color-blue">Funeral Home</h3>
						  </div>
						  <div class="card-body">
							<div class="card shadow-none">
              <div class="card-header">
                <div class="row p-4 mb-4" style="background: #f3f3f3;
border-radius: 8px;">
                	<div class="col-md-12">
                		<h3>Filters:</h3>
                	</div>
										<div class="col-md-3">
												<select class="form-control" id="userType" name="userType">
												  	<option value="">Select User Type</option>
												  	<option value="1">Partner</option>
												  	<option value="2">Agency</option>
												  	<option value="3">Agent</option>
												  </select>
										</div>
										<div class="col-md-3">
												<select class="form-control" id="stateList" name="stateList">
												  	<option value="">Select State</option>
												  </select>
										</div>
										<div class="col-md-2">
												<select class="form-control" id="cityList" name="cityList">
												  	<option value="">Select City</option>
												  </select>
										</div>
										
										<div class="col-md-2" style="text-align: center;">
												<button  id="userTblFilter" onclick="showFHResult()" class="btn btn-primary">Search</button>
										</div>
										<div class="col-md-2" style="text-align: left;">
												<button  id="userTblFilterClr" onclick="clearFilter('funeral')" class="btn btn-primary">Reset</button>
										</div>
										
              </div>
              <!-- /.card-header -->
              <div class="card-body  pl-0 pr-0">
                <table id="adminFHList" class="table table-bordered table-striped table-hover" style="width:100%">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>User</th>
                    <th>Address</th>
                    <th>State / City</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody class="fhListBody">
                 
                  
                  
                  </tbody>
                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
							  
								
							
						  </div>
						  <!-- /.card-body -->
						  
						</div>
						<!-- /.card -->
					  </div>
					</div>
				
				<!-- register form stepper -->
				
			</div>
		
	</div>

	
	
	@include('include.admin_footer')

<script>
  $(function () {
  	loadFH();
    
  });

  function loadFH(){
  	$.ajax({
	            type: "GET",
	            url: "/admin/load_fh_list",
	            dataType: 'json', 
  				encode  : true,
  				 async: false,
  				 beforeSend: function() {
                   $.blockUI({ css: { 
			            border: 'none', 
			            padding: '15px', 
			            backgroundColor: '#000', 
			            '-webkit-border-radius': '10px', 
			            '-moz-border-radius': '10px', 
			            opacity: .5, 
			            color: '#fff' 
			        } }); 
                },
	            success: function (data) {
	             	
	             	//console.log(data);
	             	var tbl = '';
	             	var i = 1;
	             	var address3 = '';
	             	for(res in data){

	             		if(data[res]["address3"] != null){
	             				address3 = data[res]["address3"];
	             		}

	             		tbl +='<tr><td>'+i+'</td>';
	             		tbl +='<td>'+data[res]["funeral_home_name"]+'</td>';
	             		if(data[res]["role"] =='3'){
	             			firstName = data[res]["first_name"];
										middleName = data[res]["middle_name"];
										lastName = data[res]["last_name"];
										if(firstName == null){
											firstName = '';
										}
										if(middleName == null){
											middleName = '';
										}
										if(lastName == null){
											lastName = '';
										}
	             			tbl += '<td>'+firstName+' '+middleName+' '+lastName+'</td>';
	             		}
	             		else{
	             			tbl +='<td>'+data[res]["business_name"]+'</td>';
	             		}
	             		
	             		
	             		tbl +='<td>'+data[res]["address1"]+' '+data[res]["address2"]+' '+address3+'</td>';
	             		tbl +='<td>'+data[res]["stateName"]+' / '+data[res]["cityName"]+'</td>';
	             		tbl +='<td><a alt="Delete Funeral Home" title="Delete Funeral Home" href="#" onclick="removeFH('+data[res]["funeral_home_id"]+')" class="btn"><i class="fa fa-trash-alt" style="color:#f00"></i> </a></td></tr>';

	             		
	             		i++;
	             	}

	             	$('.fhListBody').html(tbl);
	             	$('#adminFHList').DataTable({
						      "paging": true,
						      "lengthChange": true,
						      "searching": true,
						      "ordering": true,
						      "info": true,
						      "autoWidth": true,
						      "responsive": true,
						    });
	             	$.unblockUI();
	             //	console.log(data);
	             		//return false;
	            },
	            error: function (data) {
	               
	            }
	        });
  }

  function showFHResult(){
	var userType = $('#userType').val();
	var stateId = $('#stateList').val();
	var cityId = $('#cityList').val();
	

	$.ajax({
	            type: "POST",
	            url: "/admin/show_fh_search_result",
	            dataType: 'json', 
  				encode  : true,
  				 data: {
  				 	'userType':userType,
  				 	'stateId':stateId,
  				 	'cityId':cityId,
  				 

  				 },
  				 beforeSend: function() {
                   $.blockUI({ css: { 
			            border: 'none', 
			            padding: '15px', 
			            backgroundColor: '#000', 
			            '-webkit-border-radius': '10px', 
			            '-moz-border-radius': '10px', 
			            opacity: .5, 
			            color: '#fff' 
			        } }); 
                },
	            success: function (data) {
					$("#adminFHList").dataTable().fnDestroy();
	            	var tbl = '';
	             	var i = 1;
	             	var address3 = '';
	             	for(res in data){

	             		if(data[res]["address3"] != null){
	             				address3 = data[res]["address3"];
	             		}

	             		tbl +='<tr><td>'+i+'</td>';
	             		tbl +='<td>'+data[res]["funeral_home_name"]+'</td>';
	             		if(data[res]["role"] =='3'){
	             			firstName = data[res]["first_name"];
										middleName = data[res]["middle_name"];
										lastName = data[res]["last_name"];
										if(firstName == null){
											firstName = '';
										}
										if(middleName == null){
											middleName = '';
										}
										if(lastName == null){
											lastName = '';
										}
	             			tbl += '<td>'+firstName+' '+middleName+' '+lastName+'</td>';
	             		}
	             		else{
	             			tbl +='<td>'+data[res]["business_name"]+'</td>';
	             		}
	             		
	             		
	             		tbl +='<td>'+data[res]["address1"]+' '+data[res]["address2"]+' '+address3+'</td>';
	             		tbl +='<td>'+data[res]["stateName"]+' / '+data[res]["cityName"]+'</td>';
	             		tbl +='<td><a alt="Delete Funeral Home" title="Delete Funeral Home" href="#" onclick="removeFH('+data[res]["funeral_home_id"]+')" class="btn"><i class="fa fa-trash-alt" style="color:#f00"></i> </a></td></tr>';

	             		
	             		i++;
	             	}

	             	$('.fhListBody').html(tbl);
	             	$('#adminFHList').DataTable({
						      "paging": true,
						      "lengthChange": true,
						      "searching": true,
						      "ordering": true,
						      "info": true,
						      "autoWidth": true,
						      "responsive": true,
						    });
	             	$.unblockUI();
	             //	console.log(data);
	             		//return false;
	            },
	            error: function (data) {
	               
	            }
	        });
}

function removeFH(id){

	if(confirm("Are you sure you want to remove Funeral Home?")){
        $.ajax({
	            type: "POST",
	            url: "/admin/remove_fh",
	            dataType: 'json', 
  				encode  : true,
  				 data: {
  				 	'fhId':id
  				 },
  				 beforeSend: function() {
                   $.blockUI({ css: { 
			            border: 'none', 
			            padding: '15px', 
			            backgroundColor: '#000', 
			            '-webkit-border-radius': '10px', 
			            '-moz-border-radius': '10px', 
			            opacity: .5, 
			            color: '#fff' 
			        } }); 
                },
	            success: function (data) {
								$.unblockUI();
	             	console.log(data);
	             	//location.reload();
	             	 toastr.success('Funeral Home removed successfully')
	             	showFHResult();
	             	
	            },
	            error: function (data) {
	               
	            }
	        });
    }
    else{
       console.log("no");
    }

}
</script>
