<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo e($title); ?></title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo e(asset('public/adminlte/plugins/fontawesome-free/css/all.min.css')); ?>">

    <!-- Toastr -->
  <link rel="stylesheet" href="<?php echo e(asset('public/adminlte/plugins/toastr/toastr.min.css')); ?>">


  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="<?php echo e(asset('public/adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')); ?>">

  <!-- BS Stepper -->
  <link rel="stylesheet" href="<?php echo e(asset('public/adminlte/plugins/bs-stepper/css/bs-stepper.min.css')); ?>">


   <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo e(asset('public/adminlte/dist/css/adminlte.min.css')); ?>">
  
  <!-- custom css -->
  <link href="https://fonts.googleapis.com/css2?family=Rajdhani:wght@400;500;600&display=swap" rel="stylesheet"> 
  <link rel="stylesheet" href="<?php echo e(asset('public/adminlte/dist/css/custom-sytle.css')); ?>">
   <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
</head>

<body>

 
    
    <div class="wrapper-cl bg-cl-grey">
<header class="header-landing">
        <div class="container">
            <div class="row">
                <div class="col-6 col-lg-6">
                    <div class="landing-pg-logo"><a href="<?php echo e(route('landing_page')); ?>">
                        <img src="<?php echo e(asset('public/adminlte/dist/img/logo-white.png')); ?>" class="img-fluid"></a>
                    </div>
                </div>
                <div class="col-6 col-lg-6 align-self-center">
                    <div class="d-flex">
                        

                        <?php if(Auth::check()): ?>
                           <a href="<?php echo e(route('logout')); ?>" class="btn btn-login ml-auto">LOG OUT</a>
                        <?php else: ?>
                           <a href="<?php echo e(route('signin')); ?>" class="btn btn-login ml-auto">SIGN IN</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </header><?php /**PATH F:\xampp\htdocs\cyruslife\resources\views/include/header.blade.php ENDPATH**/ ?>