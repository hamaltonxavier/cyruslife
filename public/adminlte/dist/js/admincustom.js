$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

var crmUrl = 'https://ch.flatworldinfotech.com/public';

$(document).ready(function(){
	//loadUserList();

	loadStateList();

	$(".numberOnly").on('keydown keyup blur focus', function(e) {
console.log("sdfs");
        var value =e.key;
        /*var ascii=value.charCodeAt(0);
        $('textarea').append(ascii);
        $('textarea').append(value);
        console.log(e);*/
        // Only numbers
        if (!regExp.test(value)
          && e.which != 8   // backspace
          && e.which != 9		// tab
          && e.which != 46  // delete
          && (e.which < 37  // arrow keys
            || e.which > 40)) {
              e.preventDefault();
              return false;
        }
      });

	
});

function loadUserList(){
	 $.ajax({
	            type: "GET",
	            url: "/admin/load_user_list",
	            dataType: 'json', 
  				encode  : true,
  				 async: false,
  				 beforeSend: function() {
                   $.blockUI({ css: { 
			            border: 'none', 
			            padding: '15px', 
			            backgroundColor: '#000', 
			            '-webkit-border-radius': '10px', 
			            '-moz-border-radius': '10px', 
			            opacity: .5, 
			            color: '#fff' 
			        } }); 
                },
	            success: function (data) {
	             	
	             	
	             	var tbl = '';
	             	var i = 1;
	             	var userRole = '';
	             	var pApp = '';
	             	var pAppbtnColor = "badge-success";
	             	var agApp = '';
	             	var agAppbtnColor = "badge-success";
	             	var atApp = '';
	             	var atAppbtnColor = "badge-success";
	             	for(res in data){

	             		var userTypeData  = data[res]["role"];
	             		var businessName  = data[res]["business_name"];
	             		var pApp  = data[res]["pApp"];
	             		var agApp  = data[res]["agApp"];
	             		var atApp  = data[res]["atApp"];
						var pAppbtnColor = "badge-success";
						var agAppbtnColor = "badge-success";
						var atAppbtnColor = "badge-success";
						//console.log(data[res]["role"]);

	             		if(pApp == null || pApp == "saved"){
	             			pApp = 'Not Submitted';
	             			pAppbtnColor = " btn-info btn-sm";
	             		}

	             		if(agApp == null || agApp == "saved" ){
	             			agApp = 'Not Submitted';
	             			agAppbtnColor = " btn-info btn-sm";
	             		}
	             		

	             		if(atApp == null || atApp == "saved"){
	             			atApp = 'Not Submitted';
	             			atAppbtnColor = "btn-info btn-sm";
	             		}

	             		if(pApp == "rejected"){
	             			
	             			pAppbtnColor = "badge-danger";
	             		}

	             		if(agApp == "rejected" ){
	             			
	             			agAppbtnColor = "badge-danger";
	             		}
	             		

	             		if(atApp == "rejected"){
	             			
	             			atAppbtnColor = "badge-danger";
	             		}

	             		if(businessName == null){
	             			businessName = '';
	             		}
	             		if(userTypeData == '1'){
	             			userRole = "Partner";
	             		}
	             		else if(userTypeData == '2'){
	             			userRole = "Agency";
	             		}
	             		else{
	             			userRole = "Agent";
	             		}



	             		tbl += '<tr>';
	             		tbl += '<td>'+i+'</td>';
	             		tbl += '<td>'+userRole+'</td>';
	             		tbl += '<td>'+businessName+'</td><td>';
						if((data[res]["first_name"] !=null) && (data[res]["last_name"] !=null)){
							tbl += data[res]["title"]+'. '+data[res]["first_name"]+' '+data[res]["last_name"];
						}
						
	             		tbl += '</td><td>';
	             		if(data[res]["stateName"] != null){
	             			tbl += data[res]["cityName"]+' / '+data[res]["stateName"];
	             		}
	             		
	             		tbl += '</td>';
	             		if(data[res]["crm_user_id"] != null){
	             			tbl += '<td style="text-transform: capitalize;"><span class="badge ">CRM User Created</span></td>';
	             		}
	             		else{
	             			if(data[res]["role"] == '1'){
		             			tbl += '<td style="text-transform: capitalize;"><span class="badge '+pAppbtnColor+'">'+pApp+'</span></td>';
		             		}
		             		if(data[res]["role"] == '2'){
		             			tbl += '<td style="text-transform: capitalize;"><span class="badge '+agAppbtnColor+'">'+agApp+'</span></td>';
		             		}
		             		if(data[res]["role"] == '3'){
		             			tbl += '<td style="text-transform: capitalize;"><span class="badge '+atAppbtnColor+'">'+atApp+'</span></td>';
		             		}
	             		}
	             		


	             		tbl += '<td class="listActionBtn"> ';
	             		if(data[res]["crm_user_id"] == null){

	             		if((data[res]["role"] == '1') && ((pApp == 'submitted') || (pApp == 'approved') || (pApp == 'rejected') || (pApp == 'revision'))){
	             			tbl += '<div class="btn-group"><button type="button" class="btn btn-sm btn-default">Action</button><button type="button" class="btn btn-sm btn-default dropdown-toggle dropdown-icon" data-toggle="dropdown">	<span class="sr-only">Toggle Dropdown</span></button><div class="dropdown-menu" role="menu"><a target="_blank" class="dropdown-item" href="/admin/user_application/'+userTypeData+'/'+data[res]["user_id"]+'" title="Application View" alt="Application View"><i class="fas fa-tasks" aria-hidden="true"></i> Application View</a><a class="dropdown-item" href="#" href="javascript:;" onclick="showCommentsModal('+data[res]["user_id"]+','+data[res]["role"]+','+data[res]["pAppId"]+')" title="Comments" alt="Comments"><i class="fas fa-pencil-alt" aria-hidden="true" ></i> Add/Edit Comments</a><a class="dropdown-item" href="#" onclick="updateApplication('+data[res]["user_id"]+','+data[res]["role"]+','+data[res]["pAppId"]+',1)" title="Approve"  alt="Approve"><i class="fas fa-check" aria-hidden="true"></i> Approve</a><a class="dropdown-item" href="#" onclick="updateApplication('+data[res]["user_id"]+','+data[res]["role"]+','+data[res]["pAppId"]+',2)" title="Reject" alt="Reject"><i class="fas fa-thumbs-down" aria-hidden="true"></i> Reject</a><a class="dropdown-item" href="#" onclick="showAdminReviewList('+data[res]["pAppId"]+')" title="Review Log" alt="Review Log"><i class="fas fa-book" aria-hidden="true"></i> Review Log</a></div></div>';
	             			
	             		
	             		}
	             		if((data[res]["role"] == '2') && ((agApp == 'submitted') || (agApp == 'approved') || (agApp == 'rejected') || (agApp == 'revision'))){
	             			tbl += '<div class="btn-group"><button type="button" class="btn btn-sm btn-default">Action</button><button type="button" class="btn btn-sm btn-default dropdown-toggle dropdown-icon" data-toggle="dropdown">	<span class="sr-only">Toggle Dropdown</span></button><div class="dropdown-menu" role="menu"><a target="_blank" class="dropdown-item" href="/admin/user_application/'+userTypeData+'/'+data[res]["user_id"]+'" title="Application View" alt="Application View"><i class="fas fa-tasks" aria-hidden="true"></i> Application View</a><a class="dropdown-item" href="#" href="javascript:;" onclick="showCommentsModal('+data[res]["user_id"]+','+data[res]["role"]+','+data[res]["atAppId"]+')" title="Comments" alt="Comments"><i class="fas fa-pencil-alt" aria-hidden="true" ></i> Add/Edit Comments</a><a class="dropdown-item" href="#" onclick="updateApplication('+data[res]["user_id"]+','+data[res]["role"]+','+data[res]["agAppId"]+',1)" title="Approve"  alt="Approve"><i class="fas fa-check" aria-hidden="true"></i> Approve</a><a class="dropdown-item" href="#" onclick="updateApplication('+data[res]["user_id"]+','+data[res]["role"]+','+data[res]["agAppId"]+',2)" title="Reject" alt="Reject"><i class="fas fa-thumbs-down" aria-hidden="true"></i> Reject</a><a class="dropdown-item" href="#" onclick="showAdminReviewList('+data[res]["agAppId"]+')" title="Review Log" alt="Review Log"><i class="fas fa-book" aria-hidden="true"></i> Review Log</a></div></div> ';
	             			
	             		
	             		}
	             		if((data[res]["role"] == '3') && ((atApp == 'submitted') || (atApp == 'approved') || (atApp == 'rejected') || (atApp == 'revision'))){
	             			tbl += '<div class="btn-group"><button type="button" class="btn btn-sm btn-default">Action</button><button type="button" class="btn btn-sm btn-default dropdown-toggle dropdown-icon" data-toggle="dropdown">	<span class="sr-only">Toggle Dropdown</span></button><div class="dropdown-menu" role="menu"><a target="_blank" class="dropdown-item" href=/admin/user_application/'+userTypeData+'/'+data[res]["user_id"]+'" title="Application View" alt="Application View"><i class="fas fa-tasks" aria-hidden="true"></i> Application View</a><a class="dropdown-item" href="#" href="javascript:;" onclick="showCommentsModal('+data[res]["user_id"]+','+data[res]["role"]+','+data[res]["atAppId"]+')" title="Comments" alt="Comments"><i class="fas fa-pencil-alt" aria-hidden="true" ></i> Add/Edit Comments</a><a class="dropdown-item" href="#" onclick="updateApplication('+data[res]["user_id"]+','+data[res]["role"]+','+data[res]["atAppId"]+',1)" title="Approve"  alt="Approve"><i class="fas fa-check" aria-hidden="true"></i> Approve</a><a class="dropdown-item" href="#" onclick="updateApplication('+data[res]["user_id"]+','+data[res]["role"]+','+data[res]["atAppId"]+',2)" title="Reject" alt="Reject"><i class="fas fa-thumbs-down" aria-hidden="true"></i> Reject</a><a class="dropdown-item" href="#" onclick="showAdminReviewList('+data[res]["atAppId"]+')" title="Review Log" alt="Review Log"><i class="fas fa-book" aria-hidden="true"></i> Review Log</a></div></div>';
	             			

	             		
	             		}
	             	}


	             		
	             		
	             		tbl += '<span ></span> ';
	             		
	             		tbl += '</td>';
	             		tbl += '<td class="listActionBtn"><a target="_blank" class="btn btn-success btn-sm" href="/admin/user_profile/'+userTypeData+'/'+data[res]["user_id"]+'" title="View Profile"> <i class="fas fa-eye"></i></a> ';

	             		if((data[res]["role"] == '1') && (pApp == 'approved')){
	             			tbl += '<span><a class="btn btn-info btn-sm" href="#"   onclick="showManagerPopup('+data[res]["user_id"]+')" title="Assign Manager" alt="Assign Manager"><i class="fas fa-user-tie" aria-hidden="true"></i></a></span>  ';
	             			if(data[res]["crm_user_id"] == null){
		             			tbl += '<span><a class="btn btn-info btn-sm" href="#"   onclick="createCRMUser('+data[res]["role"]+','+data[res]["user_id"]+')" title="Create CRM Account" alt="Assign Manager"><i class="fa fa-cube" aria-hidden="true"></i></a></span>  ';
		             		}
	             		}
	             		if((data[res]["role"] == '2') && (agApp == 'approved')){
	             			tbl += '<span><a class="btn btn-info btn-sm" href="#"   onclick="showManagerPopup('+data[res]["user_id"]+')" title="Assign Manager"  alt="Assign Manager"><i class="fas fa-user-tie" aria-hidden="true"></i></a></span>  ';
	             			if(data[res]["crm_user_id"] == null){
		             			tbl += '<span><a class="btn btn-info btn-sm" href="#"   onclick="createCRMUser('+data[res]["role"]+','+data[res]["user_id"]+')" title="Create CRM Account" alt="Assign Manager"><i class="fa fa-cube" aria-hidden="true"></i></a></span>  ';
		             		}
	             		}
	             		if((data[res]["role"] == '3') && (atApp == 'approved')){
	             			tbl += '<span><a class="btn btn-info btn-sm" href="#"   onclick="showManagerPopup('+data[res]["user_id"]+')" title="Assign Manager" alt="Assign Manager"><i class="fas fa-user-tie" aria-hidden="true"></i></a></span>  ';
	             			if(data[res]["crm_user_id"] == null){
		             			tbl += '<span><a class="btn btn-info btn-sm" href="#"   onclick="createCRMUser('+data[res]["role"]+','+data[res]["user_id"]+')" title="Create CRM Account" alt="Assign Manager"><i class="fa fa-cube" aria-hidden="true"></i></a></span>  ';
		             		}
	             		}

	             		
	             		/*tbl += '<span><a class="btn btn-danger btn-sm" href="#" alt="Reject"><i class="fas fa-trash" aria-hidden="true"></i></a></span>  ';
	             		tbl += '<span ><a class="btn btn-info btn-sm" href="#" title="" alt="View"><i class="fas fa-pencil-alt" aria-hidden="true"></i></a></span> ';*/
	             		
	             		tbl += '</td>';
	             		tbl += '</tr>';
	             		//console.log(data[res]["first_name"]);
	             		i++;
	             	}

	             	$('.userListBody').html(tbl);
	             	$('#adminUsersList').DataTable({
				      "paging": true,
				      "lengthChange": true,
				      "searching": true,
				      "ordering": true,
				      "info": true,
				      "autoWidth": true,
				      "responsive": true,
				    });
	             	$.unblockUI();
	             	console.log(data);
	             		return false;
	            },
	            error: function (data) {
	               
	            }
	        });
	
}



function loadStateList(){
	var countryId = '233';
	      
	        $.ajax({
	            type: "POST",
	            url: "/state_details",
	            dataType: 'json', 
  				encode  : true,
  				 data: {
  				 	'countryId':countryId
  				 },
  				 beforeSend: function() {
                   $.blockUI({ css: { 
			            border: 'none', 
			            padding: '15px', 
			            backgroundColor: '#000', 
			            '-webkit-border-radius': '10px', 
			            '-moz-border-radius': '10px', 
			            opacity: .5, 
			            color: '#fff' 
			        } }); 
                },
	            success: function (data) {

	             	var html = '<option value="">Select State</option>';
	             	for(res in data){
	             		html += '<option value="'+data[res]["id"]+'">'+data[res]["name"]+'</option>'
	             		
	             	}
	             	$('#stateList').html(html);
	             	$.unblockUI();
	            },
	            error: function (data) {
	               
	            }
	        });
}


function showSearchResult(){
	var userType = $('#userType').val();
	var stateId = $('#stateList').val();
	var cityId = $('#cityList').val();
	var adminStatus = $('#adminStatus').val();

	$.ajax({
	            type: "POST",
	            url: "/show_search_result",
	            dataType: 'json', 
  				encode  : true,
  				 data: {
  				 	'userType':userType,
  				 	'stateId':stateId,
  				 	'cityId':cityId,
  				 	'adminStatus':adminStatus

  				 },
  				 beforeSend: function() {
                   $.blockUI({ css: { 
			            border: 'none', 
			            padding: '15px', 
			            backgroundColor: '#000', 
			            '-webkit-border-radius': '10px', 
			            '-moz-border-radius': '10px', 
			            opacity: .5, 
			            color: '#fff' 
			        } }); 
                },
	            success: function (data) {
					$("#adminUsersList").dataTable().fnDestroy();
	            var tbl = '';
	             	var i = 1;
	             	var userRole = '';
	             	var pApp = '';
	             	var pAppbtnColor = "badge-success";
	             	var agApp = '';
	             	var agAppbtnColor = "badge-success";
	             	var atApp = '';
	             	var atAppbtnColor = "badge-success";
	             	for(res in data){

	             		var userTypeData  = data[res]["role"];
	             		var businessName  = data[res]["business_name"];
	             		var pApp  = data[res]["pApp"];
	             		var agApp  = data[res]["agApp"];
	             		var atApp  = data[res]["atApp"];
						var pAppbtnColor = "badge-success";
						var agAppbtnColor = "badge-success";
						var atAppbtnColor = "badge-success";
						//console.log(data[res]["role"]);

	             		if(pApp == null || pApp == "saved"){
	             			pApp = 'Not Submitted';
	             			pAppbtnColor = " btn-info btn-sm";
	             		}

	             		if(agApp == null || agApp == "saved" ){
	             			agApp = 'Not Submitted';
	             			agAppbtnColor = " btn-info btn-sm";
	             		}
	             		

	             		if(atApp == null || atApp == "saved"){
	             			atApp = 'Not Submitted';
	             			atAppbtnColor = "btn-info btn-sm";
	             		}

	             		if(pApp == "rejected"){
	             			
	             			pAppbtnColor = "badge-danger";
	             		}

	             		if(agApp == "rejected" ){
	             			
	             			agAppbtnColor = "badge-danger";
	             		}
	             		

	             		if(atApp == "rejected"){
	             			
	             			atAppbtnColor = "badge-danger";
	             		}

	             		if(businessName == null){
	             			businessName = '';
	             		}
	             		if(userTypeData == '1'){
	             			userRole = "Partner";
	             		}
	             		else if(userTypeData == '2'){
	             			userRole = "Agency";
	             		}
	             		else{
	             			userRole = "Agent";
	             		}



	             		tbl += '<tr>';
	             		tbl += '<td>'+i+'</td>';
	             		tbl += '<td>'+userRole+'</td>';
	             		tbl += '<td>'+businessName+'</td><td>';
						if((data[res]["first_name"] !=null) && (data[res]["last_name"] !=null)){
							tbl += data[res]["title"]+'. '+data[res]["first_name"]+' '+data[res]["last_name"];
						}
						
	             		tbl += '</td><td>';
	             		if(data[res]["stateName"] != null){
	             			tbl += data[res]["cityName"]+' / '+data[res]["stateName"];
	             		}
	             		
	             		tbl += '</td>';
	             		if(data[res]["crm_user_id"] != null){
	             			tbl += '<td style="text-transform: capitalize;"><span class="badge ">CRM User Created</span></td>';
	             		}
	             		else{
	             			if(data[res]["role"] == '1'){
		             			tbl += '<td style="text-transform: capitalize;"><span class="badge '+pAppbtnColor+'">'+pApp+'</span></td>';
		             		}
		             		if(data[res]["role"] == '2'){
		             			tbl += '<td style="text-transform: capitalize;"><span class="badge '+agAppbtnColor+'">'+agApp+'</span></td>';
		             		}
		             		if(data[res]["role"] == '3'){
		             			tbl += '<td style="text-transform: capitalize;"><span class="badge '+atAppbtnColor+'">'+atApp+'</span></td>';
		             		}
	             		}
	             		


	             		tbl += '<td class="listActionBtn"> ';
	             		if(data[res]["crm_user_id"] == null){

	             		if((data[res]["role"] == '1') && ((pApp == 'submitted') || (pApp == 'approved') || (pApp == 'rejected') || (pApp == 'revision'))){
	             			tbl += '<div class="btn-group"><button type="button" class="btn btn-sm btn-default">Action</button><button type="button" class="btn btn-sm btn-default dropdown-toggle dropdown-icon" data-toggle="dropdown">	<span class="sr-only">Toggle Dropdown</span></button><div class="dropdown-menu" role="menu"><a target="_blank" class="dropdown-item" href="/admin/user_application/'+userTypeData+'/'+data[res]["user_id"]+'" title="Application View" alt="Application View"><i class="fas fa-tasks" aria-hidden="true"></i> Application View</a><a class="dropdown-item" href="#" href="javascript:;" onclick="showCommentsModal('+data[res]["user_id"]+','+data[res]["role"]+','+data[res]["pAppId"]+')" title="Comments" alt="Comments"><i class="fas fa-pencil-alt" aria-hidden="true" ></i> Add/Edit Comments</a><a class="dropdown-item" href="#" onclick="updateApplication('+data[res]["user_id"]+','+data[res]["role"]+','+data[res]["pAppId"]+',1)" title="Approve"  alt="Approve"><i class="fas fa-check" aria-hidden="true"></i> Approve</a><a class="dropdown-item" href="#" onclick="updateApplication('+data[res]["user_id"]+','+data[res]["role"]+','+data[res]["pAppId"]+',2)" title="Reject" alt="Reject"><i class="fas fa-thumbs-down" aria-hidden="true"></i> Reject</a><a class="dropdown-item" href="#" onclick="showAdminReviewList('+data[res]["pAppId"]+')" title="Review Log" alt="Review Log"><i class="fas fa-book" aria-hidden="true"></i> Review Log</a></div></div>';
	             			
	             		
	             		}
	             		if((data[res]["role"] == '2') && ((agApp == 'submitted') || (agApp == 'approved') || (agApp == 'rejected') || (agApp == 'revision'))){
	             			tbl += '<div class="btn-group"><button type="button" class="btn btn-sm btn-default">Action</button><button type="button" class="btn btn-sm btn-default dropdown-toggle dropdown-icon" data-toggle="dropdown">	<span class="sr-only">Toggle Dropdown</span></button><div class="dropdown-menu" role="menu"><a target="_blank" class="dropdown-item" href="/admin/user_application/'+userTypeData+'/'+data[res]["user_id"]+'" title="Application View" alt="Application View"><i class="fas fa-tasks" aria-hidden="true"></i> Application View</a><a class="dropdown-item" href="#" href="javascript:;" onclick="showCommentsModal('+data[res]["user_id"]+','+data[res]["role"]+','+data[res]["atAppId"]+')" title="Comments" alt="Comments"><i class="fas fa-pencil-alt" aria-hidden="true" ></i> Add/Edit Comments</a><a class="dropdown-item" href="#" onclick="updateApplication('+data[res]["user_id"]+','+data[res]["role"]+','+data[res]["agAppId"]+',1)" title="Approve"  alt="Approve"><i class="fas fa-check" aria-hidden="true"></i> Approve</a><a class="dropdown-item" href="#" onclick="updateApplication('+data[res]["user_id"]+','+data[res]["role"]+','+data[res]["agAppId"]+',2)" title="Reject" alt="Reject"><i class="fas fa-thumbs-down" aria-hidden="true"></i> Reject</a><a class="dropdown-item" href="#" onclick="showAdminReviewList('+data[res]["agAppId"]+')" title="Review Log" alt="Review Log"><i class="fas fa-book" aria-hidden="true"></i> Review Log</a></div></div> ';
	             			
	             		
	             		}
	             		if((data[res]["role"] == '3') && ((atApp == 'submitted') || (atApp == 'approved') || (atApp == 'rejected') || (atApp == 'revision'))){
	             			tbl += '<div class="btn-group"><button type="button" class="btn btn-sm btn-default">Action</button><button type="button" class="btn btn-sm btn-default dropdown-toggle dropdown-icon" data-toggle="dropdown">	<span class="sr-only">Toggle Dropdown</span></button><div class="dropdown-menu" role="menu"><a target="_blank" class="dropdown-item" href="/admin/user_application/'+userTypeData+'/'+data[res]["user_id"]+'" title="Application View" alt="Application View"><i class="fas fa-tasks" aria-hidden="true"></i> Application View</a><a class="dropdown-item" href="#" href="javascript:;" onclick="showCommentsModal('+data[res]["user_id"]+','+data[res]["role"]+','+data[res]["atAppId"]+')" title="Comments" alt="Comments"><i class="fas fa-pencil-alt" aria-hidden="true" ></i> Add/Edit Comments</a><a class="dropdown-item" href="#" onclick="updateApplication('+data[res]["user_id"]+','+data[res]["role"]+','+data[res]["atAppId"]+',1)" title="Approve"  alt="Approve"><i class="fas fa-check" aria-hidden="true"></i> Approve</a><a class="dropdown-item" href="#" onclick="updateApplication('+data[res]["user_id"]+','+data[res]["role"]+','+data[res]["atAppId"]+',2)" title="Reject" alt="Reject"><i class="fas fa-thumbs-down" aria-hidden="true"></i> Reject</a><a class="dropdown-item" href="#" onclick="showAdminReviewList('+data[res]["atAppId"]+')" title="Review Log" alt="Review Log"><i class="fas fa-book" aria-hidden="true"></i> Review Log</a></div></div>';
	             			

	             		
	             		}
	             	}


	             		
	             		
	             		tbl += '<span ></span> ';
	             		
	             		tbl += '</td>';
	             		tbl += '<td class="listActionBtn"><a target="_blank" class="btn btn-success btn-sm" href="/admin/user_profile/'+userTypeData+'/'+data[res]["user_id"]+'" title="View Profile"> <i class="fas fa-eye"></i></a> ';

	             		if((data[res]["role"] == '1') && (pApp == 'approved')){
	             			tbl += '<span><a class="btn btn-info btn-sm" href="#"   onclick="showManagerPopup('+data[res]["user_id"]+')" title="Assign Manager" alt="Assign Manager"><i class="fas fa-user-tie" aria-hidden="true"></i></a></span>  ';
	             			if(data[res]["crm_user_id"] == null){
		             			tbl += '<span><a class="btn btn-info btn-sm" href="#"   onclick="createCRMUser('+data[res]["role"]+','+data[res]["user_id"]+')" title="Create CRM Account" alt="Assign Manager"><i class="fa fa-cube" aria-hidden="true"></i></a></span>  ';
		             		}
	             		}
	             		if((data[res]["role"] == '2') && (agApp == 'approved')){
	             			tbl += '<span><a class="btn btn-info btn-sm" href="#"   onclick="showManagerPopup('+data[res]["user_id"]+')" title="Assign Manager"  alt="Assign Manager"><i class="fas fa-user-tie" aria-hidden="true"></i></a></span>  ';
	             			if(data[res]["crm_user_id"] == null){
		             			tbl += '<span><a class="btn btn-info btn-sm" href="#"   onclick="createCRMUser('+data[res]["role"]+','+data[res]["user_id"]+')" title="Create CRM Account" alt="Assign Manager"><i class="fa fa-cube" aria-hidden="true"></i></a></span>  ';
		             		}
	             		}
	             		if((data[res]["role"] == '3') && (atApp == 'approved')){
	             			tbl += '<span><a class="btn btn-info btn-sm" href="#"   onclick="showManagerPopup('+data[res]["user_id"]+')" title="Assign Manager" alt="Assign Manager"><i class="fas fa-user-tie" aria-hidden="true"></i></a></span>  ';
	             			if(data[res]["crm_user_id"] == null){
		             			tbl += '<span><a class="btn btn-info btn-sm" href="#"   onclick="createCRMUser('+data[res]["role"]+','+data[res]["user_id"]+')" title="Create CRM Account" alt="Assign Manager"><i class="fa fa-cube" aria-hidden="true"></i></a></span>  ';
		             		}
	             		}

	             		
	             		/*tbl += '<span><a class="btn btn-danger btn-sm" href="#" alt="Reject"><i class="fas fa-trash" aria-hidden="true"></i></a></span>  ';
	             		tbl += '<span ><a class="btn btn-info btn-sm" href="#" title="" alt="View"><i class="fas fa-pencil-alt" aria-hidden="true"></i></a></span> ';*/
	             		
	             		tbl += '</td>';
	             		tbl += '</tr>';
	             		//console.log(data[res]["first_name"]);
	             		i++;
	             	}

	             	$('.userListBody').html(tbl);
	             	$('#adminUsersList').DataTable({
				      "paging": true,
				      "lengthChange": true,
				      "searching": true,
				      "ordering": true,
				      "info": true,
				      "autoWidth": true,
				      "responsive": true,
				    });
	             	$.unblockUI();
	             	console.log(data);
	             		return false;
	            },
	            error: function (data) {
	               
	            }
	        });
}

function adminAddUserValidation(){
 var emailErrorVal = $('.uniqueEmailError').val();
 $('.finalErr').html('');
 ('#business_name').removeClass('is-invalid');
 $('#email').removeClass('is-invalid');
 $('#cityList').removeClass('is-invalid');
 $('#stateList').removeClass('is-invalid');
 $('#countryList').removeClass('is-invalid');
 $('#first_name').removeClass('is-invalid');
 $('#last_name').removeClass('is-invalid');
 
 if($('#roleTypeVal').val() !='3'){
     if ($('#business_name').val()==''){
		       $('#business_name').addClass('is-invalid');
		       $('.finalErr').html('Please enter business name');
		       return false;
		      }
 }
 
	 if ($('#email').val()==''){
		       $('#email').addClass('is-invalid');
		       $('.finalErr').html('Email Id required');
		       return false;
		      }

	if($('#countryList').val()==''){
    $('#countryList').addClass('is-invalid');
    $('.finalErr').html('Please select country');
		return false;
  }

  if($('#stateList').val()==''){
  $('#stateList').addClass('is-invalid');
  $('.finalErr').html('Please select state');
		return false;
  }

	if($('#cityList').val()==''){
    $('#cityList').addClass('is-invalid');
    $('.finalErr').html('Please select city');
		return false;
  }
  
  if($('#first_name').val()==''){
    $('#first_name').addClass('is-invalid');
    $('.finalErr').html('Please enter first name');
		return false;
  }

  if($('#last_name').val()==''){
    $('#last_name').addClass('is-invalid');
    $('.finalErr').html('Please enter last name');
		return false;
  }

  

if(emailErrorVal == '1'){
			$('.finalErr').html('Email Id already in use. Please choose different Email Id');

			return false;
		}

	

	//return false;
}

function showCommentsModal(userId,userType,applicationId){
	$('#adminCommentsModal').modal('show');

	$('#selectedUserId').val(userId);
	$('#selectedUserType').val(userType);
	$('#selectedApplicationId').val(applicationId);

}

function submitReview(){
	var reviewData = $('#summernote').summernote('code');
	var selectedUserId = $('#selectedUserId').val();
	var selectedUserType = $('#selectedUserType').val();
	var selectedApplicationId = $('#selectedApplicationId').val();
	
$('#adminCommentsModal').modal('hide');
	$.ajax({
	            type: "POST",
	            url: "/admin/submit_review",
	            dataType: 'json', 
  				encode  : true,
  				 data: {
  				 	'reviewData':reviewData,
  				 	'selectedUserId':selectedUserId,
  				 	'selectedUserType':selectedUserType,
  				 	'selectedApplicationId':selectedApplicationId

  				 },
  				 beforeSend: function() {
                   $.blockUI({ css: { 
			            border: 'none', 
			            padding: '15px', 
			            backgroundColor: '#000', 
			            '-webkit-border-radius': '10px', 
			            '-moz-border-radius': '10px', 
			            opacity: .5, 
			            color: '#fff' 
			        } }); 
                },
	            success: function (data) {
					 $.unblockUI();
	             	console.log(data);
					$('#selectedUserId').val('');
					$('#selectedUserType').val('');
					$('#selectedApplicationId').val('');

	             	
				   
	            },
	            error: function (data) {
	               
	            }
	        });
}

function updateApplication(userId,userType,applicationId,applicationStatus){
	$.ajax({
	            type: "POST",
	            url: "/admin/update_application",
	            dataType: 'json', 
  				encode  : true,
  				 data: {
  				 	'userId':userId,
  				 	'userType':userType,
  				 	'applicationId':applicationId,
  				 	'applicationStatus':applicationStatus

  				 },
  				 beforeSend: function() {
                   $.blockUI({ css: { 
			            border: 'none', 
			            padding: '15px', 
			            backgroundColor: '#000', 
			            '-webkit-border-radius': '10px', 
			            '-moz-border-radius': '10px', 
			            opacity: .5, 
			            color: '#fff' 
			        } }); 
                },
	            success: function (data) {
					 $.unblockUI();
	             	console.log(data);
					toastr.success('Application Status has been updated');
	             	// location.reload();
	             	$("#adminUsersList").dataTable().fnDestroy();
	             	loadUserList();
				   
	            },
	            error: function (data) {
	               
	            }
	        });
}

function showAdminReviewList(applicationId){

	$.ajax({
	            type: "POST",
	            url: "/admin/load_review_log",
	            dataType: 'json', 
  				encode  : true,
  				 data: {
  				 	'applicationId':applicationId,
  				 	

  				 },
  				 beforeSend: function() {
                   $.blockUI({ css: { 
			            border: 'none', 
			            padding: '15px', 
			            backgroundColor: '#000', 
			            '-webkit-border-radius': '10px', 
			            '-moz-border-radius': '10px', 
			            opacity: .5, 
			            color: '#fff' 
			        } }); 
                },
	            success: function (data) {
					 $.unblockUI();
	             	console.log(data);
	             	html = '';
	             	var i = 1;
					for(res in data){ 
						html += '		<div class="card ">';
				        html += '            <div class="card-header">';
				       html += '               <h4 class="card-title w-100">';
				        html += '                <a class="d-block w-100" data-toggle="collapse" href="#collapse'+i+'">';
				        html += data[res]["created_date"];
				        html += '                </a>';
				        html += '              </h4>';
				        html += '            </div>';
				        html += '            <div id="collapse'+i+'" class="collapse" data-parent="#accordion">';
				        html += '              <div class="card-body">';
				        html += data[res]["comments"];
				                        
				        html += '              </div>';
				        html += '            </div>';
				        html += '          </div>';

				        $('.adminReviewAccordion').html(html);
				        $('#adminCommentsList').modal('show');
				        i++;
					}
	             	
	             	
				   
	            },
	            error: function (data) {
	               
	            }
	        });
	
		
}

function showManagerPopup(userId){
	$('#managerModal').modal('show');
	$('#managerUserId').val(userId);
	$('.managerError').html('');
	$.ajax({
	            type: "POST",
	            url: "/admin/show_manager",
	            dataType: 'json', 
  				encode  : true,
  				 data: {
  				 	'userId':userId,
  				 	
  				 	

  				 },
  				 beforeSend: function() {
                   $.blockUI({ css: { 
			            border: 'none', 
			            padding: '15px', 
			            backgroundColor: '#000', 
			            '-webkit-border-radius': '10px', 
			            '-moz-border-radius': '10px', 
			            opacity: .5, 
			            color: '#fff' 
			        } }); 
                },
	            success: function (data) {
					 $.unblockUI();
	             	console.log(data[0]["manager_name"]);
					$('#managerName').val(data[0]["manager_name"]);
	             
				   
	            },
	            error: function (data) {
	               
	            }
	        });
}


function assignManager(){
	var managerUserId = $('#managerUserId').val();
	var managerName = $('#managerName').val();

	if(managerName == ""){
		$('.managerError').html('Please Enter Manager Name');
		return false;
	}
	$.ajax({
	            type: "POST",
	            url: "/admin/assign_manager",
	            dataType: 'json', 
  				encode  : true,
  				 data: {
  				 	'userId':managerUserId,
  				 	'managerName':managerName,
  				 	

  				 },
  				 beforeSend: function() {
                   $.blockUI({ css: { 
			            border: 'none', 
			            padding: '15px', 
			            backgroundColor: '#000', 
			            '-webkit-border-radius': '10px', 
			            '-moz-border-radius': '10px', 
			            opacity: .5, 
			            color: '#fff' 
			        } }); 
                },
	            success: function (data) {
					 $.unblockUI();
	             	console.log(data);
					$('#managerUserId').val('');
					$('#managerName').val('');
	             	$('#managerModal').modal('hide');
	             	toastr.success('Manager assigned successfully');
				   
	            },
	            error: function (data) {
	               
	            }
	        });
}

function createCRMUser(role,id){
	$.ajax({
	            type: "POST",
	            url: "/admin/create_crm_user",
	            dataType: 'json', 
  				encode  : true,
  				 data: {
  				 	'role':role,
  				 	'userId':id,
  				 	

  				 },
  				 beforeSend: function() {
                   $.blockUI({ css: { 
			            border: 'none', 
			            padding: '15px', 
			            backgroundColor: '#000', 
			            '-webkit-border-radius': '10px', 
			            '-moz-border-radius': '10px', 
			            opacity: .5, 
			            color: '#fff' 
			        } }); 
                },
	            success: function (data) {
							 $.unblockUI();
	             	console.log(data);
								toastr.success('CRM User Created');
	             	// location.reload();
	             	$("#adminUsersList").dataTable().fnDestroy();
	             	loadUserList();			
				   
	            },
	            error: function (data) {
	               
	            }
	        });
}

function clearFilter(filterPage){
	$('.form-control').val('');
	
	if(filterPage == 'user'){
		$("#adminUsersList").dataTable().fnDestroy();
		loadUserList();
	}

	if(filterPage == 'funeral'){
		$("#adminFHList").dataTable().fnDestroy();
		
		loadFH();
	}
	
}

