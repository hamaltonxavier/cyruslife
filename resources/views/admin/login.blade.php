@include('include.header')
	<div class="content-body pb-5 pt-5">
		
		<div class="login-box cl-wrap ml-auto mr-auto">
			  <!--<div class="login-logo">
				<a href="../../index2.html"><b>Admin</b>LTE</a>
			  </div> -->
			  <!-- /.login-logo -->
			  <div class="card">
				<div class="card-body login-card-body">
				  
				  <div class="row equal">
					<div class="col-lg-6 align-self-center">
						<div class="login-form-wrap">
						 <h2 class="login-box-msg color-blue font-weight700  pb-2">Admin Login</h2>
							<div class="bar-line"></div>
						  <form action="{{url('/login')}}" method="post">
        @csrf
							<div class="form-group">
								<label>User name</label>
								<input type="email" name="email" class="form-control" placeholder="Username">
							</div>
							<div class="form-group">
								<label>Password</label>
								<input type="password" name="password" class="form-control" placeholder="Password">
							</div>
							
							
							<div class="row">
							  <div class="col-12 text-right">
								<!--<a href="{{route('forgot_password')}}" class="font-weight700">I forgot my password</a>-->
							  </div>
							  <!-- /.col -->
							</div>
							
							<div class="form-group mt-4">
								<button type="submit" class="btn btn-primary btn-block btn-lg">Sign In</button>
							</div>
							
						  </form>


						 </div>
					</div>
					
					<div class="d-none  d-lg-block  col-lg-6 p-0" style="background: #e2f5ff;">
						<div class="login-image-wrap login-page p-0 mt-auto mb-auto">
							<img src="{{ asset('public/adminlte/dist/img/login-img-v2.jpg')}}" class="img-fluid">
						</div>
						<div class="login-conten-wrap">
							<h3>Welcome Back Admin!</h3>
							<p>Looking forward to see you aboard </p>
						</div>
					</div>
				  </div>
				  
				  
				 
				  
				</div>
				<!-- /.login-card-body -->
			  </div>
			</div>
		
	</div>
	
	@include('include.footer')

<!-- BS-Stepper -->
<script src="{{ asset('public/adminlte/plugins/bs-stepper/js/bs-stepper.min.js')}}"></script>
<script>

	
	  // BS-Stepper Init
	  document.addEventListener('DOMContentLoaded', function () {
		window.stepper = new Stepper(document.querySelector('.bs-stepper'))
	  })
	  
	  
</script>


</html>