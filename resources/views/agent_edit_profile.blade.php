@include('include.header')	
@foreach($userDetails as $data)
<div class="content-body pb-5 pt-5">
		
		<div class="container w-75">
				
				<!-- register form stepper -->
					<form id="agencyRegistrationForm" class="registrationForm" method="POST" action="{{ route('update_profile') }}" enctype="multipart/form-data" onsubmit="return agentFormValidation()">
  @csrf
					<div class="row">
					  <div class="col-md-12">
						<div class="card card-default">
						  <div class="card-header">
							<h3 class="pl-4 mt-3 font-weight700 color-orange">Edit - Profile</h3>
						  </div>
						  <div class="card-body p-5">
							
							  
								<div id="register-part-1" class="content">
								  <h4 class="color-blue">Name and Address</h4>
								  <hr class="pb-2">
								  <input type="hidden" id="userType" name="userType" value="{{ Request::segment(2) }}" />
									<input type="hidden" id="userId" name="userId" value="{{ Request::segment(3) }}" />
										<div class="row">
											<div class="col-md-3 col-lg-2">
												<label>Salutation/Title</label>
												<select class="form-control" id="title" name="title">

											<option value="Mr" @if($data->title == "Mr") selected @endif>Mr</option>
											<option value="Mrs" @if($data->title == "Mrs") selected @endif>Mrs</option>
										</select>
											</div>
											
											<div class="col-md-9 col-lg-4">
												<div class="form-group">
													<label>First Name</label>
													<input type="text" class="form-control alphabetsOnly"  value="{{$data->first_name}}" id="first_name" name="first_name">
												  </div>
											</div>
											
											<div class="col-md-6 col-lg-3">
												<div class="form-group">
													<label>Middle Name</label>
													<input type="text" class="form-control alphabetsOnly"  value="{{$data->middle_name}}" id="middle_name" name="middle_name">
												  </div>
											</div>
											
											<div class="col-md-6 col-lg-3">
												<div class="form-group">
													<label>Last Name</label>
													<input type="text" class="form-control alphabetsOnly"  value="{{$data->last_name}}" id="last_name" name="last_name">
												  </div>
											</div>
											
										  </div>
										  
										  
										  <div class="row">
											<div class="col-md-6 col-lg-6">
												<label>Mobile Phone</label>
												<input type="text" class="form-control numberOnly"  value="{{$data->mobile}}" id="mobile" name="mobile">
											</div>
											
											<div class="col-md-6  col-lg-6">
												<div class="form-group">
													<label>Email</label>
													<label class="form-control">{{$data->email}}</lable>
												  </div>
											</div>
										  </div>
										  
										  
										  
										  <div class="row">
											<div class="col-md-12 col-lg-4">
													 <div class="form-group">
														<label>Physical Address 1</label>
														<input type="text" class="form-control"  value="{{$data->address1}}" id="address1" name="address1">
													  </div>
												</div>
												
												<div class="col-md-6 col-lg-4">
													 <div class="form-group">
														<label>Physical Address 2</label>
														<input type="text" class="form-control"  value="{{$data->address2}}" id="address2" name="address2">
													  </div>
												</div>
												<div class="col-md-6 col-lg-4">
													 <div class="form-group">
														<label>Physical Address 3</label>
														<input type="text" class="form-control"  value="{{$data->address3}}" id="address3" name="address3">
													  </div>
												</div>
										  </div>
										  
										  <div class="row">
											<div class="col-md-4 col-lg-4">
													 <div class="form-group">
														<label>Country</label>
														 <select class="form-control" id="countryList" name="country">
												

													@foreach($countryDetails as $dataVal)
													<option value="{{$dataVal->id}}" @if($data->countryId == $dataVal->id) selected @endif  >{{$dataVal->name}}</option>
													@endforeach
												  </select>
													  </div>
												</div>
												
												<div class="col-md-4 col-lg-4">
													 <div class="form-group">
														<label>State</label>
														<select class="form-control" id="stateList" name="state">
												  	@foreach($statesDetails as $dataVal)
													<option value="{{$dataVal->id}}" @if($data->stateId == $dataVal->id) selected @endif  >{{$dataVal->name}}</option>
													@endforeach
												  </select>
													  </div>
												</div>
												<div class="col-md-4 col-lg-4">
													 <div class="form-group">
														<label>City</label>
														 <select class="form-control" id="cityList" name="city">
													@foreach($cityDetails as $dataVal)
													<option value="{{$dataVal->id}}" @if($data->cityId == $dataVal->id) selected @endif  >{{$dataVal->name}}</option>
													@endforeach
												  </select>
													  </div>
												</div>
												<div class="col-md-4 col-lg-4">
													 <div class="form-group">
														<label>Postal Code</label>
														<input type="text" class="form-control numberOnly" maxlength="6"  value="{{$data->postal_code}}" id="postal_code" name="postal_code">
													  </div>
												</div>
										  </div>
										  
									
								  
								  
								  
								  
								  
								  
								</div>
								
						
								<!-- register form step 2-->
								<div id="register-part-2" class="content mt-5">
								  <h4 class="color-blue">License / ID</h4>
								  <hr class="pb-2">
								  <div class="row">
									
									
									<div class="col-lg-6">
										<div class="form-group">
											<label>Insurance License <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control numberOnly"  value="{{$data->license_number}}" id="license_number" name="license_number">
										  </div>
									</div>
									</div>
<div class="form-group row mb-2">
									<label class="col-11 col-sm-11 col-md-11 col-lg-4 col-form-label">License type 
									</label>
									
									<div class="col-sm-7 col-lg-7 pl-0">
										<div class="row">
										  <div class="col-lg-11">
											<div class="form-group d-flex align-items-start mb-2">
												<input type="radio" class="form-check-input ml-0" id="license_type_opt" name="license_type_opt" value="Limited" {{ ($data->license_type_opt=="Limited")? "checked" : "" }}>>
												<label class="form-check-label pl-4">Limited (dollar amount and/or insurance type restriction) </label>
											</div>
										  </div>
										</div>
										
									
										<div class="row ">
										  <div class="col-lg-11">
											<div class="form-group d-flex align-items-start mb-2">
												<input type="radio" class="form-check-input ml-0" id="license_type_opt" name="license_type_opt" value="Full" {{ ($data->license_type_opt=="Full")? "checked" : "" }}>
												<label class="form-check-label pl-4">Full (any life product, any dollar amount)</label>
											</div>
										  </div>
										</div>
										
										
									</div>
								</div> <!-- row end -->

									<div class="row">
									
									<div class="col-lg-6">
										<div class="form-group">
											<label>License Status <span class="requiredSymbol">*</span></label>
											<select class="form-control" id="license_status" name="license_status">
											<option value="active" @if($data->license_status == "active") selected @endif>Active</option>
											<option value="expired" @if($data->license_status == "expired") selected @endif>Expired</option>
										</select>
										  </div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>Expiration Date <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control"  value="{{$data->license_expiry_date}}" id="license_expiry_date" name="license_expiry_date">
										  </div>
									</div>
									</div>

									<div class="row">
									
									<div class="col-lg-6">
										<div class="form-group">
											<label>Designated Home State <span class="requiredSymbol">*</span></label>
											<!--<input type="text" class="form-control"  value="{{$data->license_home_state}}" id="license_home_state" name="license_home_state">-->
											<select class="form-control" id="license_home_state" name="license_home_state">
												  	@foreach($statesDetails as $dataVal)
													<option value="{{$dataVal->id}}" @if($data->license_home_state == $dataVal->id) selected @endif  >{{$dataVal->name}}</option>
													@endforeach
												  </select>
										  </div>
									</div>
									
									</div>

								
								</div>
								
								
								<!-- register form step 3-->
								<div id="register-part-3" class="content mt-5">
								   <h4 class="color-blue">Business Overview</h4>
								  <hr class="pb-2">
								  <div class="row">								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Location of services (Regions)</label>
											<input type="text" class="form-control alphabetsOnly"  value="{{$data->location_of_services}}" id="location_of_services" name="location_of_services">
										  </div>
									</div>
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Business turn over (range)</label>
											<!--<input type="text" class="form-control"  value="{{$data->business_turn_over}}" id="business_turn_over" name="business_turn_over">-->
											<div class="input-group">
												<div class="input-group-prepend">
												<span class="input-group-text"><b>$</b></span>
												</div>
												<!--<input type="text" class="form-control numberOnly" placeholder="Email Address" value="{{$data->annual_preneed_volume}}" id="annual_preneed_volume" name="annual_preneed_volume">-->
												
												<select class="form-control" id="business_turn_over" name="business_turn_over">
														<option value="100 - 1,000" @if($data->business_turn_over == "100 - 1,000") selected @endif>100 - 1,000</option>
														<option value="1,000 - 10,000" @if($data->business_turn_over == "1,000 - 10,000") selected @endif>1,000 - 10,000</option>
														<option value="10,000 - 100,000" @if($data->business_turn_over == "10,000 - 100,000") selected @endif>10,000 - 100,000</option>
														<option value="100,000 - 1,000,000" @if($data->business_turn_over == "100,000 - 1,000,000") selected @endif>100,000 - 1,000,000</option>
														<option value="1,000,000 - 10,000,000" @if($data->business_turn_over == "1,000,000 - 10,000,000") selected @endif>1,000,000 - 10,000,000</option>
													</select>

											</div>
										  </div>
									</div>
								  </div>
								  
								  <div class="row">								
								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Number of years in business</label>	
											<input type="text" class="form-control numberOnly"  value="{{$data->number_of_years_in_business}}" id="number_of_years_in_business" name="number_of_years_in_business">					
										</div>
									</div>
									
									<!--<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label for="exampleInputFile">Required Document Uploads</label>
											<div class="input-group">
											   <input type="file" name="idFiles[]" class="myfrm form-control">
											 
											</div>
											
										  </div>
									</div>-->
								  </div>

								  @foreach($docDetails as $docData)
								   <div class="row">								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											
											
											  	@if($docData->document_type == "business")
											  <label	>{{$docData->document_name}}</label>
											  	@endif
											 
										  </div>
									</div>
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label></label>
											
											  	@if($docData->document_type == "business")
											  	
											  		
											  	<a target="_blank" alt="View Document" title="View Document" href="{{asset('storage/app/'.$docData->doucment_path)}}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> </a>
											  	<!--<a alt="Delete Document" title="Delete Document"  href="#" onclick="removeDocument('{{$docData->document_id}}')" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> </a>-->

											  		
											  	@endif
											  
										  </div>
									</div>

								</div>

								 @endforeach
								  
								<!--First-->
									<div class="row increment">
								  		<div class="col-md-6 col-lg-6">
												<div class="form-group">
													<div class="input-group realprocode control-group lst" >
								  				<select class="form-control" id="" name="filetitle[]">
														<option value="Producer-Agreement">Producer Agreement</option>
														<option value="Direct-Deposit-Authorization">Direct Deposit Authorization</option>
														<option value="Producer-Compensation-Schedule">Producer Compensation Schedule</option>
														<option value="Explanation-of-Information-found-on-MIB">Explanation of Information found on MIB</option>
														<option value="Other-Supporting-Documentation">Other Supporting Documentation</option>
													</select>
											      
											    </div>
												</div>
											</div>

											<div class="col-md-6 col-lg-6">
												<div class="form-group">
													<div class="input-group realprocode control-group lst " >
								  				
											      <input type="file" accept=".gif,.jpg,.jpeg,.png,.doc,.docx,.pdf" name="filenames[]" class="myfrm form-control">
											      <div class="input-group-btn"> 
											        <button class="btn btn-success addFile" type="button"> <i class="fldemo glyphicon glyphicon-plus"></i>Add</button>
											      </div>
											    </div>
												</div>
											</div>
								  </div>

								<!--Second-->
							<div class="clone d-none " >
								<div class="row ">
									<div class="realprocode col-md-12 col-lg-12">
								  		<div class="col-md-6 col-lg-6" style="float: left;">
												<div class="form-group">
													<div class=" control-group lst input-group" style="margin-top:10px">
											      <select class="form-control" id="" name="filetitle[]">
														<option value="Driver-License">State-Issued Driver's License</option>
														<option value="Producer-Agreement">Producer Agreement</option>
														<option value="Direct-Deposit-Authorization">Direct Deposit Authorization</option>
														<option value="Producer-Compensation-Schedule">Producer Compensation Schedule</option>
														<option value="Explanation-of-Information-found-on-MIB">Explanation of Information found on MIB</option>
														<option value="Other-Supporting-Documentation">Other Supporting Documentation</option>
													</select>
											        
											      </div>
												</div>
											</div>

											<div class="col-md-6 col-lg-6" style="float: left;">
												<div class="form-group">
													<div class=" control-group lst input-group" style="margin-top:10px">
											      	
											        <input type="file" accept=".gif,.jpg,.jpeg,.png,.doc,.docx,.pdf" name="filenames[]" class="myfrm form-control">
											        <div class="input-group-btn"> 
											          <button class="btn btn-danger" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>
											        </div>
											      </div>
												</div>
											</div>
										</div>
								  </div>

								</div> 
								 
								 
								</div>
								
								
							 <button type="submit" class="mt-4 btn btn-lg btn-primary ">UPDATE DETAILS</button>
							<span style="display:none" class="error finalErr">Please fill the required fields *</span>
						  </div>
						  <!-- /.card-body -->
						  
						</div>
						<!-- /.card -->
					  </div>
					</div>
				</form>
				<!-- register form stepper -->
				
			</div>
		
	</div>
	@endforeach
	@include('include.footer')
	<script>
		$(document).ready(function(){
			var userController = "{{ Request::path() }}";
			var userTypeVal = $('#userType').val();
			var userId = $('#userId').val();
			if(userController !="edit-profile"){
				$('.registrationForm').attr('action','{{ url("update-sub-profile") }}/'+userTypeVal+'/'+userId);
				//$('.registrationForm').attr('method','GET');
			}
		});
	</script>
	<script type="text/javascript">
    $(document).ready(function() {
      $(".addFile").click(function(){ 
          var lsthmtl = $(".clone").html();
          $(".increment").after(lsthmtl);
      });
      $("body").on("click",".btn-danger",function(){ 
          $(this).parents(".realprocode").remove();
      });
    });
</script>

