@include('include.header')	
@foreach($userDetails as $data)

	<div class="content-body pb-5 pt-5">
		
		<div class="container w-75">
				
				<!-- register form stepper -->
					
					<div class="row">
					  <div class="col-md-12">
						<div class="card card-default">
						  <div class="card-header">
							<h3 class="mt-3 font-weight700 color-blue pl-4">View Profile</h3>
						  </div>
						  <div class="card-body p-5">
							
							
							 
								
								<div id="register-part-1" class="content">
									 <h4 class="color-blue">Business Name and Address</h4>
									<hr class="pb-2">
								  <div class="row">
									<div class="col-md-12 col-lg-12">
										<div class="form-group">
											<label>Name of the Business</label>
											<input type="text" class="form-control"  value="{{$data->business_name}}"disabled>
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										 <div class="form-group">
											<label>Physical Address 1</label>
											<input type="text" class="form-control"  value="{{$data->address1}}"disabled>
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										 <div class="form-group">
											<label>Physical Address 2</label>
											<input type="text" class="form-control"  value="{{$data->address2}}"disabled>
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										 <div class="form-group">
											<label>Physical Address 3</label>
											<input type="text" class="form-control"  value="{{$data->address3}}"disabled>
										  </div>
									</div>
									
									
									<div class="col-md-6 col-lg-4">
										<div class="form-group">
												  <label>Country</label>
												  <input type="text" class="form-control"  value="{{$data->countryName}}"disabled>
												</div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										<div class="form-group">
												  <label>State</label>
												<input type="text" class="form-control"  value="{{$data->stateName}}"disabled>
												</div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										<div class="form-group">
												  <label>City</label>
												  <input type="text" class="form-control"  value="{{$data->cityName}}"disabled>
												</div>
									</div>
									
									<div class="col-md-6  col-lg-6">
										<div class="form-group">
													<label>Postal Code</label>
													<input type="text" class="form-control numberOnly" maxlength="6"  value="{{$data->postal_code}}"disabled>
												  </div>
									</div>
									
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Main Business Telephone</label>
											<input type="text" class="form-control numberOnly"  value="{{$data->telephone}}"disabled>
										  </div>
									</div>
									
								  </div>
								 
								</div>
								
								<!-- registration part 2-->
								<div id="register-part-2" class="content mt-5">
								  <h4 class="color-blue">Agency Manager details</h4>
									<hr class="pb-2">
								  <div class="row">
									<div class="col-md-3 col-lg-2">
										<label>Salutation/Title</label>
										<input type="text" class="form-control"  value="{{$data->title}}"disabled>
									</div>
									
									<div class="col-md-9 col-lg-4">
										<div class="form-group">
											<label>First Name</label>
											<input type="text" class="form-control"  value="{{$data->first_name}}"disabled>
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-3">
										<div class="form-group">
											<label>Middle Name</label>
											<input type="text" class="form-control"  value="{{$data->middle_name}}"disabled>
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-3">
										<div class="form-group">
											<label>Last Name</label>
											<input type="text" class="form-control"  value="{{$data->last_name}}"disabled>
										  </div>
									</div>
									
								  </div>
								  
								  <div class="row">
									<div class="col-md-6 col-lg-6">
										<label>Mobile Phone</label>
										<input type="text" class="form-control numberOnly"  value="{{$data->mobile}}"disabled>
									</div>
									
									<div class="col-md-6  col-lg-6">
										<div class="form-group">
											<label>Email</label>
											<input type="text" class="form-control"  value="{{$data->email}}"disabled>
										  </div>
									</div>
									
									<div class="col-lg-12">
										
										
										
										<div class="form-group">
											<div class="row">
												<div class="col-md-6 col-lg-6">
													<label>Agent (Y/N)</label>
													<input type="text" class="form-control"  value="{{$data->agent}}"disabled>
												</div>
												
											</div>
											
										</div>
									</div>

									
								  </div>
								  
								</div>
								
								
								<!-- register form step 3-->
								<div id="register-part-3" class="content mt-5">
								  <h4 class="color-blue">License / ID</h4>
									<hr class="pb-2">
								  
								  <div class="row">
									
									<div class="col-lg-6">
										<div class="form-group">
											<label>Business License </label>
											<input type="text" class="form-control"  value="{{$data->license_type}}"disabled>
										  </div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>Insurance License  </label>
											<input type="text" class="form-control"  value="{{$data->license_number}}"disabled>
										  </div>
									</div>
									</div>
<div class="row">
									
									<div class="col-lg-6">
										<div class="form-group">
											<label>License Type </label>
											<input type="text" class="form-control" placeholder="License Type" name="license_type_opt" id="license_type_opt" value="{{$data->license_type_opt}}"disabled>
										  </div>
									</div>
									
									</div>
									<div class="row">
									
									<div class="col-lg-6">
										<div class="form-group">
											<label>License Status </label>
											<input type="text" class="form-control"  value="{{$data->license_status}}"disabled>
										  </div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>Expiration Date </label>
											<input type="text" class="form-control"  value="{{$data->license_expiry_date}}"disabled>
										  </div>
									</div>
									</div>

									<div class="row">
									
									<div class="col-lg-6">
										<div class="form-group">
											<label>Designated Home State </label>
											<input type="text" class="form-control"  value="{{$data->stateName}}"disabled>
										  </div>
									</div>
									
									</div>

								</div>
								
								
								<!-- register form step 4-->
								<div id="register-part-4" class="content mt-5">
								   <h4 class="color-blue">Business Overview</h4>
									<hr class="pb-2">
								  <div class="row">								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Location of services (Regions)</label>
											<input type="text" class="form-control"  value="{{$data->location_of_services}}"disabled>
										  </div>
									</div>
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Number of Agents</label>	
										<input type="text" class="form-control numberOnly"  value="{{$data->number_of_agents}}"disabled>
										</div>
									</div>
								  </div>
								  
								  <div class="row">								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Business turn over (range)</label>
											<!--<input type="text" class="form-control"  value="{{$data->business_turn_over}}"disabled>-->
											<div class="input-group">
												<div class="input-group-prepend">
												<span class="input-group-text"><b>$</b></span>
												</div>
												<input type="text" class="form-control"  value="{{$data->business_turn_over}}"disabled>

											</div>
										  </div>
									</div>
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Number of years in business</label>	
											<input type="text" class="form-control numberOnly"  value="{{$data->number_of_years_in_business}}"disabled>					
										</div>
									</div>
								  </div>
								  
								  <div class="row">								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Lead to Prospect Ratio</label>
											<input type="text" class="form-control"  value="{{$data->lead_to_prospect_ratio}}"disabled>
										  </div>
									</div>
									
									
										
								  </div>
								 @foreach($docDetails as $docData)
								   <div class="row">								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											
											
											  	@if($docData->document_type == "business")
											  <label	>{{$docData->document_name}}</label>
											  	@endif
											 
										  </div>
									</div>
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label></label>
											
											  	@if($docData->document_type == "business")
											  	
											  		
											  	<a target="_blank" alt="View Document" title="View Document" href="{{asset('storage/app/'.$docData->doucment_path)}}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> </a>
											  	<!--<a alt="Delete Document" title="Delete Document"  href="#" onclick="removeDocument('{{$docData->document_id}}')" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> </a>-->

											  		
											  	@endif
											  
										  </div>
									</div>

								</div>

								 @endforeach
								 
								</div>
								<a href="{{route('edit-profile')}}" class="mt-4 btn btn-lg btn-primary ">EDIT DETAILS</a>
								
							 
							
						  </div>
						  <!-- /.card-body -->
						  
						</div>
						<!-- /.card -->
					  </div>
					</div>
				
				<!-- register form stepper -->
				
			</div>
		
	</div>
	@endforeach
	@include('include.footer')

