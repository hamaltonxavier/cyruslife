<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Models\File;
use Auth;
use DB;





class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
       

    }

    public function listAllUsers(){
        $userRole  = Auth::user()->role;
       if($userRole !="superadmin"){
         return redirect('/show-profile');
       }
        $title = 'CYRUS | User List';
        $managerList = $this->getCRMManagerList();
        //var_dump(($managerList));
        return view('admin.list_users', compact(['title','managerList']));
    }


    public function listUserDetails(){

       
        $userDetails =DB::table('user_details')
           ->leftJoin('partner_application_details', 'partner_application_details.user_id', '=', 'user_details.user_id')
            ->leftJoin('agency_application_form', 'agency_application_form.user_id', '=', 'user_details.user_id')
            ->leftJoin('agent_application_form', 'agent_application_form.user_id', '=', 'user_details.user_id')
            ->leftJoin('countries', 'countries.id', '=', 'user_details.country')
            ->leftJoin('states', 'states.id', '=', 'user_details.state')
            ->leftJoin('cities', 'cities.id', '=', 'user_details.city')
            ->join('users','users.id','=','user_details.user_id')
            ->select( 'user_details.*', 'countries.name as countryName','states.name as stateName','cities.name as cityName', 'users.role as role', 'partner_application_details.application_status as pApp','agent_application_form.application_status as atApp','agency_application_form.application_status as agApp', 'partner_application_details.application_details_id as pAppId','agent_application_form.application_details_id as atAppId','agency_application_form.application_details_id as agAppId')
            ->orderBy('user_details.user_id', 'DESC')
            ->get()->toArray();

        
         return response()->json($userDetails);

        
        
    }


    public function showSearchResult(Request $request){
       /* $userType = $request->userType;
        $query  =DB::table('user_details')
         ->leftJoin('partner_application_details', 'partner_application_details.user_id', '=', 'user_details.user_id')
            ->leftJoin('agency_application_form', 'agency_application_form.user_id', '=', 'user_details.user_id')
            ->leftJoin('agent_application_form', 'agent_application_form.user_id', '=', 'user_details.user_id')
            ->leftJoin('countries', 'countries.id', '=', 'user_details.country')
            ->leftJoin('states', 'states.id', '=', 'user_details.state')
            ->leftJoin('cities', 'cities.id', '=', 'user_details.city')
            ->join('users','users.id','=','user_details.user_id');
            
          
        
         $query->when(request('userType') != null, function ($q) {
            return $q->where('users.role', request('userType'));
        });

         $query->when(request('stateId') != null, function ($q) {
            return $q->where('user_details.state', request('stateId'));
        });

         $query->when(request('cityId') != null, function ($q) {
            return $q->where('user_details.city', request('cityId'));
        });

         $query->when(request('adminStatus') != null, function ($q) {
            if(request('adminStatus') == "not"){
                return $q->where('partner_application_details.application_status', null)
                    ->Where('agency_application_form.application_status', null)
                    ->Where('agent_application_form.application_status', null);
            }
            else{
            return $q->where('partner_application_details.application_status', request('adminStatus'))
                    ->orWhere('agency_application_form.application_status', request('adminStatus'))
                    ->orWhere('agent_application_form.application_status', request('adminStatus'));
                }
        });
      $query->select( 'user_details.*', 'countries.name as countryName','states.name as stateName','cities.name as cityName', 'users.role as role', 'partner_application_details.application_status as pApp','agent_application_form.application_status as atApp','agency_application_form.application_status as agApp', 'partner_application_details.application_details_id as pAppId','agent_application_form.application_details_id as atAppId','agency_application_form.application_details_id as agAppId');
        $query->orderBy('user_details.user_id', 'DESC');
      
        $result = $query->get(); */
        $result =   "SELECT `user_details`.*, `countries`.`name` AS `countryName`,`states`.`name` AS `stateName`, `cities`.`name` AS `cityName`, `users`.`role` AS `role`, `partner_application_details`.`application_status` AS `pApp`, `agent_application_form`.`application_status` AS `atApp`, `agency_application_form`.`application_status` AS `agApp`, `partner_application_details`.`application_details_id` AS `pAppId`, `agent_application_form`.`application_details_id` AS `atAppId`, `agency_application_form`.`application_details_id` AS `agAppId`            FROM   `user_details` LEFT JOIN `partner_application_details` ON `partner_application_details`.`user_id` = `user_details`.`user_id` LEFT JOIN `agency_application_form` ON `agency_application_form`.`user_id` = `user_details`.`user_id` LEFT JOIN `agent_application_form` ON `agent_application_form`.`user_id` = `user_details`.`user_id` LEFT JOIN `countries` ON `countries`.`id` = `user_details`.`country` LEFT JOIN `states`ON `states`.`id` = `user_details`.`state` LEFT JOIN `cities` ON `cities`.`id` = `user_details`.`city` INNER JOIN `users` ON `users`.`id` = `user_details`.`user_id` WHERE `users`.`id` <> '' ";
        if($request->userType !=''){
            $result .=" AND users.role = ".$request->userType;
        }

        if($request->stateId !=''){
            $result .=" AND user_details.state = ".$request->stateId;
        }

        if($request->cityId !=''){
            $result .=" AND user_details.city = ".$request->cityId;
        }

        if($request->adminStatus !=''){
            if($request->adminStatus !='not'){
                $result .=" AND ( `partner_application_details`.`application_status` = '".$request->adminStatus."' OR `agency_application_form`.`application_status` = '".$request->adminStatus."' OR `agent_application_form`.`application_status` = '".$request->adminStatus."')";
            }
            else{
                $result .=" AND  `partner_application_details`.`application_status` IS NULL AND `agency_application_form`.`application_status` IS NULL AND `agent_application_form`.`application_status` IS NULL ";
            }

            
        }
        $result .=" ORDER  BY `user_details`.`user_id` DESC";

        $resultVal = DB::select($result);
        return response()->json($resultVal);


    }

    public function viewUserData(Request $request){
        $userRole = $request->userType;
        $userId = $request->userId;
        
       $userDetails =DB::table('user_details')
         ->leftJoin('countries', 'countries.id', '=', 'user_details.country')
         ->leftJoin('states', 'states.id', '=', 'user_details.state')
         ->leftJoin('cities', 'cities.id', '=', 'user_details.city')
         ->leftJoin('countries as oc', 'oc.id', '=', 'user_details.office_country')
         ->leftJoin('states as os', 'os.id', '=', 'user_details.office_state')
         ->leftJoin('cities as oci', 'oci.id', '=', 'user_details.office_city')
         ->select('user_details.*', 'countries.name as countryName','states.name as stateName','cities.name as cityName', 'oc.name as ocName','os.name as osName','oci.name as ociName' )
         ->where('user_details.user_id', '=', $userId)
        ->get(); 
        
         $docDetails = DB::table('document_path')->whereIn('user_id', [$userId])->get();


        if($userRole == '1'){
            $title = 'CYRUS | Partner Profile';
            return view('admin.partner_view_profile', compact(['title','userDetails','docDetails']));
        }

         if($userRole == '2'){
            $title = 'CYRUS | Agency Profile';
            return view('admin.agency_view_profile', compact(['title','userDetails','docDetails']));
        }

         if($userRole == '3'){
            $title = 'CYRUS | Agent Profile';
            return view('admin.agent_view_profile', compact(['title','userDetails','docDetails']));
        }
    }


    public function showPartnerRegister(){
        
        $title = 'CYRUS | Add Partner';
        $countryDetails = DB::table('countries')->whereIn('id', [233])->get()->toArray();
        return view('admin.partner_add_profile', compact(['title','countryDetails']));
    }

    public function showAgencyRegister(){
        
        $title = 'CYRUS | Add Agency';
        $countryDetails = DB::table('countries')->whereIn('id', [233])->get()->toArray();
        return view('admin.agency_add_profile', compact(['title','countryDetails']));
    }


    public function showAgentRegister(){
        
        $title = 'CYRUS | Add Agent';
        $countryDetails = DB::table('countries')->whereIn('id', [233])->get()->toArray();
        return view('admin.agent_add_profile', compact(['title','countryDetails']));
    }


    public function saveUsers(Request $request){

       

        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $passwordVal = substr(str_shuffle($str_result), 0, 7);
        //$testPwd = "testpassword";
       
        $name=  $request->business_name;
      
        if($request->role == '3'){
            $name=  $request->first_name;
        }




            $emailId = $request->email;
          
         $insert = DB::table('users')->insert([
                'name' =>  $name,
                'email' => $request->email,
                'role' =>$request->role,
                'password' => Hash::make($passwordVal),
                //'password' => Hash::make($testPwd),
         ]);

         $lastInsertedId = DB::getPdo()->lastInsertId();

         if($request->role == '1'){
            $insert = DB::table('user_details')->insert([
                'user_id' =>  $lastInsertedId,
                'business_name' => $request->business_name,
                'address1' => $request->address1,
                'address2' => $request->address2,
                'address3' => $request->address3,
                'city' => $request->city,
                'state' => $request->state,
                'country' => $request->country,
                'postal_code' => $request->postal_code,
                'telephone' => $request->telephone,
                'title' => $request->title,
                'first_name' => $request->first_name,
                'middle_name' => $request->middle_name,
                'last_name' => $request->last_name,
                'designation' => $request->designation,
                //'department' => $request->department,
                'office_location' => $request->office_location,
                'office_address1' => $request->office_address1,
                'office_address2' => $request->office_address2,
                'office_address3' => $request->office_address3,
                'office_country' => $request->office_country,
                'office_state' => $request->office_state,
                'office_city' => $request->office_city,
                'office_postal_code' => $request->office_postal_code,
                 'mobile' => $request->mobile,
                'email' => $request->email,
                'agent' => $request->agent,
                'license_type' => $request->license_type,
                'license_type_opt' => $request->license_type_opt,
                'license_number' => $request->license_number,
                'license_status' => $request->license_status,
                'license_expiry_date' => $request->license_expiry_date,
                'license_home_state' => $request->license_home_state,
                'established_details' => $request->established_details,
                'funeral_number' => $request->funeral_number,
                'casked_funeral' => $request->casked_funeral,
                'cremations_annually' => $request->cremations_annually,
                'annual_preneed_volume' => $request->annual_preneed_volume,
               
            ]);
         }

         if($request->role == '2'){
            $insert = DB::table('user_details')->insert([
                'user_id' =>  $lastInsertedId,
                'business_name' => $request->business_name,
                'address1' => $request->address1,
                'address2' => $request->address2,
                'address3' => $request->address3,
                'city' => $request->city,
                'state' => $request->state,
                'country' => $request->country,
                'postal_code' => $request->postal_code,
                'telephone' => $request->telephone,
                'title' => $request->title,
                'first_name' => $request->first_name,
                'middle_name' => $request->middle_name,
                'last_name' => $request->last_name,
                 'mobile' => $request->mobile,
                'email' => $request->email,
                'agent' => $request->agent,
                'license_type' => $request->license_type,
                'license_type_opt' => $request->license_type_opt,
                'license_number' => $request->license_number,
                'license_status' => $request->license_status,
                'license_expiry_date' => $request->license_expiry_date,
                'license_home_state' => $request->license_home_state,
                'location_of_services' => $request->location_of_services,
                'number_of_agents' => $request->number_of_agents,
                'business_turn_over' => $request->business_turn_over,
                'number_of_years_in_business' => $request->number_of_years_in_business,
                'lead_to_prospect_ratio' => $request->lead_to_prospect_ratio,
                
            ]);
         }


         if($request->role == '3'){
            $insert = DB::table('user_details')->insert([
                'user_id' =>  $lastInsertedId,
                'address1' => $request->address1,
                'address2' => $request->address2,
                'address3' => $request->address3,
                'city' => $request->city,
                'state' => $request->state,
                'country' => $request->country,
                'postal_code' => $request->postal_code,
                'title' => $request->title,
                'first_name' => $request->first_name,
                'middle_name' => $request->middle_name,
                'last_name' => $request->last_name,
                 'mobile' => $request->mobile,
                'email' => $request->email,
                'license_type' => $request->license_type,
                'license_type_opt' => $request->license_type_opt,
                'license_number' => $request->license_number,
                'license_status' => $request->license_status,
                'license_expiry_date' => $request->license_expiry_date,
                'license_home_state' => $request->license_home_state,
                'location_of_services' => $request->location_of_services,
                'business_turn_over' => $request->business_turn_over,
                'number_of_years_in_business' => $request->number_of_years_in_business,
                
                
            ]);
         }


         if($request->hasfile('idFiles'))
         {
          
            $total = count($request->file('idFiles'));
            for( $i=0 ; $i < $total ; $i++ ) {
            
                $fileNameVal = time().rand(1,100).'.'.$request->file('idFiles')[$i]->extension();
                $filePathVal =  $request->file('idFiles')[$i]->store('public/files');  
                $files[] = $fileNameVal; 
                //$imageType = $request->filetitle[$i];
               


                $insert = DB::table('document_path')->insert([
                   'user_id' =>  $lastInsertedId,
                   'doucment_path' => $filePathVal,
                    'document_type'=>'idproof'
                 ]);
              
            }
            
         }

        

        /*  if($request->hasfile('files'))
         {
            foreach($request->file('files') as $key => $file)
            {
                $path = $file->store('public/files');
             
                $insert = DB::table('document_path')->insert([
                   'user_id' =>  $lastInsertedId,
                   'doucment_path' => $path,
                
                 ]);
 
            }
         }*/

         $files = [];
      
        if($request->hasfile('filenames'))
         {
          
            $total = count($request->file('filenames'));
            for( $i=0 ; $i < $total ; $i++ ) {
            
                $fileNameVal = time().rand(1,100).'.'.$request->file('filenames')[$i]->extension();
                $filePathVal =  $request->file('filenames')[$i]->store('public/files');  
                $files[] = $fileNameVal; 
                $imageType = $request->filetitle[$i];
                $documentNameVal = $request->filetitle[$i];


                $insert = DB::table('document_path')->insert([
                   'user_id' =>  $lastInsertedId,
                   'doucment_path' => $filePathVal,
                    'document_name'=>$documentNameVal,
                    'document_type'=>'business'
                 ]);
              
            }
            
         }


           $messageData = [
                'email' => $emailId,
                'name' => $name,
                'codeVal' => base64_encode($emailId)
                
            ];

            Mail::send('account_activation_email', $messageData, function($message) use ($emailId) {
            $message->to($emailId)
            ->subject("Activate your account");
            $message->from("info@cyruslife.com","Activate your account");
            });
 
       return redirect(url('/admin/users_list'))->with(['message-success'=>'User has been created successfully.']);

    }

    public function viewUserApplicationData(Request $request){
        $userRole = $request->userType;
        $userId = $request->userId;
        
       

        if($userRole == '1'){
            $title = 'CYRUS | Partner Application';

            $applicationCount = DB::table('partner_application_details')
                        ->where(['user_id' => $userId])
                        ->count();
            if($applicationCount > 0){      
                    $applicationDetails =DB::table('partner_application_details')
                 ->select('partner_application_details.*')
                 ->where('user_id', '=', $userId)
                 ->get();
                 
             }
             else{
                 $applicationDetails = '1';
             }
            return view('admin.partner_view_application_form', compact(['title','applicationDetails']));
        }

         if($userRole == '2'){
            $title = 'CYRUS | Agency Application';

            $applicationCount = DB::table('agency_application_form')
                        ->where(['user_id' => $userId])
                        ->count();
            if($applicationCount > 0){ 
                    $applicationDetails =DB::table('agency_application_form')
                 ->select('agency_application_form.*')
                 ->where('user_id', '=', $userId)
                 ->get();
                
             }
             else{
                $applicationDetails = '1';
             }
            return view('admin.agency_view_application_form', compact(['title','applicationDetails']));
        }

         if($userRole == '3'){
            $title = 'CYRUS | Agent Application';

            $applicationCount = DB::table('agent_application_form')
                        ->where(['user_id' => $userId])
                        ->count();
            if($applicationCount > 0){ 
                    $applicationDetails =DB::table('agent_application_form')
                 ->select('agent_application_form.*')
                 ->where('user_id', '=', $userId)
                 ->get();
                 
             }
             else{
                  $applicationDetails = '1';
             }
            return view('admin.agent_view_application_form', compact(['title','applicationDetails']));
        }
    }

    public function loadApplicationDetails(Request $request){
       $userRole = $request->userType;
        $userId = $request->userId;
        if($userRole == '1'){
             $applicationCount = DB::table('partner_application_details')
                        ->where(['user_id' => $userId])
                        ->count();
            if($applicationCount > 0){      
                    $applicationDetails =DB::table('partner_application_details')
                 ->select('partner_application_details.*')
                 ->where('user_id', '=', $userId)
                 ->get();
                  return response()->json($applicationDetails);
             }
             else{
                 return response()->json('1');
             }
        }

        if($userRole == '2'){
            $applicationCount = DB::table('agency_application_form')
                        ->where(['user_id' => $userId])
                        ->count();
            if($applicationCount > 0){ 
                    $applicationDetails =DB::table('agency_application_form')
                 ->select('agency_application_form.*')
                 ->where('user_id', '=', $userId)
                 ->get();
                 return response()->json($applicationDetails);
             }
             else{
                 return response()->json('1');
             }
        }


        if($userRole == '3'){
            $applicationCount = DB::table('agent_application_form')
                        ->where(['user_id' => $userId])
                        ->count();
            if($applicationCount > 0){ 
                 $applicationDetails =DB::table('agent_application_form')
                 ->select('agent_application_form.*')
                 ->where('user_id', '=', $userId)
                 ->get();
                 return response()->json($applicationDetails);
             }
             else{
                 return response()->json('1');
             }
        }
        
        
        
    }

    public function submitReview(Request $request){
        $insert = DB::table('admin_application_review')->insert([
                'user_type' =>  $request->selectedUserType,
                'user_id' => $request->selectedUserId,
                'application_id' =>$request->selectedApplicationId,
                'comments' => $request->reviewData,
                
         ]);

        $userDetails =DB::table('users')
                 ->select('users.email','name')
                 ->where('id', '=', $request->selectedUserId)
                 ->get();
         $emailId = $userDetails[0]->email;

          $messageData = [
                'email' => $userDetails[0]->email,
                'name' => $userDetails[0]->name,
                'reviewData' => $request->reviewData
                
            ];

            Mail::send('admin_application_review_email', $messageData, function($message) use ($emailId) {
            $message->to($emailId)
            ->subject("Your Application Feedbck");
            $message->from("info@cyruslife.com","Your Application Feedbck");
            });

        return response()->json('1'); 
    }

    public function updateApplication(Request $request){
        $appTbl = "";
        $appStatus = "approved";
        if($request->userType == "1"){
            $appTbl = "partner_application_details";
        }
        else if($request->userType == "2"){
            $appTbl = "agency_application_form";
        }
        else{
            $appTbl = "agent_application_form";
        }

        if($request->applicationStatus == "2"){
            $appStatus ="rejected";
        }
        
        $update =  DB::table($appTbl)->where('application_details_id',$request->applicationId )->update(array(
                         'application_status' => $appStatus,
                       
                    ));

        $emailDetails =DB::table('users')
            ->select( 'users.email','users.name')
            ->where('users.id',$request->userId)
            ->get();

            $emailIdVal = $emailDetails[0]->email;

         $messageData = [
                'email' => $emailIdVal,
                'name' => $emailDetails[0]->name,
                'status' => $appStatus
                
            ];

            Mail::send('application_status_email', $messageData, function($message) use ($emailIdVal) {
            $message->to($emailIdVal)
            ->subject("Application Status");
            $message->from("info@cyruslife.com","Application Status");
            });
             return response()->json('1');
      
    }

    public function loadReviewLog(Request $request){
            $reviewDetails =DB::table('admin_application_review')
            ->select( 'admin_application_review.*')
            ->where('admin_application_review.application_id', '=', $request->applicationId)
            ->orderBy('admin_application_review.review_id', 'DESC')
            ->get()->toArray();

        
         return response()->json($reviewDetails);
    }

    public function showManagerName(Request $request){
         $managerDetails =DB::table('user_details')
                 ->select('manager_name')
                 ->where('user_id', '=', $request->userId)
                 ->get();
            return response()->json($managerDetails);
    }

    public function assignManager(Request $request){

         $update =  DB::table('user_details')->where('user_id',$request->userId )->update(array(
                         'manager_name' => $request->managerName,
                       
                    ));
        return response()->json('1'); 
    }

    public function addFuneralHome(){
        $userRole  = Auth::user()->role;
       if($userRole !="superadmin"){
         return redirect('/logout');
       }
        $countryDetails = DB::table('countries')->whereIn('id', [233])->get()->toArray();
        $title = 'CYRUS | Funeral Home';
        return view('admin.add_funeral_home', compact(['title','countryDetails']));
    }

    public function showUserBasedOnRole(Request $request){
        $userRole = $request->userType;

        $userDetails =DB::table('user_details')
            ->join('users','users.id','=','user_details.user_id')
            ->select( 'users.id','user_details.business_name', 'user_details.first_name', 'user_details.middle_name', 'user_details.last_name' )
            ->where('users.role', '=', $userRole)
            ->orderBy('user_details.business_name', 'ASC')
            ->get()->toArray();

        if($userRole == '3'){
            $userDetails =DB::table('user_details')
            ->join('users','users.id','=','user_details.user_id')
            ->select( 'users.id','user_details.business_name', 'user_details.first_name', 'user_details.middle_name', 'user_details.last_name' )
            ->where('users.role', '=', $userRole)
            ->orderBy('user_details.first_name', 'ASC')
            ->get()->toArray();
        }

        
         return response()->json($userDetails);

    }

    public function insertFuneralHome(Request $request){
         $insert = DB::table('funeral_home')->insert([
                'user_id' =>  $request->user_id,
                'user_type' => $request->user_type,
                'funeral_home_name' =>$request->funeral_home_name,
                'address1' => $request->address1,
                'address2' => $request->address2,
                'address3' => $request->address3,
                'city' => $request->city,
                'state' => $request->state,
                'country' => $request->country,
                'postal_code' => $request->postal_code,
                
         ]);

         return redirect(route('add_funeral_home'))->with(['message-success'=>'Funeral Home has been added successfully.']);
    }


     public function listFuneralHome(){
        $userRole  = Auth::user()->role;
       if($userRole !="superadmin"){
         return redirect('/logout');
       }
        $countryDetails = DB::table('countries')->whereIn('id', [233])->get()->toArray();
        $title = 'CYRUS | Funeral Home';
        return view('admin.list_funeral_home', compact(['title','countryDetails']));
    }


    public function listFHDetails(){
        $fhDetails =DB::table('funeral_home')
            ->leftJoin('countries', 'countries.id', '=', 'funeral_home.country')
            ->leftJoin('states', 'states.id', '=', 'funeral_home.state')
            ->leftJoin('cities', 'cities.id', '=', 'funeral_home.city')
            ->leftJoin('users','users.id','=','funeral_home.user_id')
            ->leftJoin('user_details','user_details.user_id','=','funeral_home.user_id')
            ->select( 'funeral_home.*', 'countries.name as countryName','states.name as stateName','cities.name as cityName', 'users.role as role', 'user_details.business_name', 'user_details.first_name', 'user_details.middle_name', 'user_details.last_name')
            ->orderBy('funeral_home.funeral_home_id', 'DESC')
            ->get()->toArray();

        
         return response()->json($fhDetails);
    }

    public function showFHSearchResult(Request $request){
        $userType = $request->userType;
        $query  =DB::table('funeral_home')
         ->leftJoin('countries', 'countries.id', '=', 'funeral_home.country')
            ->leftJoin('states', 'states.id', '=', 'funeral_home.state')
            ->leftJoin('cities', 'cities.id', '=', 'funeral_home.city')
            ->leftJoin('users','users.id','=','funeral_home.user_id')
            ->leftJoin('user_details','user_details.user_id','=','funeral_home.user_id');
       
         $query->when(request('userType') != null, function ($q) {
            return $q->where('users.role', request('userType'));
        });

         $query->when(request('stateId') != null, function ($q) {
            return $q->where('funeral_home.state', request('stateId'));
        });

         $query->when(request('cityId') != null, function ($q) {
            return $q->where('funeral_home.city', request('cityId'));
        });

         
      $query->select( 'funeral_home.*', 'countries.name as countryName','states.name as stateName','cities.name as cityName', 'users.role as role', 'user_details.business_name', 'user_details.first_name', 'user_details.middle_name', 'user_details.last_name');
           
        $query->orderBy('funeral_home.funeral_home_id', 'DESC');
      
        $result = $query->get(); 



        return response()->json($result);


    }

    public function removeFH(Request $request){
       $deleteDoc = DB::table('funeral_home')
        ->where('funeral_home_id', $request->fhId)
        ->delete();

        return response()->json("removed"); 
    }

    public function getCRMManagerList(){
        $url = 'https://ch.flatworldinfotech.com/public/index.php?entryPoint=ManagersList';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL,$url);
        $result=curl_exec($ch);
        curl_close($ch);
        return (json_decode($result, true));
    }

    public function createCRMUser(Request $request){
      /*  $userDetails =DB::table('user_details')
            ->join('users','users.id','=','user_details.user_id')
            ->select( 'users.email','user_details.business_name', 'user_details.first_name', 'user_details.middle_name', 'user_details.last_name' )
            ->where('users.id', '=', $request->userId)
            ->get();*/
            $userDetails =DB::table('user_details')
         ->leftJoin('countries', 'countries.id', '=', 'user_details.country')
         ->leftJoin('states', 'states.id', '=', 'user_details.state')
         ->leftJoin('cities', 'cities.id', '=', 'user_details.city')
         ->leftJoin('countries as oc', 'oc.id', '=', 'user_details.office_country')
         ->leftJoin('states as os', 'os.id', '=', 'user_details.office_state')
         ->leftJoin('cities as oci', 'oci.id', '=', 'user_details.office_city')
         ->select('user_details.*', 'countries.name as countryName','states.name as stateName','cities.name as cityName', 'oc.name as ocName','os.name as osName','oci.name as ociName' )
         ->where('user_details.user_id', '=', $request->userId)
        ->get();
        
        $docDetails = DB::table('document_path')->where('user_id','=', $request->userId)->get();
        
        //echo $userDetails[0]->email;
        //$docDetailsVal[] = $docDetails;
            $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
            $passwordVal = substr(str_shuffle($str_result), 0, 7);
            $userNameVal = explode('@',$userDetails[0]->email);
            $userRole = '';
            if($request->role == '1'){
                $userRole = 'b764e8b1-50eb-6630-7130-6278a33ac8f0';
            }
            else if($request->role == '2'){
                $userRole = 'b9331d7f-aea3-7359-19f9-6278a3f40ae9';
            }
            else{
                $userRole = 'bdef2d20-3fa6-0b0b-753d-6278a35649c4';
            }
            // set post fields
           $userNameRand = rand(10,99);
            $userNameRes = $userNameVal[0].''.$userNameRand;
            
             $appTbl = "";
        $agency_entity_name = "";
        $agency_name = "";
        $agent_entity_name = "";
        $app_license_type = "";
        $lines_of_business = "";
        $national_producer_number = "";
        $state_issued_funeral_id = "";
        $background_check_c = "";
        $irs_form_w9_c = "";
        $proof_of_good_standing_c = "";
        $drivers_license_c = "";
            
        if($request->role == "1"){
            $appTbl = "partner_application_details";
           
        }
        else if($request->role == "2"){
            $appTbl = "agency_application_form";
           
        }
        else{
            $appTbl = "agent_application_form";
           
        }

        $applicationDetails =DB::table($appTbl)
                 ->select($appTbl.'.*')
                 ->where('user_id', '=',$request->userId)
                 ->get();

       

      
        
        if($request->role == "1"){
            $appTbl = "partner_application_details";
            $state_issued_funeral_id = $applicationDetails[0]->crematoria_licence_id;
             $agency_entity_name = $applicationDetails[0]->agency_entity_name;
             $agent_entity_name = $applicationDetails[0]->agent_entity_name;
             $irs_form_w9_c = $applicationDetails[0]->irs_form_w9_doc_path;
        }
        else if($request->role == "2"){
            $appTbl = "agency_application_form";
            $agency_name = $applicationDetails[0]->agency_licence_name;
            $lines_of_business = $applicationDetails[0]->lines_of_business;
            $national_producer_number = $applicationDetails[0]->npn;
            $background_check_c = $applicationDetails[0]->bca_doc_path;
            $irs_form_w9_c = $applicationDetails[0]->irs_form_w9_doc_path;
            $proof_of_good_standing_c = $applicationDetails[0]->application_doc_path;
        }
        else{
            $appTbl = "agent_application_form";
            $app_license_type = $applicationDetails[0]->license_type;
            $national_producer_number = $applicationDetails[0]->npn;
            $irs_form_w9_c = $applicationDetails[0]->irs_form_w9_doc_path;
            $background_check_c = $applicationDetails[0]->bca_doc_path;
            $drivers_license_c = $applicationDetails[0]->license_doc_path;
        }
        
        
        
        


        
            $post = [
                'user_name' => $userNameRes,
                'user_hash' => $passwordVal,
                'first_name'   => $userDetails[0]->first_name,
                'last_name'   => $userDetails[0]->last_name,
                'email'   => $userDetails[0]->email,
                'user_role'   => $userRole,
                'business_name'   => $userDetails[0]->business_name,
                'address1'   => $userDetails[0]->address1,
                'address2'   => $userDetails[0]->address2,
                'address3'   => $userDetails[0]->address3,
                'countryName'   => $userDetails[0]->countryName,
                'stateName'   => $userDetails[0]->stateName,
                'cityName'   => $userDetails[0]->cityName,
                'postal_code'   => $userDetails[0]->postal_code,
                'telephone'   => $userDetails[0]->telephone,
                'license_type'   => $userDetails[0]->license_type,
                'mobile'   => $userDetails[0]->mobile,
                'designation'   => $userDetails[0]->designation,
                'office_address1'   => $userDetails[0]->office_address1,
                'office_address2'   => $userDetails[0]->office_address2,
                'office_address3'   => $userDetails[0]->office_address3,
                'ocName'   => $userDetails[0]->ocName,
                'osName'   => $userDetails[0]->osName,
                'ociName'   => $userDetails[0]->ociName,
                'office_postal_code'   => $userDetails[0]->office_postal_code,
                'license_number'   => $userDetails[0]->license_number,
                'license_type_opt'   => $userDetails[0]->license_type_opt,
                'license_status'   => $userDetails[0]->license_status,
                'license_expiry_date'   => $userDetails[0]->license_expiry_date,
                'stateName'   => $userDetails[0]->stateName,
                'established_details'   => $userDetails[0]->established_details,
                'funeral_number'   => $userDetails[0]->funeral_number,
                'casked_funeral'   => $userDetails[0]->casked_funeral,
                'cremations_annually'   => $userDetails[0]->cremations_annually,
                'annual_preneed_volume'   => $userDetails[0]->annual_preneed_volume,
                'business_turn_over'   => $userDetails[0]->business_turn_over,
                'lead_to_prospect_ratio'   => $userDetails[0]->lead_to_prospect_ratio,
                'location_of_services'   => $userDetails[0]->location_of_services,
                'number_of_agents'   => $userDetails[0]->number_of_agents,
                'number_of_years_in_business'   => $userDetails[0]->number_of_years_in_business,
                'agency_entity_name'   => $agency_entity_name,
                'agency_name'   => $agency_name,
                'agent_entity_name'   => $agent_entity_name,
                'app_license_type'   => $app_license_type,
                'lines_of_business'   => $lines_of_business,
                'national_producer_number'   => $national_producer_number,
                'state_issued_funeral_id'   => $state_issued_funeral_id,
                'background_check_c'   => $background_check_c,
                'irs_form_w9_c'   => $irs_form_w9_c,
                'proof_of_good_standing_c'   => $proof_of_good_standing_c,
                'drivers_license_c'   => $drivers_license_c,
                'doc_details' => json_encode($docDetails),
                'user_id' =>$request->userId,
                'user_role_type'=>$request->role


            ];
            
           

            $ch = curl_init('https://ch.flatworldinfotech.com/public/index.php?entryPoint=MyTimeEntryPoint');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            $response = curl_exec($ch);
            curl_close($ch);
            //return response()->json(strpos($response,"User");
           // return $response;

            if(strpos($response, "User") !== false){
                return response()->json('Existed');
            } else{
                $update =  DB::table('user_details')->where('user_id',$request->userId )->update(array(
                    'crm_user_id' => $response,
                    'admin_status' => 'CRM-user-created'
            ));
                return response()->json('Created');
            }
    }
}
