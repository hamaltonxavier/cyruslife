<?php echo $__env->make('include.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	<style>
	body{font-weight:500}
	.font18{font-size:18px;}
	.font24{font-size:24px; font-weight:500}
	#inner-navbarNavDropdown .nav-link {
	  color: #004367;
	  font-weight: 500;
	  font-size: 17px;
	}
	
	.btn-apply-link {
  background: #FED56D;
  border-radius: 4px;
  padding: 4px 10px;
    padding-right: 10px;
    padding-left: 10px;
  font-weight: 600 !important;
}
	
  </style>
	

	
	<div class="content-body pb-5">
	
	
		<!-- content header -->
		
		<div class="content-header bg-white mb-4 pt-2 pb-2">
		  <div class="container">
			<div class="row  align-items-center">
			  <div class="col-sm-4">
				<h3 class="m-0 color-blue"> <small>Partner:</small> Solarium Inc. </h3>
			  </div><!-- /.col -->
			  <div class="col-sm-6">
				<div class="d-flex">
						<button class="btn btn-primary btn-apply-link">Apply for Partnership</button>
						 <div class="dropdown">
						  <button type="button" class="btn dropdown-toggle color-blue" data-toggle="dropdown">
							Profile
						  </button>
						  <ul class="dropdown-menu">
							<li><a class="dropdown-item" href="#">Link 1</a></li>
							<li><a class="dropdown-item" href="#">Link 2</a></li>
							<li><a class="dropdown-item" href="#">Link 3</a></li>
						  </ul>
						</div> 
				</div>
			  </div><!-- /.col -->
			  
			   <div class="col-sm-2">
				<div class="user-role text-right color-blue font18">
					Role: <strong>Partner</strong>
				</div>
			  </div><!-- /.col -->
			</div><!-- /.row -->
		  </div><!-- /.container-fluid -->
		</div>
	
		
		<div class="container">
				
				<!-- table agency -->
					<div class="row">
					  <div class="col-md-12">
						
						<div class="card">
						  <div class="card-header d-flex align-items-center">
							<h3 class="card-title mr-auto font24">Agencies</h3>
							<div class="card-tools">
								<button  class="btn btn-primary">+ Add New</button>
							  </div>
						  </div>
						  <!-- /.card-header -->
						  
						  <div class="card-body">
							<table id="example2" class="table table-bordered table-striped dataTable dtr-inline">
							  <thead>
								  <tr>
									<th>#</th>
									<th>Agency</th>
									<th>Primary Contact</th>
									<th>State/City</th>
									<th>Agents</th>
									<th>Status</th>
									<th>Actions</th>
								  </tr>
							  </thead>
							  
							  <tbody>
								<!-- single row -->
								  <tr>
									<td>-</td>
									<td>Flip Flop</td>
									<td>-</td>
									<td>-</td>
									<td>-</td>
									<td><span class="badge badge-success">Approved</span></td>
									<td class="project-actions text-right">
										  <a class="btn btn-primary btn-sm" href="#">
											  <i class="fas fa-plus">
											  </i>
											  Agent
										  </a>
										  <a class="btn btn-success btn-sm" href="#">
											  <i class="fas fa-eye">
											  </i>
										  </a>
										  <a class="btn btn-info btn-sm" href="#">
											  <i class="fas fa-pencil-alt">
											  </i>
										  </a>
										  <a class="btn btn-danger btn-sm" href="#">
											  <i class="fas fa-trash">
											  </i>
										  </a>
									</td>
								  </tr>
								  <!-- single row -->
								  
								  <!-- single row -->
								  <tr>
									<td>-</td>
									<td>Flip Flop</td>
									<td>-</td>
									<td>-</td>
									<td>-</td>
									<td><span class="badge badge-secondary">pending</span></td>
									<td class="project-actions text-right">
										  <a class="btn btn-primary btn-sm" href="#">
											  <i class="fas fa-plus">
											  </i>
											  Agent
										  </a>
										  <a class="btn btn-success btn-sm" href="#">
											  <i class="fas fa-eye">
											  </i>
										  </a>
										  <a class="btn btn-info btn-sm" href="#">
											  <i class="fas fa-pencil-alt">
											  </i>
										  </a>
										  <a class="btn btn-danger btn-sm" href="#">
											  <i class="fas fa-trash">
											  </i>
										  </a>
									</td>
								  </tr>
								  <!-- single row -->
								 
								  
							  </tbody>
							  
							  
							  
							</table>
						  </div>
						  <!-- /.card-body -->
					</div>
					<!-- /.card -->
						
					  </div>
					</div>
				<!-- table agency end -->
				
				
				<!-- table agency -->
					<div class="row">
					  <div class="col-md-12">
						
						<div class="card">
						  <div class="card-header d-flex align-items-center">
							<h3 class="card-title mr-auto font24">Agent</h3>
							<div class="card-tools">
								<button  class="btn btn-primary">+ Add New</button>
							  </div>
						  </div>
						  <!-- /.card-header -->
						  
						  <div class="card-body">
							<table id="example2" class="table table-bordered table-striped dataTable dtr-inline">
							  <thead>
								  <tr>
									<th>#</th>
									<th>Agent Name</th>
									<th>State/City</th>
									<th>Status</th>
									<th>Actions</th>
								  </tr>
							  </thead>
							  
							  <tbody>
								<!-- single row -->
								  <tr>
									<td>-</td>
									<td>Flip Flop</td>
									<td>-</td>
								
									<td><span class="badge badge-success">Approved</span></td>
									<td class="project-actions text-right">
										  <a class="btn btn-primary btn-sm" href="#">
											  <i class="fas fa-plus">
											  </i>
											  ON BOARDING APPLICATION
										  </a>
										  <a class="btn btn-success btn-sm" href="#">
											  <i class="fas fa-eye">
											  </i>
										  </a>
										  <a class="btn btn-info btn-sm" href="#">
											  <i class="fas fa-pencil-alt">
											  </i>
										  </a>
										  <a class="btn btn-danger btn-sm" href="#">
											  <i class="fas fa-trash">
											  </i>
										  </a>
									</td>
								  </tr>
								  <!-- single row -->
								  
								  <!-- single row -->
								  <tr>
									<td>-</td>
									<td>Flip Flop</td>
									<td>-</td>
									
									<td><span class="badge badge-secondary">pending</span></td>
									<td class="project-actions text-right">
										  <a class="btn btn-primary btn-sm" href="#">
											  <i class="fas fa-plus">
											  </i>
											  ON BOARDING APPLICATION
										  </a>
										  <a class="btn btn-success btn-sm" href="#">
											  <i class="fas fa-eye">
											  </i>
										  </a>
										  <a class="btn btn-info btn-sm" href="#">
											  <i class="fas fa-pencil-alt">
											  </i>
										  </a>
										  <a class="btn btn-danger btn-sm" href="#">
											  <i class="fas fa-trash">
											  </i>
										  </a>
									</td>
								  </tr>
								  <!-- single row -->
								 
								  
							  </tbody>
							  
							  
							  
							</table>
						  </div>
						  <!-- /.card-body -->
					</div>
					<!-- /.card -->
						
					  </div>
					</div>
				<!-- table agency end -->
				
				
		</div>
		
	</div>
	
	
	<?php echo $__env->make('include.admin_footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
	  "sDom": 'l<"toolbar">frtip'
    });
	
  });
</script>


</html><?php /**PATH C:\xampp\htdocs\cyruslife\resources\views/partner-profile-listing.blade.php ENDPATH**/ ?>