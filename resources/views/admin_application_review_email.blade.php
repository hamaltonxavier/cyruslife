<!DOCTYPE html>
<html lang="en">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cyrus Life Insurance</title>
	<head> </head>

	<body style="padding:0; margin:0">
		<table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#f6f6f6" align="center">
			<tbody>
				<tr>
					<td valign="top" align="center">
					
					<!-- CONTENT -->
						
						<table style="width:600px;max-width:600px; font-family:arial; background-color:#fff" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
							<tr>
								<td style="background-color:#004367; text-align:center">
									<div style="font-size:20px;height:20px;line-height:20px;">&nbsp;</div>
									<!--<img src="{{ asset('public/adminlte/dist/img/logo-white.png')}}" style="max-width:100%; border:0 none; outline:0 none;">-->
									<img src="{{ asset('public/adminlte/dist/img/logo-white.png')}}" width="220" style="max-width:100%; border:0 none; outline:0 none;">
									<div style="font-size:20px;height:20px;line-height:20px;">&nbsp;</div>
								</td>
							</tr>
							
							<tr>
								<td style="font-size:20px;height:20px;line-height:20px; background-color:#FED56D">&nbsp;</td>
							</tr>
							
							<tr>
								<td style="text-align:center; font-size:28px; font-weight:bold; background-color:#FED56D; color:#004367">Application Feedback</td>
							</tr>
							
							<tr>
								<td style="font-size:20px;height:20px;line-height:20px; background-color:#FED56D">&nbsp;</td>
							</tr>
							
							<tr>
								<td style="font-size:30px;height:30px;line-height:30px;">&nbsp;</td>
							</tr>
							
							<tr>
								<td align="center" style="text-align:center">Hi {{$name}},</td>
							</tr>
							
							<tr>
								<td style="font-size:30px;height:30px;line-height:30px;">&nbsp;</td>
							</tr>
							
							
							<tr>
								<td style="padding:20px" >Thank you for submitting your application to join the Cyrus Life Partner Network.
								<br/>
								We reviewed your application and need the following data/documentation/clarification/data for processing your application. </td>
							</tr>
							<tr>
								<td style="font-size:10px;height:10px;line-height:10px;">&nbsp;</td>
							</tr>

							<tr>
								<td style="padding:20px" >
									{!! html_entity_decode($reviewData) !!}
									

								</td>
							</tr>

							<tr>
								<td style="padding:20px" >
									
									Please amend your application and resubmit the same for approval.

								</td>
							</tr>
							<tr>
								<td style="font-size:40px;height:10px;line-height:40px;">&nbsp;</td>
							</tr>
							
							<tr>
								<td style="padding:20px" >Thanks </td>
							</tr>
							
							<tr>
								<td style="padding:20px" >Cyrus Life Insurance </td>
							</tr>
							<tr>
								<td style="font-size:40px;height:40px;line-height:40px;">&nbsp;</td>
							</tr>
							<tr>
								<td style="background-color:#004367; text-align:center; color:#fff; font-size:13px;">
									<div style="font-size:20px;height:20px;line-height:20px;">&nbsp;</div>
									Copyright &copy; 2022. Cyrus Life Insurance
									<div style="font-size:20px;height:20px;line-height:20px;">&nbsp;</div>
								</td>
							</tr>
							
						</table>
					
					<!-- CONTENT -->
					</td>
				</tr>
			</tbody>
		</table>
</body>
</html>