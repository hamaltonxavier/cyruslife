<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\admin\AdminController;
use App\Http\Controllers\DocusignController;
use App\Http\Controllers\ApplicationController;

//use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get("/", [HomeController::class, 'showLandingPage'])->name('landing_page');

Route::get("/signin", [AuthController::class, 'index'])->name('signin');

Route::get("/agency_registration", [HomeController::class, 'showAgencyRegister'])->name('agency_registration');

Route::get("/partner_registration", [HomeController::class, 'showPartnerRegister'])->name('partner_registration');

Route::get("/agent_registration", [HomeController::class, 'showAgentRegister'])->name('agent_registration');

Route::post("/state_details", [HomeController::class, 'loadStateBasedonCountry']);

Route::post("/city_details", [HomeController::class, 'loadCityBasedonCountry']);

Route::post("/unique_email", [RegisterController::class, 'uniqueEmail']);

Route::get("/testemail", [RegisterController::class, 'testemail']);

Route::match(['GET','POST'],"/account_activate/{code}", [RegisterController::class, 'activate_account']);

Route::post("/create_new_password", [RegisterController::class, 'createPassword'])->name('create_password');


Route::post("/registration", [RegisterController::class, 'saveUsers'])->name('registration');


Route::get('/show-profile',[ProfileController::class,'showProfile']);

Route::get('/show-sub-profile/{usertype}/{userid}',[ProfileController::class,'showProfile']);

Route::get('/edit-profile',[ProfileController::class,'editProfile'])->name('edit-profile');

Route::get('/edit-sub-profile/{usertype}/{userid}',[ProfileController::class,'editProfile']);

Route::post("/update-profile", [ProfileController::class, 'updateUsers'])->name('update_profile');

Route::post("/update-sub-profile/{usertype}/{userid}", [ProfileController::class, 'updateUsers']);

Route::match(['GET','POST'],'/login', [AuthController::class,'authenticate']);


Route::get('/logout',[AuthController::class,'logout'])->name('logout');

Route::get("/forgot_password", [HomeController::class, 'forgotPassword'])->name('forgot_password');

Route::match(['GET','POST'],'/forgot_password_email', [AuthController::class,'forgotPasswordEmail']);

Route::get("/reset_new_password/{tokken}/{email}", [HomeController::class, 'resetNewPassword'])->name('reset_new_password');


Route::post("/update_reset_password", [AuthController::class, 'updateResetPassword'])->name('update_reset_password');


Route::get('/show_applicationform/',[ApplicationController::class,'showApplicationForm']);

Route::get('/show_sub_applicationform/{usertype}/{userid}',[ApplicationController::class,'showApplicationForm']);

Route::get('/user_dashboard',[ProfileController::class,'showUserDashboard']);

Route::post("/application_save", [ApplicationController::class, 'application_save'])->name('application_save');

Route::post("/application_details", [ApplicationController::class, 'loadApplicationDetails']);
Route::post("/sub_application_details/{usertype}/{userid}", [ApplicationController::class, 'loadApplicationDetails']);

Route::get("/show_user_details", [ProfileController::class, 'showUserDetails']);

Route::post("/show_sub_user_details", [ProfileController::class, 'showSubUsersDetails']);

Route::post("/remove_doc", [ProfileController::class, 'removeDoc']);


Route::get("/thankyou", [HomeController::class, 'thankyou'])->name('thankyou');

Route::match(['GET','POST'],"/user_reg_doc/{userid}", [HomeController::class, 'getDocDetails']);


//Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



//Admin Screens
//Route::get("/admin_signin", [LoginController::class, 'index'])->name('admin_signin');

Route::match(['GET','POST'],'/admin_login', [AdminLoginController::class,'authenticate']);

Route::get('/admin/users_list',[AdminController::class,'listAllUsers']);

Route::get('/admin/load_user_list',[AdminController::class,'listUserDetails']);

Route::post("/show_search_result", [AdminController::class, 'showSearchResult']);

Route::match(['GET','POST'],"/admin/user_profile/{userType}/{userId}", [AdminController::class, 'viewUserData']);

Route::get('/admin/add_partner',[AdminController::class,'showPartnerRegister'])->name('partner_add_profile');

Route::get('/admin/add_agency',[AdminController::class,'showAgencyRegister'])->name('agency_add_profile');

Route::get('/admin/add_agent',[AdminController::class,'showAgentRegister'])->name('agent_add_profile');

Route::post("admin/add_user", [AdminController::class, 'saveUsers'])->name('add_user');

Route::match(['GET','POST'],"/admin/user_application/{userType}/{userId}", [AdminController::class, 'viewUserApplicationData']);

Route::post("/admin/application_details", [AdminController::class, 'loadApplicationDetails']);

Route::post("/admin/submit_review", [AdminController::class, 'submitReview']);

Route::post("/admin/update_application", [AdminController::class, 'updateApplication']);

Route::post("/admin/load_review_log", [AdminController::class, 'loadReviewLog']);

Route::post("/admin/show_manager", [AdminController::class, 'showManagerName']);

Route::post("/admin/assign_manager", [AdminController::class, 'assignManager']);

Route::get('/admin/add_funeral_home',[AdminController::class,'addFuneralHome'])->name('add_funeral_home');

Route::post("/admin/show_role_based_user", [AdminController::class, 'showUserBasedOnRole']);

Route::post("/admin/add_funeral_home", [AdminController::class, 'insertFuneralHome']);

Route::get('/admin/list_funeral_home',[AdminController::class,'listFuneralHome'])->name('list_funeral_home');

Route::get('/admin/load_fh_list',[AdminController::class,'listFHDetails']);

Route::post("/admin/show_fh_search_result", [AdminController::class, 'showFHSearchResult']);

Route::post("/admin/remove_fh", [AdminController::class, 'removeFH']);


Route::post("/admin/create_crm_user", [AdminController::class, 'createCRMUser']);


//Docusign
Route::get('docusign',[DocusignController::class, 'index'])->name('docusign');
Route::get('connect-docusign',[DocusignController::class, 'connectDocusign'])->name('connect.docusign');
Route::get('docusign/callback',[DocusignController::class,'callback'])->name('docusign.callback');
Route::get('sign-document',[DocusignController::class,'signDocument'])->name('docusign.sign');

