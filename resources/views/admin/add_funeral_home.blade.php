@include('include.admin_header')	

<div class="content-body pb-5 pt-5">
		
		<div class="container w-75">
				
				<!-- register form stepper -->
					<form id="funeralHomeForm" class="funeralHomeForm" method="POST" action="{{route('add_funeral_home')}}" enctype="multipart/form-data" onsubmit="return adminFuneralHome()" >
  @csrf
					<div class="row">
					  <div class="col-md-12">
						<div class="card card-default">
						  <div class="card-header">
							<h3 class="pl-4 mt-3 font-weight700 color-blue">Add Funeral Home</h3>
						  </div>
						  <div class="card-body p-5">
							
							  
								<div id="register-part-1" class="content">
								  
								  
										
										  
										  
										  <div class="row">
										  	<div class="col-md-12 col-lg-4">
												<label>User Type <span class="requiredSymbol">*</span></label>
												<select class="form-control" id="user_type" name="user_type">
													<option value="" >Select User Type</option>
													<option value="1" >Partner</option>
													<option value="2" >Agency</option>
													<option value="3" >Agent</option>
												</select>
										
											</div>

											<div class="col-md-12 col-lg-4">
												<label>User Name <span class="requiredSymbol">*</span></label>
												<select class="form-control" id="user_id" name="user_id">
													<option value="" >Select User</option>
													
												</select>
										
											</div>
										<div class="col-md-12 col-lg-4">
													 <div class="form-group">
														<label>Name of the Funeral Home <span class="requiredSymbol">*</span></label>
														<input type="text" class="form-control"  value="" id="funeral_home_name" name="funeral_home_name">
													  </div>
												</div>
											
										
											<div class="col-md-12 col-lg-4">
													 <div class="form-group">
														<label>Physical Address 1 <span class="requiredSymbol">*</span></label>
														<input type="text" class="form-control"  value="" id="address1" name="address1">
													  </div>
												</div>
												
												<div class="col-md-6 col-lg-4">
													 <div class="form-group">
														<label>Physical Address 2 </label>
														<input type="text" class="form-control"  value="" id="address2" name="address2">
													  </div>
												</div>
												<div class="col-md-6 col-lg-4">
													 <div class="form-group">
														<label>Physical Address 3</label>
														<input type="text" class="form-control"  value="" id="address3" name="address3">
													  </div>
												</div>
										  </div>
										  
										  <div class="row">
											<div class="col-md-4 col-lg-4">
													<div class="form-group">
												  <label>Country <span class="requiredSymbol">*</span></label>
												   <select class="form-control" id="countryList" name="country">
													<option value="">Select Country</option>

													@foreach($countryDetails as $data)
													<option value="{{$data->id}}">{{$data->name}}</option>
													@endforeach
												  </select>
												</div>
												</div>
												
												<div class="col-md-4 col-lg-4">
													 <div class="form-group">
												  <label>State <span class="requiredSymbol">*</span></label>
												   <select class="form-control" id="stateList" name="state">
												  	<option value="">Select State</option>
												  </select>
												</div>
												</div>
												<div class="col-md-4 col-lg-4">
													 <div class="form-group">
												  <label>City <span class="requiredSymbol">*</span></label>
												  <select class="form-control" id="cityList" name="city">
													<option value="">Select City</option>
												  </select>
												</div>
												</div>
												<div class="col-md-4 col-lg-4">
													 <div class="form-group">
														<label>Postal Code <span class="requiredSymbol">*</span></label>
														<input type="text" class="form-control numberOnly" maxlength="6"  value="" id="postal_code" name="postal_code">
													  </div>
												</div>
										  </div>
										  
									
								  
								  
								  
								  
								  
								  
								</div>
								
						
								
								
							 <button type="submit" class="mt-4 btn btn-lg btn-primary ">ADD DETAILS</button>
							<span  class="error finalErr"></span>
						  </div>
						  <!-- /.card-body -->
						  
						</div>
						<!-- /.card -->
					  </div>
					</div>
				</form>
				<!-- register form stepper -->
				
			</div>
		
	</div>
	
	@include('include.admin_footer')
	
<script type="text/javascript">
    $(document).ready(function() {
    		
    		$('#user_type').on('change', function() {
				       var userType = $(this).val();
				      
				        $.ajax({
				            type: "POST",
				            url: "/admin/show_role_based_user",
				            dataType: 'json', 
			  				encode  : true,
			  				 data: {
			  				 	'userType':userType
			  				 },
			  				 beforeSend: function() {
			                   $.blockUI({ css: { 
						            border: 'none', 
						            padding: '15px', 
						            backgroundColor: '#000', 
						            '-webkit-border-radius': '10px', 
						            '-moz-border-radius': '10px', 
						            opacity: .5, 
						            color: '#fff' 
						        } }); 
			                },
				            success: function (data) {
				            	console.log(data);
				             	var html = '<option value="">Select User</option>';
				             	var firstName = '';
				             	var middleName = '';
				             	var lastName = '';

				             	for(res in data){
				             		if(userType == '3'){
													firstName = data[res]["first_name"];
													middleName = data[res]["middle_name"];
													lastName = data[res]["last_name"];
													if(firstName == null){
														firstName = '';
													}
													if(middleName == null){
														middleName = '';
													}
													if(lastName == null){
														lastName = '';
													}
				             			html += '<option value="'+data[res]["id"]+'">'+firstName+' '+middleName+' '+lastName+'</option>';
				             		}
				             		else{
				             			if(data[res]["business_name"] != null){
				             				html += '<option value="'+data[res]["id"]+'">'+data[res]["business_name"]+'</option>';
				             			}
				             		}
				             		
				             		
				             	}
				             	$('#user_id').html(html);
				             	$.unblockUI();
				            },
				            error: function (data) {
				               
				            }
				        });
				});
     	
    });

function adminFuneralHome(){

			$('.form-control').removeClass('is-invalid');
    	var userType = $('#user_type').val();
    	var userId = $('#user_id').val();
    	var funeralHomeName = $('#funeral_home_name').val();
    	var address1 = $('#address1').val();
    	var countryList = $('#countryList').val();
    	var stateList = $('#stateList').val();
    	var cityList = $('#cityList').val();
    	var postal_code = $('#postal_code').val();

    	if(userType == '' || userId == '' || funeralHomeName == '' || address1 == '' || countryList == '' || stateList == '' || cityList == ''  || postal_code == ''){
    		if(userType == ''){
    		 $('#user_type').addClass('is-invalid');
	       
    	}

    	if(userId == ''){
    		 $('#user_id').addClass('is-invalid');
	       
    	}

    	if(funeralHomeName == ''){
    		 $('#funeral_home_name').addClass('is-invalid');
	      
    	}

    	if(address1 == ''){
    		 $('#address1').addClass('is-invalid');
	       
    	}

    	if(countryList == ''){
    		 $('#countryList').addClass('is-invalid');
	       
    	}

    	if(stateList == ''){
    		 $('#stateList').addClass('is-invalid');
	       
    	}

    	if(cityList == ''){
    		 $('#cityList').addClass('is-invalid');
	       
    	}

    	if(postal_code == ''){
    		 $('#postal_code').addClass('is-invalid');
	       
    	}
    		return false;

    	}
    	
}
</script>
