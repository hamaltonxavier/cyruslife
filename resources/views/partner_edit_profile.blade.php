@include('include.header')	

@foreach($userDetails as $data)

	<div class="content-body pb-5 pt-5">
		
		<div class="container w-75">
				
				<!-- register form stepper -->
					<form id="partnerRegistrationForm" class="registrationForm" method="POST" action="{{ route('update_profile') }}" enctype="multipart/form-data" onsubmit="return partnerFormValidation()">
				
  @csrf
					<div class="row">
					  <div class="col-md-12">
						<div class="card card-default">
						  <div class="card-header">
							<h3 class="pl-4 mt-3 font-weight700 color-orange">Edit  Profile</h3>
							
						  </div>
						  <div class="card-body p-5">
							
							
								<div id="register-part-1" class="content">
								  
								  <h3 class="color-blue">Business Name and Address</h3>
								  <hr class="pb-2">
								  
								  <div class="row">
									<div class="col-md-12 col-lg-12">
										<div class="form-group">
											<label>Name of the Business <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control" placeholder="Name of the Business" value="{{$data->business_name}}" name="business_name" id="business_name" >
											<input type="hidden" id="formType" value="edit"  />
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										 <div class="form-group">
											<label>Physical Address 1 <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control"  placeholder="Address 1" value="{{$data->address1}}" name="address1" id="address1">
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										 <div class="form-group">
											<label>Physical Address 2 <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control"  placeholder="Address 2" value="{{$data->address2}}" name="address2" id="address2">
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										 <div class="form-group">
											<label>Physical Address 3</label>
											<input type="text" class="form-control"  placeholder="Address 3" value="{{$data->address3}}" name="address3" id="address3" >
										  </div>
									</div>
									
									
										<div class="col-md-6 col-lg-4">
										<div class="form-group">
												  <label>Country <span class="requiredSymbol">*</span></label>
												  <select class="form-control" id="countryList" name="country">
												

													@foreach($countryDetails as $dataVal)
													<option value="{{$dataVal->id}}" @if($data->countryId == $dataVal->id) selected @endif  >{{$dataVal->name}}</option>
													@endforeach
												  </select>
												</div>
									</div>
									
									
									<div class="col-md-6 col-lg-4">
										<div class="form-group">
												  <label>State <span class="requiredSymbol">*</span></label>
												  <select class="form-control" id="stateList" name="state">
												  	@foreach($statesDetails as $dataVal)
													<option value="{{$dataVal->id}}" @if($data->stateId == $dataVal->id) selected @endif  >{{$dataVal->name}}</option>
													@endforeach
												  </select>
												</div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										<div class="form-group">
												  <label>City <span class="requiredSymbol">*</span></label>
												  <select class="form-control" id="cityList" name="city">
													@foreach($cityDetails as $dataVal)
													<option value="{{$dataVal->id}}" @if($data->cityId == $dataVal->id) selected @endif  >{{$dataVal->name}}</option>
													@endforeach
												  </select>
												</div>
									</div>
									
									<div class="col-md-6  col-lg-6">
										<div class="form-group">
													<label>Postal Code <span class="requiredSymbol">*</span></label>
													<input type="text" maxlength="6" class="form-control numberOnly"  placeholder="Postal Code" value="{{$data->postal_code}}"  name="postal_code" id="postal_code">
												  </div>
									</div>
										
										
										  
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Main Business Telephone <span class="requiredSymbol">*</span></label>
											<input type="tel" class="form-control numberOnly"  placeholder="Mobile / Telephone Number" value="{{$data->telephone}}" name="telephone" id="telephone" >
										  </div>
									</div>
									
									<div class="col-lg-6">
										<div class="form-group">
											<label>Business License <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control" placeholder="Business License" name="license_type" id="license_type" value="{{$data->license_type}}" >
											<!--<select class="form-control" id="license_type" name="license_type">
											<option value="" >Select License Type</option>
											<option value="Driving Licence" @if($data->license_type == "Driving Licence") selected @endif >Driving Licence</option>
											<option value="Passport" @if($data->license_type == "Passport") selected @endif >Passport</option>
											<option value="Social Security Card" @if($data->license_type == "Social Security Card") selected @endif >Social Security Card</option>
										</select>-->
										  </div>
									</div>	

									</div>
								  

								</div>
								
								<!-- registration part 2-->
								<div id="register-part-2" class="content mt-5">
								  <h3 class="color-blue">Primary Contact Details</h3>
								  <hr class="pb-2">
								 
								  <div class="row">
									<div class="col-md-3 col-lg-2">
										<label>Salutation/Title <span class="requiredSymbol">*</span></label>
										<select class="form-control" id="title" name="title">

											<option value="Mr" @if($data->title == "Mr") selected @endif>Mr</option>
											<option value="Mrs" @if($data->title == "Mrs") selected @endif>Mrs</option>
										</select>
									</div>
									
									<div class="col-md-9 col-lg-4">
										<div class="form-group">
											<label>First Name <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control alphabetsOnly" placeholder="First Name" value="{{$data->first_name}}" name="first_name" id="first_name">
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-3">
										<div class="form-group">
											<label>Middle Name</label>
											<input type="text" class="form-control alphabetsOnly" placeholder="Middle Name" value="{{$data->middle_name}}" name="middle_name" id="middle_name">
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-3">
										<div class="form-group">
											<label>Last Name <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control alphabetsOnly" placeholder="Last Name" value="{{$data->last_name}}" name="last_name" id="last_name">
										  </div>
									</div>
									
								  </div>
								  
								  <div class="row">
									<div class="col-md-6 col-lg-6">
										<label>Mobile Phone <span class="requiredSymbol">*</span></label>
										<input type="tel" class="form-control numberOnly" placeholder="Mobile Phone" value="{{$data->mobile}}" name="mobile" id="mobile">
									</div>
									
									<div class="col-md-6  col-lg-6">
										<div class="form-group">
											<label>Email <span class="requiredSymbol">*</span></label><br/>
											<input type="text" class="form-control" placeholder="Email Address" id="email" value="{{$data->email}}" disabled >
											<!--<label>{{$data->email}}</lable>-->
										  </div>
									</div>
									<div class="col-md-6  col-lg-6">
										<div class="form-group">
											<label>Designation</label>
											<input type="text" class="form-control" placeholder="Designation" value="{{$data->designation}}">
										  </div>
									</div>

									<div class="col-md-6  col-lg-6">
										<div class="form-group">
											<label>Is your office located at the business address given on prior screen?</label>
											<!--<input type="text" class="form-control" placeholder="" value="{{$data->office_location}}">-->
											<select class="form-control partnerEditOffice" id="office_location" name="office_location">

											<option value="Yes" @if($data->office_location == "Yes") selected @endif>Yes</option>
											<option value="No" @if($data->office_location == "No") selected @endif>No</option>
										</select>
										  </div>
									</div>

<div class="officeLocationDiv">
									<div class="col-md-6  col-lg-4 offAddressDetails " >
										<div class="form-group">
											<label>Office Address 1 <span class="requiredSymbol">*</span></label>
											<input type="text" id="office_address1" name="office_address1" class="form-control" placeholder="" value="{{$data->office_address1}}">
										  </div>
									</div>

									<div class="col-md-6  col-lg-4 offAddressDetails">
										<div class="form-group">
											<label>Office Address 2 </label>
											<input type="text" id="office_address2" name="office_address2" class="form-control" placeholder="" value="{{$data->office_address2}}">
										  </div>
									</div>

									<div class="col-md-6  col-lg-4 offAddressDetails">
										<div class="form-group">
											<label>Office Address 3</label>
											<input type="text" id="office_address3" name="office_address3" class="form-control" placeholder="" value="{{$data->office_address3}}">
										  </div>
									</div>

									<div class="col-md-6 col-lg-4 offAddressDetails">
										<div class="form-group">
												  <label>Office Country <span class="requiredSymbol">*</span></label>
												  <select class="form-control" id="officeCountryList" name="office_country">
												

													@foreach($countryDetails as $dataVal)
													<option value="{{$dataVal->id}}" @if($data->countryId == $dataVal->id) selected @endif  >{{$dataVal->name}}</option>
													@endforeach
												  </select>
												</div>
									</div>
									
									
									<div class="col-md-6 col-lg-4 offAddressDetails">
										<div class="form-group">
												  <label>Office State <span class="requiredSymbol">*</span></label>
												  <select class="form-control"  id="officeStateList" name="office_state">
												  	@foreach($osDetails as $dataVal)
													<option value="{{$dataVal->id}}" @if($data->office_state == $dataVal->id) selected @endif  >{{$dataVal->name}}</option>
													@endforeach
												  </select>
												</div>
									</div>
									
									<div class="col-md-6 col-lg-4 offAddressDetails">
										<div class="form-group">
												  <label>Office City <span class="requiredSymbol">*</span></label>
												  <select class="form-control" id="officeCityList" name="office_city">
													@foreach($ociDetails as $dataVal)
													<option value="{{$dataVal->id}}" @if($data->office_city == $dataVal->id) selected @endif  >{{$dataVal->name}}</option>
													@endforeach
												  </select>
												</div>
									</div>

									<div class="col-md-6  col-lg-4 offAddressDetails">
										<div class="form-group">
											<label>Office Postal Code <span class="requiredSymbol">*</span></label>
											<input type="text" name="office_postal_code" id="office_postal_code" class="form-control" placeholder="" value="{{$data->office_postal_code}}">
										  </div>
									</div>
								</div>
									<div class="col-lg-12">
										
										
										
										<div class="form-group">
											<div class="row">
												<div class="col-md-6 col-lg-6">
													<label>Is your business also registered as an insurance agency? </label>
												</div>
												<div class="col-md-1 col-lg-1">
													<div class="form-check">
													  <input class="form-check-input" name="agent" type="radio" checked="" value="yes" {{ ($data->agent=="yes")? "checked" : "" }}>
													  <label class="form-check-label">Yes</label>
													</div>
												</div>
												<div class="col-md-1 col-lg-1">
													<div class="form-check">
													  <input class="form-check-input" name="agent" type="radio" value="no" {{ ($data->agent=="no")? "checked" : "" }}>
													  <label class="form-check-label">No</label>
													</div>
												</div>
											</div>
											
										</div>
									</div>

									
								  </div>
								  
								 
								  
								
								</div>
								
						<div class="agencyLicenceDiv" style="display:none">		
								<!-- register form step 3-->
								<div id="register-part-3" class="content mt-5">
								  <h3 class="color-blue">License / ID</h3>
								  <hr class="pb-2">
								   <div class="row">
									
									
									<div class="col-lg-6">
										<div class="form-group">
											<label>Insurance License <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control" placeholder="License Number" name="license_number" id="license_number" value="{{$data->license_number}}" >
										  </div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>License Status <span class="requiredSymbol">*</span></label>
											<select class="form-control" id="license_status" name="license_status">
											<option value="active" @if($data->license_status == "active") selected @endif>Active</option>
											<option value="expired" @if($data->license_status == "expired") selected @endif>Expired</option>
										</select>
										  </div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>Expiration Date <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control" placeholder="MM/YY" name="license_expiry_date" id="license_expiry_date" value="{{$data->license_expiry_date}}" >
										  </div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>Designated Home State <span class="requiredSymbol">*</span></label>
										<!--	<input type="hidden" class="form-control" placeholder="Designated Home State"  value="{{$data->license_home_state}}" >-->
											<select class="form-control" id="license_home_state" name="license_home_state">
												  	@foreach($statesDetails as $dataVal)
													<option value="{{$dataVal->id}}" @if($data->license_home_state == $dataVal->id) selected @endif  >{{$dataVal->name}}</option>
													@endforeach
												  </select>
										  </div>
									</div>
									</div>
<div class="form-group row mb-2">
									<label class="col-11 col-sm-11 col-md-11 col-lg-4 col-form-label">License type 
									</label>
									
									<div class="col-sm-7 col-lg-7 pl-0">
										<div class="row">
										  <div class="col-lg-11">
											<div class="form-group d-flex align-items-start mb-2">
												<input type="radio" class="form-check-input ml-0" id="license_type_opt" name="license_type_opt" value="Limited" {{ ($data->license_type_opt=="Limited")? "checked" : "" }}>>
												<label class="form-check-label pl-4">Limited (dollar amount and/or insurance type restriction) </label>
											</div>
										  </div>
										</div>
										
									
										<div class="row ">
										  <div class="col-lg-11">
											<div class="form-group d-flex align-items-start mb-2">
												<input type="radio" class="form-check-input ml-0" id="license_type_opt" name="license_type_opt" value="Full" {{ ($data->license_type_opt=="Full")? "checked" : "" }}>
												<label class="form-check-label pl-4">Full (any life product, any dollar amount)</label>
											</div>
										  </div>
										</div>
										
										
									</div>
								</div> <!-- row end -->
									<div class="row">
									
									
									</div>

									<div class="row">
									
									

									<div class="col-lg-6">
										<div class="form-group">
											<label for="exampleInputFile">Required Document Uploads</label>
											<div class="input-group">
											  <input type="file" accept=".gif,.jpg,.jpeg,.png,.doc,.docx,.pdf" name="idFiles[]" class="myfrm form-control">
											 
											</div>
											
										  </div>
									</div>

									<div class="col-lg-6">
										@foreach($docDetails as $docData)
@if($docData->document_type == "idproof")
									<div class="col-lg-6">
										<div class="form-group">
											<label for="exampleInputFile"></label>
											<div class="input-group">
											  
											  	
											  	
											  	<a target="_blank" alt="View Document" title="View Document" href="{{asset('storage/app/'.$docData->doucment_path)}}" class="btn"><i class="fa fa-eye"></i> </a>
											  	<a alt="Delete Document" title="Delete Document" href="#" onclick="removeDocument('{{$docData->document_id}}')" class="btn"><i class="fa fa-trash"></i> </a>

											  
											 
											</div>
											
										  </div>
									</div>
										@endif
											 
							 @endforeach
									</div>
									
									</div>


								
								</div>
						</div>		
								
								<!-- register form step 4-->
								<div id="register-part-4" class="content mt-5">
								  <h3 class="color-blue">Business Overview</h3>
								  <hr class="pb-2">
								  <div class="row">								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Month/Year established <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control" placeholder="ID Proof / State issued Licenses" value="{{$data->established_details}}" id="established_details" name="established_details">
										  </div>
									</div>
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Number of Funeral Directors/Embalmers on staff <span class="requiredSymbol">*</span></label>	
											<input type="text" class="form-control numberOnly" placeholder="Number of Funeral Directors/Embalmers on staff" value="{{$data->funeral_number}}" id="funeral_number" name="funeral_number">
										</div>
									</div>
								  </div>
								  
								  <div class="row">								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Number of casketed funerals annually <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control numberOnly" placeholder="Number of casketed funerals annually" value="{{$data->casked_funeral}}" id="casked_funeral" name="casked_funeral">
										  </div>
									</div>
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Number of cremations annually <span class="requiredSymbol">*</span></label>	
											<input type="text" class="form-control numberOnly" placeholder="Number of cremations annually" value="{{$data->cremations_annually}}" id="cremations_annually" name="cremations_annually">					
										</div>
									</div>
								  </div>
								  
								  <div class="row">								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Annual preneed volume <span class="requiredSymbol">*</span></label>
											<div class="input-group">
												<div class="input-group-prepend">
												<span class="input-group-text"><b>$</b></span>
												</div>
												<!--<input type="text" class="form-control numberOnly" placeholder="Email Address" value="{{$data->annual_preneed_volume}}" id="annual_preneed_volume" name="annual_preneed_volume">-->
												
												<select class="form-control" id="annual_preneed_volume" name="annual_preneed_volume">
														<option value="100 - 1,000" @if($data->annual_preneed_volume == "100 - 1,000") selected @endif>100 - 1,000</option>
														<option value="1,000 - 10,000" @if($data->annual_preneed_volume == "1,000 - 10,000") selected @endif>1,000 - 10,000</option>
														<option value="10,000 - 100,000" @if($data->annual_preneed_volume == "10,000 - 100,000") selected @endif>10,000 - 100,000</option>
														<option value="100,000 - 1,000,000" @if($data->annual_preneed_volume == "100,000 - 1,000,000") selected @endif>100,000 - 1,000,000</option>
														<option value="1,000,000 - 10,000,000" @if($data->annual_preneed_volume == "1,000,000 - 10,000,000") selected @endif>1,000,000 - 10,000,000</option>
													</select>

											</div>
											
										  </div>
									</div>
									
									<!--<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label for="exampleInputFile">Required Document Uploads</label>
											<div class="input-group">
											  <div class="custom-file">
												<input type="file" class="custom-file-input" id="exampleInputFile">
												<label class="custom-file-label" for="exampleInputFile">Choose file</label>
											  </div>
											  <div class="input-group-append">
												<span class="input-group-text">Upload</span>
											  </div>
											</div>
											
										  </div>
									</div>-->
								  </div>

								  @foreach($docDetails as $docData)
								   <div class="row">								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											
											
											  	@if($docData->document_type == "business")
											  <label	>{{$docData->document_name}}</label>
											  	@endif
											 
										  </div>
									</div>
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label></label>
											
											  	@if($docData->document_type == "business")
											  	
											  		
											  	<a target="_blank" alt="View Document" title="View Document" href="{{asset('storage/app/'.$docData->doucment_path)}}" class="btn"><i class="fa fa-eye"></i> </a>
											  	<!--<a alt="Delete Document" title="Delete Document"  href="#" onclick="removeDocument('{{$docData->document_id}}')" class="btn"><i class="fa fa-trash"></i> </a>-->

											  		
											  	@endif
											  
										  </div>
									</div>

								</div>

								 @endforeach
<!--First-->
									<div class="row increment">
								  		<div class="col-md-6 col-lg-6">
												<div class="form-group">
													<div class="input-group realprocode control-group lst" >
								  				<select class="form-control" id="" name="filetitle[]">
														<option value="Producer-Agreement">Producer Agreement</option>
														<option value="Direct-Deposit-Authorization">Direct Deposit Authorization</option>
														<option value="Producer-Compensation-Schedule">Producer Compensation Schedule</option>
														<option value="Explanation-of-Information-found-on-MIB">Explanation of Information found on MIB</option>
														<option value="Other-Supporting-Documentation">Other Supporting Documentation</option>
													</select>
											      
											    </div>
												</div>
											</div>

											<div class="col-md-6 col-lg-6">
												<div class="form-group">
													<div class="input-group realprocode control-group lst " >
								  				
											      <input type="file" accept=".gif,.jpg,.jpeg,.png,.doc,.docx,.pdf" name="filenames[]" class="myfrm form-control">
											      <div class="input-group-btn"> 
											        <button class="btn btn-success addFile" type="button"> <i class="fldemo glyphicon glyphicon-plus"></i>Add</button>
											      </div>
											    </div>
												</div>
											</div>
								  </div>

								<!--Second-->
							<div class="clone d-none " >
								<div class="row ">
									<div class="realprocode col-md-12 col-lg-12">
								  		<div class="col-md-6 col-lg-6" style="float: left;">
												<div class="form-group">
													<div class=" control-group lst input-group" style="margin-top:10px">
											      <select class="form-control" id="" name="filetitle[]">
														<option value="Driver-License">State-Issued Driver's License</option>
														<option value="Producer-Agreement">Producer Agreement</option>
														<option value="Direct-Deposit-Authorization">Direct Deposit Authorization</option>
														<option value="Producer-Compensation-Schedule">Producer Compensation Schedule</option>
														<option value="Explanation-of-Information-found-on-MIB">Explanation of Information found on MIB</option>
														<option value="Other-Supporting-Documentation">Other Supporting Documentation</option>
													</select>
											        
											      </div>
												</div>
											</div>

											<div class="col-md-6 col-lg-6" style="float: left;">
												<div class="form-group">
													<div class=" control-group lst input-group" style="margin-top:10px">
											      	
											        <input type="file" accept=".gif,.jpg,.jpeg,.png,.doc,.docx,.pdf" name="filenames[]" class="myfrm form-control">
											        <div class="input-group-btn"> 
											          <button class="btn btn-danger" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>
											        </div>
											      </div>
												</div>
											</div>
										</div>
								  </div>

								</div>
								 
								  
								</div>
								
								
							<button type="submit" class="mt-4 btn btn-lg btn-primary ">UPDATE DETAILS</button>
							 <span style="display:none" class="error finalErr">Please fill the required fields *</span>
								 							
						  </div>
						  <!-- /.card-body -->
						  
						</div>
						<!-- /.card -->
					  </div>
					</div>
				</form>
				<!-- register form stepper -->
				
			</div>
		
	</div>
	@endforeach
	@include('include.footer')

<script type="text/javascript">
    $(document).ready(function() {
		$('.officeLocationDiv').css('display','block');
    	if($('.partnerEditOffice').val()== "Yes"){
    		$('.officeLocationDiv').css('display','none');
    	}

		$('.agencyLicenceDiv').css('display','none');
		var agencyRadVal = $('input[name="agent"]:checked').val();
		if(agencyRadVal == 'yes'){
		$('.agencyLicenceDiv').css('display','block');
		}

		$("input[name='agent']").click(function(){
	      	$('.agencyLicenceDiv').css('display','none');
	      	console.log($(this).val());
	      	$('#license_number').val('');
	      	$('#license_status').val('');
	      	$('#license_expiry_date').val('');
	      	$('#license_home_state').val('');
	      	if($(this).val() == 'yes'){
	      		$('.agencyLicenceDiv').css('display','block');
	      	}
	      });
		

	      $(".addFile").click(function(){ 
	          var lsthmtl = $(".clone").html();
	          $(".increment").after(lsthmtl);
	      });
	      $("body").on("click",".btn-danger",function(){ 
	          $(this).parents(".realprocode").remove();
	      });
    });
</script>