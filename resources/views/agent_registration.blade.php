@include('include.header')
	
	<div class="content-body pb-5 pt-5">
		
		<div class="container w-75">
				
				<!-- register form stepper -->
					<form id="agencyRegistrationForm" class="registrationForm" method="POST" action="{{ route('registration') }}" enctype="multipart/form-data" onsubmit="return agentFormValidation()">
  @csrf
					<div class="row">
					  <div class="col-md-12">
						<div class="card card-default">
						  <div class="card-header">
							<h3 class="text-center mt-3 font-weight700 color-blue">Register - Agent Account</h3>
							<ul>
     @foreach ($errors->all() as $error)
         <li class="error">{{ $error }}</li>
     @endforeach
</ul>
							
						  </div>
						  <div class="card-body p-0">
							<div class="bs-stepper linear">
							  <div class="bs-stepper-header" role="tablist">
								<!-- your steps here -->
								
								<div class="step active" data-target="#register-part-1">
								  <button type="button" class="step-trigger" role="tab" aria-controls="register-part-1" id="register-part-1-trigger" aria-selected="false" disabled="disabled">
									<span class="bs-stepper-circle">1</span>
									<span class="bs-stepper-label">Name and Address</span>
								  </button>
								</div>
								
								<div class="line"></div>
								
								<div class="step" data-target="#register-part-2">
								  <button type="button" class="step-trigger" role="tab" aria-controls="register-part-2" id="register-part-2-trigger" aria-selected="true">
									<span class="bs-stepper-circle">2</span>
									<span class="bs-stepper-label">License / ID</span>
								  </button>
								</div>
								
								<div class="line"></div>
								
								<div class="step" data-target="#register-part-3">
								  <button type="button" class="step-trigger" role="tab" aria-controls="register-part-3" id="register-part-3-trigger" aria-selected="true">
									<span class="bs-stepper-circle">3</span>
									<span class="bs-stepper-label">Business Overview</span>
								  </button>
								</div>
								
								
							  </div>
							  
							  
							  
							  <div class="bs-stepper-content">
								<!-- your steps content here -->
								
								<div id="register-part-1" class="content active dstepper-block" role="tabpanel" aria-labelledby="register-part-1-trigger">
								  
								  
										<div class="row">
											<div class="col-md-3 col-lg-2">
												<label>Salutation/Title</label>
												<select class="form-control" id="title" name="title">
													<option value="Mr">Mr</option>
													<option value="Mrs">Mrs</option>
												</select>
												<input type="hidden" name="role" value="3">
												<input type="hidden" id="formType" value="insert" />
											</div>
											
											<div class="col-md-9 col-lg-4">
												<div class="form-group">
													<label>First Name <span class="requiredSymbol">*</span></label>
													<input type="text" class="form-control alphabetsOnly" placeholder="First Name" id="first_name" name="first_name" value={{ old('first_name') }}>
												  </div>
											</div>
											
											<div class="col-md-6 col-lg-3">
												<div class="form-group">
													<label>Middle Name </label>
													<input type="text" class="form-control alphabetsOnly" placeholder="Middle Name" id="middle_name" name="middle_name" value={{ old('middle_name') }}>
												  </div>
											</div>
											
											<div class="col-md-6 col-lg-3">
												<div class="form-group">
													<label>Last Name <span class="requiredSymbol">*</span></label>
													<input type="text" class="form-control alphabetsOnly" placeholder="Last Name" id="last_name" name="last_name" value={{ old('last_name') }}>
												  </div>
											</div>
											
										  </div>
										  
										  
										  <div class="row">
											<div class="col-md-6 col-lg-6">
												<label>Mobile Phone <span class="requiredSymbol">*</span></label></label>
												<input type="tel" class="form-control numberOnly" placeholder="Mobile Phone" id="mobile" name="mobile" value={{ old('mobile') }}>
											</div>
											
											<div class="col-md-6  col-lg-6">
												<div class="form-group">
													<label>Email <span class="requiredSymbol">*</span></label>
													<input type="text" class="form-control" placeholder="Email Address" id="email" name="email" value=>
													<input type="hidden" class="uniqueEmailError" value="0" />
											<span class="emailErrorTxt error" ></span>
												  </div>
											</div>
											<div class="col-md-6  col-lg-4">
										<div class="form-group">
											<label>Designation </label>
											<input type="text" class="form-control" placeholder="Designation" id="designation" name="designation" value={{ old('designation') }}>
										  </div>
									</div>
									
									<!--<div class="col-md-6  col-lg-4">
										<div class="form-group">
											<label>Department </label>
											<input type="text" class="form-control" placeholder="Department" id="department" name="department" value={{ old('department') }}>
										  </div>
									</div>-->
									<div class="col-md-6  col-lg-4">
										<div class="form-group">
											<label>Office Location </label>
											<input type="text" class="form-control" placeholder="Office Location" id="office_location" name="office_location" value={{ old('office_location') }}>
										  </div>
									</div>
										  </div>
										  
										  
										  
										  <div class="row">
											<div class="col-md-12 col-lg-4">
													 <div class="form-group">
														<label>Physical Address 1 <span class="requiredSymbol">*</span></label>
														<input type="text" class="form-control"  placeholder="Address 1" id="address1" name="address1" value={{ old('address1') }}>
													  </div>
												</div>
												
												<div class="col-md-6 col-lg-4">
													 <div class="form-group">
														<label>Physical Address 2 <span class="requiredSymbol">*</span></label>
														<input type="text" class="form-control"  placeholder="Address 2" id="address2" name="address2" value={{ old('address2') }}>
													  </div>
												</div>
												<div class="col-md-6 col-lg-4">
													 <div class="form-group">
														<label>Physical Address 3</label>
														<input type="text" class="form-control"  placeholder="Address 3" id="address3" name="address3" value={{ old('address3') }}>
													  </div>
												</div>
										  </div>
										  
										  <div class="row">
											<div class="col-md-4 col-lg-4">
													 <div class="form-group">
														<label>Country <span class="requiredSymbol">*</span></label>
														   <select class="form-control" id="countryList" name="country">
															<option value="">Select Country</option>

															@foreach($countryDetails as $data)
															<option value="{{$data->id}}">{{$data->name}}</option>
															@endforeach
														  </select>
													  </div>
												</div>
												
												<div class="col-md-4 col-lg-4">
													 <div class="form-group">
														<label>State <span class="requiredSymbol">*</span></label>
												   <select class="form-control" id="stateList" name="state">
												  	<option value="">Select State</option>
												  </select>
													  </div>
												</div>

												<div class="col-md-4 col-lg-4">
													<div class="form-group">
															  <label>City <span class="requiredSymbol">*</span></label>
															  <select class="form-control" id="cityList" name="city">
																<option value="">Select City</option>
															  </select>
															</div>
												</div>

												<div class="col-md-4 col-lg-4">
													 <div class="form-group">
														<label>Postal Code<span class="requiredSymbol">*</span></label></label>
														<input type="text" class="form-control numberOnly" maxlength="6"  placeholder="Postal Code" id="postal_code" name="postal_code" value={{ old('postal_code') }}>
													  </div>
												</div>
										  </div>
										  
									
								  
								  
								  
								  
								  
								  <div class="btn btn-primary" onclick="stepper.next()">Next</div>

								</div>
								
						
								<!-- register form step 2-->
								<div id="register-part-2" class="content" role="tabpanel" aria-labelledby="register-part-2-trigger">
								  
								 <div class="row">
									
									
									<div class="col-lg-6">
										<div class="form-group">
											<label>Insurance License <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control numberOnly" placeholder="License Number" name="license_number" id="license_number" value={{ old('license_number') }}>
										  </div>
									</div>
									</div>

<div class="form-group row mb-2">
									<label class="col-11 col-sm-11 col-md-11 col-lg-4 col-form-label">License type 
									</label>
									
									<div class="col-sm-7 col-lg-7 pl-0">
										<div class="row">
										  <div class="col-lg-11">
											<div class="form-group d-flex align-items-start mb-2">
												<input type="radio" class="form-check-input ml-0" id="license_type_opt" name="license_type_opt" value="Limited">
												<label class="form-check-label pl-4">Limited (dollar amount and/or insurance type restriction) </label>
											</div>
										  </div>
										</div>
										
									
										<div class="row ">
										  <div class="col-lg-11">
											<div class="form-group d-flex align-items-start mb-2">
												<input type="radio" class="form-check-input ml-0" id="license_type_opt" name="license_type_opt" value="Full">
												<label class="form-check-label pl-4">Full (any life product, any dollar amount)</label>
											</div>
										  </div>
										</div>
										
										
									</div>
								</div> <!-- row end -->
									<div class="row">
									
									<div class="col-lg-6">
										<div class="form-group">
											<label>License Status <span class="requiredSymbol">*</span></label>
											<select class="form-control" id="license_status" name="license_status">
											<option value="active">Active</option>
											<option value="expired">Expired</option>
										</select>
										  </div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>Expiration Date <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control" placeholder="MM/YY" name="license_expiry_date" id="license_expiry_date" value={{ old('license_expiry_date') }}>
										  </div>
									</div>
									</div>

									<div class="row">
									
									<div class="col-lg-6">
										<div class="form-group">
											<label>Designated Home State <span class="requiredSymbol">*</span></label>
											<!--<input type="text" class="form-control" placeholder="Designated Home State" name="license_home_state" id="license_home_state" value={{ old('license_home_state') }}>-->
											<select class="form-control" id="license_home_state" name="license_home_state">
												  	<option value="">Select State</option>
												  </select>
										  </div>
									</div>
									
									</div>
								  <div class="row">
									
									
									
									
									
								  </div>

								  <div class="btn btn-primary" onclick="stepper.previous()">Previous</div>
								   <div class="btn btn-primary" onclick="stepper.next()">Next</div>
								</div>
								
								
								<!-- register form step 3-->
								<div id="register-part-3" class="content" role="tabpanel" aria-labelledby="register-part-4-trigger">
								  
								  <div class="row">								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Location of services (Regions) <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control alphabetsOnly" placeholder="Location of services (Regions)" id="location_of_services" name="location_of_services" value={{ old('location_of_services') }}>
											
										  </div>
									</div>
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Business turn over (range) <span class="requiredSymbol">*</span></label>
											<!--<input type="text" class="form-control" placeholder="Business turn over (range)" id="business_turn_over" name="business_turn_over" value={{ old('business_turn_over') }}>-->
											<div class="input-group">
												<div class="input-group-prepend">
												<span class="input-group-text"><b>$</b></span>
												</div>
												<!--<input type="text" class="form-control numberOnly" placeholder="Annual preneed volume" id="annual_preneed_volume" name="annual_preneed_volume">-->
												<select class="form-control" id="business_turn_over" name="business_turn_over">
														<option value="100 - 1,000">100 - 1,000</option>
														<option value="1,000 - 10,000">1,000 - 10,000</option>
														<option value="10,000 - 100,000">10,000 - 100,000</option>
														<option value="100,000 - 1,000,000">100,000 - 1,000,000</option>
														<option value="1,000,000 - 10,000,000">1,000,000 - 10,000,000</option>
													</select>

											</div>
										  </div>
									</div>
								  </div>
								  
								  <div class="row">								
								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Number of years in business <span class="requiredSymbol">*</span></label>	
											<input type="text" class="form-control numberOnly" placeholder="Number of years in business" id="number_of_years_in_business" name="number_of_years_in_business" value={{ old('number_of_years_in_business') }}>					
										</div>
									</div>
									
									<!--<div class="col-md-6 col-lg-6">
									
									<div class="form-group">
											<label for="exampleInputFile">Required Document Uploads</label>
											<div class="input-group">
											   <input type="file" name="idFiles[]" class="myfrm form-control">
											 
											</div>
											
										  </div>
									</div>-->
								  </div>
								  <!--First-->
									<div class="row increment">
								  		<div class="col-md-6 col-lg-6">
												<div class="form-group">
													<div class="input-group realprocode control-group lst" >
								  				<!--<input type="text" name="filetitle[]" class="myfrm form-control" placeholder="Document Name">-->
								  				<select class="form-control" id="" name="filetitle[]">
														<option value="Driver-License">State-Issued Driver's License</option>
														<option value="Producer-Agreement">Producer Agreement</option>
														<option value="Direct-Deposit-Authorization">Direct Deposit Authorization</option>
														<option value="Producer-Compensation-Schedule">Producer Compensation Schedule</option>
														<option value="Explanation-of-Information-found-on-MIB">Explanation of Information found on MIB</option>
														<option value="Other-Supporting-Documentation">Other Supporting Documentation</option>
													</select>
											      
											    </div>
												</div>
											</div>

											<div class="col-md-6 col-lg-6">
												<div class="form-group">
													<div class="input-group realprocode control-group lst " >
								  				
											      <input type="file" accept=".gif,.jpg,.jpeg,.png,.doc,.docx,.pdf" name="filenames[]" class="myfrm form-control">
											      <div class="input-group-btn"> 
											        <button class="btn btn-success addFile" type="button"> <i class="fldemo glyphicon glyphicon-plus"></i>Add</button>
											      </div>
											    </div>
												</div>
											</div>
								  </div>

								<!--Second-->
							<div class="clone d-none " >
								<div class="row ">
									<div class="realprocode col-md-12 col-lg-12">
								  		<div class="col-md-6 col-lg-6" style="float: left;">
												<div class="form-group">
													<div class=" control-group lst input-group" style="margin-top:10px">
											      	<!--<input type="text" name="filetitle[]" class="myfrm form-control" placeholder="Document Name">-->
											      	<select class="form-control" id="" name="filetitle[]">
														<option value="Driver-License">State-Issued Driver's License</option>
														<option value="Producer-Agreement">Producer Agreement</option>
														<option value="Direct-Deposit-Authorization">Direct Deposit Authorization</option>
														<option value="Producer-Compensation-Schedule">Producer Compensation Schedule</option>
														<option value="Explanation-of-Information-found-on-MIB">Explanation of Information found on MIB</option>
														<option value="Other-Supporting-Documentation">Other Supporting Documentation</option>
													</select>
											        
											      </div>
												</div>
											</div>

											<div class="col-md-6 col-lg-6" style="float: left;">
												<div class="form-group">
													<div class=" control-group lst input-group" style="margin-top:10px">
											      	
											        <input type="file" accept=".gif,.jpg,.jpeg,.png,.doc,.docx,.pdf" name="filenames[]" class="myfrm form-control">
											        <div class="input-group-btn"> 
											          <button class="btn btn-danger" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>
											        </div>
											      </div>
												</div>
											</div>
										</div>
								  </div>

								</div>
								 <hr class="pb-2">
<!--<h4 class="color-blue">Past/present partner Funeral Home relationships</h4>-->
<div class="row">
								  		<div class="col-md-12 col-lg-12">
								  			<div class="form-check">
												<input type="checkbox" class="form-check-input" id="bgChk">
												<label class="form-check-label" for="exampleCheck1">I hereby authorize Cyrus Life or an Agency appointed by Cyrus Life to verify the submitted records.</label>

											</div>
											<div class="error bgChkError"></div>

								  		</div>
								  	</div>
								  	<br/>
								 
								  <div class="btn btn-primary" onclick="stepper.previous()">Previous</div>
								  <button type="submit" id="formSubmit" class="btn btn-primary">Submit</button>
								  <span style="display:none" class="error finalErr">Please fill the required fields *</span>
								   <span style="display:none" class="error emailErr">Email id not valid!</span>

								</div>
								
								
							  </div>
							</div>
						  </div>
						  <!-- /.card-body -->
						  
						</div>
						<!-- /.card -->
					  </div>
					</div>
				</form>
				<!-- register form stepper -->
				
			</div>
		
	</div>
	
	@include('include.footer')
	<script type="text/javascript">
    $(document).ready(function() {
    	loadStateRegion();
      
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
    	
      $(".addFile").click(function(){ 
          var lsthmtl = $(".clone").html();
          $(".increment").after(lsthmtl);
      });
      $("body").on("click",".btn-danger",function(){ 
          $(this).parents(".realprocode").remove();
      });
    });
</script>