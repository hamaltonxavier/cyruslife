@include('include.header')

	<style>
	ul.list-ssn {
		display: flex;
		list-style: none;
		margin: 0;
		padding: 0;
	}
	
	.btn-blue{background:#fff; border-color:#004367; color:#004367}
	
	.btn-blue-solid{background:#004367; border-color:#004367; color:#fff}
	.btn-blue-solid:hover{background:#FED56D; color:#004367; border-color:#FED56D}
	
	ul.list-ssn li {
    max-width: 75px;
    width: 75px;
    margin-right: 10px
	}
	
	ul.list-ssn.format1 li:nth-child(4) {
    margin-left: 20px;
	position:relative;
}

ul.list-ssn.format1 li:nth-child(4):before {content:""; position:absolute; left:-18px; top:20px; width:12px; height:1px; background:#000}

ul.list-ssn.format1 li:nth-child(5) {
    margin-right: 20px;
	position:relative;
}
ul.list-ssn.format1 li:nth-child(5):before {content:""; position:absolute; right:-16px; top:20px; width:12px; height:1px; background:#000}


/* format 2 */
ul.list-ssn.format2 li:nth-child(3) {
    margin-left: 20px;
	position:relative;
}

ul.list-ssn.format2 li:nth-child(3):before {content:""; position:absolute; left:-18px; top:20px; width:12px; height:1px; background:#000}

.bs-stepper-content {
  padding: 40px 30px 30px;
}
	
	@media only screen and (max-width:767px){
		ul.list-ssn{flex-wrap: wrap;}
		ul.list-ssn li{max-width: 40px; width: 40px; margin-bottom:5px; margin-right: 5px;}
	}
	
	  @media only screen and (max-width: 767px){
		.bs-stepper-header .step {
		  max-width: 33%;
		}
		.btn{padding: .375rem 1rem;}
	  }
	
	@media only screen and (min-width:768px) and (max-width:991px){
		ul.list-ssn{flex-wrap: wrap;}
		ul.list-ssn li{max-width: 40px; width: 40px; margin-bottom:5px; margin-right: 5px;}
	}
	
  </style>
	
	<div class="content-body pb-5 pt-5">
		
		<div class="container">
				
				<!-- register form stepper -->
					
					<div class="row">
					  <div class="col-md-12">
						<div class="card card-default">
						  <div class="card-header">
							<h3 class="text-center mt-3 font-weight700 color-blue">Partner Application Form</h3>
						  </div>
						  <div class="card-body p-0">
							<div class="bs-stepper linear">
							  <div class="bs-stepper-header" role="tablist">
								<!-- your steps here -->
								
								<div class="step active" data-target="#register-part-1">
								  <button type="button" class="step-trigger" role="tab" aria-controls="register-part-1" id="register-part-1-trigger" aria-selected="false" disabled="disabled">
									<span class="bs-stepper-circle">1</span>
									<span class="bs-stepper-label">State issued licensing information</span>
								  </button>
								</div>
								
								<div class="line"></div>
								
								<div class="step" data-target="#register-part-2">
								  <button type="button" class="step-trigger" role="tab" aria-controls="register-part-2" id="register-part-2-trigger" aria-selected="true">
									<span class="bs-stepper-circle">2</span>
									<span class="bs-stepper-label">IRS Form W-9</span>
								  </button>
								</div>
								
								<div class="line"></div>
								
								<div class="step" data-target="#register-part-3">
								  <button type="button" class="step-trigger" role="tab" aria-controls="register-part-3" id="register-part-3-trigger" aria-selected="true">
									<span class="bs-stepper-circle">3</span>
									<span class="bs-stepper-label">Agent / Agency details</span>
								  </button>
								</div>
								
								
								
								
							  </div>
							  <div class="bs-stepper-content">
								<!-- your steps content here -->
								
								<!-- register part 1 -->
								<div id="register-part-1" class="content active dstepper-block" role="tabpanel" aria-labelledby="register-part-1-trigger">
								  
									<div class="form-group">
										<div class="row">
											<label class="col-11 col-sm-4 col-md-4 col-lg-4 col-form-label">State issued funeral ID / (Crematoria Licence ID #)</label>
											<div class="col-12 col-sm-7 col-md-7 col-ld-7">
											  <input type="text" class="form-control" name="crematoria_licence_id" id="crematoria_licence_id"  placeholder="">
											</div>
										</div>
									</div>
 
								  <div class="row mt-5">
									<div class="col-6 col-sm-6">
										<button class="btn btn-primary ml-auto" onclick="stepper.next()">Next</button>
									</div>
									<div class="col-6 col-sm-6 mbb text-right">
										<button class="btn btn-primary btn-blue mr-auto" onclick="stepper.next()">Save</button>
									</div>
									
								  </div>
								  
								</div>
								
								<!-- registration part 2-->
								<div id="register-part-2" class="content" role="tabpanel" aria-labelledby="register-part-2-trigger">
								 
									
								<div class="form-group row">
									<label class="col-11 col-sm-4 col-md-4 col-lg-4 col-form-label">Name (as shown on your income tax return)</label>
									<div class="col-12 col-sm-7 col-md-7 col-lg-7">
									  <input type="text" class="form-control"  placeholder="" name="name" id="name">
									</div>
								</div>
								<hr>
								<div class="form-group row">
									<label class="col-11 col-sm-4 col-md-4 col-lg-4 col-form-label">Business name/disregarded entity name, if different from above</label>
									<div class="col-sm-7">
									  <input type="text" class="form-control"  placeholder="" name="business_name" id="business_name">
									</div>
								</div>
								<hr>
								<div class="form-group row">
									<label class="col-11 col-sm-11 col-md-11 col-lg-11 col-form-label">Check appropriate box for federal tax classification of Name or Business Name</label>
									<div class="col-sm-12">
									  
									  <div class="row">
									  
										  <div class="col-12 col-sm-6 col-md-6 col-lg-3">
											<div class="form-group d-flex align-items-start">
												<input type="radio" class="form-radio-input ml-0" id="exampleCheck1" name="tax_clasification_name" id="tax_clasification_name">
												<label class="form-radio-label pl-4" for="exampleCheck1">Individual/sole proprietor or single-member LLC</label>
											</div>
										  </div>
										  <div class="col-12 col-sm-6 col-md-6 col-lg-2">
											<div class="form-group d-flex align-items-start">
												<input type="radio" class="form-radio-input ml-0" id="exampleCheck2" name="tax_clasification_name" id="tax_clasification_name">
												<label class="form-radio-label pl-4" for="exampleCheck2">C Corporation</label>
											</div>
										  </div>
										  <div class="col-12 col-sm-6 col-md-6 col-lg-2">
											<div class="form-group d-flex align-items-start">
												<input type="radio" class="form-radio-input ml-0" id="exampleCheck3" name="tax_clasification_name" id="tax_clasification_name">
												<label class="form-radio-label pl-4" for="exampleCheck3">S Corporation</label>
											</div>
										  </div>
										  <div class="col-12 col-sm-6 col-md-6 col-lg-2">
											<div class="form-group d-flex align-items-start">
												<input type="radio" class="form-radio-input ml-0" id="exampleCheck4" name="tax_clasification_name" id="tax_clasification_name">
												<label class="form-radio-label pl-4" for="exampleCheck4">Partnership</label>
											</div>
										  </div>
										  <div class="col-12 col-sm-6 col-md-6 col-lg-2">
											<div class="form-group d-flex align-items-start">
												<input type="radio" class="form-radio-input ml-0" id="exampleCheck5" name="tax_clasification_name" id="tax_clasification_name">
												<label class="form-radio-label pl-4" for="exampleCheck5">Trust/estate</label>
											</div>
										  </div>
										  
										</div>
										
										<div class="row mt-3">
									  
										  <div class="col-lg-12">
											<div class="form-group mb-0 d-flex align-items-start">
												<input type="radio" class="form-radio-input ml-0" id="" name="tax_clasification_name" id="tax_clasification_name">
												<label class="form-radio-label pl-4" for="">Limited liability company 
												<a class="badge bg-success" href="javascript:;" data-toggle="modal" data-target="#modal-llc"><i class="fa fa-info"></i></a></label>
											</div>
										  </div>
										  
										  <div class="col-lg-12">
											<div class="row">
												<label class="col-sm-4 col-md-8 col-lg-4 col-form-label">Enter the tax classification <br>(C=C corporation, S=S corporation, P=Partnership)</label>
												<div class="col-sm-3 col-md-4 col-lg-3">
												  <select class="form-control" name="tax_clasification" id="tax_clasification">
													<option value="C - Corporation">C - Corporation</option>
													<option value="S - Corporation">S - Corporation</option>
													<option value="Partnership">Partnership</option>
												  </select>
												</div>
											</div>
											<div class="row">
												<label class="col-sm-12 col-form-label">Note: Check the appropriate box in the above for the tax classification of the single-member owner. Do not check LLC if the LLC is classified as a single-member LLC that is disregarded from the owner unless the owner of the LLC is another LLC that is not disregarded from the owner for U.S. federal tax purposes. Otherwise, a single-member LLC that is disregarded from the owner should check the appropriate box for the tax classification of its owner.</label>
												
											</div>
										  </div>
										  
										</div> <!-- row end -->
										
										
											
										</div> 
									  
									</div>
								<hr>
									
								<div class="form-group row mb-2">
									<label class="col-12 col-sm-12 col-md-12 col-lg-12 col-form-label">Exemptions (codes apply only to certain entities, not individuals; see instructions on page <a class="badge bg-success" href="javascript:;" data-toggle="modal" data-target="#modal-line5a"><i class="fa fa-info"></i></a></label>

								</div> <!-- row end -->
								
								<div class="form-group row mb-2">
									
									<label class="col-sm-4 col-md-4 col-lg-4 col-form-label">Exempt payee code (if any) </label>
									<div class="col-10 col-sm-12 col-md-3 col-lg-3">
										<input type="text" class="form-control"  placeholder="" name="exempt_payee_code" id="exempt_payee_code">
									</div>
									<div class="col-1 col-sm-1 col-lg-1">
										<a class="badge bg-success" href="javascript:;" data-toggle="modal" data-target="#modal-line5b"><i class="fa fa-info"></i></a>
									</div>
								</div> <!-- row end -->
										
								<div class="form-group row">
									<label class="col-sm-4 col-form-label">Exemption from FATCA reporting code (if any)</label>
									<div class="col-10 col-sm-3 col-md-3 col-lg-3">
										<input type="text" class="form-control"  placeholder="" name="exemption_fatca_reporting_code" id="exemption_fatca_reporting_code"> 
									</div>
									<div class="col-1 col-sm-1 col-lg-1">
										<a class="badge bg-success" href="javascript:;" data-toggle="modal" data-target="#modal-line5c"><i class="fa fa-info"></i></a>
									</div>
								</div>
								
								<hr>
									
								<div class="form-group row mb-2">
									<label class="col-11 col-sm-4 col-md-4 col-lg-4 col-form-label">Address (number, street, and apt. or suite no.) </label>
									<div class="col-sm-7 pl-0">
										<input type="text" class="form-control"  placeholder="" name="address" id="address">
									</div>
								</div> <!-- row end -->
								
								<hr>
									
								<div class="form-group row mb-2">
									<label class="col-11 col-sm-4 col-md-4 col-lg-4 col-form-label">City, state, and ZIP code</label>
									<div class="col-sm-7 pl-0">
										<input type="text" class="form-control"  placeholder="" name="city_state_zipcode" id="city_state_zipcode">
									</div>
								</div> <!-- row end -->
								
								<hr>
									
								<div class="form-group row mb-2">
									<label class="col-11 col-sm-4 col-md-4 col-lg-4 col-form-label">List account number(s) here (optional) <a class="badge bg-success" href="javascript:;" data-toggle="modal" data-target="#modal-line8"><i class="fa fa-info"></i></a></label>
									<div class="col-sm-7 pl-0">
										<input type="text" class="form-control"  placeholder="" name="list_account_number" id="list_account_number">
									</div>
								</div> <!-- row end -->
								
								<hr>
								<div class="form-group row mb-2">
									<label class="col-11 col-sm-4 col-md-4 col-lg-4 col-form-label">Requester’s name and address (optional)</label>
									<div class="col-sm-7 pl-0">
										<input type="text" class="form-control"  placeholder="" name="requester_name_address" id="requester_name_address">
									</div>
								</div> <!-- row end -->
								
								<hr>
								
								
								<div class="form-group row mb-2">
									<label class="col-11 col-sm-11 col-md-11 col-lg-4 col-form-label">Taxpayer Identification Number (TIN) 
									<a class="badge bg-success" href="javascript:;" data-toggle="modal" data-target="#modal-line10"><i class="fa fa-info"></i></a></label>
									
									<div class="col-sm-12 col-lg-12 pl-0">
										<div class="row">
										  <div class="offset-lg-1 col-lg-11">
											<div class="form-group d-flex align-items-start mb-2">
												<input type="radio" class="form-radio-input ml-0" name="tin_type" id="tin_type" value="Social security number">
												<label class="form-check-label pl-4">Social security number</label>
											</div>
										  </div>
										  <div class="offset-lg-1 col-lg-6">
											<ul class="list-ssn format1">
												<li><input type="text" class="form-control"  placeholder="" name="social_security_number[]" ></li>
												<li><input type="text" class="form-control"  placeholder="" name="social_security_number[]" ></li>
												<li><input type="text" class="form-control"  placeholder="" name="social_security_number[]" ></li>
												<li><input type="text" class="form-control"  placeholder="" name="social_security_number[]" ></li>
												<li><input type="text" class="form-control"  placeholder="" name="social_security_number[]" ></li>
												<li><input type="text" class="form-control"  placeholder="" name="social_security_number" ></li>
												<li><input type="text" class="form-control"  placeholder="" name="social_security_number[]" ></li>
												<li><input type="text" class="form-control"  placeholder="" name="social_security_number[]" ></li>
												<li><input type="text" class="form-control"  placeholder="" name="social_security_number[]" ></li>
											</ul>
										  </div>
										</div>
										
										<div class="row mt-3">
										  <div class="offset-lg-1 col-lg-11">
											(OR)
										  </div>
										  
										</div>
										
										<div class="row mt-3 mb-3">
										  <div class="offset-lg-1 col-lg-11">
											<div class="form-group d-flex align-items-start mb-2">
												<input type="radio" class="form-radio-input ml-0" name="tin_type" id="tin_type">
												<label class="form-check-label pl-4">Employer identification number</label>
											</div>
										  </div>
										  <div class="offset-lg-1 col-lg-6">
											<ul class="list-ssn format2">
												<li><input type="text" class="form-control"  placeholder="" name="employer_identification_number[]" ></li>
												<li><input type="text" class="form-control"  placeholder="" name="employer_identification_number[]" ></li>
												<li><input type="text" class="form-control"  placeholder="" name="employer_identification_number[]"></li>
												<li><input type="text" class="form-control"  placeholder="" name="employer_identification_number[]"></li>
												<li><input type="text" class="form-control"  placeholder="" name="employer_identification_number[]"></li>
												<li><input type="text" class="form-control"  placeholder="" name="employer_identification_number[]"></li>
												<li><input type="text" class="form-control"  placeholder="" name="employer_identification_number[]"></li>
												<li><input type="text" class="form-control"  placeholder="" name="employer_identification_number[]"></li>
												<li><input type="text" class="form-control"  placeholder="" name="employer_identification_number[]"></li>
											</ul>
										  </div>
										</div>
										
										
									</div>
								</div> <!-- row end -->
										
								<hr>
								<div class="form-group row mb-2">
									<label class="col-11 col-sm-4 col-md-4 col-lg-4 col-form-label">Certification</label>
									<div class="col-sm-12 mb-5">
										<p>Under penalties of perjury, I certify that:</p>
										<ol>
											<li>The number shown on this form is my correct taxpayer identification number (or I am waiting for a number to be issued to me); and</li>
											<li>I am not subject to backup withholding because: (a) I am exempt from backup withholding, or (b) I have not been notified by the Internal Revenue Service (IRS) that I am subject to backup withholding as a result of a failure to report all interest or dividends, or (c) the IRS has notified me that I am no longer subject to backup withholding; and</li>
											<li>I am a U.S. citizen or other U.S. person (defined below); and</li>
											<li>The FATCA code(s) entered on this form (if any) indicating that I am exempt from FATCA reporting is correct.</li>
										</ol>
									</div>
									
									<div class="col-sm-3 col-md-3 col-lg-2 align-self-center">
										<h4 class="">Sign Here</h4>	
									</div>
									<div class="col-sm-5">
										<div class="row">
											<label class="col-sm-12 col-form-label">Signature of U.S. person</label>
											<div class="col-sm-10 pl-0">
												<input type="text" class="form-control"  placeholder="" name="signature" id="signature">
											</div>	
										</div>
									</div>
									<div class="col-sm-4">
										<div class="row">
											<label class="col-sm-12 col-form-label">Date</label>
											<div class="col-sm-12 col-md-12 col-lg-12 pl-0">
												<div class="input-group date" id="reservationdate" data-target-input="nearest">
													<input type="text" class="form-control datetimepicker-input" data-target="#reservationdate" name="application_date" id="application_date"/>
													<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
														<div class="input-group-text"><i class="fa fa-calendar"></i></div>
													</div>
												</div>
											</div>	
										</div>
									</div>
									
								</div> <!-- row end -->
								
								
								 
								
								  
								  
								  <div class="row mt-5">
									<div class="col-8 col-sm-6">
										 <button class="btn btn-primary" onclick="stepper.previous()">Previous</button>
										<button class="btn btn-primary" onclick="stepper.next()">Next</button>
									</div>
									<div class="col-4 col-sm-6 mbb text-right">
										<button class="btn btn-primary btn-blue mr-auto" onclick="stepper.next()">Save</button>
									</div>
									
								  </div>
								  
								</div> <!-- register-part-2 end -->
								
								
								
								
								
								<!-- register form step 3-->
								<div id="register-part-3" class="content" role="tabpanel" aria-labelledby="register-part-3-trigger">
								  
									
											
								<div class="form-group row mb-2">
									
									<div class="col-sm-12 col-lg-12 pl-0">
										<div class="row">
										  <div class="col-lg-12">
											<div class="form-group d-flex align-items-start mb-2">
												<input type="checkbox" class="form-check-input ml-0" name="crematoria_licence_id" id="crematoria_licence_id">
												<label class="form-check-label pl-4">Agent entity established by the funeral home</label>
											</div>
											
											<div class="form-group row">
												<label class="col-11 col-sm-4 col-md-4 offset-1 col-lg-3 col-form-label">Agent entity name</label>
												<div class="col-12 col-sm-5 col-md-5 col-ld-5" >
												  <input type="text" class="form-control"  placeholder="" name="agent_entity_name" id="agent_entity_name">
												</div>
											</div>
										  </div>
										 
										</div>

									</div>
								</div> <!-- row end -->
								
								<div class="form-group row mb-2">
									
									<div class="col-sm-12 col-lg-12 pl-0">
										<div class="row">
										  <div class="col-lg-12">
											<div class="form-group d-flex align-items-start mb-2">
												<input type="checkbox" class="form-check-input ml-0" name="crematoria_licence_id" id="crematoria_licence_id">
												<label class="form-check-label pl-4">Agency entity established by the funeral home</label>
											</div>
											
											<div class="form-group row">
												<label class="col-11 col-sm-4 col-md-4 offset-1 col-lg-3 col-form-label">Agency entity name</label>
												<div class="col-12 col-sm-5 col-md-5 col-ld-5">
												  <input type="text" class="form-control"  placeholder="" name="agency_entity_name" id="agency_entity_name">
												</div>
											</div>
										  </div>
										 
										</div>

									</div>
								</div> <!-- row end -->
								
									
									
									<div class="row mt-5">
									<div class="col-4 col-sm-6">
										 <button class="btn btn-primary" onclick="stepper.previous()">Previous</button>
									</div>
									<div class="col-8 col-sm-6 mbb text-right">
										<button type="submit" class="btn btn-primary btn-blue-solid mb-2">Submit</button>
									  <button type="submit" class="btn btn-primary btn-blue mb-2">Save</button>
									</div>
									
								  </div>
									
									
								</div>
								
								
							  </div>
							</div>
						  </div>
						  <!-- /.card-body -->
						  
						</div>
						<!-- /.card -->
					  </div>
					</div>
				
				<!-- register form stepper -->
				
			</div>
		
	</div>
	
	

<!-- models -->
<div class="modal fade" id="modal-llc">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Limited liability company </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Note: Check the appropriate box in the line above for the tax classification of the single-member owner. Do not check LLC if the LLC is classified as a single-member LLC that is disregarded from the owner unless the owner of the LLC is another LLC that is not disregarded from the owner for U.S. federal tax purposes. Otherwise, a single-member LLC that is disregarded from the owner should check the appropriate box for the tax classification of its owner.</p>
            </div>
           
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
	  
	  
	  <!-- models -->
<div class="modal fade" id="modal-line5a">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Exemptions </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>If you are exempt from backup withholding and/or FATCA reporting, enter in the appropriate space on line 4 any code(s) that may apply to you.</p>
            </div>
           
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


	  <!-- models -->
<div class="modal fade" id="modal-line5b">
        <div class="modal-dialog modal-dialog-centered modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Exempt payee code </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <ul>
				<li>Generally, individuals (including sole proprietors) are not exempt from backup withholding.</li>
				<li>Except as provided below, corporations are exempt from backup withholding for certain payments, including interest and dividends.</li>
				<li>Corporations are not exempt from backup withholding for payments made in settlement of payment card or third party network transactions.</li>
				<li>Corporations are not exempt from backup withholding with respect to attorneys’ fees or gross proceeds paid to attorneys, and corporations that provide medical or health care services are not exempt with respect to payments reportable on Form 1099-MISC.</li>
			  </ul>
			  
			  <p>The following codes identify payees that are exempt from backup withholding. Enter the appropriate code in the space in line 5.</p>
				<ul>
					<li>1—An organization exempt from tax under section 501(a), any IRA, or a custodial account under section 403(b)(7) if the account satisfies the requirements of section 401(f)(2)</li>
					<li>2—The United States or any of its agencies or instrumentalities</li>
					<li>3—A state, the District of Columbia, a U.S. commonwealth or possession, or any of their political subdivisions or instrumentalities</li>
					<li>4—A foreign government or any of its political subdivisions, agencies, or instrumentalities</li>
					<li>5—A corporation</li>
					<li>6—A dealer in securities or commodities required to register in the United States, the District of Columbia, or a U.S. commonwealth or possession</li>
					<li>7—A futures commission merchant registered with the Commodity Futures Trading Commission</li>
					<li>8—A real estate investment trust</li>
					<li>9—An entity registered at all times during the tax year under the Investment Company Act of 1940</li>
					<li>10—A common trust fund operated by a bank under section 584(a)</li>
					<li>11—A financial institution</li>
					<li>12—A middleman known in the investment community as a nominee or custodian</li>
					<li>13—A trust exempt from tax under section 664 or described in section 4947</li>
				</ul>
				
				<p>The following chart shows types of payments that may be exempt from backup withholding. The chart applies to the exempt payees listed above, 1 through 13.</p>
				
				<div class="responsive">
					<table class="table">
						<thead>
							<tr>
								<th>IF the payment is for . . .</th>
								<th>THEN the payment is exempt for . . .</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Interest and dividend payments</td>
								<td>All exempt payees except for 7</td>
							</tr>
							<tr>
								<td>Broker transactions</td>
								<td>Exempt payees 1 through 4 and 6 through 11 and all C corporations. S corporations must not enter an exempt payee code because they are exempt only for sales of noncovered securities acquired prior to 2012.</td>
							</tr>
							<tr>
								<td>Barter exchange transactions and patronage dividends</td>
								<td>Exempt payees 1 through 4</td>
							</tr>
							<tr>
								<td>Payments over $600 required to be reported and direct sales over $5,0001</td>
								<td>Generally, exempt payees 1 through 5<sup>2</sup></td>
							</tr>
							<tr>
								<td>Payments made in settlement of payment card or third party network transactions</td>
								<td>Exempt payees 1 through 4</td>
							</tr>
						</tbody>
					</table>
					
					<ul>
						<li>See Form 1099-MISC, Miscellaneous Income, and its instructions.</li>
						<li>However, the following payments made to a corporation and reportable on Form 1099-MISC are not exempt from backup withholding: medical and health care payments, attorneys’ fees, gross proceeds paid to an attorney reportable under section 6045(f), and payments for services paid by a federal executive agency.</li>
					</ul>
				</div>
            </div>
           
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


<div class="modal fade" id="modal-line5c">
        <div class="modal-dialog modal-dialog-centered modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Exemption from FATCA reporting code </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
				<p>The following codes identify payees that are exempt from reporting under FATCA. These codes apply to persons submitting this form for accounts maintained outside of the United States by certain foreign financial institutions. Therefore, if you are only submitting this form for an account you hold in the United States, you may leave this field blank. Consult with the person requesting this form if you are uncertain if the financial institution is subject to these requirements. A requester may indicate that a code is not required by providing you with a Form W-9 with “Not Applicable” (or any similar indication) written or printed on the line for a FATCA exemption code.</p>
              <ul>
				<li>A—An organization exempt from tax under section 501(a) or any individual retirement plan as defined in section 7701(a)(37)</li>
				<li>B—The United States or any of its agencies or instrumentalities</li>
				<li>C—A state, the District of Columbia, a U.S. commonwealth or possession, or any of their political subdivisions or instrumentalities</li>
				<li>D—A corporation the stock of which is regularly traded on one or more established securities markets, as described in Regulations section 1.1472-1(c)(1)(i)</li>
				<li>E—A corporation that is a member of the same expanded affiliated group as a corporation described in Regulations section 1.1472-1(c)(1)(i)</li>
				<li>F—A dealer in securities, commodities, or derivative financial instruments (including notional principal contracts, futures, forwards, and options) that is registered as such under the laws of the United States or any state</li>
				<li>G—A real estate investment trust</li>
				<li>H—A regulated investment company as defined in section 851 or an entity registered at all times during the tax year under the Investment Company Act of 1940</li>
				<li>I—A common trust fund as defined in section 584(a)</li>
				<li>J—A bank as defined in section 581</li>
				<li>K—A broker</li>
				<li>L—A trust exempt from tax under section 664 or described in section 4947(a)(1)</li>
				<li>M—A tax exempt trust under a section 403(b) plan or section 457(g) plan</li>
			  </ul>
			  
			  <p><strong>Note:</strong> You may wish to consult with the financial institution requesting this form to determine whether the FATCA code and/or exempt payee code should be completed.</p>
				

            </div>
           
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
	  
	  
<div class="modal fade" id="modal-line6">
        <div class="modal-dialog modal-dialog-centered modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Exemption from FATCA reporting code </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
				<p>Enter your address (number, street, and apartment or suite number). This is where the requester of this Form W-9 will mail your information returns. If this address differs from the one the requester already has on file, write NEW at the top. If a new address is provided, there is still a chance the old address will be used until the payor changes your address in their records.</p>
              
            </div>
           
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->	  
	  


<div class="modal fade" id="modal-line8">
        <div class="modal-dialog modal-dialog-centered modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">List account number(s)</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
				<p>Pellentesque in mi velit. Curabitur molestie viverra justo quis egestas. In hac habitasse platea dictumst. Morbi accumsan quam non libero varius sit amet luctus nunc mattis. Suspendisse gravida libero ut tellus rutrum cursus. </p>
              
            </div>
           
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
	  
<div class="modal fade" id="modal-line10">
        <div class="modal-dialog modal-dialog-centered modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Part I. Taxpayer Identification Number (TIN)</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
				<p><strong>Enter your TIN in the appropriate box.</strong> If you are a resident alien and you do not have and are not eligible to get an SSN, your TIN is your IRS individual taxpayer identification number (ITIN). Enter it in the social security number box. If you do not have an ITIN, see How to get a TIN below.</p>
				<p>If you are a sole proprietor and you have an EIN, you may enter either your SSN or EIN.<p>
				<p>If you are a single-member LLC that is disregarded as an entity separate from its owner, enter the owner’s SSN (or EIN, if the owner has one). Do not enter the disregarded entity’s EIN. If the LLC is classified as a corporation or partnership, enter the entity’s EIN.</p>
				<p><strong>Note:</strong> See What Name and Number To Give the Requester, later, for further clarification of name and TIN combinations.</p>
				<p><strong>How to get a TIN.</strong> If you do not have a TIN, apply for one immediately. To apply for an SSN, get Form SS-5, Application for a Social Security Card, from your local SSA office or get this form online at www.SSA.gov. You may also get this form by calling 1-800-772-1213. Use Form W-7, Application for IRS Individual Taxpayer Identification Number, to apply for an ITIN, or Form SS-4, Application for Employer Identification Number, to apply for an EIN. You can apply for an EIN online by accessing the IRS website at www.irs.gov/Businesses and clicking on Employer Identification Number (EIN) under Starting a Business. Go to www.irs.gov/Forms to view, download, or print Form W-7 and/or Form SS-4. Or, you can go to www.irs.gov/OrderForms to place an order and have Form W-7 and/or SS-4 mailed to you within 10 business days.</p>
				<p>If you are asked to complete Form W-9 but do not have a TIN, apply for a TIN and write “Applied For” in the space for the TIN, sign and date the form, and give it to the requester. For interest and dividend payments, and certain payments made with respect to readily tradable instruments, generally you will have 60 days to get a TIN and give it to the requester before you are subject to backup withholding on payments. The 60-day rule does not apply to other types of payments. You will be subject to backup withholding on all such payments until you provide your TIN to the requester.</p>
				<p><strong>Note:</strong> Entering “Applied For” means that you have already applied for a TIN or that you intend to apply for one soon.</p>
				<p><strong>Caution:</strong> A disregarded U.S. entity that has a foreign owner must use the appropriate Form W-8.</p>
              
            </div>
           
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

@include('include.footer')