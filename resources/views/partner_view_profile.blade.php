@include('include.header')	

@foreach($userDetails as $data)

	<div class="content-body pb-5 pt-5">
		
		<div class="container w-75">
				
				<!-- register form stepper -->
					
					<div class="row">
					  <div class="col-md-12">
						<div class="card card-default">
						  <div class="card-header">
							<h3 class="pl-4 mt-3 font-weight700 color-blue">View  Profile</h3>
							
						  </div>
						  <div class="card-body p-5">
							
							
								<div id="register-part-1" class="content">
								  
								  <h3 class="color-blue">Business Name and Address</h3>
								  <hr class="pb-2">
								  
								  <div class="row">
									<div class="col-md-12 col-lg-12">
										<div class="form-group">
											<label>Name of the Business</label>
											<input type="text" class="form-control" placeholder="Name of the Business" value="{{$data->business_name}}"disabled>
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										 <div class="form-group">
											<label>Physical Address 1</label>
											<input type="text" class="form-control"  placeholder="Address 1" value="{{$data->address1}}"disabled>
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										 <div class="form-group">
											<label>Physical Address 2</label>
											<input type="text" class="form-control"  placeholder="Address 2" value="{{$data->address2}}"disabled>
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										 <div class="form-group">
											<label>Physical Address 3</label>
											<input type="text" class="form-control"  placeholder="Address 3" value="{{$data->address3}}"disabled>
										  </div>
									</div>
									
									
									<div class="col-md-6 col-lg-4">
										<div class="form-group">
												  <label>Country</label>
												  <input type="text" class="form-control" placeholder="Name of the Business" value="{{$data->countryName}}"disabled>
												</div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										<div class="form-group">
												  <label>State</label>
												  <input type="text" class="form-control" placeholder="Name of the Business" value="{{$data->stateName}}"disabled>
												</div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										<div class="form-group">
												  <label>City</label>
												 <input type="text" class="form-control" placeholder="Name of the Business" value="{{$data->cityName}}"disabled>
												</div>
									</div>
									
									<div class="col-md-6  col-lg-6">
										<div class="form-group">
													<label>Postal Code</label>
													<input type="text" maxlength="6" class="form-control"  placeholder="Postal Code" value="{{$data->postal_code}}"disabled>
												  </div>
									</div>
										
										
										  
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Main Business Telephone</label>
											<input type="tel" class="form-control numberOnly"  placeholder="Mobile / Telephone Number" value="{{$data->telephone}}"disabled>
										  </div>
									</div>
										
										<div class="col-lg-6">
										<div class="form-group">
											<label>Business License </label>
											<input type="text" class="form-control" placeholder="Business License" name="license_type" id="license_type" value="{{$data->license_type}}"disabled>
										  </div>
									</div>
									</div>
								  

								</div>
								
								<!-- registration part 2-->
								<div id="register-part-2" class="content mt-5">
								  <h3 class="color-blue">Primary Contact Details</h3>
								  <hr class="pb-2">
								 
								  <div class="row">
									<div class="col-md-3 col-lg-2">
										<label>Salutation/Title</label>
										<input type="tel" class="form-control"  placeholder="Mobile / Telephone Number" value="{{$data->title}}"disabled>
									</div>
									
									<div class="col-md-9 col-lg-4">
										<div class="form-group">
											<label>First Name</label>
											<input type="text" class="form-control" placeholder="First Name" value="{{$data->first_name}}"disabled>
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-3">
										<div class="form-group">
											<label>Middle Name</label>
											<input type="text" class="form-control" placeholder="Middle Name" value="{{$data->middle_name}}"disabled>
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-3">
										<div class="form-group">
											<label>Last Name</label>
											<input type="text" class="form-control" placeholder="Last Name" value="{{$data->last_name}}"disabled>
										  </div>
									</div>
									
								  </div>
								  
								  <div class="row">
									<div class="col-md-6 col-lg-6">
										<label>Mobile Phone</label>
										<input type="tel" class="form-control numberOnly" placeholder="Mobile Phone" value="{{$data->mobile}}"disabled>
									</div>
									
									<div class="col-md-6  col-lg-6">
										<div class="form-group">
											<label>Email</label>
											<input type="text" class="form-control" placeholder="Email Address" value="{{$data->email}}"disabled>
										  </div>
									</div>

									<div class="col-md-6  col-lg-6">
										<div class="form-group">
											<label>Designation</label>
											<input type="text" class="form-control" placeholder="Designation" value="{{$data->designation}}"disabled>
										  </div>
									</div>

									<div class="col-md-6  col-lg-6">
										<div class="form-group">
											<label>Is your office located at the business address given on prior screen?</label>
											<input type="text" class="form-control" placeholder="" value="{{$data->office_location}}"disabled>
										  </div>
									</div>
@if($data->office_location == 'No')
									<div class="col-md-6  col-lg-4">
										<div class="form-group">
											<label>Office Address 1</label>
											<input type="text" class="form-control" placeholder="" value="{{$data->office_address1}}"disabled>
										  </div>
									</div>

									<div class="col-md-6  col-lg-4">
										<div class="form-group">
											<label>Office Address 2</label>
											<input type="text" class="form-control" placeholder="" value="{{$data->office_address2}}"disabled>
										  </div>
									</div>

									<div class="col-md-6  col-lg-4">
										<div class="form-group">
											<label>Office Address 3</label>
											<input type="text" class="form-control" placeholder="" value="{{$data->office_address3}}"disabled>
										  </div>
									</div>

									<div class="col-md-6  col-lg-4">
										<div class="form-group">
											<label>Office Country</label>
											<input type="text" class="form-control" placeholder="" value="{{$data->ocName}}"disabled>
										  </div>
									</div>

									<div class="col-md-6  col-lg-4">
										<div class="form-group">
											<label>Office State</label>
											<input type="text" class="form-control" placeholder="" value="{{$data->osName}}"disabled>
										  </div>
									</div>

									<div class="col-md-6  col-lg-4">
										<div class="form-group">
											<label>Office City</label>
											<input type="text" class="form-control" placeholder="" value="{{$data->ociName}}"disabled>
										  </div>
									</div>

									<div class="col-md-6  col-lg-4">
										<div class="form-group">
											<label>Office Postal Code</label>
											<input type="text" class="form-control" placeholder="" value="{{$data->office_postal_code}}"disabled>
										  </div>
									</div>
									@endif
									
									<div class="col-lg-12">
										
										
										
										<div class="form-group">
											<div class="row">
												<div class="col-md-6 col-lg-6">
													<label>Is your business also registered as an insurance agency?</label>
													 <input type="text" class="form-control" placeholder="Email Address" value="{{$data->agent}}"disabled>
												</div>
												
												
											</div>
											
										</div>
									</div>

									
								  </div>
								  
								 
								  
								
								</div>
								
						@if($data->agent == 'yes')		
								<!-- register form step 3-->
								<div id="register-part-3" class="content mt-5">
								  <h3 class="color-blue">License / ID</h3>
								  <hr class="pb-2">
								 
									 

									<div class="row">
										<div class="col-lg-6">
										<div class="form-group">
											<label>Insurance License </label>
											<input type="text" class="form-control" placeholder="License Number" name="license_number" id="license_number" value="{{$data->license_number}}"disabled>
										  </div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>License Type </label>
											<input type="text" class="form-control" placeholder="License Type" name="license_type_opt" id="license_type_opt" value="{{$data->license_type_opt}}"disabled>
										  </div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>License Status </label>
											<input type="text" class="form-control" placeholder="License Number" name="license_number" id="license_number" value="{{$data->license_status}}"disabled>
										  </div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>Expiration Date </label>
											<input type="text" class="form-control" placeholder="MM/YY" name="license_expiry_date" id="license_expiry_date" value="{{$data->license_expiry_date}}"disabled>
										  </div>
									</div>
									</div>

									<div class="row">
									
									<div class="col-lg-6">
										<div class="form-group">
											<label>Designated Home State </label>
											<input type="text" class="form-control" placeholder="Designated Home State" name="license_home_state" id="license_home_state" value="{{$data->stateName}}"disabled>
										  </div>
									</div>
@foreach($docDetails as $docData)
@if($docData->document_type == "idproof")
									<div class="col-lg-6">
										<div class="form-group">
											<label for="exampleInputFile">Required Document<span class="requiredSymbol">*</span></label>
											<div class="input-group">
											  
											  	
											  	
											  	<a target="_blank" alt="View Document" title="View Document" href="{{asset('storage/app/'.$docData->doucment_path)}}" class="btn"><i class="fa fa-eye"></i> </a>
											  	<!--<a alt="Delete Document" title="Delete Document" href="#" onclick="removeDocument('{{$docData->document_id}}')" class="btn"><i class="fa fa-trash"></i> </a>-->

											  
											 
											</div>
											
										  </div>
									</div>
										@endif
											 
							 @endforeach		
									</div>
								
								</div>
								@endif
								
								<!-- register form step 4-->
								<div id="register-part-4" class="content mt-5">
								  <h3 class="color-blue">Business Overview</h3>
								  <hr class="pb-2">
								  <div class="row">								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Month/Year established</label>
											<input type="text" class="form-control" placeholder="ID Proof / State issued Licenses" value="{{$data->established_details}}"disabled>
										  </div>
									</div>
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Number of Funeral Directors/Embalmers on staff</label>	
											<input type="text" class="form-control" placeholder="Number of Funeral Directors/Embalmers on staff" value="{{$data->funeral_number}}"disabled>
										</div>
									</div>
								  </div>
								  
								  <div class="row">								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Number of casketed funerals annually</label>
											<input type="text" class="form-control" placeholder="Number of casketed funerals annually" value="{{$data->casked_funeral}}"disabled>
										  </div>
									</div>
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Number of cremations annually</label>	
											<input type="text" class="form-control" placeholder="Email Address" value="{{$data->cremations_annually}}"disabled>					
										</div>
									</div>
								  </div>
								  
								  <div class="row">								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Annual preneed volume</label>
											<div class="input-group">
												<div class="input-group-prepend">
												<span class="input-group-text"><b>$</b></span>
												</div>
												<input type="text" class="form-control" placeholder="Annual preneed volume" value="{{$data->annual_preneed_volume}}"disabled>

											</div>
											
										  </div>
									</div>
									
									<!--<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label for="exampleInputFile">Required Document Uploads</label>
											<div class="input-group">
											  <div class="custom-file">
												<input type="file" class="custom-file-input" id="exampleInputFile">
												<label class="custom-file-label" for="exampleInputFile">Choose file</label>
											  </div>
											  <div class="input-group-append">
												<span class="input-group-text">Upload</span>
											  </div>
											</div>
											
										  </div>
									</div>-->
								  </div>
@foreach($docDetails as $docData)
								   <div class="row">								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											
											
											  	@if($docData->document_type == "business")
											  <label	>{{$docData->document_name}}</label>
											  	@endif
											 
										  </div>
									</div>
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label></label>
											
											  	@if($docData->document_type == "business")
											  	
											  		
											  	<a target="_blank" alt="View Document" title="View Document" href="{{asset('storage/app/'.$docData->doucment_path)}}" class="btn"><i class="fa fa-eye"></i> </a>
											  	<!--<a alt="Delete Document" title="Delete Document"  href="#" onclick="removeDocument('{{$docData->document_id}}')" class="btn"><i class="fa fa-trash"></i> </a>-->

											  		
											  	@endif
											  
										  </div>
									</div>

								</div>

								 @endforeach
								 
								  
								</div>
								
								
							<a href="{{route('edit-profile')}}" class="mt-4 btn btn-lg btn-primary ">EDIT DETAILS</a>
							
						  </div>
						  <!-- /.card-body -->
						  
						</div>
						<!-- /.card -->
					  </div>
					</div>
				
				<!-- register form stepper -->
				
			</div>
		
	</div>
	@endforeach
	@include('include.footer')

