$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });



var regExp = /[0-9]/;
$(document).ready(function(){

	
      $(".numberOnly").on('keydown keyup blur focus', function(e) {

        var value =e.key;
        console.log(e.which);
        /*var ascii=value.charCodeAt(0);
        $('textarea').append(ascii);
        $('textarea').append(value);
        console.log(e);*/
        // Only numbers
        if (!regExp.test(value)
          && e.which != 8   // backspace
          && e.which != 9		// tab
          && e.which != 46  // delete
          && (e.which < 37  // arrow keys
            || e.which > 40)) {
              e.preventDefault();
              return false;
        }
      });
      
      $('.alphabetsOnly').on('keydown keyup blur focus', function(event) {
        var inputValue = event.which;
                        //Allow letters, white space, backspace and tab. 
                        //Backspace ASCII = 8 
                        //Tab ASCII = 9 
                        if (!(inputValue >= 65 && inputValue <= 123)
                            && (inputValue != 32 && inputValue != 0)
                            && (inputValue != 48 && inputValue != 8)
                            && (inputValue != 9)){
                                event.preventDefault(); 
                        }
                        console.log(inputValue);
    });

	

	$('input[name="license_expiry_date"]').datepicker( {
	    format: "mm/yyyy",
	    viewMode: "months", 
	    minViewMode: "months",
	    startDate: new Date()
	});

	$('input[name="established_details"]').datepicker( {
	    format: "mm/yyyy",
	    viewMode: "months", 
	    minViewMode: "months",
	    endDate: '+0d',
	});

	$('.applicationDate').datepicker( {
	    
	});
	
	
	//update expiry date based on status
$('#license_status').on('change', function() {
		var selectedVal = $('#license_status').val();
		$('#license_expiry_date').val('');
		$( 'input[name="license_expiry_date"]' ).datepicker("destroy");

		if(selectedVal == "active"){
				$('input[name="license_expiry_date"]').datepicker( {
				    format: "mm/yyyy",
				    viewMode: "months", 
				    minViewMode: "months",
				    startDate: new Date()
				});
		}
		else{
			console.log(selectedVal);
				$('input[name="license_expiry_date"]').datepicker( {
				    format: "mm/yyyy",
				    viewMode: "months", 
				    minViewMode: "months",
				   endDate: new Date()

				});
		}

		

});

	//Load State details based on country change
	$('#countryList').on('change', function() {
	       var countryId = $(this).val();
	      
	        $.ajax({
	            type: "POST",
	            url: "/state_details",
	            dataType: 'json', 
  				encode  : true,
  				 data: {
  				 	'countryId':countryId
  				 },
  				 beforeSend: function() {
                   $.blockUI({ css: { 
			            border: 'none', 
			            padding: '15px', 
			            backgroundColor: '#000', 
			            '-webkit-border-radius': '10px', 
			            '-moz-border-radius': '10px', 
			            opacity: .5, 
			            color: '#fff' 
			        } }); 
                },
	            success: function (data) {

	             	var html = '<option value="">Select State</option>';
	             	for(res in data){
	             		html += '<option value="'+data[res]["id"]+'">'+data[res]["name"]+'</option>'
	             		
	             	}
	             	$('#stateList').html(html);
	             	$.unblockUI();
	            },
	            error: function (data) {
	               
	            }
	        });
	});



	//Load City details based on state change
	$('#stateList').on('change', function() {
	       var stateId = $(this).val();
	      
	        $.ajax({
	            type: "POST",
	            url: "/city_details",
	            dataType: 'json', 
  				encode  : true,
  				 data: {
  				 	'stateId':stateId
  				 },
  				 beforeSend: function() {
                   $.blockUI({ css: { 
			            border: 'none', 
			            padding: '15px', 
			            backgroundColor: '#000', 
			            '-webkit-border-radius': '10px', 
			            '-moz-border-radius': '10px', 
			            opacity: .5, 
			            color: '#fff' 
			        } }); 
                },
	            success: function (data) {
	             	
	             	var html = '<option value="">Select City</option>';
	             	for(res in data){
	             		html += '<option value="'+data[res]["id"]+'">'+data[res]["name"]+'</option>'
	             		
	             	}
	             	$('#cityList').html(html);
	             	$.unblockUI();
	            },
	            error: function (data) {
	               
	            }
	        });
	});


	//Load State details based on country change
	$('#officeCountryList').on('change', function() {
	       var countryId = $(this).val();
	      
	        $.ajax({
	            type: "POST",
	            url: "/state_details",
	            dataType: 'json', 
  				encode  : true,
  				 data: {
  				 	'countryId':countryId
  				 },
  				 beforeSend: function() {
                   $.blockUI({ css: { 
			            border: 'none', 
			            padding: '15px', 
			            backgroundColor: '#000', 
			            '-webkit-border-radius': '10px', 
			            '-moz-border-radius': '10px', 
			            opacity: .5, 
			            color: '#fff' 
			        } }); 
                },
	            success: function (data) {

	             	var html = '<option value="">Select State</option>';
	             	for(res in data){
	             		html += '<option value="'+data[res]["id"]+'">'+data[res]["name"]+'</option>'
	             		
	             	}
	             	$('#officeStateList').html(html);
	             	$.unblockUI();
	            },
	            error: function (data) {
	               
	            }
	        });
	});



	//Load City details based on state change
	$('#officeStateList').on('change', function() {
	       var stateId = $(this).val();
	      
	        $.ajax({
	            type: "POST",
	            url: "/city_details",
	            dataType: 'json', 
  				encode  : true,
  				 data: {
  				 	'stateId':stateId
  				 },
  				 beforeSend: function() {
                   $.blockUI({ css: { 
			            border: 'none', 
			            padding: '15px', 
			            backgroundColor: '#000', 
			            '-webkit-border-radius': '10px', 
			            '-moz-border-radius': '10px', 
			            opacity: .5, 
			            color: '#fff' 
			        } }); 
                },
	            success: function (data) {
	             	
	             	var html = '<option value="">Select City</option>';
	             	for(res in data){
	             		html += '<option value="'+data[res]["id"]+'">'+data[res]["name"]+'</option>'
	             		
	             	}
	             	$('#officeCityList').html(html);
	             	$.unblockUI();
	            },
	            error: function (data) {
	               
	            }
	        });
	});

	//Checking Duplicate email id
	$('#email').focusout(function() {
		//console.log($(this).val());
		var emailId = $(this).val();
		$.ajax({
			type: "POST",
			url: "/unique_email",
			dataType: 'json', 
			  encode  : true,
			   data: {
				   'emailid':emailId
			   },
			   beforeSend: function() {
			   $.blockUI({ css: { 
					border: 'none', 
					padding: '15px', 
					backgroundColor: '#000', 
					'-webkit-border-radius': '10px', 
					'-moz-border-radius': '10px', 
					opacity: .5, 
					color: '#fff' 
				} }); 
			},
			success: function (data) {
				 console.log(data);
				 if(data == 1){
					$('#email').addClass('is-invalid');
					$('.uniqueEmailError').val('1');
					$('.emailErrorTxt').html('Email Id already in use. Please choose different Email Id');
					$('.finalErr').html('Email Id already in use. Please choose different Email Id');
				 }
				 else{
					$('#email').removeClass('is-invalid');
					$('.uniqueEmailError').val('0');
					$('.emailErrorTxt').html('');
					$('.finalErr').html('');
				 }
				
				 $.unblockUI();
			},
			error: function (data) {
			   
			}
		});

	  });



	//Office location change
	$('#office_location').on('change', function() {
			$('.officeLocationDiv').css('display','none');
			$('#office_address1').val('');
			$('#office_address2').val('');
			$('#office_address3').val('');
			$('#officeCountryList').val('');
			$('#officeStateList').val('');
			$('#officeCityList').val('');
			$('#office_postal_code').val('');
			$
	       var selectedVal = $(this).val();
	      	if(selectedVal == "No"){
	      		$('.officeLocationDiv').css('display','block');
	      	}
	        
	});

});

function partnerFormValidation(){

	
	$('.error').css('display','none');


	$('#register-part-1-trigger .bs-stepper-circle').css('background-color','#6c757d');
	$('#register-part-1-trigger .bs-stepper-label').css('color','#004367');

	$('#register-part-2-trigger .bs-stepper-circle').css('background-color','#6c757d');
	$('#register-part-2-trigger .bs-stepper-label').css('color','#004367');

	$('#register-part-4-trigger .bs-stepper-circle').css('background-color','#004367');
	$('#register-part-4-trigger .bs-stepper-label').css('color','#004367');

	var nameTab = '';
	var businessTab = '';
	var contactTab = '';

	

	 if($('#business_name').val()=='' || $('#address1').val()=='' || $('#address2').val()=='' || $('#cityList').val()=='' || $('#stateList').val()=='' || $('#countryList').val()=='' || $('#postal_code').val()=='' || $('#telephone').val()=='' || $('#title').val()=='' || $('#first_name').val()==''  || $('#last_name').val()=='' || $('#mobile').val()=='' || $('#email').val()=='' || $('#agent').val()==''  || $('#established_details').val()=='' || $('#funeral_number').val()=='' || $('#casked_funeral').val()=='' || $('#cremations_annually').val()=='' || $('#license_type').val()=='' || $('#annual_preneed_volume').val()=='' ){

	 		 if ($('#business_name').val()==''){
		        $('#business_name').addClass('is-invalid');
		        nameTab = '1';
		      }else{ $('#business_name').removeClass('is-invalid');}

		     if ($('#address1').val()==''){
		        $('#address1').addClass('is-invalid');
		        nameTab = '1';
		      }else{ $('#address1').removeClass('is-invalid');}

		      if ($('#address2').val()==''){
		       $('#address2').addClass('is-invalid');
		       nameTab = '1';
		      }else{ $('#address2').removeClass('is-invalid');}

		      if ($('#cityList').val()==''){
		        $('#cityList').addClass('is-invalid');
		        nameTab = '1';
		      }else{ $('#cityList').removeClass('is-invalid');}

		      if ($('#stateList').val()==''){
		      $('#stateList').addClass('is-invalid');
		      nameTab = '1';
		      }else{ $('#stateList').removeClass('is-invalid');}

		      if ($('#countryList').val()==''){
		        $('#countryList').addClass('is-invalid');
		        nameTab = '1';
		      }else{  $('#countryList').removeClass('is-invalid');}

		      if ($('#postal_code').val()==''){
		       $('#postal_code').addClass('is-invalid');
		       nameTab = '1';
		      }else{  $('#postal_code').removeClass('is-invalid');}

		      if ($('#telephone').val()==''){
		       $('#telephone').addClass('is-invalid');
		       nameTab = '1';
		      }else{  $('#telephone').removeClass('is-invalid');}

		      if ($('#license_type').val()==''){
		       $('#license_type').addClass('is-invalid');
		       nameTab = '1';
		      }else{ $('#license_type').removeClass('is-invalid');}

		      if ($('#title').val()==''){
		       $('#title').addClass('is-invalid');
		      }else{  $('#title').removeClass('is-invalid');}

		      if ($('#first_name').val()==''){
		       $('#first_name').addClass('is-invalid');
		       contactTab = '1';
		      }else{ $('#first_name').removeClass('is-invalid');}

		     
		      if ($('#last_name').val()==''){
		       $('#last_name').addClass('is-invalid');
		       contactTab = '1';
		      }else{ $('#last_name').removeClass('is-invalid');}

		      if ($('#mobile').val()==''){
		       $('#mobile').addClass('is-invalid');
		       contactTab = '1';
		      }else{ $('#mobile').removeClass('is-invalid');}

		      if ($('#email').val()==''){
		       $('#email').addClass('is-invalid');
		       contactTab = '1';
		      }else{ $('#email').removeClass('is-invalid');}

		            



		      if ($('#agent').val()==''){
		       $('#agent').addClass('is-invalid');
		      }else{ $('#agent').removeClass('is-invalid');}

		      
		      if ($('#established_details').val()==''){
		       $('#established_details').addClass('is-invalid');
		       businessTab = '1';
		      }else{  $('#established_details').removeClass('is-invalid');}

		      if ($('#funeral_number').val()==''){
		      $('#funeral_number').addClass('is-invalid');
		      businessTab = '1';
		      }else{  $('#funeral_number').removeClass('is-invalid');}

		      if ($('#casked_funeral').val()==''){
		       $('#casked_funeral').addClass('is-invalid');
		       businessTab = '1';
		      }else{ $('#casked_funeral').removeClass('is-invalid');}

		      if ($('#cremations_annually').val()==''){
		       $('#cremations_annually').addClass('is-invalid');
		       businessTab = '1';
		      }else{  $('#cremations_annually').removeClass('is-invalid');}

		      if ($('#annual_preneed_volume').val()==''){
		     $('#annual_preneed_volume').addClass('is-invalid');
		      }else{ $('#annual_preneed_volume').removeClass('is-invalid');}

		      if(nameTab == '1'){
					 	$('#register-part-1-trigger .bs-stepper-circle').css('background-color','#dc3545');
					 	$('#register-part-1-trigger .bs-stepper-label').css('color','#dc3545');
					 }

					 if(contactTab == '1'){
					 	$('#register-part-2-trigger .bs-stepper-circle').css('background-color','#dc3545');
					 	$('#register-part-2-trigger .bs-stepper-label').css('color','#dc3545');
					 }

					 if(businessTab == '1'){
					 	$('#register-part-4-trigger .bs-stepper-circle').css('background-color','#dc3545');
					 	$('#register-part-4-trigger .bs-stepper-label').css('color','#dc3545');
					 }

	 	$('.finalErr').css('display','inline');
	 	return false;
	 }

	 if($('#office_location').val() == "No"){
		      		if ($('#office_address1').val()==''){
				       $('#office_address1').addClass('is-invalid');
				       contactTab = '1';

				      }else{ $('#office_address1').removeClass('is-invalid');}

				      if ($('#office_address1').val()==''){
				       $('#office_address1').addClass('is-invalid');
				       contactTab = '1';
				      }else{ $('#office_address1').removeClass('is-invalid');}

				      if ($('#officeCountryList').val()==''){
				       $('#officeCountryList').addClass('is-invalid');
				       contactTab = '1';
				      }else{ $('#officeCountryList').removeClass('is-invalid');}

				      if ($('#officeStateList').val()==''){
				       $('#officeStateList').addClass('is-invalid');
				       contactTab = '1';
				      }else{ $('#officeStateList').removeClass('is-invalid');}

				      if ($('#officeCityList').val()==''){
				       $('#officeCityList').addClass('is-invalid');
				       contactTab = '1';
				      }else{ $('#officeCityList').removeClass('is-invalid');}

				      if ($('#office_postal_code').val()==''){
				       $('#office_postal_code').addClass('is-invalid');
				       contactTab = '1';
				      }else{ $('#office_postal_code').removeClass('is-invalid');}
				      if(contactTab == '1'){

							 	$('#register-part-2-trigger .bs-stepper-circle').css('background-color','#dc3545');
							 	$('#register-part-2-trigger .bs-stepper-label').css('color','#dc3545');
							 	$('.finalErr').css('display','inline');
							 	return false;
							 }

							 console.log("sdfsd");

							
		      }

		      if($('input[name=agent]:checked').val() == "yes"){
		      			if ($('#license_number').val()==''){
					       $('#license_number').addClass('is-invalid');
					       contactTab = '1';
					      }else{ $('#license_number').removeClass('is-invalid');}

					      if ($('#license_status').val()==''){
					       $('#license_status').addClass('is-invalid');
					       contactTab = '1';
					      }else{ $('#license_status').removeClass('is-invalid');}

					      if ($('#license_expiry_date').val()==''){
					       $('#license_expiry_date').addClass('is-invalid');
					       contactTab = '1';
					      }else{ $('#license_expiry_date').removeClass('is-invalid');}

					      if ($('#license_home_state').val()==''){
					       $('#license_home_state').addClass('is-invalid');
					       contactTab = '1';
					      }else{ $('#license_home_state').removeClass('is-invalid');}

					      if(contactTab == '1'){
							 	$('#register-part-2-trigger .bs-stepper-circle').css('background-color','#dc3545');
							 	$('#register-part-2-trigger .bs-stepper-label').css('color','#dc3545');
							 	$('.finalErr').css('display','inline');
							 	return false;
							 }

							 
		      }






	 if($('#formType').val()=='insert'){
	 	if($('#email').val()!=''){

		 	var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i

			if(!pattern.test($('#email').val()))
			{
				$('#email').addClass('is-invalid');
//			  $('.emailErr').css('display','inline');
		 		return false;
			}

			var emailErrorVal = $('.uniqueEmailError').val();
			$('.finalErr').html('');
			if(emailErrorVal == '1'){
				$('.finalErr').html('Email Id already in use. Please choose different Email Id');

				return false;
			}
		 }

		 if(!$("#bgChk").is(':checked')){
			$('.error').css('display','block');
			$('.bgChkError').html("Please accept to perform background check");
			return false;
		}
	 }

	 

	 
}





function agencyFormValidation(){

	$('.error').css('display','none');


	$('#register-part-1-trigger .bs-stepper-circle').css('background-color','#6c757d');
	$('#register-part-1-trigger .bs-stepper-label').css('color','#004367');

	$('#register-part-2-trigger .bs-stepper-circle').css('background-color','#6c757d');
	$('#register-part-2-trigger .bs-stepper-label').css('color','#004367');

	$('#register-part-3-trigger .bs-stepper-circle').css('background-color','#6c757d');
	$('#register-part-3-trigger .bs-stepper-label').css('color','#004367');

	$('#register-part-4-trigger .bs-stepper-circle').css('background-color','#004367');
	$('#register-part-4-trigger .bs-stepper-label').css('color','#004367');

	var nameTab = '';
	var managerTab = '';
	var licenceTab = '';
	var businessTab = '';

	 if($('#business_name').val()=='' || $('#address1').val()=='' || $('#address2').val()=='' || $('#cityList').val()=='' || $('#stateList').val()=='' || $('#countryList').val()=='' || $('#postal_code').val()=='' || $('#telephone').val()=='' || $('#title').val()=='' || $('#first_name').val()==''  || $('#last_name').val()=='' || $('#mobile').val()=='' || $('#email').val()=='' || $('#agent').val()=='' || $('#id_proof').val()=='' || $('#location_of_services').val()=='' || $('#number_of_agents').val()=='' || $('#business_turn_over').val()=='' || $('#number_of_years_in_business').val()=='' || $('#lead_to_prospect_ratio').val()=='' || $('#license_type').val()=='' || $('#license_number').val()=='' || $('#license_status').val()=='' || $('#license_expiry_date').val()=='' || $('#license_home_state').val()=='' ){

	 		 if ($('#business_name').val()==''){
		        $('#business_name').addClass('is-invalid');
		        nameTab = '1';
		      }else{ $('#business_name').removeClass('is-invalid');}

		     if ($('#address1').val()==''){
		        $('#address1').addClass('is-invalid');
		        nameTab = '1';
		      }else{ $('#address1').removeClass('is-invalid');}

		      if ($('#address2').val()==''){
		       $('#address2').addClass('is-invalid');
		       nameTab = '1';
		      }else{ $('#address2').removeClass('is-invalid');}

		      if ($('#cityList').val()==''){
		        $('#cityList').addClass('is-invalid');
		        nameTab = '1';
		      }else{ $('#cityList').removeClass('is-invalid');}

		      if ($('#stateList').val()==''){
		      $('#stateList').addClass('is-invalid');
		      nameTab = '1';
		      }else{ $('#stateList').removeClass('is-invalid');}

		      if ($('#countryList').val()==''){
		        $('#countryList').addClass('is-invalid');
		        nameTab = '1';
		      }else{  $('#countryList').removeClass('is-invalid');}

		      if ($('#postal_code').val()==''){
		       $('#postal_code').addClass('is-invalid');
		       nameTab = '1';
		      }else{  $('#postal_code').removeClass('is-invalid');}

		      if ($('#telephone').val()==''){
		       $('#telephone').addClass('is-invalid');
		       nameTab = '1';
		      }else{  $('#telephone').removeClass('is-invalid');}

		      if ($('#title').val()==''){
		       $('#title').addClass('is-invalid');
		      }else{  $('#title').removeClass('is-invalid');}

		      if ($('#first_name').val()==''){
		       $('#first_name').addClass('is-invalid');
		       managerTab = '1';
		      }else{ $('#first_name').removeClass('is-invalid');}

		     
		      if ($('#last_name').val()==''){
		       $('#last_name').addClass('is-invalid');
		       managerTab = '1';
		      }else{ $('#last_name').removeClass('is-invalid');}

		      if ($('#mobile').val()==''){
		       $('#mobile').addClass('is-invalid');
		       managerTab = '1';
		      }else{ $('#mobile').removeClass('is-invalid');}

		      if ($('#email').val()==''){
		       $('#email').addClass('is-invalid');
		       managerTab = '1';
		      }else{ $('#email').removeClass('is-invalid');}

		      if ($('#agent').val()==''){
		       $('#agent').addClass('is-invalid');
		      }else{ $('#agent').removeClass('is-invalid');}

		      if ($('#license_type').val()==''){
		       $('#license_type').addClass('is-invalid');
		       licenceTab = '1';
		      }else{ $('#license_type').removeClass('is-invalid');}

		       if ($('#license_number').val()==''){
		       $('#license_number').addClass('is-invalid');
		       licenceTab = '1';
		      }else{ $('#license_number').removeClass('is-invalid');}

		       if ($('#license_status').val()==''){
		       $('#license_status').addClass('is-invalid');
		      }else{ $('#license_status').removeClass('is-invalid');}

		       if ($('#license_expiry_date').val()==''){
		       $('#license_expiry_date').addClass('is-invalid');
		       licenceTab = '1';
		      }else{ $('#license_expiry_date').removeClass('is-invalid');}

		       if ($('#license_home_state').val()==''){
		       $('#license_home_state').addClass('is-invalid');
		       licenceTab = '1';
		      }else{ $('#license_home_state').removeClass('is-invalid');}

		      if ($('#location_of_services').val()==''){
		       $('#location_of_services').addClass('is-invalid');
		       businessTab = '1';
		      }else{  $('#location_of_services').removeClass('is-invalid');}

		      if ($('#number_of_agents').val()==''){
		      $('#number_of_agents').addClass('is-invalid');
		      businessTab = '1';
		      }else{  $('#number_of_agents').removeClass('is-invalid');}

		      if ($('#business_turn_over').val()==''){
		       $('#business_turn_over').addClass('is-invalid');
		      }else{ $('#business_turn_over').removeClass('is-invalid');}

		      if ($('#number_of_years_in_business').val()==''){
		       $('#number_of_years_in_business').addClass('is-invalid');
		       businessTab = '1';
		      }else{  $('#number_of_years_in_business').removeClass('is-invalid');}

		      if ($('#lead_to_prospect_ratio').val()==''){
		     $('#lead_to_prospect_ratio').addClass('is-invalid');
		    	 businessTab = '1';
		      }else{ $('#lead_to_prospect_ratio').removeClass('is-invalid');}


		      if(nameTab == '1'){
					 	$('#register-part-1-trigger .bs-stepper-circle').css('background-color','#dc3545');
					 	$('#register-part-1-trigger .bs-stepper-label').css('color','#dc3545');
					 }

					 if(managerTab == '1'){
					 	$('#register-part-2-trigger .bs-stepper-circle').css('background-color','#dc3545');
					 	$('#register-part-2-trigger .bs-stepper-label').css('color','#dc3545');
					 }

					  if(licenceTab == '1'){
					 	$('#register-part-3-trigger .bs-stepper-circle').css('background-color','#dc3545');
					 	$('#register-part-3-trigger .bs-stepper-label').css('color','#dc3545');
					 }

					 if(businessTab == '1'){
					 	$('#register-part-4-trigger .bs-stepper-circle').css('background-color','#dc3545');
					 	$('#register-part-4-trigger .bs-stepper-label').css('color','#dc3545');
					 }

	 	$('.finalErr').css('display','inline');
	 	return false;
	 }

	 if($('#formType').val()=='insert'){
	 	if($('#email').val()!=''){

		 	var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i

			if(!pattern.test($('#email').val()))
			{
				$('#email').addClass('is-invalid');
//			  $('.emailErr').css('display','inline');
		 		return false;
			}

			var emailErrorVal = $('.uniqueEmailError').val();
			$('.finalErr').html('');
			if(emailErrorVal == '1'){
				$('.finalErr').html('Email Id already in use. Please choose different Email Id');

				return false;
			}
		 }
		 if(!$("#bgChk").is(':checked')){
		$('.error').css('display','block');
		$('.bgChkError').html("Please accept to perform background check");
		return false;
	}
	 }
	 
	 
}


function agentFormValidation(){
console.log
	$('.error').css('display','none');
	var nameAddressTab = '';
	var licenceTab = '';
	var businessTab = '';

	$('#register-part-1-trigger .bs-stepper-circle').css('background-color','#6c757d');
	$('#register-part-1-trigger .bs-stepper-label').css('color','#004367');

	$('#register-part-2-trigger .bs-stepper-circle').css('background-color','#6c757d');
	$('#register-part-2-trigger .bs-stepper-label').css('color','#004367');

	$('#register-part-3-trigger .bs-stepper-circle').css('background-color','#004367');
	$('#register-part-3-trigger .bs-stepper-label').css('color','#004367');

	 if( $('#address1').val()=='' || $('#address2').val()=='' || $('#cityList').val()=='' || $('#stateList').val()=='' || $('#countryList').val()=='' || $('#postal_code').val()==''  || $('#title').val()=='' || $('#first_name').val()==''  || $('#last_name').val()=='' || $('#mobile').val()=='' || $('#email').val()==''   || $('#location_of_services').val()=='' || $('#business_turn_over').val()=='' || $('#number_of_years_in_business').val()==''  || $('#license_type').val()=='' || $('#license_number').val()=='' || $('#license_status').val()=='' || $('#license_expiry_date').val()=='' || $('#license_home_state').val()=='' ){

	 		
		     if ($('#address1').val()==''){
		        $('#address1').addClass('is-invalid');
		        nameAddressTab = '1';
		      }else{ $('#address1').removeClass('is-invalid');}

		      if ($('#address2').val()==''){
		       $('#address2').addClass('is-invalid');
		        nameAddressTab = '1';
		      }else{ $('#address2').removeClass('is-invalid');}

		      if ($('#cityList').val()==''){
		        $('#cityList').addClass('is-invalid');
		        nameTab = '1';
		      }else{ $('#cityList').removeClass('is-invalid');}

		      if ($('#stateList').val()==''){
		      $('#stateList').addClass('is-invalid');
		      nameTab = '1';
		      }else{ $('#stateList').removeClass('is-invalid');}

		      if ($('#countryList').val()==''){
		        $('#countryList').addClass('is-invalid');
		        nameTab = '1';
		      }else{  $('#countryList').removeClass('is-invalid');}

		      if ($('#postal_code').val()==''){
		       $('#postal_code').addClass('is-invalid');
		        nameAddressTab = '1';
		      }else{  $('#postal_code').removeClass('is-invalid');}

		      

		      if ($('#title').val()==''){
		       $('#title').addClass('is-invalid');
		      }else{  $('#title').removeClass('is-invalid');}

		      if ($('#first_name').val()==''){
		       $('#first_name').addClass('is-invalid');
		        nameAddressTab = '1';
		      }else{ $('#first_name').removeClass('is-invalid');}

		     
		      if ($('#last_name').val()==''){
		       $('#last_name').addClass('is-invalid');
		        nameAddressTab = '1';
		      }else{ $('#last_name').removeClass('is-invalid');}

		      if ($('#mobile').val()==''){
		       $('#mobile').addClass('is-invalid');
		        nameAddressTab = '1';
		      }else{ $('#mobile').removeClass('is-invalid');}

		      if ($('#email').val()==''){
		       $('#email').addClass('is-invalid');
		        nameAddressTab = '1';
		      }else{ $('#email').removeClass('is-invalid');}

		    
		      if ($('#license_type').val()==''){
		       $('#license_type').addClass('is-invalid');
		      }else{ $('#license_type').removeClass('is-invalid');}

		       if ($('#license_number').val()==''){
		       $('#license_number').addClass('is-invalid');
		       licenceTab = '1';
		      }else{ $('#license_number').removeClass('is-invalid');}

		       if ($('#license_status').val()==''){
		       $('#license_status').addClass('is-invalid');
		      }else{ $('#license_status').removeClass('is-invalid');}

		       if ($('#license_expiry_date').val()==''){
		       $('#license_expiry_date').addClass('is-invalid');
		       licenceTab = '1';
		      }else{ $('#license_expiry_date').removeClass('is-invalid');}

		       if ($('#license_home_state').val()==''){
		       $('#license_home_state').addClass('is-invalid');
		       licenceTab = '1';
		      }else{ $('#license_home_state').removeClass('is-invalid');}

		      if ($('#location_of_services').val()==''){
		       $('#location_of_services').addClass('is-invalid');
		       businessTab = '1';
		      }else{  $('#location_of_services').removeClass('is-invalid');}

		      if ($('#business_turn_over').val()==''){
		       $('#business_turn_over').addClass('is-invalid');
		       businessTab = '1';
		      }else{ $('#business_turn_over').removeClass('is-invalid');}

		      if ($('#number_of_years_in_business').val()==''){
		       $('#number_of_years_in_business').addClass('is-invalid');
		       businessTab = '1';
		      }else{  $('#number_of_years_in_business').removeClass('is-invalid');}

		      if(nameAddressTab == '1'){
					 	$('#register-part-1-trigger .bs-stepper-circle').css('background-color','#dc3545');
					 	$('#register-part-1-trigger .bs-stepper-label').css('color','#dc3545');
					 }

					 if(licenceTab == '1'){
					 	$('#register-part-2-trigger .bs-stepper-circle').css('background-color','#dc3545');
					 	$('#register-part-2-trigger .bs-stepper-label').css('color','#dc3545');
					 }

					 if(businessTab == '1'){
					 	$('#register-part-3-trigger .bs-stepper-circle').css('background-color','#dc3545');
					 	$('#register-part-3-trigger .bs-stepper-label').css('color','#dc3545');
					 }

	 	$('.finalErr').css('display','inline');
	 	return false;
	 }

	 

	 if($('#formType').val()=='insert'){
	 	if($('#email').val()!=''){

		 	var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i

			if(!pattern.test($('#email').val()))
			{
				$('#email').addClass('is-invalid');
			//  $('.emailErr').css('display','inline');
		 		return false;
			}

			var emailErrorVal = $('.uniqueEmailError').val();
			$('.finalErr').html('');
			if(emailErrorVal == '1'){
				$('.finalErr').html('Email Id already in use. Please choose different Email Id');

				return false;
			}
		 }

		 if(!$("#bgChk").is(':checked')){
		$('.error').css('display','block');
		$('.bgChkError').html("Please accept to perform background check");
		return false;
	}
	 }
	 
	 
}





function loadStateRegion(){
	$.ajax({
	            type: "POST",
	            url: "/state_details",
	            dataType: 'json', 
  				encode  : true,
  				 data: {
  				 	'countryId':'233'
  				 },
  				 beforeSend: function() {
                   $.blockUI({ css: { 
			            border: 'none', 
			            padding: '15px', 
			            backgroundColor: '#000', 
			            '-webkit-border-radius': '10px', 
			            '-moz-border-radius': '10px', 
			            opacity: .5, 
			            color: '#fff' 
			        } }); 
                },
	            success: function (data) {

	             	var html = '<option value="">Select State</option>';
	             	for(res in data){
	             		html += '<option value="'+data[res]["id"]+'">'+data[res]["name"]+'</option>'
	             		
	             	}
	             	$('#license_home_state').html(html);
	             	$.unblockUI();
	            },
	            error: function (data) {
	               
	            }
	        });
}


function submitApplication(submitType){

	if(submitType == '1'){
		$('#submitType').val('submitted');
	}
	else{
		$('#submitType').val('saved');
	}

	$( "#applicationForm" ).submit();

}

function showUserDetails(){
	$.ajax({
	            type: "GET",
	            url: "/show_user_details",
	            dataType: 'json', 
  				encode  : true,
  				 
  				 beforeSend: function() {
                   $.blockUI({ css: { 
			            border: 'none', 
			            padding: '15px', 
			            backgroundColor: '#000', 
			            '-webkit-border-radius': '10px', 
			            '-moz-border-radius': '10px', 
			            opacity: .5, 
			            color: '#fff' 
			        } }); 
                },
	            success: function (data) {

	             	console.log(data[0]["business_name"]);
	             	$('.businessNameSpam').html(data[0]["business_name"]);
	             	if(data[0]["role"] == '1'){
	             		$('.dashboardUserRole').html("Partner");
	             		if(data[0]["pApp"] == null){
		             		$('.userDashboardAppStatus').html('<a href="/show_applicationform">Apply for Partnership</a>');

		             	}
		             	else{
		             		$('.userDashboardAppStatus').html(data[0]["pApp"]);
		             	}
	             	}

	             	if(data[0]["role"] == '2'){
	             		$('.dashboardUserRole').html("Agency");
	             		if(data[0]["agApp"] == null){
		             		$('.userDashboardAppStatus').html('<a href="/show_applicationform">Apply for Agency</a>');

		             	}
		             	else{
		             		$('.userDashboardAppStatus').html(data[0]["agApp"]);
		             	}
	             	}

	             	if(data[0]["role"] == '3'){
	             		$('.dashboardUserRole').html("Agent");
	             		if(data[0]["atApp"] == null){
		             		$('.userDashboardAppStatus').html('<a href="/show_applicationform">Apply for Agent</a>');

		             	}
		             	else{
		             		$('.userDashboardAppStatus').html(data[0]["atApp"]);
		             	}
	             	}
	             	
	             	$.unblockUI();
	            },
	            error: function (data) {
	               
	            }
	        });
}

function showPartnerAgencySubUsers(){
	$.ajax({
	            type: "POST",
	            url: "/show_sub_user_details",
	            dataType: 'json', 
  				encode  : true,
  				  data: {
  				 	'userTypeVal':'2'
  				 },
  				 beforeSend: function() {
                   $.blockUI({ css: { 
			            border: 'none', 
			            padding: '15px', 
			            backgroundColor: '#000', 
			            '-webkit-border-radius': '10px', 
			            '-moz-border-radius': '10px', 
			            opacity: .5, 
			            color: '#fff' 
			        } }); 
                },
	            success: function (data) {

	             	console.log(data);
	             	var tbl = '';
	             	var i = 1;
	             	var userRole = '';
	             	
	             	var agApp = '';
	             	var agAppbtnColor = "badge-success";
	             	
	             	for(res in data){

	             		var userTypeData  = data[res]["role"];
	             		var businessName  = data[res]["business_name"];
	             	
	             		var agApp  = data[res]["agApp"];
	             		
						var agAppbtnColor = "badge-success";
						
						

	             		if(agApp == null ){
	             			agApp = 'Not Submitted';
	             			agAppbtnColor = " btn-info btn-sm";
	             		}
	             		

	             		

	             		if(agApp == "rejected" ){
	             			
	             			agAppbtnColor = "badge-danger";
	             		}
	             		

	             		
	             		if(businessName == null){
	             			businessName = '';
	             		}
	             		
	             		if(userTypeData == '2'){
	             			userRole = "Agency";
	             		}
	             		


	             		tbl += '<tr>';
	             		tbl += '<td>'+i+'</td>';
	             		tbl += '<td>'+businessName+'</td>';
	             		tbl += '<td>'+data[res]["title"]+'. '+data[res]["first_name"]+' '+data[res]["last_name"]+'</td>';
	             		tbl += '<td>'+data[res]["cityName"]+' / '+data[res]["stateName"]+'</td>';
	             		
	             		if(data[res]["role"] == '2'){
	             			tbl += '<td style="text-transform: capitalize;"><span class="badge '+agAppbtnColor+'">'+agApp+'</span></td>';
	             		}
	             		

	             		tbl += '<td class="listActionBtn"> ';

	             		
	             		if((data[res]["role"] == '2') && ((agApp == 'submitted') || (agApp == 'approved') || (agApp == 'rejected') || (agApp == 'revision'))){
	             			tbl += '<span ><a class="btn btn-dark btn-sm" href="show_sub_applicationform/'+userTypeData+'/'+data[res]["user_id"]+'" title="Application View" alt="Application View"><i class="fas fa-tasks" aria-hidden="true"></i></a></span> ';
	             		
	             		}
	             		else{
	             			tbl += '<span ><a class="btn btn-primary btn-sm" href="show_sub_applicationform/'+userTypeData+'/'+data[res]["user_id"]+'"><i class="fas fa-plus"></i>ON BOARDING APPLICATION</a> ';
	             		}
	             		



	             		
	             		
	             		tbl += '<span ></span> ';
	             		
	             		tbl += '</td>';
	             		tbl += '<td class="listActionBtn"><a class="btn btn-success btn-sm" href="edit-sub-profile/'+userTypeData+'/'+data[res]["user_id"]+'" title="Edit Profile"> <i class="fas fa-eye"></i></a> ';

	             		
	             		
	             		

	             		
	             		/*tbl += '<span><a class="btn btn-danger btn-sm" href="#" alt="Reject"><i class="fas fa-trash" aria-hidden="true"></i></a></span>  ';
	             		tbl += '<span ><a class="btn btn-info btn-sm" href="#" title="" alt="View"><i class="fas fa-pencil-alt" aria-hidden="true"></i></a></span> ';*/
	             		
	             		tbl += '</td>';
	             		tbl += '</tr>';
	             		//console.log(data[res]["first_name"]);
	             		i++;
	             	}

	             	$('.partnerAgencyTblBody').html(tbl);

	             	$('#partnerAgencyTbl').DataTable({
				      "paging": true,
				      "lengthChange": true,
				      "searching": true,
				      "ordering": true,
				      "info": true,
				      "autoWidth": true,
				      "responsive": true,
				    });
	             	
	             	
	             	$.unblockUI();
	            },
	            error: function (data) {
	               
	            }
	        });
}



function showPartnerAgentSubUsers(){
	$.ajax({
	            type: "POST",
	            url: "/show_sub_user_details",
	            dataType: 'json', 
  				encode  : true,
  				  data: {
  				 	'userTypeVal':'3'
  				 },
  				 beforeSend: function() {
                   $.blockUI({ css: { 
			            border: 'none', 
			            padding: '15px', 
			            backgroundColor: '#000', 
			            '-webkit-border-radius': '10px', 
			            '-moz-border-radius': '10px', 
			            opacity: .5, 
			            color: '#fff' 
			        } }); 
                },
	            success: function (data) {

	             	console.log(data);
	             	var tbl = '';
	             	var i = 1;
	             	var userRole = '';
	             	
	             	var atApp = '';
	             	var atAppbtnColor = "badge-success";
	             	
	             	for(res in data){

	             		var userTypeData  = data[res]["role"];
	             		var businessName  = data[res]["business_name"];
	             	
	             		var atApp  = data[res]["atApp"];
	             		
						var atAppbtnColor = "badge-success";
						
						

	             		if(atApp == null ){
	             			atApp = 'Not Submitted';
	             			atAppbtnColor = " btn-info btn-sm";
	             		}
	             		

	             		

	             		if(atApp == "rejected" ){
	             			
	             			atAppbtnColor = "badge-danger";
	             		}
	             		

	             		
	             		if(businessName == null){
	             			businessName = '';
	             		}
	             		
	             		if(userTypeData == '2'){
	             			userRole = "Agent";
	             		}
	             		


	             		tbl += '<tr>';
	             		tbl += '<td>'+i+'</td>';
	             		
	             		tbl += '<td>'+data[res]["title"]+'. '+data[res]["first_name"]+' '+data[res]["last_name"]+'</td>';
	             		tbl += '<td>'+data[res]["cityName"]+' / '+data[res]["stateName"]+'</td>';
	             		
	             		if(data[res]["role"] == '3'){
	             			tbl += '<td style="text-transform: capitalize;"><span class="badge '+atAppbtnColor+'">'+atApp+'</span></td>';
	             		}
	             		

	             		tbl += '<td class="listActionBtn"> ';

	             		
	             		if((data[res]["role"] == '3') && ((atApp == 'submitted') || (atApp == 'approved') || (atApp == 'rejected') || (atApp == 'revision'))){
	             			tbl += '<span ><a class="btn btn-dark btn-sm" href="show_sub_applicationform/'+userTypeData+'/'+data[res]["user_id"]+'" title="Application View" alt="Application View"><i class="fas fa-tasks" aria-hidden="true"></i></a></span> ';
	             		
	             		}
	             		else{
	             			tbl += '<span ><a class="btn btn-primary btn-sm" href="show_sub_applicationform/'+userTypeData+'/'+data[res]["user_id"]+'"><i class="fas fa-plus"></i>ON BOARDING APPLICATION</a> ';
	             		}
	             		



	             		
	             		
	             		tbl += '<span ></span> ';
	             		
	             		tbl += '</td>';
	             		tbl += '<td class="listActionBtn"><a class="btn btn-success btn-sm" href="edit-sub-profile/'+userTypeData+'/'+data[res]["user_id"]+'" title="View Profile"> <i class="fas fa-eye"></i></a> ';

	             		
	             		

	             		
	             		/*tbl += '<span><a class="btn btn-danger btn-sm" href="#" alt="Reject"><i class="fas fa-trash" aria-hidden="true"></i></a></span>  ';
	             		tbl += '<span ><a class="btn btn-info btn-sm" href="#" title="" alt="View"><i class="fas fa-pencil-alt" aria-hidden="true"></i></a></span> ';*/
	             		
	             		tbl += '</td>';
	             		tbl += '</tr>';
	             		//console.log(data[res]["first_name"]);
	             		i++;
	             	}

	             	$('.partnerAgentTblBody').html(tbl);

	             	$('#partnerAgentTbl').DataTable({
				      "paging": true,
				      "lengthChange": true,
				      "searching": true,
				      "ordering": true,
				      "info": true,
				      "autoWidth": true,
				      "responsive": true,
				    });
	             	
	             	
	             	$.unblockUI();
	            },
	            error: function (data) {
	               
	            }
	        });
}


function removeDocument(id){
	//console.log(id);
	if(confirm("Are you sure you want to delete this document?")){
        $.ajax({
	            type: "POST",
	            url: "/remove_doc",
	            dataType: 'json', 
  				encode  : true,
  				 data: {
  				 	'docId':id
  				 },
  				 beforeSend: function() {
                   $.blockUI({ css: { 
			            border: 'none', 
			            padding: '15px', 
			            backgroundColor: '#000', 
			            '-webkit-border-radius': '10px', 
			            '-moz-border-radius': '10px', 
			            opacity: .5, 
			            color: '#fff' 
			        } }); 
                },
	            success: function (data) {
					$.unblockUI();
	             	console.log(data);
	             	location.reload();
	             	
	            },
	            error: function (data) {
	               
	            }
	        });
    }
    else{
       console.log("no");
    }
}