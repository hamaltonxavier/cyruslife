<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Auth;
use DB;
class AuthController extends Controller
{   
    public function __construct()
    {
      //  $this->middleware('auth');
    }
    
    public function index()
    {
         $title = 'CYRUS | Sign In';
        if (auth()->user()) {
            return redirect('/show-profile');
        }
    	return view('login', compact('title'));
    }

    public function authenticate(Request $request)
    {  
       // var_dump($request);
        //exit;
        if (auth()->user()) {
            return redirect('/show-profile');
        }
         $data = $request->all();


       $this->validate($request, [
        'email' => 'required|email',
        'password' => 'required'
        ]);

        $credentials=$request->only('email','password');
        if (Auth::attempt($credentials)) {
            //echo "success";
            
             $userRole = Auth::user()->role;

             if($userRole == "superadmin"){
                return redirect(url('/admin/users_list'))->with(['message-success'=>'Signin successfully']);
             }
             else{
                    $userActiveCount = DB::table('users')
                    ->where(['email' => $request->email,'active'=>'0'])
                    ->count();
                        if($userActiveCount != 0){
                           return redirect(url('/signin'))->with(['message-info'=>'Your account not activated. Please check your mail']);

                        }

                        $userpasswordLinkCount = DB::table('users')
                                ->where(['email' => $request->email,'active'=>'0', 'password_link'=>'0'])
                                ->count();

                        if($userpasswordLinkCount != 0){
                           return redirect(url('/signin'))->with(['message-info'=>'Password not yet set for this email id.']);

                        }

                        else{
                            return redirect('/show-profile')->with(['message-success'=>'Signin successfully']);
                        }
             }

            
            
            
        } else {
           // echo "fails";
            return redirect("/signin")->with(['message-danger'=>'Oopes! You have entered invalid credentials']);
        }

    }

    public function logout()
    {
    	if (auth()->user()) {
            Auth::logout();
        }
        return redirect('/signin')->with(['message-success'=>'You have been signed out success']);
    }


    public function forgotPasswordEmail(Request $request){

        $this->validate($request, [
        'email' => 'required|email|exists:users,email',
        
        ]);

         $emailId = $request->email;
        $token = \Str::random(64);
       // echo $token;
        $insert = DB::table('password_resets')->insert([
                'email' =>  $emailId,
                'token' => $token,
                
         ]);

        //check Account Activation logic

//        $actionLink = route('reset_new_password',['token'=>$token,'email'=>$emailId]);

        $messageData = [
                'email' => $emailId,
                'token' => $token,
              //  'actionLink'=>$actionLink
            ];


            Mail::send('forgot_password_link_email', $messageData, function($message) use ($emailId) {
            $message->to($emailId)
            ->subject("Reset your password");
            $message->from("info@cyruslife.com","Reset your password");
            });

             return redirect(url('/forgot_password'))->with(['message-success'=>'Please check your mail for reset password']);


    }

    public function updateResetPassword(Request $request){
        $this->validate($request, [
        'email' => 'required|email|exists:users,email',
        'password' => 'required|confirmed|min:6|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/', 
        
        ]);

        $checkTokken = DB::table('password_resets')->where(['email'=>$request->email,'token'=>$request->token])->first();
        if(!$checkTokken){
            return redirect(url('/reset_new_password/'.$request->token.'/'.$request->email))->with(['message-danger'=>'Invalid Token!']);
        }
        else{
            $passwordVal = Hash::make($request->password);
         $emailVal = $request->email;
         $activateStatus = DB::table('users')->where('email', $emailVal)->update(array('password' => $passwordVal));

         $deleteTokken = DB::table('password_resets')->where(['email'=>$request->email])->delete();

         if (auth()->user()) {
            Auth::logout();
        }
          return redirect(url('/signin'))->with(['message-success'=>'Password has been updated. Please login']);
        }

    }
}
