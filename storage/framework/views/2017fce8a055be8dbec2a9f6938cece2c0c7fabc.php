<?php echo $__env->make('include.admin_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>	

<div class="content-body pb-5 pt-5">
		
		<div class="container w-100">
				
				<!-- register form stepper -->
					
					<div class="row">
					  <div class="col-md-12">
						<div class="card card-default">
						  <div class="card-header">
							<h3 class="pl-4 mt-3 font-weight700 color-blue">Users</h3>
						  </div>
						  <div class="card-body p-5">
							<div class="card">
              <div class="card-header">
                <div class="row">
										<div class="col-3">
												<select class="form-control" id="userType" name="userType">
												  	<option value="">Select User Type</option>
												  	<option value="1">Partner</option>
												  	<option value="2">Agency</option>
												  	<option value="3">Agent</option>
												  </select>
										</div>
										<div class="col-3">
												<select class="form-control" id="stateList" name="stateList">
												  	<option value="">Select State</option>
												  </select>
										</div>
										<div class="col-3">
												<select class="form-control" id="cityList" name="cityList">
												  	<option value="">Select City</option>
												  </select>
										</div>
										<div class="col-3">
												<button  id="userTblFilter" onclick="showSearchResult()" class="btn btn-primary">Search</button>
										</div>
										
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="adminUsersList" class="table table-bordered table-hover" style="width:100%">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>User Type</th>
                    <th>Business Name</th>
                    <th>Name</th>
                    <th>State / City</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody class="userListBody">
                 
                  
                  
                  </tbody>
                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
							  
								
							
						  </div>
						  <!-- /.card-body -->
						  
						</div>
						<!-- /.card -->
					  </div>
					</div>
				
				<!-- register form stepper -->
				
			</div>
		
	</div>
	
	<?php echo $__env->make('include.admin_footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<script>
  $(function () {
    
    
  });
</script>
<?php /**PATH F:\xampp\htdocs\cyruslife\resources\views/admin/list_users.blade.php ENDPATH**/ ?>