<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{{ $title }}</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('public/adminlte/plugins/fontawesome-free/css/all.min.css')}}">

    <!-- Toastr -->
  <link rel="stylesheet" href="{{ asset('public/adminlte/plugins/toastr/toastr.min.css')}}">
 <!-- BS Stepper -->
  <link rel="stylesheet" href="{{ asset('public/adminlte/plugins/bs-stepper/css/bs-stepper.min.css')}}">


  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="{{ asset('public/adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">

<!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('public/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{ asset('public/adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{ asset('public/adminlte/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">

    <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('public/adminlte/dist/css/adminlte.min.css') }}">
  

   <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('public/adminlte/dist/css/bootstrap-datepicker.css') }}">
  
  <!-- custom css -->
  <link href="https://fonts.googleapis.com/css2?family=Rajdhani:wght@400;500;600&display=swap" rel="stylesheet"> 
  <link rel="stylesheet" href="{{ asset('public/adminlte/dist/css/custom-sytle.css') }}">
   <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body>

 
    
    <div class="wrapper-cl bg-cl-grey">
<!--<header class="header-landing">
        <div class="container">
            <div class="row">
                <div class="col-6 col-lg-6">
                    <div class="landing-pg-logo"><a href="{{route('landing_page')}}">
                        <img src="{{ asset('public/adminlte/dist/img/logo-white.png')}}" class="img-fluid"></a>
                    </div>
                </div>
                <div class="col-6 col-lg-6 align-self-center">
                    <div class="d-flex">
                        

                        @if(Auth::check())
                           <a href="{{route('logout')}}" class="btn btn-login ml-auto">LOG OUT</a>
                        @else
                           <a href="{{route('signin')}}" class="btn btn-login ml-auto">SIGN IN</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </header>-->

    <!-- header section start -->
    <header class="header-landing">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-3 col-lg-6">
                    <div class="landing-pg-logo">
                        <img src="{{ asset('public/adminlte/dist/img/logo-white.png')}}" class="img-fluid">
                    </div>
                </div>
                <div class="col-12 col-sm-9 col-lg-6 align-self-center">
                    
                    <div class="navbar navbar-expand-md">
                         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNavDropdown">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('/admin/users_list')}}">User List</a>
                                </li>
                               
                                <li class="nav-item dropdown">
                                    <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
                                     Add User <i class="right fas fa-angle-down"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                                      <a href="{{route('partner_add_profile')}}" class="dropdown-item">Partner</a>
                                      <div class="dropdown-divider"></div>
                                      <a href="{{route('agency_add_profile')}}" class="dropdown-item">Agency</a>
                                      <div class="dropdown-divider"></div>
                                      <a href="{{route('agent_add_profile')}}" class="dropdown-item">Agent</a>
                                      <div class="dropdown-divider"></div>
                                    </div>
                                  </li>

                                  <!--<li class="nav-item dropdown">
                                    <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
                                     Funeral Home <i class="right fas fa-angle-down"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                                      <a href="{{route('add_funeral_home')}}" class="dropdown-item">Add</a>
                                      <div class="dropdown-divider"></div>
                                      <a href="{{route('list_funeral_home')}}" class="dropdown-item">List</a>
                                      <div class="dropdown-divider"></div>
                                      
                                    </div>
                                  </li>-->

                                  
                            </ul>
                        </div>
                        
                        <ul class="navbar-nav ml-auto profile-nav">
                             
                              
                              
                              <li class="nav-item dropdown">
                                <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
                                 Hi, Admin <i class="right fas fa-angle-down"></i>
                                </a>
                                <div class="dropdown-menu profdrop dropdown-menu-lg dropdown-menu-right">
                                  
                                  <div class="dropdown-divider"></div>
                                  

                                   @if(Auth::check())
                          <a href="{{route('logout')}}" class="dropdown-item dropdown-footer">Log Out <i class="fa fa-sign-out-alt"></i></a>
                        @else
                        <a href="{{route('signin')}}" class="dropdown-item dropdown-footer">Sign In <i class="fa fa-sign-out-alt"></i></a>
                          
                        @endif
                                </div>
                              </li>
                              
                        </ul>
                    </div>
                    
                    
                    <!-- login button -->
                    
                    <!--<div class="d-flex">
                        <a href="#" class="btn btn-login ml-auto">SIGN IN</a>
                    </div> -->
                    
                    
                </div>
            </div>
        </div>
    </header>