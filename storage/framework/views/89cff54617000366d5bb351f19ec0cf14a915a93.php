<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo e($title); ?></title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo e(asset('public/adminlte/plugins/fontawesome-free/css/all.min.css')); ?>">

    <!-- Toastr -->
  <link rel="stylesheet" href="<?php echo e(asset('public/adminlte/plugins/toastr/toastr.min.css')); ?>">


  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="<?php echo e(asset('public/adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')); ?>">

  <!-- BS Stepper -->
  <link rel="stylesheet" href="<?php echo e(asset('public/adminlte/plugins/bs-stepper/css/bs-stepper.min.css')); ?>">


   <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo e(asset('public/adminlte/dist/css/adminlte.min.css')); ?>">
  
   <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo e(asset('public/adminlte/dist/css/bootstrap-datepicker.css')); ?>">
  
  <!-- custom css -->
  <link href="https://fonts.googleapis.com/css2?family=Rajdhani:wght@400;500;600&display=swap" rel="stylesheet"> 
  <link rel="stylesheet" href="<?php echo e(asset('public/adminlte/dist/css/custom-sytle.css')); ?>">
   <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
</head>

<body>

 <?php
if(Auth::check()){
$userDetail = Auth::user();
$userRole = $userDetail->role;
if($userRole == '1'){
    $userTypeVal = 'Partner';
}
else if($userRole == '2'){
    $userTypeVal = 'Agency';
}
else if($userRole == '3'){
    $userTypeVal = 'Agent';
}
else{
$userTypeVal = 'Admin';
}
}


 ?>
    
    <div class="wrapper-cl bg-cl-grey">
<header class="header-landing">
        <div class="container">
            <div class="row">
                <div class="col-6 col-lg-6">
                    <div class="landing-pg-logo"><a href="<?php echo e(route('landing_page')); ?>">
                        <img src="<?php echo e(asset('public/adminlte/dist/img/logo-white.png')); ?>" class="img-fluid"></a>
                    </div>
                </div>
                <div class="col-6 col-lg-6 align-self-center">
                    
                        

                        <?php if(Auth::check()): ?>
                           <div class="navbar navbar-expand-md">
                         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNavDropdown">
                            
                            <ul class="navbar-nav">
                                <?php if(Auth::check() && Auth::user()->role != '3'): ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo e(url('user_dashboard')); ?>">Dashboard</a>
                                </li>
                                <?php endif; ?>
                               
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo e(url('show-profile')); ?>">Edit Profile</a>
                                </li>


                               
                                <li class="nav-item dropdown">
                                    <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
                                     Application <i class="right fas fa-angle-down"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                                      <a href="<?php echo e(url('show_applicationform')); ?>" class="dropdown-item">Submit Application</a>
                                      <!--<div class="dropdown-divider"></div>
                                      <a href="#" class="dropdown-item">View Application</a>-->
                                      
                                    </div>
                                  </li>
                            </ul>
                        

                                           </div>
                        
                        <ul class="navbar-nav ml-auto profile-nav">
                             
                              
                              
                              <li class="nav-item dropdown">
                                <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
                                 Hi, <?php echo $userTypeVal?> <i class="right fas fa-angle-down"></i>
                                </a>
                                <div class="dropdown-menu profdrop dropdown-menu-lg dropdown-menu-right">
                                  <a href="#" class="dropdown-item">
                                    <!-- Message Start -->
                                   Profile
                                    <!-- Message End -->
                                  </a>
                                  <div class="dropdown-divider"></div>
                                  

                                  
                          <a href="<?php echo e(route('logout')); ?>" class="dropdown-item dropdown-footer">Log Out <i class="fa fa-sign-out-alt"></i></a>
                       
                                </div>
                              </li>
                              
                        </ul>
                    </div>
                        <?php else: ?>
                        <div class="d-flex">
                           <a href="<?php echo e(route('signin')); ?>" class="btn btn-login ml-auto">SIGN IN</a>
                           </div>
                        <?php endif; ?>
                    
                </div>
            </div>
        </div>
    </header><?php /**PATH C:\xampp\htdocs\cyruslife\resources\views/include/header.blade.php ENDPATH**/ ?>