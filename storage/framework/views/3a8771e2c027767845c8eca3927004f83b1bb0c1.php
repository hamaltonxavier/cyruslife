<?php echo $__env->make('include.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	<div class="content-body pb-5 pt-5">
		
		<div class="login-box cl-wrap ml-auto mr-auto">
			  <!--<div class="login-logo">
				<a href="../../index2.html"><b>Admin</b>LTE</a>
			  </div> -->
			  <!-- /.login-logo -->
			  <div class="card">
				<div class="card-body login-card-body">
				  
				  <div class="row equal">
					<div class="col-lg-6 align-self-center">
						<div class="login-form-wrap">
						 <h2 class="login-box-msg color-blue font-weight700  pb-2">Create New Password</h2>
							<div class="bar-line"></div>
						  <form action="<?php echo e(url('/create_new_password')); ?>" method="post">
        <?php echo csrf_field(); ?>
							<div class="form-group">
								
								<input type="hidden" name="email" class="form-control" value="<?php echo e($emailVal); ?>" >
							</div>

							<div class="form-group">
								<label>Password</label>
								<input type="password" name="password" class="form-control  <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" placeholder="Password" required >
								
							</div>
							<div class="form-group">
								<label>Confirm Password</label>
								<input type="password" name="password_confirmation" class="form-control <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" placeholder=" Confirm Password" required>
								<?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
				                    <span class="invalid-feedback" role="alert">
				                        <strong><?php echo e($message); ?></strong>
				                    </span>
				                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
							</div>
							
					
							<div class="form-group mt-4">
								<button type="submit" class="btn btn-primary btn-block btn-lg">Create</button>
							</div>

							<div class="form-group mt-4">
								Note: Password contain at-least 1 Uppercase, 1 Lowercase, 1 Numeric and 1 special character and min 6 character
							</div>
							
						  </form>


						 </div>
					</div>
					
					<div class="d-none  d-lg-block  col-lg-6 p-0" style="background: #e2f5ff;">
						<div class="login-image-wrap login-page p-0 mt-auto mb-auto">
							<img src="<?php echo e(asset('public/adminlte/dist/img/login-img-v2.jpg')); ?>" class="img-fluid">
						</div>
						<div class="login-conten-wrap">
							<h3>Welcome Back!</h3>
							<p>Your Account Has Been Activated </p>
						</div>
					</div>
				  </div>
				  
				  
				 
				  
				</div>
				<!-- /.login-card-body -->
			  </div>
			</div>
		
	</div>
	
	<?php echo $__env->make('include.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<!-- BS-Stepper -->
<script src="<?php echo e(asset('public/adminlte/plugins/bs-stepper/js/bs-stepper.min.js')); ?>"></script>
<script>

	
	  // BS-Stepper Init
	  document.addEventListener('DOMContentLoaded', function () {
		window.stepper = new Stepper(document.querySelector('.bs-stepper'))
	  })
	  
	  
</script>


</html><?php /**PATH C:\xampp\htdocs\cyruslife\resources\views/new_password.blade.php ENDPATH**/ ?>