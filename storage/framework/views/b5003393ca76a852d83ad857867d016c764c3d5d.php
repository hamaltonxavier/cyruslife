<?php echo $__env->make('include.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>	
<?php $__currentLoopData = $userDetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<div class="content-body pb-5 pt-5">
		
		<div class="container w-75">
				
				<!-- register form stepper -->
					<form id="agencyRegistrationForm" class="registrationForm" method="POST" action="<?php echo e(route('update_profile')); ?>" enctype="multipart/form-data" onsubmit="return agentFormValidation()">
  <?php echo csrf_field(); ?>
					<div class="row">
					  <div class="col-md-12">
						<div class="card card-default">
						  <div class="card-header">
							<h3 class="pl-4 mt-3 font-weight700 color-orange">Edit - Profile</h3>
						  </div>
						  <div class="card-body p-5">
							
							  
								<div id="register-part-1" class="content">
								  <h4 class="color-blue">Name and Address</h4>
								  <hr class="pb-2">
								  <input type="hidden" id="userType" name="userType" value="<?php echo e(Request::segment(2)); ?>" />
									<input type="hidden" id="userId" name="userId" value="<?php echo e(Request::segment(3)); ?>" />
										<div class="row">
											<div class="col-md-3 col-lg-2">
												<label>Salutation/Title</label>
												<select class="form-control" id="title" name="title">

											<option value="Mr" <?php if($data->title == "Mr"): ?> selected <?php endif; ?>>Mr</option>
											<option value="Mrs" <?php if($data->title == "Mrs"): ?> selected <?php endif; ?>>Mrs</option>
										</select>
											</div>
											
											<div class="col-md-9 col-lg-4">
												<div class="form-group">
													<label>First Name</label>
													<input type="text" class="form-control"  value="<?php echo e($data->first_name); ?>" id="first_name" name="first_name">
												  </div>
											</div>
											
											<div class="col-md-6 col-lg-3">
												<div class="form-group">
													<label>Middle Name</label>
													<input type="text" class="form-control"  value="<?php echo e($data->middle_name); ?>" id="middle_name" name="middle_name">
												  </div>
											</div>
											
											<div class="col-md-6 col-lg-3">
												<div class="form-group">
													<label>Last Name</label>
													<input type="text" class="form-control"  value="<?php echo e($data->last_name); ?>" id="last_name" name="last_name">
												  </div>
											</div>
											
										  </div>
										  
										  
										  <div class="row">
											<div class="col-md-6 col-lg-6">
												<label>Mobile Phone</label>
												<input type="text" class="form-control numberOnly"  value="<?php echo e($data->mobile); ?>" id="mobile" name="mobile">
											</div>
											
											<div class="col-md-6  col-lg-6">
												<div class="form-group">
													<label>Email</label>
													<label class="form-control"><?php echo e($data->email); ?></lable>
												  </div>
											</div>
										  </div>
										  
										  
										  
										  <div class="row">
											<div class="col-md-12 col-lg-4">
													 <div class="form-group">
														<label>Physical Address 1</label>
														<input type="text" class="form-control"  value="<?php echo e($data->address1); ?>" id="address1" name="address1">
													  </div>
												</div>
												
												<div class="col-md-6 col-lg-4">
													 <div class="form-group">
														<label>Physical Address 2</label>
														<input type="text" class="form-control"  value="<?php echo e($data->address2); ?>" id="address2" name="address2">
													  </div>
												</div>
												<div class="col-md-6 col-lg-4">
													 <div class="form-group">
														<label>Physical Address 3</label>
														<input type="text" class="form-control"  value="<?php echo e($data->address3); ?>" id="address3" name="address3">
													  </div>
												</div>
										  </div>
										  
										  <div class="row">
											<div class="col-md-4 col-lg-4">
													 <div class="form-group">
														<label>Country</label>
														 <select class="form-control" id="countryList" name="country">
												

													<?php $__currentLoopData = $countryDetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dataVal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<option value="<?php echo e($dataVal->id); ?>" <?php if($data->countryId == $dataVal->id): ?> selected <?php endif; ?>  ><?php echo e($dataVal->name); ?></option>
													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												  </select>
													  </div>
												</div>
												
												<div class="col-md-4 col-lg-4">
													 <div class="form-group">
														<label>State</label>
														<select class="form-control" id="stateList" name="state">
												  	<?php $__currentLoopData = $statesDetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dataVal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<option value="<?php echo e($dataVal->id); ?>" <?php if($data->stateId == $dataVal->id): ?> selected <?php endif; ?>  ><?php echo e($dataVal->name); ?></option>
													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												  </select>
													  </div>
												</div>
												<div class="col-md-4 col-lg-4">
													 <div class="form-group">
														<label>City</label>
														 <select class="form-control" id="cityList" name="city">
													<?php $__currentLoopData = $cityDetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dataVal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<option value="<?php echo e($dataVal->id); ?>" <?php if($data->cityId == $dataVal->id): ?> selected <?php endif; ?>  ><?php echo e($dataVal->name); ?></option>
													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												  </select>
													  </div>
												</div>
												<div class="col-md-4 col-lg-4">
													 <div class="form-group">
														<label>Postal Code</label>
														<input type="text" class="form-control numberOnly" maxlength="6"  value="<?php echo e($data->postal_code); ?>" id="postal_code" name="postal_code">
													  </div>
												</div>
										  </div>
										  
									
								  
								  
								  
								  
								  
								  
								</div>
								
						
								<!-- register form step 2-->
								<div id="register-part-2" class="content mt-5">
								  <h4 class="color-blue">License / ID</h4>
								  <hr class="pb-2">
								  <div class="row">
									
									
									<div class="col-lg-6">
										<div class="form-group">
											<label>Insurance License <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control numberOnly"  value="<?php echo e($data->license_number); ?>" id="license_number" name="license_number">
										  </div>
									</div>
									</div>
<div class="form-group row mb-2">
									<label class="col-11 col-sm-11 col-md-11 col-lg-4 col-form-label">License type 
									</label>
									
									<div class="col-sm-7 col-lg-7 pl-0">
										<div class="row">
										  <div class="col-lg-11">
											<div class="form-group d-flex align-items-start mb-2">
												<input type="radio" class="form-check-input ml-0" id="license_type_opt" name="license_type_opt" value="Limited" <?php echo e(($data->license_type_opt=="Limited")? "checked" : ""); ?>>>
												<label class="form-check-label pl-4">Limited (dollar amount and/or insurance type restriction) </label>
											</div>
										  </div>
										</div>
										
									
										<div class="row ">
										  <div class="col-lg-11">
											<div class="form-group d-flex align-items-start mb-2">
												<input type="radio" class="form-check-input ml-0" id="license_type_opt" name="license_type_opt" value="Full" <?php echo e(($data->license_type_opt=="Full")? "checked" : ""); ?>>
												<label class="form-check-label pl-4">Full (any life product, any dollar amount)</label>
											</div>
										  </div>
										</div>
										
										
									</div>
								</div> <!-- row end -->

									<div class="row">
									
									<div class="col-lg-6">
										<div class="form-group">
											<label>License Status <span class="requiredSymbol">*</span></label>
											<select class="form-control" id="license_status" name="license_status">
											<option value="active" <?php if($data->license_status == "active"): ?> selected <?php endif; ?>>Active</option>
											<option value="expired" <?php if($data->license_status == "expired"): ?> selected <?php endif; ?>>Expired</option>
										</select>
										  </div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>Expiration Date <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control"  value="<?php echo e($data->license_expiry_date); ?>" id="license_expiry_date" name="license_expiry_date">
										  </div>
									</div>
									</div>

									<div class="row">
									
									<div class="col-lg-6">
										<div class="form-group">
											<label>Designated Home State <span class="requiredSymbol">*</span></label>
											<!--<input type="text" class="form-control"  value="<?php echo e($data->license_home_state); ?>" id="license_home_state" name="license_home_state">-->
											<select class="form-control" id="license_home_state" name="license_home_state">
												  	<?php $__currentLoopData = $statesDetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dataVal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<option value="<?php echo e($dataVal->id); ?>" <?php if($data->license_home_state == $dataVal->id): ?> selected <?php endif; ?>  ><?php echo e($dataVal->name); ?></option>
													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												  </select>
										  </div>
									</div>
									
									</div>

								
								</div>
								
								
								<!-- register form step 3-->
								<div id="register-part-3" class="content mt-5">
								   <h4 class="color-blue">Business Overview</h4>
								  <hr class="pb-2">
								  <div class="row">								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Location of services (Regions)</label>
											<input type="text" class="form-control"  value="<?php echo e($data->location_of_services); ?>" id="location_of_services" name="location_of_services">
										  </div>
									</div>
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Business turn over (range)</label>
											<input type="text" class="form-control"  value="<?php echo e($data->business_turn_over); ?>" id="business_turn_over" name="business_turn_over">
										  </div>
									</div>
								  </div>
								  
								  <div class="row">								
								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Number of years in business</label>	
											<input type="text" class="form-control numberOnly"  value="<?php echo e($data->number_of_years_in_business); ?>" id="number_of_years_in_business" name="number_of_years_in_business">					
										</div>
									</div>
									
									<!--<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label for="exampleInputFile">Required Document Uploads</label>
											<div class="input-group">
											   <input type="file" name="idFiles[]" class="myfrm form-control">
											 
											</div>
											
										  </div>
									</div>-->
								  </div>
								  
								<!--First-->
									<div class="row increment">
								  		<div class="col-md-6 col-lg-6">
												<div class="form-group">
													<div class="input-group realprocode control-group lst" >
								  				<select class="form-control" id="" name="filetitle[]">
														<option value="Producer-Agreement">Producer Agreement</option>
														<option value="Direct-Deposit-Authorization">Direct Deposit Authorization</option>
														<option value="Producer-Compensation-Schedule">Producer Compensation Schedule</option>
														<option value="Explanation-of-Information-found-on-MIB">Explanation of Information found on MIB</option>
														<option value="Other-Supporting-Documentation">Other Supporting Documentation</option>
													</select>
											      
											    </div>
												</div>
											</div>

											<div class="col-md-6 col-lg-6">
												<div class="form-group">
													<div class="input-group realprocode control-group lst " >
								  				
											      <input type="file" name="filenames[]" class="myfrm form-control">
											      <div class="input-group-btn"> 
											        <button class="btn btn-success addFile" type="button"> <i class="fldemo glyphicon glyphicon-plus"></i>Add</button>
											      </div>
											    </div>
												</div>
											</div>
								  </div>

								<!--Second-->
							<div class="clone d-none " >
								<div class="row ">
									<div class="realprocode col-md-12 col-lg-12">
								  		<div class="col-md-6 col-lg-6" style="float: left;">
												<div class="form-group">
													<div class=" control-group lst input-group" style="margin-top:10px">
											      <select class="form-control" id="" name="filetitle[]">
														<option value="Driver-License">State-Issued Driver's License</option>
														<option value="Producer-Agreement">Producer Agreement</option>
														<option value="Direct-Deposit-Authorization">Direct Deposit Authorization</option>
														<option value="Producer-Compensation-Schedule">Producer Compensation Schedule</option>
														<option value="Explanation-of-Information-found-on-MIB">Explanation of Information found on MIB</option>
														<option value="Other-Supporting-Documentation">Other Supporting Documentation</option>
													</select>
											        
											      </div>
												</div>
											</div>

											<div class="col-md-6 col-lg-6" style="float: left;">
												<div class="form-group">
													<div class=" control-group lst input-group" style="margin-top:10px">
											      	
											        <input type="file" name="filenames[]" class="myfrm form-control">
											        <div class="input-group-btn"> 
											          <button class="btn btn-danger" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>
											        </div>
											      </div>
												</div>
											</div>
										</div>
								  </div>

								</div> 
								 
								 
								</div>
								
								
							 <button type="submit" class="mt-4 btn btn-lg btn-primary ">UPDATE DETAILS</button>
							<span style="display:none" class="error finalErr">Please fill the required fields *</span>
						  </div>
						  <!-- /.card-body -->
						  
						</div>
						<!-- /.card -->
					  </div>
					</div>
				</form>
				<!-- register form stepper -->
				
			</div>
		
	</div>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	<?php echo $__env->make('include.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	<script>
		$(document).ready(function(){
			var userController = "<?php echo e(Request::path()); ?>";
			var userTypeVal = $('#userType').val();
			var userId = $('#userId').val();
			if(userController !="edit-profile"){
				$('.registrationForm').attr('action','<?php echo e(url("update-sub-profile")); ?>/'+userTypeVal+'/'+userId);
				//$('.registrationForm').attr('method','GET');
			}
		});
	</script>
	<script type="text/javascript">
    $(document).ready(function() {
      $(".addFile").click(function(){ 
          var lsthmtl = $(".clone").html();
          $(".increment").after(lsthmtl);
      });
      $("body").on("click",".btn-danger",function(){ 
          $(this).parents(".realprocode").remove();
      });
    });
</script>

<?php /**PATH C:\xampp\htdocs\cyruslife\resources\views/agent_edit_profile.blade.php ENDPATH**/ ?>