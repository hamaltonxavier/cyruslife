@include('include.header')
	<div class="content-body pb-5 pt-5">
		
		<div class="login-box cl-wrap ml-auto mr-auto">
			  <!--<div class="login-logo">
				<a href="../../index2.html"><b>Admin</b>LTE</a>
			  </div> -->
			  <!-- /.login-logo -->
			  <div class="card">
				<div class="card-body login-card-body">
				  
				  <div class="row equal">
					<div class="col-lg-6 align-self-center">
						<div class="login-form-wrap">
						 <h2 class="login-box-msg color-blue font-weight700  pb-2">Reset Password</h2>
							<div class="bar-line"></div>
						  <form action="{{url('/update_reset_password')}}" method="post">
        @csrf

        @error('email')
    	<div class="error">{{ $errors->first('email') }}</div>
		@enderror
							<div class="form-group">
								
								<input type="hidden" name="email" class="form-control" value="{{$email}}" >
								<input type="hidden" name="token" class="form-control" value="{{$token}}" >
							</div>



							<div class="form-group">
								<label>Password</label>
								<input type="password" name="password" class="form-control  @error('password') is-invalid @enderror" placeholder="Password" required >
								
							</div>
							<div class="form-group">
								<label>Confirm Password</label>
								<input type="password" name="password_confirmation" class="form-control @error('password') is-invalid @enderror" placeholder=" Confirm Password" required>
								@error('password')
				                    <span class="invalid-feedback" role="alert">
				                        <strong>{{ $message }}</strong>
				                    </span>
				                @enderror
							</div>
							
					
							<div class="form-group mt-4">
								<button type="submit" class="btn btn-primary btn-block btn-lg">Update</button>
							</div>

							<div class="form-group mt-4">
								Note: Password contain at-least 1 Uppercase, 1 Lowercase, 1 Numeric and 1 special character and min 6 character
							</div>
							
						  </form>


						 </div>
					</div>
					
					<div class="d-none  d-lg-block  col-lg-6 p-0" style="background: #e2f5ff;">
						<div class="login-image-wrap login-page p-0 mt-auto mb-auto">
							<img src="{{ asset('public/adminlte/dist/img/login-img-v2.jpg')}}" class="img-fluid">
						</div>
						<div class="login-conten-wrap">
							<h3>Welcome Back!</h3>
							<p>Please Reset Your Password</p>
						</div>
					</div>
				  </div>
				  
				  
				 
				  
				</div>
				<!-- /.login-card-body -->
			  </div>
			</div>
		
	</div>
	
	@include('include.footer')

<!-- BS-Stepper -->
<script src="{{ asset('public/adminlte/plugins/bs-stepper/js/bs-stepper.min.js')}}"></script>
<script>

	
	  // BS-Stepper Init
	  document.addEventListener('DOMContentLoaded', function () {
		window.stepper = new Stepper(document.querySelector('.bs-stepper'))
	  })
	  
	  
</script>


</html>