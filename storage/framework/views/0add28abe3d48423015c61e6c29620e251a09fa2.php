<?php echo $__env->make('include.admin_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>	

<?php $__currentLoopData = $userDetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

	<div class="content-body pb-5 pt-5">
		
		<div class="container w-75">
				
				<!-- register form stepper -->
					
					<div class="row">
					  <div class="col-md-12">
						<div class="card card-default">
						  <div class="card-header">
							<h3 class="mt-3 font-weight700 color-blue pl-4 " style="float:left">View Profile</h3>
							<a href="<?php echo e(url('/admin/users_list')); ?>"><span class="backlList">Back to list</span></a>
						  </div>
						  <div class="card-body p-5">
							
							
								<div id="register-part-1" class="content">
								  
								  <h3 class="color-blue">Business Name and Address</h3>
								  <hr class="pb-2">
								  
								  <div class="row">
									<div class="col-md-12 col-lg-12">
										<div class="form-group">
											<label>Name of the Business</label>
											<input type="text" class="form-control" placeholder="Name of the Business" value="<?php echo e($data->business_name); ?>"disabled>
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										 <div class="form-group">
											<label>Physical Address 1</label>
											<input type="text" class="form-control"  placeholder="Address 1" value="<?php echo e($data->address1); ?>"disabled>
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										 <div class="form-group">
											<label>Physical Address 2</label>
											<input type="text" class="form-control"  placeholder="Address 2" value="<?php echo e($data->address2); ?>"disabled>
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										 <div class="form-group">
											<label>Physical Address 3</label>
											<input type="text" class="form-control"  placeholder="Address 3" value="<?php echo e($data->address3); ?>"disabled>
										  </div>
									</div>
									
									
									<div class="col-md-6 col-lg-4">
										<div class="form-group">
												  <label>Country</label>
												  <input type="text" class="form-control" placeholder="Country" value="<?php echo e($data->countryName); ?>"disabled>
												</div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										<div class="form-group">
												  <label>State</label>
												  <input type="text" class="form-control" placeholder="State" value="<?php echo e($data->stateName); ?>"disabled>
												</div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										<div class="form-group">
												  <label>City</label>
												 <input type="text" class="form-control" placeholder="City" value="<?php echo e($data->cityName); ?>"disabled>
												</div>
									</div>
									
									<div class="col-md-6  col-lg-6">
										<div class="form-group">
													<label>Postal Code</label>
													<input type="text" class="form-control"  placeholder="Postal Code" value="<?php echo e($data->postal_code); ?>"disabled>
												  </div>
									</div>
										
										
										  
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Main Business Telephone</label>
											<input type="tel" class="form-control"  placeholder="Mobile / Telephone Number" value="<?php echo e($data->telephone); ?>"disabled>
										  </div>
									</div>
										<div class="col-lg-6">
										<div class="form-group">
											<label>Business License </label>
											<input type="text" class="form-control" placeholder="Business License" name="license_type" id="license_type" value="<?php echo e($data->license_type); ?>"disabled>
										  </div>
									</div>
									</div>
								  

								</div>
								
								<!-- registration part 2-->
								<div id="register-part-2" class="content mt-5">
								  <h3 class="color-blue">Primary Contact Details</h3>
								  <hr class="pb-2">
								 
								  <div class="row">
									<div class="col-md-3 col-lg-2">
										<label>Salutation/Title</label>
										<input type="tel" class="form-control"  placeholder="Mobile / Telephone Number" value="<?php echo e($data->title); ?>"disabled>
									</div>
									
									<div class="col-md-9 col-lg-4">
										<div class="form-group">
											<label>First Name</label>
											<input type="text" class="form-control" placeholder="First Name" value="<?php echo e($data->first_name); ?>"disabled>
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-3">
										<div class="form-group">
											<label>Middle Name</label>
											<input type="text" class="form-control" placeholder="Middle Name" value="<?php echo e($data->middle_name); ?>"disabled>
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-3">
										<div class="form-group">
											<label>Last Name</label>
											<input type="text" class="form-control" placeholder="Last Name" value="<?php echo e($data->last_name); ?>"disabled>
										  </div>
									</div>
									
								  </div>
								  
								  <div class="row">
									<div class="col-md-6 col-lg-6">
										<label>Mobile Phone</label>
										<input type="tel" class="form-control" placeholder="Mobile Phone" value="<?php echo e($data->mobile); ?>"disabled>
									</div>
									
									<div class="col-md-6  col-lg-6">
										<div class="form-group">
											<label>Email</label>
											<input type="text" class="form-control" placeholder="Email Address" value="<?php echo e($data->email); ?>"disabled>
										  </div>
									</div>

									<div class="col-md-6  col-lg-6">
										<div class="form-group">
											<label>Designation</label>
											<input type="text" class="form-control" placeholder="Designation" value="<?php echo e($data->designation); ?>"disabled>
										  </div>
									</div>

									<div class="col-md-6  col-lg-6">
										<div class="form-group">
											<label>Is your office located at the business address given on prior screen?</label>
											<input type="text" class="form-control" placeholder="" value="<?php echo e($data->office_location); ?>"disabled>
										  </div>
									</div>
<?php if($data->office_location == 'No'): ?>
									<div class="col-md-6  col-lg-4">
										<div class="form-group">
											<label>Office Address 1</label>
											<input type="text" class="form-control" placeholder="" value="<?php echo e($data->office_address1); ?>"disabled>
										  </div>
									</div>

									<div class="col-md-6  col-lg-4">
										<div class="form-group">
											<label>Office Address 2</label>
											<input type="text" class="form-control" placeholder="" value="<?php echo e($data->office_address2); ?>"disabled>
										  </div>
									</div>

									<div class="col-md-6  col-lg-4">
										<div class="form-group">
											<label>Office Address 3</label>
											<input type="text" class="form-control" placeholder="" value="<?php echo e($data->office_address3); ?>"disabled>
										  </div>
									</div>

									<div class="col-md-6  col-lg-4">
										<div class="form-group">
											<label>Office Country</label>
											<input type="text" class="form-control" placeholder="" value="<?php echo e($data->ocName); ?>"disabled>
										  </div>
									</div>

									<div class="col-md-6  col-lg-4">
										<div class="form-group">
											<label>Office State</label>
											<input type="text" class="form-control" placeholder="" value="<?php echo e($data->osName); ?>"disabled>
										  </div>
									</div>

									<div class="col-md-6  col-lg-4">
										<div class="form-group">
											<label>Office City</label>
											<input type="text" class="form-control" placeholder="" value="<?php echo e($data->ociName); ?>"disabled>
										  </div>
									</div>

									<div class="col-md-6  col-lg-4">
										<div class="form-group">
											<label>Office Postal Code</label>
											<input type="text" class="form-control" placeholder="" value="<?php echo e($data->office_postal_code); ?>"disabled>
										  </div>
									</div>
									<?php endif; ?>
									
									<div class="col-lg-12">
										
										
										
										<div class="form-group">
											<div class="row">
												<div class="col-md-6 col-lg-6">
													<label>Is your business also registered as an insurance agency?</label>
													 <input type="text" class="form-control" placeholder="Email Address" value="<?php echo e($data->agent); ?>"disabled>
												</div>
												
												
											</div>
											
										</div>
									</div>

									
								  </div>
								  
								 
								  
								
								</div>
								
								
								<?php if($data->agent == 'yes'): ?>		
								<!-- register form step 3-->
								<div id="register-part-3" class="content mt-5">
								  <h3 class="color-blue">License / ID</h3>
								  <hr class="pb-2">
								 
									 

									<div class="row">
										<div class="col-lg-6">
										<div class="form-group">
											<label>Insurance License </label>
											<input type="text" class="form-control" placeholder="License Number" name="license_number" id="license_number" value="<?php echo e($data->license_number); ?>"disabled>
										  </div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>License Type </label>
											<input type="text" class="form-control" placeholder="License Type" name="license_type_opt" id="license_type_opt" value="<?php echo e($data->license_type_opt); ?>"disabled>
										  </div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>License Status <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control" placeholder="License Number" name="license_number" id="license_number" value="<?php echo e($data->license_status); ?>"disabled>
										  </div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>Expiration Date <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control" placeholder="MM/YY" name="license_expiry_date" id="license_expiry_date" value="<?php echo e($data->license_expiry_date); ?>"disabled>
										  </div>
									</div>
									</div>

									<div class="row">
									
									<div class="col-lg-6">
										<div class="form-group">
											<label>Designated Home State <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control" placeholder="Designated Home State" name="license_home_state" id="license_home_state" value="<?php echo e($data->stateName); ?>"disabled>
										  </div>
									</div>
<?php $__currentLoopData = $docDetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $docData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<?php if($docData->document_type == "idproof"): ?>
									<div class="col-lg-6">
										<div class="form-group">
											<label for="exampleInputFile">Required Document<span class="requiredSymbol">*</span></label>
											<div class="input-group">
											  
											  	
											  	
											  	<a target="_blank" alt="View Document" title="View Document" href="<?php echo e(asset('storage/app/'.$docData->doucment_path)); ?>" class="btn"><i class="fa fa-eye"></i> </a>
											  	<a alt="Delete Document" title="Delete Document" href="#" onclick="removeDocument('<?php echo e($docData->document_id); ?>')" class="btn"><i class="fa fa-trash"></i> </a>

											  
											 
											</div>
											
										  </div>
									</div>
										<?php endif; ?>
											 
							 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>		
									</div>
								
								</div>
								<?php endif; ?>
								
								
								<!-- register form step 4-->
								<div id="register-part-4" class="content mt-5">
								  <h3 class="color-blue">Business Overview</h3>
								  <hr class="pb-2">
								  <div class="row">								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Month/Year established</label>
											<input type="text" class="form-control" placeholder="ID Proof / State issued Licenses" value="<?php echo e($data->established_details); ?>"disabled>
										  </div>
									</div>
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Number of Funeral Directors/Embalmers on staff</label>	
											<input type="text" class="form-control" placeholder="Email Address" value="<?php echo e($data->funeral_number); ?>"disabled>
										</div>
									</div>
								  </div>
								  
								  <div class="row">								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Number of casketed funerals annually</label>
											<input type="text" class="form-control" placeholder="Email Address" value="<?php echo e($data->casked_funeral); ?>"disabled>
										  </div>
									</div>
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Number of cremations annually</label>	
											<input type="text" class="form-control" placeholder="Email Address" value="<?php echo e($data->cremations_annually); ?>"disabled>					
										</div>
									</div>
								  </div>
								  
								  <div class="row">								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Annual preneed volume</label>
											<div class="input-group">
												<div class="input-group-prepend">
												<span class="input-group-text"><b>$</b></span>
												</div>
												<input type="text" class="form-control" placeholder="Email Address" value="<?php echo e($data->annual_preneed_volume); ?>"disabled>

											</div>
											
										  </div>
									</div>
									
									<!--<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label for="exampleInputFile">Required Document Uploads</label>
											<div class="input-group">
											  <div class="custom-file">
												<input type="file" class="custom-file-input" id="exampleInputFile">
												<label class="custom-file-label" for="exampleInputFile">Choose file</label>
											  </div>
											  <div class="input-group-append">
												<span class="input-group-text">Upload</span>
											  </div>
											</div>
											
										  </div>
									</div>-->
								  </div>
<?php $__currentLoopData = $docDetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $docData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								   <div class="row">	
								   <?php if($docData->document_type == "business"): ?>							
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											
											
											  	
											  <label	><?php echo e($docData->document_name); ?></label>
											  	
											 
										  </div>
									</div>
									<?php endif; ?>
										<?php if($docData->document_type == "business"): ?>
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label><i class="fas fa-link fa-fw" style="color: rgba(0,0,0,0.2);"></i></label>
											
											  
											  	
											  		
											  	<a target="_blank" alt="View Document" title="View Document" href="<?php echo e(asset('storage/app/'.$docData->doucment_path)); ?>" class="btn"><i class="fa fa-eye"></i> View Document</a>
											  	

											  		
											  	
											  
										  </div>
									</div>
									<?php endif; ?>
								</div>

								 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								 
								  
								</div>
								
								
							
							
						  </div>
						  <!-- /.card-body -->
						  
						</div>
						<!-- /.card -->
					  </div>
					</div>
				
				<!-- register form stepper -->
				
			</div>
		
	</div>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	<?php echo $__env->make('include.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php /**PATH C:\xampp\htdocs\cyruslife\resources\views/admin/partner_view_profile.blade.php ENDPATH**/ ?>