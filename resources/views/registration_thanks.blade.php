@include('include.header')	
	
	<div class="content-body">
		<div class="section-register-content">
			<div class="container w-75">
			<div class="row d-flex justify-content-center">
				
				<div class="col-12 col-md-10 col-lg-10">
					<div class="registration-box bg-white text-center" style="background:#fff !important">
						<img src="{{ asset('public/adminlte/dist/img/thanks-tick.png')}}" class="img-fluid">
						<div class="h2 mt-4">Thanks for registering!</div>
						<p class="h3">User has been created successfully. Please check your mail to activate the account</p>
						
						<!--<div class="mt-2">
							<a href="#" class="btn btn-primary">Login Now / OK</a>
						</div>-->
					</div>
				</div>
				
				
				
				
			</div>
		</div>
		</div>
	</div>
	
	@include('include.footer')