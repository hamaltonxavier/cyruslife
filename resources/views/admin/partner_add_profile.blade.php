@include('include.admin_header')	



	<div class="content-body pb-5 pt-5">
		
		<div class="container w-75">
				
				<!-- register form stepper -->
					<form id="partnerRegistrationForm" class="registrationForm" method="POST" action="{{ route('add_user') }}" enctype="multipart/form-data" onsubmit="return adminAddUserValidation()" >
						
				
  @csrf
					<div class="row">
					  <div class="col-md-12">
						<div class="card card-default">
						  <div class="card-header">
							<h3 class="pl-4 mt-3 font-weight700 color-blue">Add Profile</h3>
						  </div>
						  <div class="card-body p-5">
							
							
								<div id="register-part-1" class="content">
								  
								  <h3 class="color-blue">Business Name and Address</h3>
								  <hr class="pb-2">
								  
								  <div class="row">
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Name of the Business <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control" placeholder="Name of the Business" value="" name="business_name" id="business_name" >
											<input type="hidden" id="formType" value="insert" />
											<input type="hidden" name="role" value="1" id="roleTypeVal">
										  </div>
									</div>

									<div class="col-md-6  col-lg-6">
										<div class="form-group">
											<label>Email <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control" placeholder="Email Address" id="email" name="email" value="">
											<input type="hidden" class="uniqueEmailError" value="0" />
											<span class="emailErrorTxt error" ></span>  
										</div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										 <div class="form-group">
											<label>Physical Address 1</label>
											<input type="text" class="form-control"  placeholder="Address 1" value="" name="address1" id="address1">
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										 <div class="form-group">
											<label>Physical Address 2</label>
											<input type="text" class="form-control"  placeholder="Address 2" value="" name="address2" id="address2">
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										 <div class="form-group">
											<label>Physical Address 3</label>
											<input type="text" class="form-control"  placeholder="Address 3" value="" name="address3" id="address3" >
										  </div>
									</div>
									
									
										<div class="col-md-6 col-lg-4">
									<div class="form-group">
												  <label>Country <span class="requiredSymbol">*</span></label>
												   <select class="form-control" id="countryList" name="country">
													<option value="">Select Country</option>

													@foreach($countryDetails as $data)
													<option value="{{$data->id}}">{{$data->name}}</option>
													@endforeach
												  </select>
												</div>
									</div>
									
									
									<div class="col-md-6 col-lg-4">
										<div class="form-group">
												  <label>State <span class="requiredSymbol">*</span></label>
												   <select class="form-control" id="stateList" name="state">
												  	<option value="">Select State</option>
												  </select>
												</div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										<div class="form-group">
												  <label>City <span class="requiredSymbol">*</span></label>
												  <select class="form-control" id="cityList" name="city">
													<option value="">Select City</option>
												  </select>
												</div>
									</div>
									
									<div class="col-md-6  col-lg-6">
										<div class="form-group">
													<label>Postal Code</label>
													<input type="text" class="form-control numberOnly"  maxlength="6" placeholder="Postal Code" value=""  name="postal_code" id="postal_code">
												  </div>
									</div>
										
										
										  
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Main Business Telephone</label>
											<input type="tel" class="form-control numberOnly"  placeholder="Mobile / Telephone Number" value="" name="telephone" id="telephone" >
										  </div>
									</div>
										<div class="col-lg-6">
										<div class="form-group">
											<label>Business License </label>
											<input type="text" class="form-control" placeholder="License Type" name="license_type" id="license_type" value="" >
										<!--<select class="form-control" id="license_type" name="license_type">
											<option value="" >Select License Type</option>
											<option value="Driving Licence" >Driving Licence</option>
											<option value="Passport" >Passport</option>
											<option value="Social Security Card" >Social Security Card</option>
										</select>-->
										  </div>
									</div>
									</div>
								  

								</div>
								
								<!-- registration part 2-->
								<div id="register-part-2" class="content mt-5">
								  <h3 class="color-blue">Primary Contact Details</h3>
								  <hr class="pb-2">
								 
								  <div class="row">
									<div class="col-md-3 col-lg-2">
										<label>Salutation/Title</label>
										<select class="form-control" id="title" name="title">

											<option value="Mr" >Mr</option>
											<option value="Mrs" >Mrs</option>
										</select>
									</div>
									
									<div class="col-md-9 col-lg-4">
										<div class="form-group">
											<label>First Name <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control alphabetsOnly" placeholder="First Name" value="" name="first_name" id="first_name">
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-3">
										<div class="form-group">
											<label>Middle Name</label>
											<input type="text" class="form-control alphabetsOnly" placeholder="Middle Name" value="" name="middle_name" id="middle_name">
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-3">
										<div class="form-group">
											<label>Last Name <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control alphabetsOnly" placeholder="Last Name" value="" name="last_name" id="last_name">
										  </div>
									</div>
									
								  </div>
								  
								  <div class="row">
									<div class="col-md-6 col-lg-6">
										<label>Mobile Phone</label>
										<input type="tel" class="form-control numberOnly" placeholder="Mobile Phone" value="" name="mobile" id="mobile">
									</div>
									<div class="col-md-6  col-lg-4">
										<div class="form-group">
											<label>Designation </label>
											<input type="text" class="form-control" placeholder="Designation" id="designation" name="designation" value={{ old('designation') }}>
										  </div>
									</div>
									<!--<div class="col-md-6  col-lg-4">
										<div class="form-group">
											<label>Department </label>
											<input type="text" class="form-control" placeholder="Department" id="department" name="department" value={{ old('department') }}>
										  </div>
									</div>-->
									<div class="col-md-6  col-lg-6">
										<div class="form-group">
											<label>Is your office located at the business address given on prior screen? </label>
											<!--<input type="text" class="form-control" placeholder="Office Location" id="office_location" name="office_location" value={{ old('office_location') }}>-->
											<select class="form-control" id="office_location" name="office_location">
											<option value="Yes">Yes</option>
											<option value="No">No</option>
										</select>
										  </div>
									</div>
									<div class="officeLocationDiv" style="display:none">
									<div class="col-md-6 col-lg-4 offAddressDetails">
										 <div class="form-group">
											<label>Office Address 1 </label>
											<input type="text" class="form-control"  placeholder="Office Address 1" id="office_address1" name="office_address1" value={{ old('office_address1office_address2') }}>
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-4 offAddressDetails">
										 <div class="form-group">
											<label>Office Address 2 </label>
											<input type="text" class="form-control"  placeholder="Office Address 2" id="office_address2" name="office_address2" value={{ old('office_address2') }}>
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-4 offAddressDetails">
										 <div class="form-group">
											<label>Office  Address 3</label>
											<input type="text" class="form-control"  placeholder="Office Address 3" name="office_address3" id="office_address3" value={{ old('office_address3') }}>
										  </div>
									</div>
									
									
									<div class="col-md-6 col-lg-4 offAddressDetails">
										<div class="form-group">
												  <label>Office Country </label>
												  <select class="form-control" id="officeCountryList" name="office_country">
													<option value="">Select Country</option>

													@foreach($countryDetails as $data)
													<option value="{{$data->id}}">{{$data->name}}</option>
													@endforeach
												  </select>
												</div>
									</div>
									
									<div class="col-md-6 col-lg-4 offAddressDetails">
										<div class="form-group">
												  <label>Office State </label>
												  <select class="form-control" id="officeStateList" name="office_state">
												  	<option value="">Select State</option>
												  </select>
												</div>
									</div>
									
									<div class="col-md-6 col-lg-4 offAddressDetails">
										<div class="form-group">
												  <label> Office City </label>
												  <select class="form-control" id="officeCityList" name="office_city">
													<option value="">Select City</option>
												  </select>
												</div>
									</div>
									
									<div class="col-md-6  col-lg-6 offAddressDetails">
										<div class="form-group">
													<label>Office Postal Code </label>
													<input type="text" class="form-control numberOnly" maxlength="6"  placeholder="Office Postal Code" name="office_postal_code" id="office_postal_code" value={{ old('office_postal_code') }}>
												  </div>
									</div>
									</div>
									
									
									<div class="col-lg-12 pt-4">
										
										
										
										<div class="form-group">
											<div class="row">
												<div class="col-md-6 col-lg-6">
													<label>Is your business also registered as an insurance agency? </label>
												</div>
												<div class="col-md-1 col-lg-1">
													<div class="form-check">
													  <input class="form-check-input" name="agent" type="radio" value="yes" >
													  <label class="form-check-label">Yes</label>
													</div>
												</div>
												<div class="col-md-1 col-lg-1">
													<div class="form-check">
													  <input class="form-check-input" name="agent" type="radio" checked=""  value="no" >
													  <label class="form-check-label">No</label>
													</div>
												</div>
											</div>
											
										</div>
									</div>

									
								  </div>
								  
								 
								  
								
								</div>
								
								<div class="agencyLicenceDiv" style="display:none">	
								<!-- register form step 3-->
								<div id="register-part-3" class="content mt-5">
								  <h3 class="color-blue">License / ID</h3>
								  <hr class="pb-2">
								   <div class="row">
									
									
									<div class="col-lg-6">
										<div class="form-group">
											<label>License Number </label>
											<input type="text" class="form-control" placeholder="License Number" name="license_number" id="license_number" value="" >
										  </div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>License Status </label>
											<select class="form-control" id="license_status" name="license_status">
											<option value="active" >Active</option>
											<option value="expired" >Expired</option>
										</select>
										  </div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>Expiration Date </label>
											<input type="text" class="form-control" placeholder="MM/YYYY" name="license_expiry_date" id="license_expiry_date" value="" >
											
									</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>Designated Home State </label>
										<!--	<input type="text" class="form-control" placeholder="Designated Home State" name="license_home_state" id="license_home_state" value="" >-->
										<select class="form-control" id="license_home_state" name="license_home_state">
												  	<option value="">Select State</option>
												  </select>
										  </div>
									</div>
									</div>
<div class="form-group row mb-2">
									<label class="col-11 col-sm-11 col-md-11 col-lg-4 col-form-label">License type 
									</label>
									
									<div class="col-sm-7 col-lg-7 pl-0">
										<div class="row">
										  <div class="col-lg-11">
											<div class="form-group d-flex align-items-start mb-2">
												<input type="radio" class="form-check-input ml-0" id="license_type_opt" name="license_type_opt" value="Limited">
												<label class="form-check-label pl-4">Limited (dollar amount and/or insurance type restriction) </label>
											</div>
										  </div>
										</div>
										
									
										<div class="row ">
										  <div class="col-lg-11">
											<div class="form-group d-flex align-items-start mb-2">
												<input type="radio" class="form-check-input ml-0" id="license_type_opt" name="license_type_opt" value="Full">
												<label class="form-check-label pl-4">Full (any life product, any dollar amount)</label>
											</div>
										  </div>
										</div>
										
										
									</div>
								</div> <!-- row end -->
									

									<div class="row">
									
									

									<div class="col-lg-6">
										<div class="form-group">
											<label for="exampleInputFile">Required Document Uploads</label>
											<div class="input-group">
											  <input type="file" accept=".gif,.jpg,.jpeg,.png,.doc,.docx,.pdf" name="idFiles[]" class="myfrm form-control">
											 
											</div>
											
										  </div>
									</div>
									
									</div>
								
							
								</div>
							</div>
								
								<!-- register form step 4-->
								<div id="register-part-4" class="content mt-5">
								  <h3 class="color-blue">Business Overview</h3>
								  <hr class="pb-2">
								  <div class="row">								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Month/Year established</label>
											<input type="text" class="form-control" placeholder="MM/YYYY" value="" id="established_details" name="established_details">
										  </div>
									</div>
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Number of Funeral Directors/Embalmers on staff</label>	
											<input type="text" class="form-control numberOnly" placeholder="Number of Funeral Directors/Embalmers on staff" value="" id="funeral_number" name="funeral_number">
										</div>
									</div>
								  </div>
								  
								  <div class="row">								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Number of casketed funerals annually</label>
											<input type="text" class="form-control numberOnly" placeholder="Number of casketed funerals annually" value="" id="casked_funeral" name="casked_funeral">
										  </div>
									</div>
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Number of cremations annually</label>	
											<input type="text" class="form-control numberOnly" placeholder="Number of cremations annually" value="" id="cremations_annually" name="cremations_annually">					
										</div>
									</div>
								  </div>
								  
								  <div class="row">								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Annual preneed volume</label>
											<div class="input-group">
												<div class="input-group-prepend">
												<span class="input-group-text"><b>$</b></span>
												</div>
												<!--<input type="text" class="form-control " placeholder="Annual preneed volume" value="" id="annual_preneed_volume" name="annual_preneed_volume">-->
												<select class="form-control" id="annual_preneed_volume" name="annual_preneed_volume">
														<option value="100 - 1,000">100 - 1,000</option>
														<option value="1,000 - 10,000">1,000 - 10,000</option>
														<option value="10,000 - 100,000">10,000 - 100,000</option>
														<option value="100,000 - 1,000,000">100,000 - 1,000,000</option>
														<option value="1,000,000 - 10,000,000">1,000,000 - 10,000,000</option>
													</select>

											</div>
											
										  </div>
									</div>
									
									<!--<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label for="exampleInputFile">Required Document Uploads</label>
											<div class="input-group">
											  <div class="custom-file">
												<input type="file" class="custom-file-input" id="exampleInputFile">
												<label class="custom-file-label" for="exampleInputFile">Choose file</label>
											  </div>
											  <div class="input-group-append">
												<span class="input-group-text">Upload</span>
											  </div>
											</div>
											
										  </div>
									</div>-->
								  </div>
<!--First-->
									<div class="row increment">
								  		<div class="col-md-6 col-lg-6">
												<div class="form-group">
													<div class="input-group realprocode control-group lst" >
								  				<select class="form-control" id="" name="filetitle[]">
														<option value="Producer-Agreement">Producer Agreement</option>
														<option value="Direct-Deposit-Authorization">Direct Deposit Authorization</option>
														<option value="Producer-Compensation-Schedule">Producer Compensation Schedule</option>
														<option value="Explanation-of-Information-found-on-MIB">Explanation of Information found on MIB</option>
														<option value="Other-Supporting-Documentation">Other Supporting Documentation</option>
													</select>
											      
											    </div>
												</div>
											</div>

											<div class="col-md-6 col-lg-6">
												<div class="form-group">
													<div class="input-group realprocode control-group lst " >
								  				
											      <input type="file" accept=".gif,.jpg,.jpeg,.png,.doc,.docx,.pdf" name="filenames[]" class="myfrm form-control">
											      <div class="input-group-btn"> 
											        <button class="btn btn-success addFile" type="button"> <i class="fldemo glyphicon glyphicon-plus"></i>Add</button>
											      </div>
											    </div>
												</div>
											</div>
								  </div>

								<!--Second-->
							<div class="clone d-none " >
								<div class="row ">
									<div class="realprocode col-md-12 col-lg-12">
								  		<div class="col-md-6 col-lg-6" style="float: left;">
												<div class="form-group">
													<div class=" control-group lst input-group" style="margin-top:10px">
											      	<select class="form-control" id="" name="filetitle[]">
														<option value="Driver-License">State-Issued Driver's License</option>
														<option value="Producer-Agreement">Producer Agreement</option>
														<option value="Direct-Deposit-Authorization">Direct Deposit Authorization</option>
														<option value="Producer-Compensation-Schedule">Producer Compensation Schedule</option>
														<option value="Explanation-of-Information-found-on-MIB">Explanation of Information found on MIB</option>
														<option value="Other-Supporting-Documentation">Other Supporting Documentation</option>
													</select>
											        
											      </div>
												</div>
											</div>

											<div class="col-md-6 col-lg-6" style="float: left;">
												<div class="form-group">
													<div class=" control-group lst input-group" style="margin-top:10px">
											      	
											        <input type="file" accept=".gif,.jpg,.jpeg,.png,.doc,.docx,.pdf" name="filenames[]" class="myfrm form-control">
											        <div class="input-group-btn"> 
											          <button class="btn btn-danger" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>
											        </div>
											      </div>
												</div>
											</div>
										</div>
								  </div>

								</div>
								 
								  
								</div>
								
								
							<button type="submit" class="mt-4 btn btn-lg btn-primary ">ADD DETAILS</button>
							 <span  class="error finalErr"></span>
								 							
						  </div>
						  <!-- /.card-body -->
						  
						</div>
						<!-- /.card -->
					  </div>
					</div>
				</form>
				<!-- register form stepper -->
				
			</div>
		
	</div>

	@include('include.admin_footer')

<script type="text/javascript">
    $(document).ready(function() {
    	loadStateRegion();
      $(".addFile").click(function(){ 
          var lsthmtl = $(".clone").html();
          $(".increment").after(lsthmtl);
      });
      $("body").on("click",".btn-danger",function(){ 
          $(this).parents(".realprocode").remove();
      });

      $("input[name='agent']").click(function(){
      	$('.agencyLicenceDiv').css('display','none');
      	console.log($(this).val());
      	$('#license_number').val('');
      	$('#license_status').val('');
      	$('#license_expiry_date').val('');
      	$('#license_home_state').val('');
      	if($(this).val() == 'yes'){
      		$('.agencyLicenceDiv').css('display','block');
      	}
      });
    });
</script>