@include('include.admin_header')


	<style>
	ul.list-ssn {
		display: flex;
		list-style: none;
		margin: 0;
		padding: 0;
	}
	
	.list-forms-docs {
  display: flex;
  flex-direction: row;
    justify-content: center;
  margin: 0;
padding: 0;
}

.list-forms-docs li {
  list-style: none;
  box-shadow: 0 0 6px 0 rgba(0,0,0,0.2);
  padding: 20px;
  width: 20%;
  border-radius: 8px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  text-align: center;
  font-weight: 600;
  margin-left: 15px;
margin-right: 15px;
}

.list-forms-docs li i {
  margin-bottom: 10px;
  font-size: 24px;
  color: #999;
}
	
	.btn-blue{background:#fff; border-color:#004367; color:#004367}
	
	.btn-blue-solid{background:#004367; border-color:#004367; color:#fff}
	.btn-blue-solid:hover{background:#FED56D; color:#004367; border-color:#FED56D}
	
	ul.list-ssn li {
    max-width: 75px;
    width: 75px;
    margin-right: 10px
	}
	
	ul.list-ssn.format1 li:nth-child(4) {
    margin-left: 20px;
	position:relative;
}

ul.list-ssn.format1 li:nth-child(4):before {content:""; position:absolute; left:-18px; top:20px; width:12px; height:1px; background:#000}

ul.list-ssn.format1 li:nth-child(5) {
    margin-right: 20px;
	position:relative;
}
ul.list-ssn.format1 li:nth-child(5):before {content:""; position:absolute; right:-16px; top:20px; width:12px; height:1px; background:#000}


/* format 2 */
ul.list-ssn.format2 li:nth-child(3) {
    margin-left: 20px;
	position:relative;
}

ul.list-ssn.format2 li:nth-child(3):before {content:""; position:absolute; left:-18px; top:20px; width:12px; height:1px; background:#000}

.bs-stepper-content {
  padding: 40px 30px 30px;
}

.tools-links{margin:0; padding:0; display:flex; margin-right:15px;}
.tools-links li{list-style:none; margin-left:10px; font-weight:500; position:relative; }
.tools-links li:after{content:""; position:absolute; right:0; top:5px; width:1px; height:12px; background:#999;}
.tools-links li a{color:#004367; padding:0 20px; font-weight:600}
.tools-links li a i{font-size:12px; color:#666}
.tools-links li:last-child a{padding-right:0}
.tools-links li:last-child:after{background:none;}
	
	@media only screen and (max-width:767px){
		ul.list-ssn{flex-wrap: wrap;}
		ul.list-ssn li{max-width: 40px; width: 40px; margin-bottom:5px; margin-right: 5px;}
		.list-forms-docs{flex-direction: column; justify-content: center;}
		.list-forms-docs li{padding: 15px; width: 100%; flex-direction: row;justify-content: flex-start; align-items: flex-start; margin-left: 5px;
margin-right: 5px; margin-bottom: 20px; text-align: left; }
.list-forms-docs li i{margin-right: 10px; }
	}
	
	  @media only screen and (max-width: 767px){
		.bs-stepper-header .step {
		  max-width: 33%;
		}
		.btn{padding: .375rem 1rem;}
		
		.card-header.d-flex {flex-direction: column; align-items: flex-start !important;}
			.tools-links li{margin-left:0}
			.card-header > .card-tools{float:none;}
			.tools-links li:first-child a{padding: 0 20px 0 0;}
		
	  }
	
	@media only screen and (min-width:768px) and (max-width:991px){
		ul.list-ssn{flex-wrap: wrap;}
		ul.list-ssn li{max-width: 40px; width: 40px; margin-bottom:5px; margin-right: 5px;}
	}
	
  </style>
	
	<div class="content-body pb-5 pt-5">
		
		<div class="container">
				
				<!-- register form stepper -->
					
					<div class="row">
					  <div class="col-md-12">
						<div class="card card-default">
						  <div class="card-header d-flex align-items-center">
							<h3 class="mt-3 font-weight700 color-blue mr-auto">Partner - Application Form</h3>

							<div class="card-tools">
								<ul class="tools-links">
								<!--	<li><a href="#"><i class="fa fa-file-pdf"></i> Link 1</a></li>-->
									<li><a href="javascript:window.open('','_self').close();"><span class="backlList">Back to list</span></a></li>
								</ul>								
							 </div>
							
						  </div>
						  <div class="card-body p-0">
							<div class="bs-stepper linear">
							  <div class="bs-stepper-header" role="tablist">
								<!-- your steps here -->
								
								<div class="step active" data-target="#register-part-1">
								  <button type="button" class="step-trigger" role="tab" aria-controls="register-part-1" id="register-part-1-trigger" aria-selected="false" disabled="disabled">
									<span class="bs-stepper-circle">1</span>
									<span class="bs-stepper-label">State issued licensing information</span>
								  </button>
								</div>
								
								<div class="line"></div>
								
								<div class="step" data-target="#register-part-2">
								  <button type="button" class="step-trigger" role="tab" aria-controls="register-part-2" id="register-part-2-trigger" aria-selected="true">
									<span class="bs-stepper-circle">2</span>
									<span class="bs-stepper-label">IRS Form W-9</span>
								  </button>
								</div>
								
								<div class="line"></div>
								
								<div class="step" data-target="#register-part-3">
								  <button type="button" class="step-trigger" role="tab" aria-controls="register-part-3" id="register-part-3-trigger" aria-selected="true">
									<span class="bs-stepper-circle">3</span>
									<span class="bs-stepper-label">Agent / Agency details</span>
								  </button>
								</div>
								
								<!--<div class="line"></div>
								
								<div class="step" data-target="#register-part-4">
								  <button type="button" class="step-trigger" role="tab" aria-controls="register-part-4" id="register-part-4-trigger">
									<span class="bs-stepper-circle">4</span>
									<span class="bs-stepper-label">Forms and documentation</span>
								  </button>
								</div>-->
								
								
							  </div>
							  <div class="bs-stepper-content">
								<!-- your steps content here -->
								
								<!-- register part 1 -->
								<div id="register-part-1" class="content active dstepper-block" role="tabpanel" aria-labelledby="register-part-1-trigger">
								  
									<div class="form-group">
										<div class="row">
											<label class="col-11 col-sm-4 col-md-4 col-lg-4 col-form-label">State issued funeral ID / (Crematoria License ID #)</label>
											<div class="col-12 col-sm-7 col-md-7 col-ld-7">
											  <input type="text" class="form-control"  placeholder="" id="crematoria_licence_id" name="crematoria_licence_id">
											  <input type="hidden" id="userId" name="userId" value="{{Route::input('userId')}}" />
											  <input type="hidden" id="userType" name="userType" value="{{Route::input('userType')}}" />
											</div>
										</div>
									</div>
 
								  <div class="row mt-5">
									<div class="col-6 col-sm-6">
										<div class="btn btn-primary ml-auto" onclick="stepper.next()">Next</div>
									</div>
									<div class="col-6 col-sm-6 mbb text-right">
										
									</div>
									
								  </div>
								  
								</div>
								
								<!-- registration part 2-->
								<div id="register-part-2" class="content" role="tabpanel" aria-labelledby="register-part-2-trigger">
								 
									<div class="updatedDocDetails">Form not yet submitted</div>
								
								
								 
								
								  
								  
								  <div class="row mt-5">
									<div class="col-8 col-sm-6">
										 <div class="btn btn-primary" onclick="stepper.previous()">Previous</div>
										<div class="btn btn-primary" onclick="stepper.next()">Next</div>
									</div>
									<div class="col-4 col-sm-6 mbb text-right">
										
									</div>
									
								  </div>
								  
								</div> <!-- register-part-2 end -->
								
								
								
								
								
								<!-- register form step 3-->
								<div id="register-part-3" class="content" role="tabpanel" aria-labelledby="register-part-3-trigger">
								  
									
											
								<div class="form-group row mb-2">
									
									<div class="col-sm-12 col-lg-12 pl-0">
										<div class="row">
										  <div class="col-lg-12">
											<div class="form-group d-flex align-items-start mb-2">
												<input type="checkbox" class="form-check-input ml-0" id="agent_entity_name_chk">
												<label class="form-check-label pl-4">Agent entity established by the funeral home</label>
											</div>
											
											<div class="form-group row">
												<label class="col-11 col-sm-4 col-md-4 offset-1 col-lg-3 col-form-label">Agent entity name</label>
												<div class="col-12 col-sm-5 col-md-5 col-ld-5">
												  <input type="text" class="form-control"  placeholder="" id="agent_entity_name" name="agent_entity_name">
												</div>
											</div>
										  </div>
										 
										</div>

									</div>
								</div> <!-- row end -->
								
								<div class="form-group row mb-2">
									
									<div class="col-sm-12 col-lg-12 pl-0">
										<div class="row">
										  <div class="col-lg-12">
											<div class="form-group d-flex align-items-start mb-2">
												<input type="checkbox" class="form-check-input ml-0" id="agency_entity_name_chk">
												<label class="form-check-label pl-4">Agency entity established by the funeral home</label>
											</div>
											
											<div class="form-group row">
												<label class="col-11 col-sm-4 col-md-4 offset-1 col-lg-3 col-form-label">Agency entity name</label>
												<div class="col-12 col-sm-5 col-md-5 col-ld-5">
												  <input type="text" class="form-control"  placeholder="" id="agency_entity_name" name="agency_entity_name">
												</div>
											</div>
										  </div>
										 
										</div>

									</div>
								</div> <!-- row end -->
								
									
									
									
									<div class="row mt-5">
									<div class="col-4 col-sm-6">
										 <button class="btn btn-primary" onclick="stepper.previous()">Previous</button>
									</div>
									<div class="col-8 col-sm-6 mbb text-right">
										
									</div>
									
								  </div>
									
									
								</div>
								
								
								<!-- registere step 4 -->
								<!--<div id="register-part-4" class="content" role="tabpanel" aria-labelledby="register-part-4-trigger">
									
									
										<div class="row align-items-top">											
											<div class="col-sm-12">
												<ul class="list-forms-docs">
													<li>
														<i class="fa fa-file-pdf"></i>
														<a href="summary_your-rights-under-fcra.pdf" target="_blank">Summary of your rights under FCRA<br> <small>Download</small></a>
													</li>
													<li>
														<i class="fa fa-file-pdf"></i>
														<a href="#">IRS form W9 <br> <small>Download</small></a>
													</li>
													<li>
														<i class="fa fa-file-pdf"></i>
														<a href="#">Background check disclosure form <br> <small>Download</small></a>
													</li>
												</ul>
											</div>
										</div>
									
									
								  <div class="row mt-5">
									<div class="col-12 col-sm-12 col-md-12 mbb text-center">
										<button class="btn btn-primary btn-blue mr-auto" onclick="stepper.next()">Download All</button>
									</div>
									
								  </div>
								  
								</div>-->
								
								
							  </div>
							</div>
						  </div>
						  <!-- /.card-body -->
						  
						</div>
						<!-- /.card -->
					  </div>
					</div>
				
				<!-- register form stepper -->
		
				
			</div>
		
	</div>
	
	


<!-- models -->
<div class="modal fade" id="modal-llc">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Limited liability company </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Note: Check the appropriate box in the line above for the tax classification of the single-member owner. Do not check LLC if the LLC is classified as a single-member LLC that is disregarded from the owner unless the owner of the LLC is another LLC that is not disregarded from the owner for U.S. federal tax purposes. Otherwise, a single-member LLC that is disregarded from the owner should check the appropriate box for the tax classification of its owner.</p>
            </div>
           
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
	  
	  
	  <!-- models -->
<div class="modal fade" id="modal-line5a">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Exemptions </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>If you are exempt from backup withholding and/or FATCA reporting, enter in the appropriate space on line 4 any code(s) that may apply to you.</p>
            </div>
           
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


	  <!-- models -->
<div class="modal fade" id="modal-line5b">
        <div class="modal-dialog modal-dialog-centered modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Exempt payee code </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <ul>
				<li>Generally, individuals (including sole proprietors) are not exempt from backup withholding.</li>
				<li>Except as provided below, corporations are exempt from backup withholding for certain payments, including interest and dividends.</li>
				<li>Corporations are not exempt from backup withholding for payments made in settlement of payment card or third party network transactions.</li>
				<li>Corporations are not exempt from backup withholding with respect to attorneys’ fees or gross proceeds paid to attorneys, and corporations that provide medical or health care services are not exempt with respect to payments reportable on Form 1099-MISC.</li>
			  </ul>
			  
			  <p>The following codes identify payees that are exempt from backup withholding. Enter the appropriate code in the space in line 5.</p>
				<ul>
					<li>1—An organization exempt from tax under section 501(a), any IRA, or a custodial account under section 403(b)(7) if the account satisfies the requirements of section 401(f)(2)</li>
					<li>2—The United States or any of its agencies or instrumentalities</li>
					<li>3—A state, the District of Columbia, a U.S. commonwealth or possession, or any of their political subdivisions or instrumentalities</li>
					<li>4—A foreign government or any of its political subdivisions, agencies, or instrumentalities</li>
					<li>5—A corporation</li>
					<li>6—A dealer in securities or commodities required to register in the United States, the District of Columbia, or a U.S. commonwealth or possession</li>
					<li>7—A futures commission merchant registered with the Commodity Futures Trading Commission</li>
					<li>8—A real estate investment trust</li>
					<li>9—An entity registered at all times during the tax year under the Investment Company Act of 1940</li>
					<li>10—A common trust fund operated by a bank under section 584(a)</li>
					<li>11—A financial institution</li>
					<li>12—A middleman known in the investment community as a nominee or custodian</li>
					<li>13—A trust exempt from tax under section 664 or described in section 4947</li>
				</ul>
				
				<p>The following chart shows types of payments that may be exempt from backup withholding. The chart applies to the exempt payees listed above, 1 through 13.</p>
				
				<div class="responsive">
					<table class="table">
						<thead>
							<tr>
								<th>IF the payment is for . . .</th>
								<th>THEN the payment is exempt for . . .</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Interest and dividend payments</td>
								<td>All exempt payees except for 7</td>
							</tr>
							<tr>
								<td>Broker transactions</td>
								<td>Exempt payees 1 through 4 and 6 through 11 and all C corporations. S corporations must not enter an exempt payee code because they are exempt only for sales of noncovered securities acquired prior to 2012.</td>
							</tr>
							<tr>
								<td>Barter exchange transactions and patronage dividends</td>
								<td>Exempt payees 1 through 4</td>
							</tr>
							<tr>
								<td>Payments over $600 required to be reported and direct sales over $5,0001</td>
								<td>Generally, exempt payees 1 through 5<sup>2</sup></td>
							</tr>
							<tr>
								<td>Payments made in settlement of payment card or third party network transactions</td>
								<td>Exempt payees 1 through 4</td>
							</tr>
						</tbody>
					</table>
					
					<ul>
						<li>See Form 1099-MISC, Miscellaneous Income, and its instructions.</li>
						<li>However, the following payments made to a corporation and reportable on Form 1099-MISC are not exempt from backup withholding: medical and health care payments, attorneys’ fees, gross proceeds paid to an attorney reportable under section 6045(f), and payments for services paid by a federal executive agency.</li>
					</ul>
				</div>
            </div>
           
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


<div class="modal fade" id="modal-line5c">
        <div class="modal-dialog modal-dialog-centered modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Exemption from FATCA reporting code </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
				<p>The following codes identify payees that are exempt from reporting under FATCA. These codes apply to persons submitting this form for accounts maintained outside of the United States by certain foreign financial institutions. Therefore, if you are only submitting this form for an account you hold in the United States, you may leave this field blank. Consult with the person requesting this form if you are uncertain if the financial institution is subject to these requirements. A requester may indicate that a code is not required by providing you with a Form W-9 with “Not Applicable” (or any similar indication) written or printed on the line for a FATCA exemption code.</p>
              <ul>
				<li>A—An organization exempt from tax under section 501(a) or any individual retirement plan as defined in section 7701(a)(37)</li>
				<li>B—The United States or any of its agencies or instrumentalities</li>
				<li>C—A state, the District of Columbia, a U.S. commonwealth or possession, or any of their political subdivisions or instrumentalities</li>
				<li>D—A corporation the stock of which is regularly traded on one or more established securities markets, as described in Regulations section 1.1472-1(c)(1)(i)</li>
				<li>E—A corporation that is a member of the same expanded affiliated group as a corporation described in Regulations section 1.1472-1(c)(1)(i)</li>
				<li>F—A dealer in securities, commodities, or derivative financial instruments (including notional principal contracts, futures, forwards, and options) that is registered as such under the laws of the United States or any state</li>
				<li>G—A real estate investment trust</li>
				<li>H—A regulated investment company as defined in section 851 or an entity registered at all times during the tax year under the Investment Company Act of 1940</li>
				<li>I—A common trust fund as defined in section 584(a)</li>
				<li>J—A bank as defined in section 581</li>
				<li>K—A broker</li>
				<li>L—A trust exempt from tax under section 664 or described in section 4947(a)(1)</li>
				<li>M—A tax exempt trust under a section 403(b) plan or section 457(g) plan</li>
			  </ul>
			  
			  <p><strong>Note:</strong> You may wish to consult with the financial institution requesting this form to determine whether the FATCA code and/or exempt payee code should be completed.</p>
				

            </div>
           
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
	  
	  
<div class="modal fade" id="modal-line6">
        <div class="modal-dialog modal-dialog-centered modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Exemption from FATCA reporting code </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
				<p>Enter your address (number, street, and apartment or suite number). This is where the requester of this Form W-9 will mail your information returns. If this address differs from the one the requester already has on file, write NEW at the top. If a new address is provided, there is still a chance the old address will be used until the payor changes your address in their records.</p>
              
            </div>
           
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->	  
	  


<div class="modal fade" id="modal-line8">
        <div class="modal-dialog modal-dialog-centered modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">List account number(s)</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
				<p>Pellentesque in mi velit. Curabitur molestie viverra justo quis egestas. In hac habitasse platea dictumst. Morbi accumsan quam non libero varius sit amet luctus nunc mattis. Suspendisse gravida libero ut tellus rutrum cursus. </p>
              
            </div>
           
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
	  
<div class="modal fade" id="modal-line10">
        <div class="modal-dialog modal-dialog-centered modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Part I. Taxpayer Identification Number (TIN)</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
				<p><strong>Enter your TIN in the appropriate box.</strong> If you are a resident alien and you do not have and are not eligible to get an SSN, your TIN is your IRS individual taxpayer identification number (ITIN). Enter it in the social security number box. If you do not have an ITIN, see How to get a TIN below.</p>
				<p>If you are a sole proprietor and you have an EIN, you may enter either your SSN or EIN.<p>
				<p>If you are a single-member LLC that is disregarded as an entity separate from its owner, enter the owner’s SSN (or EIN, if the owner has one). Do not enter the disregarded entity’s EIN. If the LLC is classified as a corporation or partnership, enter the entity’s EIN.</p>
				<p><strong>Note:</strong> See What Name and Number To Give the Requester, later, for further clarification of name and TIN combinations.</p>
				<p><strong>How to get a TIN.</strong> If you do not have a TIN, apply for one immediately. To apply for an SSN, get Form SS-5, Application for a Social Security Card, from your local SSA office or get this form online at www.SSA.gov. You may also get this form by calling 1-800-772-1213. Use Form W-7, Application for IRS Individual Taxpayer Identification Number, to apply for an ITIN, or Form SS-4, Application for Employer Identification Number, to apply for an EIN. You can apply for an EIN online by accessing the IRS website at www.irs.gov/Businesses and clicking on Employer Identification Number (EIN) under Starting a Business. Go to www.irs.gov/Forms to view, download, or print Form W-7 and/or Form SS-4. Or, you can go to www.irs.gov/OrderForms to place an order and have Form W-7 and/or SS-4 mailed to you within 10 business days.</p>
				<p>If you are asked to complete Form W-9 but do not have a TIN, apply for a TIN and write “Applied For” in the space for the TIN, sign and date the form, and give it to the requester. For interest and dividend payments, and certain payments made with respect to readily tradable instruments, generally you will have 60 days to get a TIN and give it to the requester before you are subject to backup withholding on payments. The 60-day rule does not apply to other types of payments. You will be subject to backup withholding on all such payments until you provide your TIN to the requester.</p>
				<p><strong>Note:</strong> Entering “Applied For” means that you have already applied for a TIN or that you intend to apply for one soon.</p>
				<p><strong>Caution:</strong> A disregarded U.S. entity that has a foreign owner must use the appropriate Form W-8.</p>
              
            </div>
           
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

@include('include.footer')

<script>
	$(document).ready(function(){

		loadApplicationDetails();
	});


function loadApplicationDetails(){
	var userId = $('#userId').val();
	var userType = $('#userType').val();
	$("#agency_entity_name_chk").attr("checked",false);
	$("#agent_entity_name_chk").attr("checked",false);
	$.ajax({
	            type: "POST",
	            url: "/admin/application_details",
	            dataType: 'json', 
  				encode  : true,
  				data: {
  				 	'userId':userId,
  				 	'userType':userType
  				 },
  				
  				 beforeSend: function() {
                   $.blockUI({ css: { 
			            border: 'none', 
			            padding: '15px', 
			            backgroundColor: '#000', 
			            '-webkit-border-radius': '10px', 
			            '-moz-border-radius': '10px', 
			            opacity: .5, 
			            color: '#fff' 
			        } }); 
                },
	            success: function (data) {
	            	
	            	if(data !='1'){
		            	for(res in data){
		            		$( ".form-control" ).prop( "disabled", true );
		            		$('#crematoria_licence_id').val(data[res]["crematoria_licence_id"]);
		            		$('#address').val(data[res]["address"]);
		            		$('#agency_entity_name').val(data[res]["agency_entity_name"]);
		            		$('#agent_entity_name').val(data[res]["agent_entity_name"]);
		            		if((data[res]["agency_entity_name"] != "") && (data[res]["agency_entity_name"] != null)){
		            			$("#agency_entity_name_chk").attr("checked",true);
		            		}
		            			if((data[res]["agent_entity_name"] != "") && (data[res]["agent_entity_name"] != null)){
		            				$("#agent_entity_name_chk").attr("checked",true);
		            			}
		            		$('#business_name').val(data[res]["business_name"]);
		            		$('#city_state_zipcode').val(data[res]["city_state_zipcode"]);
		            		$('#exempt_payee_code').val(data[res]["exempt_payee_code"]);
		            		$('#exemption_fatca_reporting_code').val(data[res]["exemption_fatca_reporting_code"]);
		            		$('#list_account_number').val(data[res]["list_account_number"]);
		            		$('#name').val(data[res]["name"]);
		            		$('#requester_name_address').val(data[res]["requester_name_address"]);
		            		$('#social_security_number').val(data[res]["social_security_number"]);
		            		$('#tax_clasification').val(data[res]["tax_clasification"]);
		            		//$('#tax_clasification_name').val(data[res]["tax_clasification_name"]);
		            		$("input:radio[name=tax_clasification_name][value='" + data[res]["tax_clasification_name"] + "']").prop('checked', true);
		            		//$("input:radio[value='"+data[res]["tax_clasification_name"]+"'][name='tax_clasification_name']").prop('checked',true);
		            		$('#tin_type').val(data[res]["tin_type"]);

		            		if((data[res]["irs_form_w9_doc_path"] != "") && (data[res]["irs_form_w9_doc_path"] != null)){
		            		//$('.fileUpdatedLabel').html("Uploaded Doc");
		            		var html = '<div class="form-group">';
										html +=' <div class="row">';
										html +='	<label class="col-11 col-sm-4 col-md-4 col-lg-4 col-form-label ">Uploaded Document</label>';
										html +='	<div class="col-12 col-sm-7 col-md-7 col-ld-7 ">';
										html +='	 <a href="/storage/app/'+data[res]["irs_form_w9_doc_path"]+'" target="_blank">View Document</a>';
										html +='	</div>';
										html +='</div>								</div>';
										$('.updatedDocDetails').html(html);
		            	}

		            	}
		            }
		            else{
		            	$('.bs-stepper').html('<p style="text-align:center;padding-top:20px"><b>Application Not Yet Submitted</b></p>');
		            }
	             	
	             	$.unblockUI();
	            },
	            error: function (data) {
	               
	            }
	        });
}

</script>