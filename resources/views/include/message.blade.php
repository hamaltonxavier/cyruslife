   @if(Session::has('message-danger'))
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-ban"></i> {{ Session::get('message-danger') }}</h5>
</div>
@endif
@if(Session::has('message-success'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-check"></i> {{ Session::get('message-success') }}</h5>
</div> 
@endif
@if(Session::has('message-info'))
<div class="alert alert-info alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-info"></i> {{ Session::get('message-info') }}</h5>
</div> 
@endif