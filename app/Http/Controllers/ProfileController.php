<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use File;

class ProfileController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    

    public function showProfile(Request $request, $usertype=null, $userid=null){
        
       /* $userRole = Auth::user()->role;
        $userId = Auth::user()->id;*/

         if($request->usertype != null){
         $userRole =  $request->usertype;
         $userId = $request->userid;
       }
       else{
        $userRole = Auth::user()->role;
        $userId = Auth::user()->id;
       }
        //var_dump($userRole);
       // $userDetails = DB::table('users')->whereIn('id', [$userId])->get()->toArray();
       
       $userDetails =DB::table('user_details')
         ->leftJoin('countries', 'countries.id', '=', 'user_details.country')
         ->leftJoin('states', 'states.id', '=', 'user_details.state')
         ->leftJoin('cities', 'cities.id', '=', 'user_details.city')
         ->leftJoin('countries as oc', 'oc.id', '=', 'user_details.office_country')
         ->leftJoin('states as os', 'os.id', '=', 'user_details.office_state')
         ->leftJoin('cities as oci', 'oci.id', '=', 'user_details.office_city')
         ->select('user_details.*', 'countries.name as countryName','states.name as stateName','cities.name as cityName', 'oc.name as ocName','os.name as osName','oci.name as ociName' )
         ->where('user_details.user_id', '=', $userId)
        ->get(); 
        
         $docDetails = DB::table('document_path')->whereIn('user_id', [$userId])->get();



        if($userRole == '1'){
            $title = 'CYRUS | Partner Profile';
            return view('partner_view_profile', compact(['title','userDetails','docDetails']));
        }

         if($userRole == '2'){
            $title = 'CYRUS | Agency Profile';
            return view('agency_view_profile', compact(['title','userDetails','docDetails']));
        }

         if($userRole == '3'){
            $title = 'CYRUS | Agent Profile';
            return view('agent_view_profile', compact(['title','userDetails','docDetails']));
        }
        
    }




    public function editProfile(Request $request, $usertype=null, $userid=null){

        /*$userRole = Auth::user()->role;
        $userId = Auth::user()->id;*/

        if($request->usertype != null){
         $userRole =  $request->usertype;
         $userId = $request->userid;
       }
       else{
        $userRole = Auth::user()->role;
        $userId = Auth::user()->id;
       }

       // $userDetails = DB::table('users')->whereIn('id', [$userId])->get()->toArray();
       
       $userDetails =DB::table('user_details')
         ->leftJoin('countries', 'countries.id', '=', 'user_details.country')
         ->leftJoin('states', 'states.id', '=', 'user_details.state')
         ->leftJoin('cities', 'cities.id', '=', 'user_details.city')
         ->select('user_details.*', 'countries.id as countryId','states.id as stateId','cities.id as cityId' )
         ->where('user_details.user_id', '=', $userId)
        ->get();

        $countryDetails = DB::table('countries')->whereIn('id', [233])->get()->toArray();
        $statesDetails = DB::table('states')->whereIn('country_id', [$userDetails[0]->countryId])->get()->toArray();
        $cityDetails = DB::table('cities')->whereIn('state_id', [$userDetails[0]->stateId])->get()->toArray();

        $docDetails = DB::table('document_path')->whereIn('user_id', [$userId])->get();


        $osDetails = DB::table('states')->whereIn('country_id', [$userDetails[0]->countryId])->get()->toArray();
        $ociDetails = DB::table('cities')->whereIn('state_id', [$userDetails[0]->office_state])->get()->toArray();

        if($userRole == '1'){
            $title = 'CYRUS | Partner Profile';
            return view('partner_edit_profile', compact(['title','userDetails','countryDetails','statesDetails','cityDetails','osDetails','ociDetails','docDetails']));
        }

         if($userRole == '2'){
            $title = 'CYRUS | Agency Profile';
            return view('agency_edit_profile', compact(['title','userDetails','countryDetails','statesDetails','cityDetails','docDetails']));
        }

         if($userRole == '3'){
            $title = 'CYRUS | Agent Profile';
            return view('agent_edit_profile', compact(['title','userDetails','countryDetails','statesDetails','cityDetails','docDetails']));
        }
        
    }




    public function updateUsers(Request $request, $usertype=null, $userid=null){

        /*$userRole = Auth::user()->role;
        $userId = Auth::user()->id;*/

        if($request->usertype != null){
         $userRole =  $request->usertype;
         $userId = $request->userid;
       }
       else{
        $userRole = Auth::user()->role;
        $userId = Auth::user()->id;
       }

         if($userRole == '1'){
          $update =  DB::table('user_details')->where('user_id',$userId )->update(array(
                'business_name' => $request->business_name,
                'address1' => $request->address1,
                'address2' => $request->address2,
                'address3' => $request->address3,
                'city' => $request->city,
                'state' => $request->state,
                'country' => $request->country,
                'postal_code' => $request->postal_code,
                'telephone' => $request->telephone,
                'title' => $request->title,
                'first_name' => $request->first_name,
                'middle_name' => $request->middle_name,
                'last_name' => $request->last_name,
                'designation' => $request->designation,
                //'department' => $request->department,
                'office_location' => $request->office_location,
                'office_address1' => $request->office_address1,
                'office_address2' => $request->office_address2,
                'office_address3' => $request->office_address3,
                'office_country' => $request->office_country,
                'office_state' => $request->office_state,
                'office_city' => $request->office_city,
                'office_postal_code' => $request->office_postal_code,
                 'mobile' => $request->mobile,
                'agent' => $request->agent,
                'license_type' => $request->license_type,
                'license_type_opt' => $request->license_type_opt,
                'license_number' => $request->license_number,
                'license_status' => $request->license_status,
                'license_expiry_date' => $request->license_expiry_date,
                'license_home_state' => $request->license_home_state,
                'established_details' => $request->established_details,
                'funeral_number' => $request->funeral_number,
                'casked_funeral' => $request->casked_funeral,
                'cremations_annually' => $request->cremations_annually,
                'annual_preneed_volume' => $request->annual_preneed_volume,
            ));


         }

         if($userRole == '2'){

            $update =  DB::table('user_details')->where('user_id',$userId )->update(array(
               
                'business_name' => $request->business_name,
                'address1' => $request->address1,
                'address2' => $request->address2,
                'address3' => $request->address3,
                'city' => $request->city,
                'state' => $request->state,
                'country' => $request->country,
                'postal_code' => $request->postal_code,
                'telephone' => $request->telephone,
                'title' => $request->title,
                'first_name' => $request->first_name,
                'middle_name' => $request->middle_name,
                'last_name' => $request->last_name,
                 'mobile' => $request->mobile,
              
                'agent' => $request->agent,
                'license_type' => $request->license_type,
                'license_type_opt' => $request->license_type_opt,
                'license_number' => $request->license_number,
                'license_status' => $request->license_status,
                'license_expiry_date' => $request->license_expiry_date,
                'license_home_state' => $request->license_home_state,
                'location_of_services' => $request->location_of_services,
                'number_of_agents' => $request->number_of_agents,
                'business_turn_over' => $request->business_turn_over,
                'number_of_years_in_business' => $request->number_of_years_in_business,
                 'lead_to_prospect_ratio' => $request->lead_to_prospect_ratio,
            ));

           
         }


         if($userRole == '3'){
            $update =  DB::table('user_details')->where('user_id',$userId )->update(array(
               
                'address1' => $request->address1,
                'address2' => $request->address2,
                'address3' => $request->address3,
                'city' => $request->city,
                'state' => $request->state,
                'country' => $request->country,
                'postal_code' => $request->postal_code,
                'title' => $request->title,
                'first_name' => $request->first_name,
                'middle_name' => $request->middle_name,
                'last_name' => $request->last_name,
                 'mobile' => $request->mobile,
                'license_type' => $request->license_type,
                'license_type_opt' => $request->license_type_opt,
                'license_number' => $request->license_number,
                'license_status' => $request->license_status,
                'license_expiry_date' => $request->license_expiry_date,
                'license_home_state' => $request->license_home_state,
                'location_of_services' => $request->location_of_services,
                'business_turn_over' => $request->business_turn_over,
                'number_of_years_in_business' => $request->number_of_years_in_business,
            ));

          
         }


         if($request->hasfile('idFiles'))
         {
          
            $total = count($request->file('idFiles'));
            for( $i=0 ; $i < $total ; $i++ ) {
            
                $fileNameVal = time().rand(1,100).'.'.$request->file('idFiles')[$i]->extension();
                $filePathVal =  $request->file('idFiles')[$i]->store('public/files');  
                $files[] = $fileNameVal; 
                //$imageType = $request->filetitle[$i];
               


                $insert = DB::table('document_path')->insert([
                   'user_id' =>  $userId,
                   'doucment_path' => $filePathVal,
                    'document_type'=>'idproof'
                 ]);
              
            }
            
         }

        


         $files = [];
      
        if($request->hasfile('filenames'))
         {
          
            $total = count($request->file('filenames'));
            for( $i=0 ; $i < $total ; $i++ ) {
            
                $fileNameVal = time().rand(1,100).'.'.$request->file('filenames')[$i]->extension();
                $filePathVal =  $request->file('filenames')[$i]->store('public/files');  
                $files[] = $fileNameVal; 
                $imageType = $request->filetitle[$i];
                $documentNameVal = $request->filetitle[$i];


                $insert = DB::table('document_path')->insert([
                   'user_id' =>  $userId,
                   'doucment_path' => $filePathVal,
                    'document_name'=>$documentNameVal,
                    'document_type'=>'business'
                 ]);
              
            }
            
         }

        // dd(DB::getQueryLog());
        // exit;
          //return redirect(url('/show-profile'))->with(['message-success'=>'User data has been updated successfully.']);

            if($request->usertype != null){
                 return redirect(url('/user_dashboard'))->with(['message-success'=>'User data has been updated successfully.']);
           }
           else{
             return redirect(url('/show-profile'))->with(['message-success'=>'User data has been updated successfully.']);
           }
       

    }

    


    public function showUserDashboard(){
        $userRole = Auth::user()->role;
        $userId = Auth::user()->id;



        if($userRole == '1'){
            $title = 'CYRUS | Partner Dashboard';

            return view('partner_dashboard', compact(['title']));
        }

         if($userRole == '2'){
            $title = 'CYRUS | Agency Dashboard';
            return view('agency_dashboard', compact(['title']));
        }

        /* if($userRole == '3'){
            $title = 'CYRUS | Agent Application';
            return view('agent_application_form', compact(['title']));
        }*/
    }

    public function showUserDetails(){
         $userRole = Auth::user()->role;
        $userId = Auth::user()->id;
        $userDetails =DB::table('user_details')
         ->leftJoin('partner_application_details', 'partner_application_details.user_id', '=', 'user_details.user_id')
            ->leftJoin('agency_application_form', 'agency_application_form.user_id', '=', 'user_details.user_id')
            ->leftJoin('agent_application_form', 'agent_application_form.user_id', '=', 'user_details.user_id')
            ->join('countries', 'countries.id', '=', 'user_details.country')
            ->join('states', 'states.id', '=', 'user_details.state')
            ->join('cities', 'cities.id', '=', 'user_details.city')
            ->join('users','users.id','=','user_details.user_id')
            ->select( 'user_details.*', 'countries.name as countryName','states.name as stateName','cities.name as cityName', 'users.role as role', 'partner_application_details.application_status as pApp','agent_application_form.application_status as atApp','agency_application_form.application_status as agApp', 'partner_application_details.application_details_id as pAppId','agent_application_form.application_details_id as atAppId','agency_application_form.application_details_id as agAppId')
         ->where('user_details.user_id', '=', $userId)
        ->get(); 
        return response()->json($userDetails);
    }


    public function showSubUsersDetails(Request $request){
        $userRole = Auth::user()->role;
        $userId = Auth::user()->id;
        $userDetails =DB::table('user_details')
           ->leftJoin('partner_application_details', 'partner_application_details.user_id', '=', 'user_details.user_id')
            ->leftJoin('agency_application_form', 'agency_application_form.user_id', '=', 'user_details.user_id')
            ->leftJoin('agent_application_form', 'agent_application_form.user_id', '=', 'user_details.user_id')
            ->join('countries', 'countries.id', '=', 'user_details.country')
            ->join('states', 'states.id', '=', 'user_details.state')
            ->join('cities', 'cities.id', '=', 'user_details.city')
            ->join('users','users.id','=','user_details.user_id')
            ->select( 'user_details.*', 'countries.name as countryName','states.name as stateName','cities.name as cityName', 'users.role as role', 'partner_application_details.application_status as pApp','agent_application_form.application_status as atApp','agency_application_form.application_status as agApp', 'partner_application_details.application_details_id as pAppId','agent_application_form.application_details_id as atAppId','agency_application_form.application_details_id as agAppId')
            ->where('user_details.parent_entity_id', '=', $userId)
            ->where('users.role', '=', $request->userTypeVal)
            ->orderBy('user_details.user_id', 'DESC')
            ->get()->toArray();

        
         return response()->json($userDetails);
    }

    public function removeDoc(Request $request){
       $deleteDoc = DB::table('document_path')
        ->where('document_id', $request->docId)
        ->delete();

        return response()->json("removed"); 
    }

}
