  
<footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6">
                    &copy; 2022 CYRUS Life Insurance
                </div>
                <div class="col-12 col-lg-6">
                    <ul class="footer-links">
                        <li><a href="#">Privacy</a></li>
                        <li><a href="#">Terms &amp; Conditions</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    
    
    </div>
</body>


<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="<?php echo e(asset('public/adminlte/plugins/jquery/jquery.min.js')); ?>"></script>
<!-- Bootstrap -->
<script src="<?php echo e(asset('public/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')); ?>"></script>
<!-- overlayScrollbars -->
<script src="<?php echo e(asset('public/adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')); ?>"></script>

<!-- Toastr -->
<script src="<?php echo e(asset('public/adminlte/plugins/toastr/toastr.min.js')); ?>"></script>

<!-- jquery-validation -->
<script src="<?php echo e(asset('public/adminlte/plugins/jquery-validation/jquery.validate.min.js')); ?>"></script>
<script src="<?php echo e(asset('public/adminlte/plugins/jquery-validation/additional-methods.min.js')); ?>"></script>

<!-- AdminLTE App -->
<script src="<?php echo e(asset('public/adminlte/dist/js/adminlte.js')); ?>"></script>

<!-- BS-Stepper -->
<script src="<?php echo e(asset('public/adminlte/plugins/bs-stepper/js/bs-stepper.min.js')); ?>"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"></script>

<!-- date-range-picker -->
<script src="<?php echo e(asset('public/adminlte/dist/js/bootstrap-datepicker.js')); ?>"></script>


<script src="<?php echo e(asset('public/adminlte/dist/js/custom.js')); ?>"></script>

<script>

    
      // BS-Stepper Init
      document.addEventListener('DOMContentLoaded', function () {
        window.stepper = new Stepper(document.querySelector('.bs-stepper'))
      })
      
      $('.toastrDefaultError').click(function() {
      toastr.error('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
    });
      
</script>

<!-- sticky footer -->

   <?php if(Session::has('message-danger')): ?>
<script>
     toastr.info('<?php echo Session::get('message-danger'); ?>')
</script>
<?php endif; ?>
<?php if(Session::has('message-success')): ?>
<script>
     toastr.success('<?php echo Session::get('message-success'); ?>')
</script>
<?php endif; ?>
<?php if(Session::has('message-info')): ?>
<script>
     toastr.error('<?php echo Session::get('message-info'); ?>')
</script> 
<?php endif; ?>


</html><?php /**PATH C:\xampp\htdocs\cyruslife\resources\views/include/footer.blade.php ENDPATH**/ ?>