<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;

class ApplicationController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }



    public function loadApplicationDetails(){
        $userRole = Auth::user()->role;
        $userId = Auth::user()->id;
        if($userRole == '1'){
             $applicationCount = DB::table('partner_application_details')
                        ->where(['user_id' => $userId])
                        ->count();
            if($applicationCount > 0){      
                    $applicationDetails =DB::table('partner_application_details')
                 ->select('partner_application_details.*')
                 ->where('user_id', '=', $userId)
                 ->get();
                  return response()->json($applicationDetails);
             }
             else{
                 return response()->json('1');
             }
        }

        if($userRole == '2'){
            $applicationCount = DB::table('agency_application_form')
                        ->where(['user_id' => $userId])
                        ->count();
            if($applicationCount > 0){ 
                    $applicationDetails =DB::table('agency_application_form')
                 ->select('agency_application_form.*')
                 ->where('user_id', '=', $userId)
                 ->get();
                 return response()->json($applicationDetails);
             }
             else{
                 return response()->json('1');
             }
        }


        if($userRole == '3'){
            $applicationCount = DB::table('agent_application_form')
                        ->where(['user_id' => $userId])
                        ->count();
            if($applicationCount > 0){ 
                    $applicationDetails =DB::table('agent_application_form')
                 ->select('agent_application_form.*')
                 ->where('user_id', '=', $userId)
                 ->get();
                 return response()->json($applicationDetails);
             }
             else{
                 return response()->json('1');
             }
        }
        
        
        
    }



    public function showApplicationForm(){
        $userRole = Auth::user()->role;
        $userId = Auth::user()->id;

        if($userRole == '1'){
            $title = 'CYRUS | Partner Application';
            return view('partner_application_form', compact(['title']));
        }

         if($userRole == '2'){
            $title = 'CYRUS | Agency Application';
            return view('agency_application_form', compact(['title']));
        }

         if($userRole == '3'){
            $title = 'CYRUS | Agent Application';
            return view('agent_application_form', compact(['title']));
        }
    }

    public function application_save(Request $request){
         $userRole = Auth::user()->role;
        $userId = Auth::user()->id;


       
        if($userRole == '1'){
            $applicationCount = DB::table('partner_application_details')
                        ->where(['user_id' => $userId])
                        ->count();

            if($applicationCount > 0){
                $update =  DB::table('partner_application_details')->where('user_id',$userId )->update(array(
                     'crematoria_licence_id' => $request->crematoria_licence_id,
                    'name' => $request->name,
                    'business_name' => $request->business_name,
                    'tax_clasification_name' => $request->tax_clasification_name,
                    'tax_clasification' => $request->tax_clasification,
                    'exempt_payee_code' => $request->exempt_payee_code,
                    'exemption_fatca_reporting_code' => $request->exemption_fatca_reporting_code,
                    'address' => $request->address,
                    'city_state_zipcode' => $request->city_state_zipcode,
                    'list_account_number' => $request->list_account_number,
                    'requester_name_address' => $request->requester_name_address,
                    'tin_type' => $request->tin_type,
                    'social_security_number' => $request->social_security_number,
                    'employer_identification_number' => $request->employer_identification_number,
                    'signature' => $request->signature,
                    'application_date' => $request->application_date,
                     'agent_entity_name' => $request->agent_entity_name,
                    'agency_entity_name' => $request->agency_entity_name,
                ));

                if($request->submitType =="Submit"){
                        $update =  DB::table('partner_application_details')->where('user_id',$userId )->update(array(
                         'application_status' => "submitted",
                       
                    ));
                }

            }
            else{
               $insert = DB::table('partner_application_details')->insert([
                    'user_id' =>  $userId,
                    'crematoria_licence_id' => $request->crematoria_licence_id,
                    'name' => $request->name,
                    'business_name' => $request->business_name,
                    'tax_clasification_name' => $request->tax_clasification_name,
                    'tax_clasification' => $request->tax_clasification,
                    'exempt_payee_code' => $request->exempt_payee_code,
                    'exemption_fatca_reporting_code' => $request->exemption_fatca_reporting_code,
                    'address' => $request->address,
                    'city_state_zipcode' => $request->city_state_zipcode,
                    'list_account_number' => $request->list_account_number,
                    'requester_name_address' => $request->requester_name_address,
                    'tin_type' => $request->tin_type,
                    'social_security_number' => $request->social_security_number,
                    'employer_identification_number' => $request->employer_identification_number,
                    'signature' => $request->signature,
                    'application_date' => $request->application_date,
                     'agent_entity_name' => $request->agent_entity_name,
                    'agency_entity_name' => $request->agency_entity_name,
                    
                   
                ]);

               $lastInsertedId = DB::getPdo()->lastInsertId();
               if($request->submitType =="Submit"){
                        $update =  DB::table('partner_application_details')->where('user_id',$lastInsertedId )->update(array(
                         'application_status' => "submitted",
                       
                    ));
                }
            
            }
        }

        if($userRole == '2'){
            $applicationCount = DB::table('agency_application_form')
                        ->where(['user_id' => $userId])
                        ->count();

            if($applicationCount > 0){
                $update =  DB::table('agency_application_form')->where('user_id',$userId )->update(array(
                     'agency_licence_name' => $request->agency_licence_name,
                    'lines_of_business' => $request->lines_of_business,
                    'npn' => $request->npn,
                    'income_tax_return_name' => $request->income_tax_return_name,
                    'business_entity_name' => $request->business_entity_name,
                    'tax_clasification_name' => $request->tax_clasification_name,
                    'tax_clasification' => $request->tax_clasification,
                    'exempt_payee_code' => $request->exempt_payee_code,
                    'exemption_fatca_reporting_code' => $request->exemption_fatca_reporting_code,
                    'address' => $request->address,
                    'city_state_zipcode' => $request->city_state_zipcode,
                    'list_account_number' => $request->list_account_number,
                    'requester_name_address' => $request->requester_name_address,
                    'tin_type' => $request->tin_type,
                    'social_security_number' => $request->social_security_number,
                    'employer_identification_number' => $request->employer_identification_number,
                     'signature' => $request->signature,
                    'application_date' => $request->application_date,
                    'first_name' => $request->first_name,
                    'middle_name' => $request->middle_name,
                    'last_name' => $request->last_name,
                    'other_name' => $request->other_name,
                    'years_used' => $request->years_used,
                    'background_check_copy' => $request->background_check_copy,
                    'background_check_authorize' => $request->background_check_authorize,
                    'background_check_signature' => $request->background_check_signature,
                    'background_check_date' => $request->background_check_date,

                ));
               // $lastInsertedId = DB::getPdo()->lastInsertId();

                if($request->hasfile('application_doc_path'))
                 {
                  
                    $total = count($request->file('application_doc_path'));
                    for( $i=0 ; $i < $total ; $i++ ) {
                    
                        $fileNameVal = time().rand(1,100).'.'.$request->file('application_doc_path')[$i]->extension();
                        $filePathVal =  $request->file('application_doc_path')[$i]->store('public/files');  
                        $files[] = $fileNameVal; 
                        //$imageType = $request->filetitle[$i];
                       
                        $update =  DB::table('agency_application_form')->where('user_id',$userId )->update(array('application_doc_path' => $filePathVal,));
                      
                    }
                    
                 }


               if($request->submitType =="Submit"){
                        $update =  DB::table('agency_application_form')->where('user_id',$userId )->update(array(
                         'application_status' => "submitted",
                       
                    ));
                }

            }
            else{
               $insert = DB::table('agency_application_form')->insert([
                    'user_id' =>  $userId,
                    'agency_licence_name' => $request->agency_licence_name,
                    'lines_of_business' => $request->lines_of_business,
                    'npn' => $request->npn,
                    'income_tax_return_name' => $request->income_tax_return_name,
                    'business_entity_name' => $request->business_entity_name,
                    'tax_clasification_name' => $request->tax_clasification_name,
                    'tax_clasification' => $request->tax_clasification,
                    'exempt_payee_code' => $request->exempt_payee_code,
                    'exemption_fatca_reporting_code' => $request->exemption_fatca_reporting_code,
                    'address' => $request->address,
                    'city_state_zipcode' => $request->city_state_zipcode,
                    'list_account_number' => $request->list_account_number,
                    'requester_name_address' => $request->requester_name_address,
                    'tin_type' => $request->tin_type,
                    'social_security_number' => $request->social_security_number,
                    'employer_identification_number' => $request->employer_identification_number,
                     'signature' => $request->signature,
                    'application_date' => $request->application_date,
                    'first_name' => $request->first_name,
                    'middle_name' => $request->middle_name,
                    'last_name' => $request->last_name,
                    'other_name' => $request->other_name,
                    'years_used' => $request->years_used,
                    'background_check_copy' => $request->background_check_copy,
                    'background_check_authorize' => $request->background_check_authorize,
                    'background_check_signature' => $request->background_check_signature,
                    'background_check_date' => $request->background_check_date,
                    
                    
                   
                ]);
                $lastInsertedId = DB::getPdo()->lastInsertId();

               if($request->hasfile('application_doc_path'))
                 {
                  
                    $total = count($request->file('application_doc_path'));
                    for( $i=0 ; $i < $total ; $i++ ) {
                    
                        $fileNameVal = time().rand(1,100).'.'.$request->file('application_doc_path')[$i]->extension();
                        $filePathVal =  $request->file('application_doc_path')[$i]->store('public/files');  
                        $files[] = $fileNameVal; 
                        //$imageType = $request->filetitle[$i];
                       


                      $update =  DB::table('agency_application_form')->where('application_details_id',$lastInsertedId )->update(array('application_doc_path' => $filePathVal,));
                      
                    }
                    
                 }

                 if($request->submitType =="Submit"){
                        $update =  DB::table('agency_application_form')->where('user_id',$lastInsertedId )->update(array(
                         'application_status' => "submitted",
                       
                    ));
                }


            
            }
        }



        if($userRole == '3'){
            $applicationCount = DB::table('agent_application_form')
                        ->where(['user_id' => $userId])
                        ->count();

            if($applicationCount > 0){
                $update =  DB::table('agent_application_form')->where('user_id',$userId )->update(array(
                    'license_type' => $request->license_type,
                    'npn' => $request->npn,
                    'income_tax_return_name' => $request->income_tax_return_name,
                    'business_entity_name' => $request->business_entity_name,
                    'tax_clasification_name' => $request->tax_clasification_name,
                    'tax_clasification' => $request->tax_clasification,
                    'exempt_payee_code' => $request->exempt_payee_code,
                    'exemption_fatca_reporting_code' => $request->exemption_fatca_reporting_code,
                    'address' => $request->address,
                    'city_state_zipcode' => $request->city_state_zipcode,
                    'list_account_number' => $request->list_account_number,
                    'requester_name_address' => $request->requester_name_address,
                    'tin_type' => $request->tin_type,
                    'social_security_number' => $request->social_security_number,
                    'employer_identification_number' => $request->employer_identification_number,
                     'signature' => $request->signature,
                    'application_date' => $request->application_date,
                    'first_name' => $request->first_name,
                    'middle_name' => $request->middle_name,
                    'last_name' => $request->last_name,
                    'other_name' => $request->other_name,
                    'years_used' => $request->years_used,
                    'background_check_copy' => $request->background_check_copy,
                    'background_check_authorize' => $request->background_check_authorize,
                    'background_check_signature' => $request->background_check_signature,
                    'background_check_date' => $request->background_check_date,

                ));
               // $lastInsertedId = DB::getPdo()->lastInsertId();

                if($request->hasfile('license_doc_path'))
                 {
                  
                    $total = count($request->file('license_doc_path'));
                    for( $i=0 ; $i < $total ; $i++ ) {
                    
                        $fileNameVal = time().rand(1,100).'.'.$request->file('license_doc_path')[$i]->extension();
                        $filePathVal =  $request->file('license_doc_path')[$i]->store('public/files');  
                        $files[] = $fileNameVal; 
                        //$imageType = $request->filetitle[$i];
                       
                        $update =  DB::table('agent_application_form')->where('user_id',$userId )->update(array('license_doc_path' => $filePathVal,));
                      
                    }
                    
                 }

                 if($request->submitType =="Submit"){
                        $update =  DB::table('agent_application_form')->where('user_id',$userId )->update(array(
                         'application_status' => "submitted",
                       
                    ));
                }

            }
            else{
               $insert = DB::table('agent_application_form')->insert([
                    'user_id' =>  $userId,
                    'license_type' => $request->license_type,
                    'npn' => $request->npn,
                    'income_tax_return_name' => $request->income_tax_return_name,
                    'business_entity_name' => $request->business_entity_name,
                    'tax_clasification_name' => $request->tax_clasification_name,
                    'tax_clasification' => $request->tax_clasification,
                    'exempt_payee_code' => $request->exempt_payee_code,
                    'exemption_fatca_reporting_code' => $request->exemption_fatca_reporting_code,
                    'address' => $request->address,
                    'city_state_zipcode' => $request->city_state_zipcode,
                    'list_account_number' => $request->list_account_number,
                    'requester_name_address' => $request->requester_name_address,
                    'tin_type' => $request->tin_type,
                    'social_security_number' => $request->social_security_number,
                    'employer_identification_number' => $request->employer_identification_number,
                     'signature' => $request->signature,
                    'application_date' => $request->application_date,
                    'first_name' => $request->first_name,
                    'middle_name' => $request->middle_name,
                    'last_name' => $request->last_name,
                    'other_name' => $request->other_name,
                    'years_used' => $request->years_used,
                    'background_check_copy' => $request->background_check_copy,
                    'background_check_authorize' => $request->background_check_authorize,
                    'background_check_signature' => $request->background_check_signature,
                    'background_check_date' => $request->background_check_date,
                    
                ]);
                $lastInsertedId = DB::getPdo()->lastInsertId();

               if($request->hasfile('license_doc_path'))
                 {
                  
                    $total = count($request->file('license_doc_path'));
                    for( $i=0 ; $i < $total ; $i++ ) {
                    
                        $fileNameVal = time().rand(1,100).'.'.$request->file('license_doc_path')[$i]->extension();
                        $filePathVal =  $request->file('license_doc_path')[$i]->store('public/files');  
                        $files[] = $fileNameVal; 
                        //$imageType = $request->filetitle[$i];
                       


                      $update =  DB::table('agent_application_form')->where('application_details_id',$lastInsertedId )->update(array('license_doc_path' => $filePathVal,));
                      
                    }
                    
                 }

                 if($request->submitType =="Submit"){
                        $update =  DB::table('agent_application_form')->where('user_id',$lastInsertedId )->update(array(
                         'application_status' => "submitted",
                       
                    ));
                }


            
            }
        }

         return redirect(url('/show_applicationform'))->with(['message-success'=>'Application saved successfully!']); 
      
    }

    

    

}
