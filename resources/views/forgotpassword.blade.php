@include('include.header')
	<div class="content-body pb-5 pt-5">
		
		<div class="login-box cl-wrap ml-auto mr-auto">
			  <!--<div class="login-logo">
				<a href="../../index2.html"><b>Admin</b>LTE</a>
			  </div> -->
			  <!-- /.login-logo -->
			  <div class="card">
				<div class="card-body login-card-body">
				  
				  <div class="row equal">
					<div class="col-lg-6 align-self-center">
						<div class="login-form-wrap">
						 <h2 class="login-box-msg color-blue font-weight700  pb-2">Forgot Password?</h2>
							<div class="bar-line"></div>
						  <form action="{{url('/forgot_password_email')}}" method="post">
        @csrf
							<div class="form-group">
								<label>User name</label>
								<input type="email"  name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Enter Username / Email">
								@error('email')
				                    <span class="invalid-feedback" role="alert">
				                        <strong>{{ $message }}</strong>
				                    </span>
				                @enderror
							</div>
							
							<div class="row">
							  <div class="col-12 text-center">
								<a href="{{route('signin')}}">Back to Login</a>
							  </div>
							  <!-- /.col -->
							</div>
							
							<div class="form-group mt-4">
								<button type="submit" class="btn btn-primary btn-block btn-lg">SUBMIT</button>
							</div>
							
						  </form>


						 </div>
					</div>
					
					<div class="d-none  d-lg-block  col-lg-6" style="background: #569aac;">
						<div class="login-image-wrap mt-auto mb-auto" style="background:#569aac">
							<img src="{{ asset('public/adminlte/dist/img/forgot-pass-bg.png')}}" class="img-fluid">
						</div>
					</div>
				  </div>
				  
				  
				 
				  
				</div>
				<!-- /.login-card-body -->
			  </div>
			</div>
		
	</div>
	
		@include('include.footer')
<script>

	
	  // BS-Stepper Init
	  document.addEventListener('DOMContentLoaded', function () {
		window.stepper = new Stepper(document.querySelector('.bs-stepper'))
	  })
	  
	  
</script>


</html>