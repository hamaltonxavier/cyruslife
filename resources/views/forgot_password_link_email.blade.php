<!DOCTYPE html>
<html lang="en">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cyrus Life Insurance</title>
	<head> </head>

	<body style="padding:0; margin:0">
		<table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#f6f6f6" align="center">
			<tbody>
				<tr>
					<td valign="top" align="center">
					
					<!-- CONTENT -->
						
						<table style="width:600px;max-width:600px; font-family:arial; background-color:#fff" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
							<tr>
								<td style="background-color:#004367; text-align:center">
									<div style="font-size:20px;height:20px;line-height:20px;">&nbsp;</div>
									<!--<img src="{{ asset('public/adminlte/dist/img/logo-white.png')}}" style="max-width:100%; border:0 none; outline:0 none;">-->
									<img src="{{ asset('public/adminlte/dist/img/logo-white.png')}}" width="220" style="max-width:100%; border:0 none; outline:0 none;">
									<div style="font-size:20px;height:20px;line-height:20px;">&nbsp;</div>
								</td>
							</tr>
							
							<tr>
								<td style="font-size:20px;height:20px;line-height:20px; background-color:#FED56D">&nbsp;</td>
							</tr>
							
							<tr>
								<td style="text-align:center; font-size:28px; font-weight:bold; background-color:#FED56D; color:#004367">Forgot Password</td>
							</tr>
							
							<tr>
								<td style="font-size:20px;height:20px;line-height:20px; background-color:#FED56D">&nbsp;</td>
							</tr>
							
							<tr>
								<td style="font-size:30px;height:30px;line-height:30px;">&nbsp;</td>
							</tr>
							
							<tr>
								<td align="center" style="text-align:center">Hi ,</td>
							</tr>
							
							<tr>
								<td style="font-size:30px;height:30px;line-height:30px;">&nbsp;</td>
							</tr>
							
							<tr>
								<td style="font-size:10px;height:10px;line-height:10px;">&nbsp;</td>
							</tr>
							<tr>
								<td align="center" style="text-align:center">We are received a request to reset the password. </td>
							</tr>
							<tr>
								<td style="font-size:10px;height:10px;line-height:10px;">&nbsp;</td>
							</tr>
							
							<tr>
								<td style="font-size:10px;height:10px;line-height:10px;">&nbsp;</td>
							</tr>
							
							<tr>
								<td style="font-size:10px;height:10px;line-height:10px;">&nbsp;</td>
							</tr>
							
							<tr>
								<td style="font-size:10px;height:10px;line-height:10px;">&nbsp;</td>
							</tr>
							<tr>
								<td align="center" style="text-align:center">Please check this link to reset the password: </td>
							</tr>
							<tr>
								<td style="font-size:10px;height:10px;line-height:10px;">&nbsp;</td>
							</tr>

							<tr>
								<td style="font-size:40px;height:40px;line-height:40px;">&nbsp;</td>
							</tr>
							<tr>
								<td align="center" style="text-align:center">
									
									<table style="border-radius:2px;" cellspacing="0" cellpadding="0" border="0" bgcolor="#004367" align="center">
											<tbody>
												<tr>
													<td  style="color:#ffffff; font-family:Arial, Segoe UI, Arial, Verdana, Trebuchet MS, sans-serif; font-weight:700; padding:15px 35px; text-transform:uppercase; font-size:13px; letter-spacing:1px;" align="left">
															<a href="{{url('reset_new_password/'.$token.'/'.$email)}}" style="color:#FED56D; text-decoration:none; font-size:20px;">
																Reset
															</a>
													</td>
												</tr>
											</tbody>
									</table>

								</td>
							</tr>
							<tr>
								<td style="font-size:40px;height:40px;line-height:40px;">&nbsp;</td>
							</tr>
							
							<tr>
								<td align="center" style="text-align:center">Thanks </td>
							</tr>
							<tr>
								<td style="font-size:5px;height:5px;line-height:5px;">&nbsp;</td>
							</tr>
							<tr>
								<td align="center" style="text-align:center">Cyrus Life Insurance </td>
							</tr>
							<tr>
								<td style="font-size:40px;height:40px;line-height:40px;">&nbsp;</td>
							</tr>
							<tr>
								<td style="background-color:#004367; text-align:center; color:#fff; font-size:13px;">
									<div style="font-size:20px;height:20px;line-height:20px;">&nbsp;</div>
									Copyright &copy; 2022. Cyrus Life Insurance
									<div style="font-size:20px;height:20px;line-height:20px;">&nbsp;</div>
								</td>
							</tr>
							
						</table>
					
					<!-- CONTENT -->
					</td>
				</tr>
			</tbody>
		</table>
</body>
</html>