<?php echo $__env->make('include.admin_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>	
<?php $__currentLoopData = $userDetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<div class="content-body pb-5 pt-5">
		
		<div class="container w-75">
				
				<!-- register form stepper -->
					
					<div class="row">
					  <div class="col-md-12">
						<div class="card card-default">
						  <div class="card-header">
							<h3 class="mt-3 font-weight700 color-blue pl-4 " style="float:left">View Profile</h3>
							<a href="<?php echo e(url('/admin/users_list')); ?>"><span class="backlList">Back to list</span></a>
						  </div>
						  <div class="card-body p-5">
							
							  
								<div id="register-part-1" class="content">
								  <h4 class="color-blue">Name and Address</h4>
								  <hr class="pb-2">
								  
										<div class="row">
											<div class="col-md-3 col-lg-2">
												<label>Salutation/Title</label>
												<input type="text" class="form-control"  value="<?php echo e($data->title); ?>"disabled>
											</div>
											
											<div class="col-md-9 col-lg-4">
												<div class="form-group">
													<label>First Name</label>
													<input type="text" class="form-control"  value="<?php echo e($data->first_name); ?>"disabled>
												  </div>
											</div>
											
											<div class="col-md-6 col-lg-3">
												<div class="form-group">
													<label>Middle Name</label>
													<input type="text" class="form-control"  value="<?php echo e($data->middle_name); ?>"disabled>
												  </div>
											</div>
											
											<div class="col-md-6 col-lg-3">
												<div class="form-group">
													<label>Last Name</label>
													<input type="text" class="form-control"  value="<?php echo e($data->last_name); ?>"disabled>
												  </div>
											</div>
											
										  </div>
										  
										  
										  <div class="row">
											<div class="col-md-6 col-lg-6">
												<label>Mobile Phone</label>
												<input type="text" class="form-control"  value="<?php echo e($data->mobile); ?>"disabled>
											</div>
											
											<div class="col-md-6  col-lg-6">
												<div class="form-group">
													<label>Email</label>
													<input type="text" class="form-control"  value="<?php echo e($data->email); ?>"disabled>
												  </div>
											</div>
										  </div>
										  
										  
										  
										  <div class="row">
											<div class="col-md-12 col-lg-4">
													 <div class="form-group">
														<label>Physical Address 1</label>
														<input type="text" class="form-control"  value="<?php echo e($data->address1); ?>"disabled>
													  </div>
												</div>
												
												<div class="col-md-6 col-lg-4">
													 <div class="form-group">
														<label>Physical Address 2</label>
														<input type="text" class="form-control"  value="<?php echo e($data->address2); ?>"disabled>
													  </div>
												</div>
												<div class="col-md-6 col-lg-4">
													 <div class="form-group">
														<label>Physical Address 3</label>
														<input type="text" class="form-control"  value="<?php echo e($data->address3); ?>"disabled>
													  </div>
												</div>
										  </div>
										  
										  <div class="row">
											<div class="col-md-4 col-lg-4">
													 <div class="form-group">
														<label>Country</label>
														<input type="text" class="form-control"  value="<?php echo e($data->countryName); ?>"disabled>
													  </div>
												</div>
												
												<div class="col-md-4 col-lg-4">
													 <div class="form-group">
														<label>State</label>
														<input type="text" class="form-control"  value="<?php echo e($data->stateName); ?>"disabled>
													  </div>
												</div>
												<div class="col-md-4 col-lg-4">
													 <div class="form-group">
														<label>City</label>
														<input type="text" class="form-control"  value="<?php echo e($data->city); ?>"disabled>
													  </div>
												</div>
												<div class="col-md-4 col-lg-4">
													 <div class="form-group">
														<label>Postal Code</label>
														<input type="text" class="form-control"  value="<?php echo e($data->postal_code); ?>"disabled>
													  </div>
												</div>
										  </div>
										  
									
								  
								  
								  
								  
								  
								  
								</div>
								
						
								<!-- register form step 2-->
								<div id="register-part-2" class="content mt-5">
								  <h4 class="color-blue">License / ID</h4>
								  <hr class="pb-2">
								  <div class="row">
									
									
									<div class="col-lg-6">
										<div class="form-group">
											<label>Insurance License</label>
											<input type="text" class="form-control"  value="<?php echo e($data->license_number); ?>"disabled>
										  </div>
									</div>
									
									
									<div class="col-lg-6">
										<div class="form-group">
											<label>License Type </label>
											<input type="text" class="form-control" placeholder="License Type" name="license_type_opt" id="license_type_opt" value="<?php echo e($data->license_type_opt); ?>"disabled>
										  </div>
									</div>
									
									
									</div>

									

									<div class="row">
									
									<div class="col-lg-6">
										<div class="form-group">
											<label>License Status <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control"  value="<?php echo e($data->license_status); ?>"disabled>
										  </div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>Expiration Date <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control"  value="<?php echo e($data->license_expiry_date); ?>"disabled>
										  </div>
									</div>
									</div>

									<div class="row">
									
									<div class="col-lg-6">
										<div class="form-group">
											<label>Designated Home State <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control"  value="<?php echo e($data->license_home_state); ?>"disabled>
										  </div>
									</div>
									
									</div>

								
								</div>
								
								
								<!-- register form step 3-->
								<div id="register-part-3" class="content mt-5">
								   <h4 class="color-blue">Business Overview</h4>
								  <hr class="pb-2">
								  <div class="row">								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Location of services (Regions)</label>
											<input type="text" class="form-control"  value="<?php echo e($data->location_of_services); ?>"disabled>
										  </div>
									</div>
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Business turn over (range)</label>
											<input type="text" class="form-control"  value="<?php echo e($data->business_turn_over); ?>"disabled>
										  </div>
									</div>
								  </div>
								  
								  <div class="row">								
								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Number of years in business</label>	
											<input type="text" class="form-control"  value="<?php echo e($data->number_of_years_in_business); ?>"disabled>					
										</div>
									</div>
									
									
								  </div>
								  
								 <?php $__currentLoopData = $docDetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $docData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								   <div class="row">								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											
											
											  	<?php if($docData->document_type == "business"): ?>
											  <label	><?php echo e($docData->document_name); ?></label>
											  	<?php endif; ?>
											 
										  </div>
									</div>
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label></label>
											
											  	<?php if($docData->document_type == "business"): ?>
											  	
											  		
											  	<a target="_blank" alt="View Document" title="View Document" href="<?php echo e(asset('storage/app/'.$docData->doucment_path)); ?>" class="btn"><i class="fa fa-eye"></i> </a>
											  	<a alt="Delete Document" title="Delete Document"  href="#" onclick="removeDocument('<?php echo e($docData->document_id); ?>')" class="btn"><i class="fa fa-trash"></i> </a>

											  		
											  	<?php endif; ?>
											  
										  </div>
									</div>

								</div>

								 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								 
								 
								</div>
								
								
							
							
						  </div>
						  <!-- /.card-body -->
						  
						</div>
						<!-- /.card -->
					  </div>
					</div>
				
				<!-- register form stepper -->
				
			</div>
		
	</div>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	<?php echo $__env->make('include.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php /**PATH C:\xampp\htdocs\cyruslife\resources\views/admin/agent_view_profile.blade.php ENDPATH**/ ?>