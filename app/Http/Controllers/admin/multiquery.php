<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Models\File;
use Auth;
use DB;





class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
       

    }

    public function listAllUsers(){
        $userRole  = Auth::user()->role;
       if($userRole !="superadmin"){
         return redirect('/show-profile');
       }
        $title = 'CYRUS | User List';
        return view('admin.list_users', compact('title'));
    }


    public function listUserDetails(){

        $userListData = array();
       // $userListData1 = array();
       
        $userDetails =DB::table('user_details')
                ->join('users','users.id','=','user_details.user_id')
                ->when(($test==2),function ($query){
                    return $query->where('id',2);
                })
                ->select( 'users.role as role' )
                ->orderBy('user_details.user_id', 'DESC')
                ->get();
         

        foreach($userDetails as $userDetailsDat) {
           
            if($userDetailsDat->role ='1'){
                $userDataVal =DB::table('user_details')
                ->leftJoin('partner_application_details', 'partner_application_details.user_id', '=', 'user_details.user_id')
                ->join('countries', 'countries.id', '=', 'user_details.country')
                ->join('states', 'states.id', '=', 'user_details.state')
                ->join('cities', 'cities.id', '=', 'user_details.city')
                ->join('users','users.id','=','user_details.user_id')
                ->select('users.role as role','partner_application_details.application_status' )
                ->where('users.role','=',$userDetailsDat->role)
                 ->orderBy('user_details.user_id', 'DESC')
                ->get();
                //$userDataVal= $userDetailsDat->role;
               // array_push($userListData,$userDataVal);
                $userDetails["userDetails"] = $userDataVal;
            }

           /* if($userDetailsDat->role ='2'){
                $userDataVal =DB::table('user_details')
                ->leftJoin('agency_application_form', 'agency_application_form.user_id', '=', 'user_details.user_id')
                ->join('countries', 'countries.id', '=', 'user_details.country')
                ->join('states', 'states.id', '=', 'user_details.state')
                ->join('cities', 'cities.id', '=', 'user_details.city')
                ->join('users','users.id','=','user_details.user_id')
                ->select( 'users.role as role','agency_application_form.application_status' )
                ->where('users.role','=',$userDetailsDat->role)
                 ->orderBy('user_details.user_id', 'DESC')
                ->get();
               // $userDataVal= $userDetailsDat->role;
               // array_push($userListData,$userDataVal);
                $userDetails["userDetails"] = $userDataVal;
            }

            if($userDetailsDat->role ='3'){
                $userDataVal =DB::table('user_details')
                ->leftJoin('agent_application_form', 'agent_application_form.user_id', '=', 'user_details.user_id')
                ->join('countries', 'countries.id', '=', 'user_details.country')
                ->join('states', 'states.id', '=', 'user_details.state')
                ->join('cities', 'cities.id', '=', 'user_details.city')
                ->join('users','users.id','=','user_details.user_id')
                ->select('users.role as role','agent_application_form.application_status' )
                ->where('users.role','=',$userDetailsDat->role)
                 ->orderBy('user_details.user_id', 'DESC')
                ->get();
                 //$userDataVal= $userDetailsDat->role;
                $userDetails["userDetails"] = $userDataVal;
                 
            }*/
                //array_add($userListData,$userDataVal);
            
               // $userListData = $userDataVal;

            
           

        }
        //$userListData[] = $userDataVal;
//array_push($userListData,$userDataVal);
        return response()->json($userDetails);
    }


    public function showSearchResult(Request $request){
        $userType = $request->userType;
        $query  =DB::table('user_details')
         ->join('countries', 'countries.id', '=', 'user_details.country')
         ->join('states', 'states.id', '=', 'user_details.state')
         ->join('cities', 'cities.id', '=', 'user_details.city')
         ->join('users','users.id','=','user_details.user_id');
        
         $query->when(request('userType') != null, function ($q) {
            return $q->where('users.role', request('userType'));
        });

         $query->when(request('stateId') != null, function ($q) {
            return $q->where('user_details.state', request('stateId'));
        });

         $query->when(request('cityId') != null, function ($q) {
            return $q->where('user_details.city', request('cityId'));
        });

         $query->when(request('adminStatus') != null, function ($q) {
            return $q->where('user_details.admin_status', request('adminStatus'));
        });
      $queryselect('user_details.*', 'countries.name as countryName','states.name as stateName','cities.name as cityName', 'users.role as role' );
      
        $result = $query->get(); 



        return response()->json($result);
    }

    public function viewUserData(Request $request){
        $userRole = $request->userType;
        $userId = $request->userId;
        
       $userDetails =DB::table('user_details')
         ->join('countries', 'countries.id', '=', 'user_details.country')
         ->join('states', 'states.id', '=', 'user_details.state')
         ->join('cities', 'cities.id', '=', 'user_details.city')
         ->select('user_details.*', 'countries.name as countryName','states.name as stateName','cities.name as cityName' )
         ->where('user_details.user_id', '=', $userId)
        ->get(); 
        
         $docDetails = DB::table('document_path')->whereIn('user_id', [$userId])->get();

        if($userRole == '1'){
            $title = 'CYRUS | Partner Profile';
            return view('admin.partner_view_profile', compact(['title','userDetails','docDetails']));
        }

         if($userRole == '2'){
            $title = 'CYRUS | Agency Profile';
            return view('admin.agency_view_profile', compact(['title','userDetails','docDetails']));
        }

         if($userRole == '3'){
            $title = 'CYRUS | Agent Profile';
            return view('admin.agent_view_profile', compact(['title','userDetails','docDetails']));
        }
    }


    public function showPartnerRegister(){
        
        $title = 'CYRUS | Add Partner';
        $countryDetails = DB::table('countries')->whereIn('id', [233])->get()->toArray();
        return view('admin.partner_add_profile', compact(['title','countryDetails']));
    }

    public function showAgencyRegister(){
        
        $title = 'CYRUS | Add Agency';
        $countryDetails = DB::table('countries')->whereIn('id', [233])->get()->toArray();
        return view('admin.agency_add_profile', compact(['title','countryDetails']));
    }


    public function showAgentRegister(){
        
        $title = 'CYRUS | Add Agent';
        $countryDetails = DB::table('countries')->whereIn('id', [233])->get()->toArray();
        return view('admin.agent_add_profile', compact(['title','countryDetails']));
    }


    public function saveUsers(Request $request){

       

        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $passwordVal = substr(str_shuffle($str_result), 0, 7);
        //$testPwd = "testpassword";
       
        $name=  $request->business_name;
      
        if($request->role == '3'){
            $name=  $request->first_name;
        }




            $emailId = $request->email;
          
         $insert = DB::table('users')->insert([
                'name' =>  $name,
                'email' => $request->email,
                'role' =>$request->role,
                'password' => Hash::make($passwordVal),
                //'password' => Hash::make($testPwd),
         ]);

         $lastInsertedId = DB::getPdo()->lastInsertId();

         if($request->role == '1'){
            $insert = DB::table('user_details')->insert([
                'user_id' =>  $lastInsertedId,
                'business_name' => $request->business_name,
                'address1' => $request->address1,
                'address2' => $request->address2,
                'address3' => $request->address3,
                'city' => $request->city,
                'state' => $request->state,
                'country' => $request->country,
                'postal_code' => $request->postal_code,
                'telephone' => $request->telephone,
                'title' => $request->title,
                'first_name' => $request->first_name,
                'middle_name' => $request->middle_name,
                'last_name' => $request->last_name,
                 'mobile' => $request->mobile,
                'email' => $request->email,
                'agent' => $request->agent,
                'license_type' => $request->license_type,
                'license_number' => $request->license_number,
                'license_status' => $request->license_status,
                'license_expiry_date' => $request->license_expiry_date,
                'license_home_state' => $request->license_home_state,
                'established_details' => $request->established_details,
                'funeral_number' => $request->funeral_number,
                'casked_funeral' => $request->casked_funeral,
                'cremations_annually' => $request->cremations_annually,
                'annual_preneed_volume' => $request->annual_preneed_volume,
               
            ]);
         }

         if($request->role == '2'){
            $insert = DB::table('user_details')->insert([
                'user_id' =>  $lastInsertedId,
                'business_name' => $request->business_name,
                'address1' => $request->address1,
                'address2' => $request->address2,
                'address3' => $request->address3,
                'city' => $request->city,
                'state' => $request->state,
                'country' => $request->country,
                'postal_code' => $request->postal_code,
                'telephone' => $request->telephone,
                'title' => $request->title,
                'first_name' => $request->first_name,
                'middle_name' => $request->middle_name,
                'last_name' => $request->last_name,
                 'mobile' => $request->mobile,
                'email' => $request->email,
                'agent' => $request->agent,
                'license_type' => $request->license_type,
                'license_number' => $request->license_number,
                'license_status' => $request->license_status,
                'license_expiry_date' => $request->license_expiry_date,
                'license_home_state' => $request->license_home_state,
                'location_of_services' => $request->location_of_services,
                'number_of_agents' => $request->number_of_agents,
                'business_turn_over' => $request->business_turn_over,
                'number_of_years_in_business' => $request->number_of_years_in_business,
                
            ]);
         }


         if($request->role == '3'){
            $insert = DB::table('user_details')->insert([
                'user_id' =>  $lastInsertedId,
                'address1' => $request->address1,
                'address2' => $request->address2,
                'address3' => $request->address3,
                'city' => $request->city,
                'state' => $request->state,
                'country' => $request->country,
                'postal_code' => $request->postal_code,
                'title' => $request->title,
                'first_name' => $request->first_name,
                'middle_name' => $request->middle_name,
                'last_name' => $request->last_name,
                 'mobile' => $request->mobile,
                'email' => $request->email,
                'license_type' => $request->license_type,
                'license_number' => $request->license_number,
                'license_status' => $request->license_status,
                'license_expiry_date' => $request->license_expiry_date,
                'license_home_state' => $request->license_home_state,
                'location_of_services' => $request->location_of_services,
                'business_turn_over' => $request->business_turn_over,
                'number_of_years_in_business' => $request->number_of_years_in_business,
                'lead_to_prospect_ratio' => $request->lead_to_prospect_ratio,
                
            ]);
         }


         if($request->hasfile('idFiles'))
         {
          
            $total = count($request->file('idFiles'));
            for( $i=0 ; $i < $total ; $i++ ) {
            
                $fileNameVal = time().rand(1,100).'.'.$request->file('idFiles')[$i]->extension();
                $filePathVal =  $request->file('idFiles')[$i]->store('public/files');  
                $files[] = $fileNameVal; 
                //$imageType = $request->filetitle[$i];
               


                $insert = DB::table('document_path')->insert([
                   'user_id' =>  $lastInsertedId,
                   'doucment_path' => $filePathVal,
                    'document_type'=>'idproof'
                 ]);
              
            }
            
         }

        

        /*  if($request->hasfile('files'))
         {
            foreach($request->file('files') as $key => $file)
            {
                $path = $file->store('public/files');
             
                $insert = DB::table('document_path')->insert([
                   'user_id' =>  $lastInsertedId,
                   'doucment_path' => $path,
                
                 ]);
 
            }
         }*/

         $files = [];
      
        if($request->hasfile('filenames'))
         {
          
            $total = count($request->file('filenames'));
            for( $i=0 ; $i < $total ; $i++ ) {
            
                $fileNameVal = time().rand(1,100).'.'.$request->file('filenames')[$i]->extension();
                $filePathVal =  $request->file('filenames')[$i]->store('public/files');  
                $files[] = $fileNameVal; 
                $imageType = $request->filetitle[$i];
                $documentNameVal = $request->filetitle[$i];


                $insert = DB::table('document_path')->insert([
                   'user_id' =>  $lastInsertedId,
                   'doucment_path' => $filePathVal,
                    'document_name'=>$documentNameVal,
                    'document_type'=>'business'
                 ]);
              
            }
            
         }


           $messageData = [
                'email' => $emailId,
                'name' => $name,
                'codeVal' => base64_encode($emailId)
                
            ];

            Mail::send('account_activation_email', $messageData, function($message) use ($emailId) {
            $message->to($emailId)
            ->subject("Activate your account");
            $message->from("info@cyruslife.com","Activate your account");
            });
 
       return redirect(url('/admin/users_list'))->with(['message-success'=>'User has been created successfully.']);

    }

    public function viewUserApplicationData(Request $request){
        $userRole = $request->userType;
        $userId = $request->userId;
        
       

        if($userRole == '1'){
            $title = 'CYRUS | Partner Application';

            $applicationCount = DB::table('partner_application_details')
                        ->where(['user_id' => $userId])
                        ->count();
            if($applicationCount > 0){      
                    $applicationDetails =DB::table('partner_application_details')
                 ->select('partner_application_details.*')
                 ->where('user_id', '=', $userId)
                 ->get();
                 
             }
             else{
                 $applicationDetails = '1';
             }
            return view('admin.partner_view_application_form', compact(['title','applicationDetails']));
        }

         if($userRole == '2'){
            $title = 'CYRUS | Agency Application';

            $applicationCount = DB::table('agency_application_form')
                        ->where(['user_id' => $userId])
                        ->count();
            if($applicationCount > 0){ 
                    $applicationDetails =DB::table('agency_application_form')
                 ->select('agency_application_form.*')
                 ->where('user_id', '=', $userId)
                 ->get();
                
             }
             else{
                $applicationDetails = '1';
             }
            return view('admin.agency_view_application_form', compact(['title','applicationDetails']));
        }

         if($userRole == '3'){
            $title = 'CYRUS | Agent Application';

            $applicationCount = DB::table('agent_application_form')
                        ->where(['user_id' => $userId])
                        ->count();
            if($applicationCount > 0){ 
                    $applicationDetails =DB::table('agent_application_form')
                 ->select('agent_application_form.*')
                 ->where('user_id', '=', $userId)
                 ->get();
                 
             }
             else{
                  $applicationDetails = '1';
             }
            return view('admin.agent_view_application_form', compact(['title','applicationDetails']));
        }
    }

    public function loadApplicationDetails(Request $request){
       $userRole = $request->userType;
        $userId = $request->userId;
        if($userRole == '1'){
             $applicationCount = DB::table('partner_application_details')
                        ->where(['user_id' => $userId])
                        ->count();
            if($applicationCount > 0){      
                    $applicationDetails =DB::table('partner_application_details')
                 ->select('partner_application_details.*')
                 ->where('user_id', '=', $userId)
                 ->get();
                  return response()->json($applicationDetails);
             }
             else{
                 return response()->json('1');
             }
        }

        if($userRole == '2'){
            $applicationCount = DB::table('agency_application_form')
                        ->where(['user_id' => $userId])
                        ->count();
            if($applicationCount > 0){ 
                    $applicationDetails =DB::table('agency_application_form')
                 ->select('agency_application_form.*')
                 ->where('user_id', '=', $userId)
                 ->get();
                 return response()->json($applicationDetails);
             }
             else{
                 return response()->json('1');
             }
        }


        if($userRole == '3'){
            $applicationCount = DB::table('agent_application_form')
                        ->where(['user_id' => $userId])
                        ->count();
            if($applicationCount > 0){ 
                    $applicationDetails =DB::table('agent_application_form')
                 ->select('agent_application_form.*')
                 ->where('user_id', '=', $userId)
                 ->get();
                 return response()->json($applicationDetails);
             }
             else{
                 return response()->json('1');
             }
        }
        
        
        
    }

}
