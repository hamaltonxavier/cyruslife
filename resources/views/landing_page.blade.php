<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{{ $title }}</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('public/adminlte/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('public/adminlte/https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css')}}">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="{{ asset('public/adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
   <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('public/adminlte/dist/css/adminlte.min.css')}}">
  
  <!-- custom css -->
  <link href="https://fonts.googleapis.com/css2?family=Rajdhani:wght@400;500;600&display=swap" rel="stylesheet"> 
  <link rel="stylesheet" href="{{ asset('public/adminlte/dist/css/custom-sytle.css')}}">
</head>

<body>
    
    <div class="wrapper-cl bg-cl-register">
    
    <header class="header-landing bg-transparent pt-4">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-7 d-flex">
                    <div class="landing-pg-logo">
                        <img src="{{ asset('public/adminlte/dist/img/logo-white.png')}}" class="img-fluid">
                    </div>
                     @if(Auth::check())
                           <a href="{{route('logout')}}" class="btn btn-login ml-auto">LOG OUT</a>
                        @else
                           <a href="{{route('signin')}}" class="btn btn-login ml-auto">SIGN IN</a>
                        @endif
                </div>
            </div>
        </div>
    </header>
    
    <div class="content-body register">
        <div class="section-register-content pt-3 pb-4">
            <div class="container">
            <div class="row">
                
                <div class="col-md-7 col-lg-7">
                    <div class="register-desc">Join the Cyrus Life network, let us co-create customer centered solutions, business processes and drive greater value for our clients.</div>
                </div>
                
                <div class="col-12 col-md-9 col-lg-7">
                    
                    <div class="row">
                    
                        <div class="col-12 col-md-4 col-lg-4 pl-0 pr-0">
                            <div class="registration-box">
                                <div class="registration-box-icon">
                                    <img src="{{ asset('public/adminlte/dist/img/icon-register-partner.png')}}" class="img-fluid">
                                </div>
                                <div class="registration-box-title">
                                    PARTNER
                                </div>
                                <div class="registration-box-desc">
                                    If your organization has a business presence in multiple states, work with agencies/funeral homes and agents, this plan is suited for your organization. 
                                </div>
                                <div class="registration-box-btn text-center mt-3">
                                    <a href="{{route('partner_registration')}}" class="btn btn-login">Join us</a>
                                </div>
                            </div>
                        </div>
                
                        <div class="col-12 col-md-4 col-lg-4 pl-0 pr-0">
                            <div class="registration-box middle">
                                <div class="registration-box-icon">
                                    <img src="{{ asset('public/adminlte/dist/img/icon-register-agency.png')}}" class="img-fluid">
                                </div>
                                <div class="registration-box-title">
                                    AGENCY
                                </div>
                                <div class="registration-box-desc">
                                    If your organization is an independent agency and work with agents, this is the right plan for your agency.
                                </div>
                                <div class="registration-box-btn text-center mt-3">
                                    <a href="{{route('agency_registration')}}" class="btn btn-login">Join us</a>
                                </div>
                            </div>
                        </div>
                
                        <div class="col-12 col-md-4 col-lg-4 pl-0 pr-0">
                            <div class="registration-box">
                                <div class="registration-box-icon">
                                    <img src="{{ asset('public/adminlte/dist/img/icon-register-agent.png')}}" class="img-fluid">
                                </div>
                                <div class="registration-box-title">
                                    AGENT
                                </div>
                                <div class="registration-box-desc">
                                    If you are an agent who operate independently and intend to work with us, you are in the right place.
                                </div>
                                <div class="registration-box-btn text-center mt-3">
                                    <a href="{{route('agent_registration')}}" class="btn btn-login">Join us</a>
                                </div>
                            </div>
                        </div>
                    
                    </div>
                
                </div>
            </div>
        </div>
        </div>
    </div>
    
    <footer id="footer" class="bg-transparent">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-7 d-flex">
                    <div>&copy; 2022 CYRUS Life insurance</div>
                    <ul class="ml-3 footer-links d-flex">
                        <li><a href="#">Privacy</a></li>
                        <li><a href="#">Terms &amp; Conditions</a></li>
                    </ul>
                </div>
                
            </div>
        </div>
    </footer>
    
    
    </div>
</body>


<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="{{ asset('public/adminlte/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{ asset('public/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('public/adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('public/adminlte/dist/js/adminlte.js')}}"></script>

<!-- sticky footer -->


</html>