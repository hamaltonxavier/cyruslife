<?php echo $__env->make('include.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>	


	<div class="content-body pb-5 pt-5">
		
		<div class="container w-75">
				
				<!-- register form stepper -->
					<form id="partnerRegistrationForm" class="registrationForm" method="POST" action="<?php echo e(route('registration')); ?>" enctype="multipart/form-data" onsubmit="return partnerFormValidation()">
				<!--<form id="partnerRegistrationForm" class="registrationForm" method="POST" action="<?php echo e(route('registration')); ?>" enctype="multipart/form-data" >-->
  <?php echo csrf_field(); ?>
					<div class="row">
					  <div class="col-md-12">
						<div class="card card-default">
						  <div class="card-header">
							<h3 class="text-center mt-3 font-weight700 color-blue">Register - Partner Account</h3>
							<h5 class=" text-center mt-3 font-weight700 color-blue"><span class="funeralHomeName" style="border-bottom: 1px solid rgba(0,0,0,.125);"></span></h5>
 <ul>		
     <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
         <li class="error"><?php echo e($error); ?></li>
     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</ul>
							 <?php echo $__env->make('include.message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
						  </div>

						  
						  <div class="card-body p-0">
							<div class="bs-stepper linear">
							  <div class="bs-stepper-header" role="tablist">
								<!-- your steps here -->
								
								<div class="step active" data-target="#register-part-1">
								  <button type="button" class="step-trigger" role="tab" aria-controls="register-part-1" id="register-part-1-trigger" aria-selected="false" disabled="disabled">
									<span class="bs-stepper-circle">1</span>
									<span class="bs-stepper-label">Business Name and Address</span>
								  </button>
								</div>
								
								<div class="line"></div>
								
								<div class="step" data-target="#register-part-2">
								  <button type="button" class="step-trigger" role="tab" aria-controls="register-part-2" id="register-part-2-trigger" aria-selected="true">
									<span class="bs-stepper-circle">2</span>
									<span class="bs-stepper-label">Primary contact details</span>
								  </button>
								</div>
								
								<div class="line"></div>
								
								<!--<div class="step" data-target="#register-part-3">
								  <button type="button" class="step-trigger" role="tab" aria-controls="register-part-3" id="register-part-3-trigger" aria-selected="true">
									<span class="bs-stepper-circle">3</span>
									<span class="bs-stepper-label">Agency Licence / ID</span>
								  </button>
								</div>
								
								<div class="line"></div>-->
								
								<div class="step" data-target="#register-part-4">
								  <button type="button" class="step-trigger" role="tab" aria-controls="register-part-4" id="register-part-4-trigger" aria-selected="true">
									<span class="bs-stepper-circle">3</span>
									<span class="bs-stepper-label">Business Overview</span>
								  </button>
								</div>
								
								
							  </div>
							  <div class="bs-stepper-content">
								<!-- your steps content here -->
								
								<div id="register-part-1" class="content active dstepper-block" role="tabpanel" aria-labelledby="register-part-1-trigger">
								  <div class="row">
									<div class="col-md-12 col-lg-12">
										<div class="form-group">
											<label>Name of the Business <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control" placeholder="Name of the Business" id="business_name" name="business_name" value=<?php echo e(old('business_name')); ?>>
											<input type="hidden" name="role" value="1">
											<input type="hidden" id="formType" value="insert" />
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										 <div class="form-group">
											<label>Physical Address 1 <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control"  placeholder="Address 1" id="address1" name="address1" value=<?php echo e(old('address1')); ?>>
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										 <div class="form-group">
											<label>Physical Address 2 <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control"  placeholder="Address 2" id="address2" name="address2" value=<?php echo e(old('address2')); ?>>
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										 <div class="form-group">
											<label>Physical Address 3</label>
											<input type="text" class="form-control"  placeholder="Address 3" name="address3" id="address3" value=<?php echo e(old('address3')); ?>>
										  </div>
									</div>
									
									
									<div class="col-md-6 col-lg-4">
										<div class="form-group">
												  <label>Country <span class="requiredSymbol">*</span></label>
												  <select class="form-control" id="countryList" name="country">
													<option value="">Select Country</option>

													<?php $__currentLoopData = $countryDetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<option value="<?php echo e($data->id); ?>"><?php echo e($data->name); ?></option>
													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												  </select>
												</div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										<div class="form-group">
												  <label>State <span class="requiredSymbol">*</span></label>
												  <select class="form-control" id="stateList" name="state">
												  	<option value="">Select State</option>
												  </select>
												</div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										<div class="form-group">
												  <label>City <span class="requiredSymbol">*</span></label>
												  <select class="form-control" id="cityList" name="city">
													<option value="">Select City</option>
												  </select>
												</div>
									</div>
									
									<div class="col-md-6  col-lg-6">
										<div class="form-group">
													<label>Postal Code <span class="requiredSymbol">*</span></label>
													<input type="text" class="form-control numberOnly" maxlength="6"  placeholder="Postal Code" name="postal_code" id="postal_code" value=<?php echo e(old('postal_code')); ?>>
												  </div>
									</div>
										
										
										  
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Main Business Telephone <span class="requiredSymbol">*</span></label>
											<input type="tel" class="form-control numberOnly"  placeholder="Mobile / Telephone Number" name="telephone" id="telephone" value=<?php echo e(old('telephone')); ?>>
										  </div>
									</div>

									<div class="col-lg-6">
										<div class="form-group">
											<label>Business License<span class="requiredSymbol">*</span></label>
											<!--<input type="text" class="form-control" placeholder="License Type" name="license_type" id="license_type" value=<?php echo e(old('license_type')); ?>>-->
											<!--<select class="form-control" id="license_type" name="license_type">
											<option value="" >Select License Type</option>
											<option value="Driving Licence" >Driving Licence</option>
											<option value="Passport" >Passport</option>
											<option value="Social Security Card" >Social Security Card</option>
										</select>-->
										<input type="text" class="form-control numberOnly" placeholder="Business License" name="license_type" id="license_type" value=<?php echo e(old('license_type')); ?>>
										  </div>
									</div>
										
									</div>
									
									
									
								  
								  
								  
								  <div class="btn btn-primary" onclick="stepper.next()">Next</div>
							</div>
								
								<!-- registration part 2-->
								<div id="register-part-2" class="content" role="tabpanel" aria-labelledby="register-part-2-trigger">
								 
								  <div class="row">
									<div class="col-md-3 col-lg-2">
										<label>Salutation/Title <span class="requiredSymbol">*</span></label>
										<select class="form-control" id="title" name="title">
											<option value="Mr">Mr</option>
											<option value="Mrs">Mrs</option>
										</select>
									</div>
									
									<div class="col-md-9 col-lg-4">
										<div class="form-group">
											<label>First Name <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control" placeholder="First Name" id="first_name" name="first_name" value=<?php echo e(old('first_name')); ?>>
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-3">
										<div class="form-group">
											<label>Middle Name</label>
											<input type="text" class="form-control" placeholder="Middle Name" name="middle_name"id="middle_name" value=<?php echo e(old('middle_name')); ?>>
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-3">
										<div class="form-group">
											<label>Last Name <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control" placeholder="Last Name" name="last_name" id="last_name" value=<?php echo e(old('last_name')); ?>>
										  </div>
									</div>
									
								  </div>
								  
								  <div class="row">
									<div class="col-md-6 col-lg-6">
										<label>Mobile Phone <span class="requiredSymbol">*</span></label>
										<input type="tel" class="form-control numberOnly" placeholder="Mobile Phone" name="mobile" id="mobile" value=<?php echo e(old('mobile')); ?>>
									</div>
									
									<div class="col-md-6  col-lg-">
										<div class="form-group">
											<label>Email <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control" placeholder="Email Address" id="email" name="email" value=<?php echo e(old('email')); ?>>
											<input type="hidden" class="uniqueEmailError" value="0" />
											<span class="emailErrorTxt error" ></span> 
										  </div>
									</div>
									<div class="col-md-6  col-lg-4">
										<div class="form-group">
											<label>Designation </label>
											<input type="text" class="form-control" placeholder="Designation" id="designation" name="designation" value=<?php echo e(old('designation')); ?>>
										  </div>
									</div>
									<!--<div class="col-md-6  col-lg-4">
										<div class="form-group">
											<label>Department </label>
											<input type="text" class="form-control" placeholder="Department" id="department" name="department" value=<?php echo e(old('department')); ?>>
										  </div>
									</div>-->
									<div class="col-md-6  col-lg-6">
										<div class="form-group">
											<label>Is your office located at the business address given on prior screen? </label>
											<!--<input type="text" class="form-control" placeholder="Office Location" id="office_location" name="office_location" value=<?php echo e(old('office_location')); ?>>-->
											<select class="form-control" id="office_location" name="office_location">
											<option value="Yes">Yes</option>
											<option value="No">No</option>
										</select>
										  </div>
									</div>
									<div class="officeLocationDiv" style="display:none">
									<div class="col-md-6 col-lg-4 offAddressDetails">
										 <div class="form-group">
											<label>Office Address 1 </label>
											<input type="text" class="form-control"  placeholder="Office Address 1" id="office_address1" name="office_address1" value=<?php echo e(old('office_address1office_address2')); ?>>
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-4 offAddressDetails">
										 <div class="form-group">
											<label>Office Address 2 </label>
											<input type="text" class="form-control"  placeholder="Office Address 2" id="office_address2" name="office_address2" value=<?php echo e(old('office_address2')); ?>>
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-4 offAddressDetails">
										 <div class="form-group">
											<label>Office  Address 3</label>
											<input type="text" class="form-control"  placeholder="Office Address 3" name="office_address3" id="office_address3" value=<?php echo e(old('office_address3')); ?>>
										  </div>
									</div>
									
									
									<div class="col-md-6 col-lg-4 offAddressDetails">
										<div class="form-group">
												  <label>Office Country </label>
												  <select class="form-control" id="officeCountryList" name="office_country">
													<option value="">Select Country</option>

													<?php $__currentLoopData = $countryDetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<option value="<?php echo e($data->id); ?>"><?php echo e($data->name); ?></option>
													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												  </select>
												</div>
									</div>
									
									<div class="col-md-6 col-lg-4 offAddressDetails">
										<div class="form-group">
												  <label>Office State </label>
												  <select class="form-control" id="officeStateList" name="office_state">
												  	<option value="">Select State</option>
												  </select>
												</div>
									</div>
									
									<div class="col-md-6 col-lg-4 offAddressDetails">
										<div class="form-group">
												  <label> Office City </label>
												  <select class="form-control" id="officeCityList" name="office_city">
													<option value="">Select City</option>
												  </select>
												</div>
									</div>
									
									<div class="col-md-6  col-lg-6 offAddressDetails">
										<div class="form-group">
													<label>Office Postal Code </label>
													<input type="text" class="form-control numberOnly" maxlength="6"  placeholder="Office Postal Code" name="office_postal_code" id="office_postal_code" value=<?php echo e(old('office_postal_code')); ?>>
												  </div>
									</div>
									</div>
									<div class="col-lg-12">
										
										
										
										<div class="form-group">
											<div class="row">
												<div class="col-md-6 col-lg-6">
													<label>Is your business also registered as an insurance agency? <span class="requiredSymbol">*</span></label>
												</div>
												<div class="col-md-1 col-lg-1">
													<div class="form-check">
													  <input class="form-check-input" name="agent" type="radio"  value="yes">
													  <label class="form-check-label">Yes</label>
													</div>
												</div>
												<div class="col-md-1 col-lg-1">
													<div class="form-check">
													  <input class="form-check-input" name="agent" type="radio" checked="" value="no">
													  <label class="form-check-label">No</label>
													</div>
												</div>
											</div>
											
										</div>
									</div>

									
								  </div>
								  <div class="agencyLicenceDiv" style="display:none">
<div class="row">
	<div class="col-lg-6">
										<div class="form-group">
											<label>Insurance License </label>
											<input type="text" class="form-control numberOnly" placeholder="Insurance License" name="license_number" id="license_number" value=<?php echo e(old('license_number')); ?>>
										  </div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>License Status </label>
											<select class="form-control" id="license_status" name="license_status">
											<option value="active">Active</option>
											<option value="expired">Expired</option>
										</select>
										  </div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>Expiration Date </label>
											<input type="text" class="form-control" placeholder="MM/YY" name="license_expiry_date" id="license_expiry_date" value=<?php echo e(old('license_expiry_date')); ?>>
										  </div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>Designated Home State </label>
											<!--<input type="text" class="form-control" placeholder="Designated Home State" name="license_home_state" id="license_home_state" value=<?php echo e(old('license_home_state')); ?>>-->
											<select class="form-control" id="license_home_state" name="license_home_state">
												  	<option value="">Select State</option>
												  </select>
										  </div>
									</div>
</div>
<div class="form-group row mb-2">
									<label class="col-11 col-sm-11 col-md-11 col-lg-4 col-form-label">License type 
									</label>
									
									<div class="col-sm-7 col-lg-7 pl-0">
										<div class="row">
										  <div class="col-lg-11">
											<div class="form-group d-flex align-items-start mb-2">
												<input type="radio" class="form-check-input ml-0" id="license_type_opt" name="license_type_opt" value="Limited">
												<label class="form-check-label pl-4">Limited (dollar amount and/or insurance type restriction) </label>
											</div>
										  </div>
										</div>
										
									
										<div class="row ">
										  <div class="col-lg-11">
											<div class="form-group d-flex align-items-start mb-2">
												<input type="radio" class="form-check-input ml-0" id="license_type_opt" name="license_type_opt" value="Full">
												<label class="form-check-label pl-4">Full (any life product, any dollar amount)</label>
											</div>
										  </div>
										</div>
										
										
									</div>
								</div> <!-- row end -->

								

									<div class="row">
									
									

									<div class="col-lg-6">
										<div class="form-group">
											<label for="exampleInputFile">Required Document Uploads</label>
											<div class="input-group">
											  <input type="file" name="idFiles[]" class="myfrm form-control">
											 
											</div>
											
										  </div>
									</div>
									
									</div>
</div>
								 
								  
								  <div class="btn btn-primary" onclick="stepper.previous()">Previous</div>
								  <div class="btn btn-primary" onclick="stepper.next()">Next</div>
								</div>
								
								
								<!-- register form step 3-->
								<!--<div id="register-part-3" class="content" role="tabpanel" aria-labelledby="register-part-3-trigger">
								 
								  <div class="row">
									
									
									
									</div>

									
									

								  <div class="btn btn-primary" onclick="stepper.previous()">Previous</div>
								   <div class="btn btn-primary" onclick="stepper.next()">Next</div>
								</div>-->
								
								
								<!-- register form step 4-->
								<div id="register-part-4" class="content" role="tabpanel" aria-labelledby="register-part-4-trigger">
								  
								  <div class="row">								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Month/Year established <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control" placeholder="MM/YYYY" name="established_details" id="established_details">
										  </div>
									</div>
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Number of Funeral Directors/Embalmers on staff <span class="requiredSymbol">*</span></label>	
											<input type="text" class="form-control numberOnly" placeholder="Number of Funeral Directors/Embalmers on staff" name="funeral_number" id="funeral_number">
										</div>
									</div>
								  </div>
								  
								  <div class="row">								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Number of casketed funerals annually <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control numberOnly" placeholder="Number of casketed funerals annually" name="casked_funeral">
										  </div>
									</div>
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Number of cremations annually <span class="requiredSymbol">*</span></label>	
											<input type="text" class="form-control numberOnly" placeholder="Number of cremations annually" id="cremations_annually" name="cremations_annually">					
										</div>
									</div>
								  </div>
								  
								  <div class="row">								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Annual preneed volume <span class="requiredSymbol">*</span></label>
											<div class="input-group">
												<div class="input-group-prepend">
												<span class="input-group-text"><b>$</b></span>
												</div>
												<!--<input type="text" class="form-control numberOnly" placeholder="Annual preneed volume" id="annual_preneed_volume" name="annual_preneed_volume">-->
												<select class="form-control" id="annual_preneed_volume" name="annual_preneed_volume">
														<option value="100 - 1,000">100 - 1,000</option>
														<option value="1,000 - 10,000">1,000 - 10,000</option>
														<option value="10,000 - 100,000">10,000 - 100,000</option>
														<option value="100,000 - 1,000,000">100,000 - 1,000,000</option>
														<option value="1,000,000 - 10,000,000">1,000,000 - 10,000,000</option>
													</select>

											</div>
											
											
											
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-6">
										<!--<div class="form-group">
											<label for="exampleInputFile">Required Document Uploads<span class="requiredSymbol">*</span></label>
											<div class="input-group">
											  <div class="custom-file">
												<input type="file" class="custom-file-input" id="exampleInputFile" name="files[]">
												<label class="custom-file-label" for="exampleInputFile">Choose file</label>
											  </div>
											 
											</div>
											
										  </div>-->
									</div>
								  </div>
<!--First-->
									<div class="row increment">
								  		<div class="col-md-6 col-lg-6">
												<div class="form-group">
													<div class="input-group realprocode control-group lst" >
								  				<!--<input type="text" name="filetitle[]" class="myfrm form-control" placeholder="Document Name">-->
								  				<select class="form-control" id="" name="filetitle[]">
														<option value="Producer-Agreement">Producer Agreement</option>
														<option value="Direct-Deposit-Authorization">Direct Deposit Authorization</option>
														<option value="Producer-Compensation-Schedule">Producer Compensation Schedule</option>
														<option value="Explanation-of-Information-found-on-MIB">Explanation of Information found on MIB</option>
														<option value="Other-Supporting-Documentation">Other Supporting Documentation</option>
													</select>
											      
											    </div>
												</div>
											</div>

											<div class="col-md-6 col-lg-6">
												<div class="form-group">
													<div class="input-group realprocode control-group lst " >
								  				
											      <input type="file" name="filenames[]" class="myfrm form-control">
											      <div class="input-group-btn"> 
											        <button class="btn btn-success addFile" type="button"> <i class="fldemo glyphicon glyphicon-plus"></i>Add</button>
											      </div>
											    </div>
												</div>
											</div>
								  </div>

								<!--Second-->
							<div class="clone d-none " >
								<div class="row ">
									<div class="realprocode col-md-12 col-lg-12">
								  		<div class="col-md-6 col-lg-6" style="float: left;">
												<div class="form-group">
													<div class=" control-group lst input-group" style="margin-top:10px">
											      	<!--<input type="text" name="filetitle[]" class="myfrm form-control" placeholder="Document Name">-->
											      	<select class="form-control" id="" name="filetitle[]">
														<option value="Driver-License">State-Issued Driver's License</option>
														<option value="Producer-Agreement">Producer Agreement</option>
														<option value="Direct-Deposit-Authorization">Direct Deposit Authorization</option>
														<option value="Producer-Compensation-Schedule">Producer Compensation Schedule</option>
														<option value="Explanation-of-Information-found-on-MIB">Explanation of Information found on MIB</option>
														<option value="Other-Supporting-Documentation">Other Supporting Documentation</option>
													</select>
											        
											      </div>
												</div>
											</div>

											<div class="col-md-6 col-lg-6" style="float: left;">
												<div class="form-group">
													<div class=" control-group lst input-group" style="margin-top:10px">
											      	
											        <input type="file" name="filenames[]" class="myfrm form-control">
											        <div class="input-group-btn"> 
											          <button class="btn btn-danger" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>
											        </div>
											      </div>
												</div>
											</div>
										</div>
								  </div>

								</div>
								<hr class="pb-2">
<!--<h4 class="color-blue">For Multi-rooftop (multi-location) Funeral Homes</h4>-->
								  <div class="row">
								  		<div class="col-md-12 col-lg-12">
								  			<div class="form-check">
												<input type="checkbox" class="form-check-input" id="bgChk">
												<label class="form-check-label" for="exampleCheck1">I hereby authorize Cyrus Life or an Agency appointed by Cyrus Life to verify the submitted records.</label>

											</div>
											<div class="error bgChkError"></div>

								  		</div>
								  	</div>
<br/>

								  <!--<div class="row">
								  		<div class="col-md-12 col-lg-12">
								  			
								  			<div class="input-group realprocode control-group lst increment" >
								  				<input type="text" name="filetitle[]" class="myfrm form-control">
											      <input type="file" name="filenames[]" class="myfrm form-control">
											      <div class="input-group-btn"> 
											        <button class="btn btn-success addFile" type="button"> <i class="fldemo glyphicon glyphicon-plus"></i>Add</button>
											      </div>
											    </div>
											    <div class="clone d-none">
											      <div class="realprocode control-group lst input-group" style="margin-top:10px">
											      	<input type="text" name="filetitle[]" class="myfrm form-control">
											        <input type="file" name="filenames[]" class="myfrm form-control">
											        <div class="input-group-btn"> 
											          <button class="btn btn-danger" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>
											        </div>
											      </div>
											    </div>
								  		</div>
								  </div>-->
								 
								  <div class="btn btn-primary" onclick="stepper.previous()">Previous</div>
								  <button type="submit" id="formSubmit" class="btn btn-primary">Submit</button>
								  <span style="display:none" class="error finalErr">Please fill the required fields *</span>
								 <!-- <span style="display:none" class="error emailErr">Email id not valid!</span>-->
								  <span class="emailErrorTxt error" ></span> 
								
								</div>
								
								
							  </div>

							 
							</div>
						  </div>
						  <!-- /.card-body -->
						  
						</div>
						<!-- /.card -->
					  </div>
					</div>
				</form>
				<!-- register form stepper -->
				
			</div>
		
	</div>
	
	<?php echo $__env->make('include.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<script type="text/javascript">
    $(document).ready(function() {
    	loadStateRegion();
      $(".addFile").click(function(){ 
          var lsthmtl = $(".clone").html();
          $(".increment").after(lsthmtl);
      });
      $("body").on("click",".btn-danger",function(){ 
          $(this).parents(".realprocode").remove();
      });
      $("input[name='agent']").click(function(){
      	$('.agencyLicenceDiv').css('display','none');
      	console.log($(this).val());
      	$('#license_number').val('');
      	$('#license_status').val('');
      	$('#license_expiry_date').val('');
      	$('#license_home_state').val('');
      	if($(this).val() == 'yes'){
      		$('.agencyLicenceDiv').css('display','block');
      	}
      });

      $('#business_name').focusout(function() {
      		$('.funeralHomeName').html($(this).val());
      });

     
    });
</script><?php /**PATH C:\xampp\htdocs\cyruslife\resources\views/partner_registration.blade.php ENDPATH**/ ?>