<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Auth;
use DB;
use App\Models\File;


class RegisterController extends Controller
{

    public function testemail(){
       /*  $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
            $passwordVal = substr(str_shuffle($str_result), 0, 7);


            $emailId = "hamalton.xavier@gmail.com";
            $messageData = [
                'email' => $emailId,
                'name' => "hamalton",
                'code' => base64_encode($emailId),
                'password'=>$passwordVal,
            ];

            Mail::send('account_activation', $messageData, function($message) use ($emailId) {
            $message->to($emailId)
            ->subject("Activate your account");
            $message->from("hamaltonlocaltest@gmail.com","Activate your account");
            });
*/

           
    }

    public function uniqueEmail(Request $request){
        $emailCount = DB::table('users')
                    ->where(['email' => $request->emailid])
                    ->count();
        if($emailCount != 0){
            return "1";
        }
        else{
            return "2";
        }
    }


    public function activate_account($email){
         $emailVal =  base64_decode($email);

          $activateStatus = DB::table('users')->where('email', $emailVal)->update(array('active' => 1));
        $title = 'CYRUS | Create Password';
        return view('new_password', compact(['title','emailVal']));
        /*$userCount = DB::table('users')
                    ->where(['email' => $emailVal])
                    ->count();
        if($userCount> 0){
            $userActiveCount = DB::table('users')
                    ->where(['email' => $emailVal,'active'=>'1', 'password_link'=>'1'])
                    ->count();
            if($userActiveCount != 0){
               return redirect(url('/signin'))->with(['message-info'=>'Your account already activated. Please login']);

            }
            else{
                $activateStatus = DB::table('users')->where('email', $emailVal)->update(array('active' => 1));
                return redirect(url('/signin'))->with(['message-success'=>'Your account has been activated. Please login']);
                
            }
        }*/

    }


    public function createPassword(Request $request){
         $this->validate($request, [
          'password' => 'required|confirmed|min:6|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/', 
       ]);

         //Updating password
         $passwordVal = Hash::make($request->password);
         $emailVal = $request->email;
         $activateStatus = DB::table('users')->where('email', $emailVal)->update(array('password' => $passwordVal, 'password_link'=>1));
          return redirect(url('/signin'))->with(['message-success'=>'Password has been updated. Please login']);

    }


    
    

    public function saveUsers(Request $request){


        $this->validate($request, [
        'email' => 'required|email|unique:users,email',
        'country' => 'required',
        'state' => 'required',
        'city' => 'required',

        ]);

        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $passwordVal = substr(str_shuffle($str_result), 0, 7);
        //$testPwd = "testpassword";
       
        $name=  $request->business_name;
      
        if($request->role == '3'){
            $name=  $request->first_name;
        }




            $emailId = $request->email;
          
         $insert = DB::table('users')->insert([
                'name' =>  $name,
                'email' => $request->email,
                'role' =>$request->role,
                'password' => Hash::make($passwordVal),
                //'password' => Hash::make($testPwd),
         ]);

         $lastInsertedId = DB::getPdo()->lastInsertId();

         if($request->role == '1'){
            $insert = DB::table('user_details')->insert([
                'user_id' =>  $lastInsertedId,
                'business_name' => $request->business_name,
                'address1' => $request->address1,
                'address2' => $request->address2,
                'address3' => $request->address3,
                'city' => $request->city,
                'state' => $request->state,
                'country' => $request->country,
                'postal_code' => $request->postal_code,
                'telephone' => $request->telephone,
                'title' => $request->title,
                'first_name' => $request->first_name,
                'middle_name' => $request->middle_name,
                'last_name' => $request->last_name,
                'designation' => $request->designation,
                //'department' => $request->department,
                'office_location' => $request->office_location,
                'office_address1' => $request->office_address1,
                'office_address2' => $request->office_address2,
                'office_address3' => $request->office_address3,
                'office_country' => $request->office_country,
                'office_state' => $request->office_state,
                'office_city' => $request->office_city,
                'office_postal_code' => $request->office_postal_code,
                 'mobile' => $request->mobile,
                'email' => $request->email,
                'agent' => $request->agent,
                'license_type' => $request->license_type,
                'license_type_opt' => $request->license_type_opt,
                'license_number' => $request->license_number,
                'license_status' => $request->license_status,
                'license_expiry_date' => $request->license_expiry_date,
                'license_home_state' => $request->license_home_state,
                'established_details' => $request->established_details,
                'funeral_number' => $request->funeral_number,
                'casked_funeral' => $request->casked_funeral,
                'cremations_annually' => $request->cremations_annually,
                'annual_preneed_volume' => $request->annual_preneed_volume,
                'annual_preneed_volume' => $request->annual_preneed_volume,

               
            ]);
         }

         if($request->role == '2'){
            $insert = DB::table('user_details')->insert([
                'user_id' =>  $lastInsertedId,
                'business_name' => $request->business_name,
                'address1' => $request->address1,
                'address2' => $request->address2,
                'address3' => $request->address3,
                'city' => $request->city,
                'state' => $request->state,
                'country' => $request->country,
                'postal_code' => $request->postal_code,
                'telephone' => $request->telephone,
                'title' => $request->title,
                'first_name' => $request->first_name,
                'middle_name' => $request->middle_name,
                'last_name' => $request->last_name,
                'designation' => $request->designation,
                //'department' => $request->department,
                'office_location' => $request->office_location,
                'mobile' => $request->mobile,
                'email' => $request->email,
                'agent' => $request->agent,
                'license_type' => $request->license_type,
                'license_type_opt' => $request->license_type_opt,
                'license_number' => $request->license_number,
                'license_status' => $request->license_status,
                'license_expiry_date' => $request->license_expiry_date,
                'license_home_state' => $request->license_home_state,
                'location_of_services' => $request->location_of_services,
                'number_of_agents' => $request->number_of_agents,
                'business_turn_over' => $request->business_turn_over,
                'number_of_years_in_business' => $request->number_of_years_in_business,
                'lead_to_prospect_ratio' => $request->lead_to_prospect_ratio,
                
            ]);
         }


         if($request->role == '3'){
            $insert = DB::table('user_details')->insert([
                'user_id' =>  $lastInsertedId,
                'address1' => $request->address1,
                'address2' => $request->address2,
                'address3' => $request->address3,
                'city' => $request->city,
                'state' => $request->state,
                'country' => $request->country,
                'postal_code' => $request->postal_code,
                'title' => $request->title,
                'first_name' => $request->first_name,
                'middle_name' => $request->middle_name,
                'last_name' => $request->last_name,
                'designation' => $request->designation,
               // 'department' => $request->department,
                'office_location' => $request->office_location,
                 'mobile' => $request->mobile,
                'email' => $request->email,
                'license_type' => $request->license_type,
                'license_type_opt' => $request->license_type_opt,
                'license_number' => $request->license_number,
                'license_status' => $request->license_status,
                'license_expiry_date' => $request->license_expiry_date,
                'license_home_state' => $request->license_home_state,
                'location_of_services' => $request->location_of_services,
                'business_turn_over' => $request->business_turn_over,
                'number_of_years_in_business' => $request->number_of_years_in_business,
                
                
            ]);
         }


         if($request->hasfile('idFiles'))
         {
          
            $total = count($request->file('idFiles'));
            for( $i=0 ; $i < $total ; $i++ ) {
            
                $fileNameVal = time().rand(1,100).'.'.$request->file('idFiles')[$i]->extension();
                $filePathVal =  $request->file('idFiles')[$i]->store('public/files');  
                $files[] = $fileNameVal; 
                //$imageType = $request->filetitle[$i];
               


                $insert = DB::table('document_path')->insert([
                   'user_id' =>  $lastInsertedId,
                   'doucment_path' => $filePathVal,
                    'document_type'=>'idproof'
                 ]);
              
            }
            
         }

        

        /*  if($request->hasfile('files'))
         {
            foreach($request->file('files') as $key => $file)
            {
                $path = $file->store('public/files');
             
                $insert = DB::table('document_path')->insert([
                   'user_id' =>  $lastInsertedId,
                   'doucment_path' => $path,
                
                 ]);
 
            }
         }*/

         $files = [];
      
        if($request->hasfile('filenames'))
         {
          
            $total = count($request->file('filenames'));
            for( $i=0 ; $i < $total ; $i++ ) {
            
                $fileNameVal = time().rand(1,100).'.'.$request->file('filenames')[$i]->extension();
                $filePathVal =  $request->file('filenames')[$i]->store('public/files');  
                $files[] = $fileNameVal; 
                $imageType = $request->filetitle[$i];
                $documentNameVal = $request->filetitle[$i];


                $insert = DB::table('document_path')->insert([
                   'user_id' =>  $lastInsertedId,
                   'doucment_path' => $filePathVal,
                    'document_name'=>$documentNameVal,
                    'document_type'=>'business'
                 ]);
              
            }
            
         }

         if (Auth::check()) {
            $userRoleVal = Auth::user()->role;
            $userIdVal = Auth::user()->id;
            $activateStatus = DB::table('user_details')->where('user_id', $lastInsertedId)->update(array('parent_entity_type' => $userRoleVal,'parent_entity_id'=>$userIdVal));
            
        }
        /*else{
            echo "sdfsddf";
        }
        exit;*/




           $messageData = [
                'email' => $emailId,
                'name' => $name,
                'codeVal' => base64_encode($emailId)
                
            ];

            Mail::send('account_activation_email', $messageData, function($message) use ($emailId) {
            $message->to($emailId)
            ->subject("Activate your account");
            $message->from("info@cyruslife.com","Activate your account");
            });
 
       /*if($request->role == '1'){
          return redirect(url('/partner_registration'))->with(['message-success'=>'User has been created successfully. Please check your mail to activate the account']);
      }

       if($request->role == '2'){
         return redirect(url('/agency_registration'))->with(['message-success'=>'User has been created successfully. Please check your mail to activate the account']);
       }

       if($request->role == '3'){
         return redirect(url('/agent_registration'))->with(['message-success'=>'User has been created successfully. Please check your mail to activate the account']);
       }*/

       return redirect(url('/thankyou'));

    }

   
}
