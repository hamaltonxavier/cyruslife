@include('include.admin_header')	


	<div class="content-body pb-5 pt-5">
		
		<div class="container w-75">
				
				<!-- register form stepper -->
					<form id="partnerRegistrationForm" class="registrationForm" method="POST" action="{{ route('add_user') }}" enctype="multipart/form-data" onsubmit="return adminAddUserValidation()" >
				
  @csrf
					<div class="row">
					  <div class="col-md-12">
						<div class="card card-default">
						  <div class="card-header">
							<h3 class="mt-3 font-weight700 color-blue pl-4">Add User</h3>
						  </div>
						  <div class="card-body p-5">
							
							
							 
								
								<div id="register-part-1" class="content">
									 <h4 class="color-blue">Business Name and Address</h4>
									<hr class="pb-2">
								  <div class="row">
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Name of the Business <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control"  value="" name="business_name" id="business_name"  >
											<input type="hidden" id="formType" value="insert" />
											<input type="hidden" name="role" id="roleTypeVal" value="2">
										  </div>
									</div>

									<div class="col-md-6  col-lg-6">
										<div class="form-group">
											<div class="form-group">
											<label>Email <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control" placeholder="Email Address" id="email" name="email" value="">
											<input type="hidden" class="uniqueEmailError" value="0" />
											<span class="emailErrorTxt error" ></span>  
										  </div>
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										 <div class="form-group">
											<label>Physical Address 1</label>
											<input type="text" class="form-control"  value=""  name="address1" id="address1">
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										 <div class="form-group">
											<label>Physical Address 2</label>
											<input type="text" class="form-control"  value=""  name="address2" id="address2">
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										 <div class="form-group">
											<label>Physical Address 3</label>
											<input type="text" class="form-control"  value=""  name="address3" id="address3">
										  </div>
									</div>
									
									
									<div class="col-md-6 col-lg-4">
										<div class="form-group">
												  <label>Country <span class="requiredSymbol">*</span></label>
												   <select class="form-control" id="countryList" name="country">
													<option value="">Select Country</option>

													@foreach($countryDetails as $data)
													<option value="{{$data->id}}">{{$data->name}}</option>
													@endforeach
												  </select>
												</div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										<div class="form-group">
												  <label>State <span class="requiredSymbol">*</span></label>
												   <select class="form-control" id="stateList" name="state">
												  	<option value="">Select State</option>
												  </select>
												</div>
									</div>
									
									<div class="col-md-6 col-lg-4">
										<div class="form-group">
												  <label>City <span class="requiredSymbol">*</span></label>
												  <select class="form-control" id="cityList" name="city">
													<option value="">Select City</option>
												  </select>
												</div>
									</div>
									
									<div class="col-md-6  col-lg-6">
										<div class="form-group">
													<label>Postal Code</label>
													<input type="text" class="form-control numberOnly" maxlength="6"  placeholder="Postal Code" value=""  name="postal_code" id="postal_code">
												  </div>
									</div>
									
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Main Business Telephone</label>
											<input type="text" class="form-control numberOnly"  value="" name="telephone" id="telephone" >
										  </div>
									</div>
									
								  </div>
								 
								</div>
								
								<!-- registration part 2-->
								<div id="register-part-2" class="content mt-5">
								  <h4 class="color-blue">Agency Manager details</h4>
									<hr class="pb-2">
								  <div class="row">
									<div class="col-md-3 col-lg-2">
										<label>Salutation/Title</label>
										<select class="form-control" id="title" name="title">

											<option value="Mr" >Mr</option>
											<option value="Mrs" >Mrs</option>
										</select>
									</div>
									
									<div class="col-md-9 col-lg-4">
										<div class="form-group">
											<label>First Name <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control alphabetsOnly"  value=""  id="first_name" name="first_name" >
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-3">
										<div class="form-group">
											<label>Middle Name</label>
											<input type="text" class="form-control alphabetsOnly"  value=""  id="middle_name" name="middle_name">
										  </div>
									</div>
									
									<div class="col-md-6 col-lg-3">
										<div class="form-group">
											<label>Last Name <span class="requiredSymbol">*</span></label>
											<input type="text" class="form-control alphabetsOnly"  value=""  id="last_name" name="last_name">
										  </div>
									</div>
									
								  </div>
								  
								  <div class="row">
									<div class="col-md-6 col-lg-6">
										<label>Mobile Phone</label>
										<input type="text" class="form-control numberOnly"  value="" id="mobile" name="mobile" >
									</div>
									
									
									
									<div class="col-lg-12 pt-4">
										
										
										<div class="form-group">
											<div class="row">
												<div class="col-md-3 col-lg-2">
													<label>Agent (Y/N) </label>
												</div>
												<div class="col-md-1 col-lg-1">
													<div class="form-check">
													  <input class="form-check-input" name="agent" type="radio" checked="" value="yes" >
													  <label class="form-check-label">Yes</label>
													</div>
												</div>
												<div class="col-md-1 col-lg-1">
													<div class="form-check">
													  <input class="form-check-input" name="agent" type="radio" value="no" >
													  <label class="form-check-label">No</label>
													</div>
												</div>
											</div>
											
										</div>
									</div>

									
								  </div>
								  
								</div>
								
								
								<!-- register form step 3-->
								<div id="register-part-3" class="content mt-5">
								  <h4 class="color-blue">Licence / ID</h4>
									<hr class="pb-2">
								  
								  <div class="row">
									
									<div class="col-lg-6">
										<div class="form-group">
											<label>Business License </label>
											<input type="text" class="form-control" id="license_type" name="license_type"  value=""  >
											
										  </div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>Insurance License </label>
											<input type="text" class="form-control"  id="license_number" name="license_number" value=""  >
										  </div>
									</div>
									</div>
<div class="form-group row mb-2">
									<label class="col-11 col-sm-11 col-md-11 col-lg-4 col-form-label">License type 
									</label>
									
									<div class="col-sm-7 col-lg-7 pl-0">
										<div class="row">
										  <div class="col-lg-11">
											<div class="form-group d-flex align-items-start mb-2">
												<input type="radio" class="form-check-input ml-0" id="license_type_opt" name="license_type_opt" value="Limited">
												<label class="form-check-label pl-4">Limited (dollar amount and/or insurance type restriction) </label>
											</div>
										  </div>
										</div>
										
									
										<div class="row ">
										  <div class="col-lg-11">
											<div class="form-group d-flex align-items-start mb-2">
												<input type="radio" class="form-check-input ml-0" id="license_type_opt" name="license_type_opt" value="Full">
												<label class="form-check-label pl-4">Full (any life product, any dollar amount)</label>
											</div>
										  </div>
										</div>
										
										
									</div>
								</div> <!-- row end -->
									<div class="row">
									
									<div class="col-lg-6">
										<div class="form-group">
											<label>License Status </label>
											<select class="form-control" id="license_status" name="license_status">
											<option value="active">Active</option>
											<option value="expired" >Expired</option>
										</select>
										  </div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>Expiration Date </label>
											<input type="text" class="form-control" id="license_expiry_date" name="license_expiry_date"  value=""  >
										  </div>
									</div>
									</div>

									<div class="row">
									
									<div class="col-lg-6">
										<div class="form-group">
											<label>Designated Home State </label>
											<!--<input type="text" class="form-control" id="license_home_state" name="license_home_state"  value=""  >-->
											<select class="form-control" id="license_home_state" name="license_home_state">
												  	<option value="">Select State</option>
												  </select>
										  </div>
									</div>
									
									</div>

								</div>
								
								
								<!-- register form step 4-->
								<div id="register-part-4" class="content mt-5">
								   <h4 class="color-blue">Business Overview</h4>
									<hr class="pb-2">
								  <div class="row">								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Location of services (Regions)</label>
											<input type="text" class="form-control" id="location_of_services" name="location_of_services"  value=""  >
											
										  </div>
									</div>
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Number of Agents</label>	
										<input type="text" class="form-control numberOnly" id="number_of_agents" name="number_of_agents"  value=""  >
										</div>
									</div>
								  </div>
								  
								  <div class="row">								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Business turn over (range)</label>
											<!--<input type="text" class="form-control" id="business_turn_over" name="business_turn_over"  value=""  >-->
											<div class="input-group">
												<div class="input-group-prepend">
												<span class="input-group-text"><b>$</b></span>
												</div>
												<!--<input type="text" class="form-control numberOnly" placeholder="Annual preneed volume" id="annual_preneed_volume" name="annual_preneed_volume">-->
												<select class="form-control" id="business_turn_over" name="business_turn_over">
														<option value="100 - 1,000">100 - 1,000</option>
														<option value="1,000 - 10,000">1,000 - 10,000</option>
														<option value="10,000 - 100,000">10,000 - 100,000</option>
														<option value="100,000 - 1,000,000">100,000 - 1,000,000</option>
														<option value="1,000,000 - 10,000,000">1,000,000 - 10,000,000</option>
													</select>

											</div>
										  </div>
									</div>
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Number of years in business</label>	
											<input type="text" class="form-control numberOnly" id="number_of_years_in_business" name="number_of_years_in_business"  value=""  >					
										</div>
									</div>
								  </div>
								  
								  <div class="row">								
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label>Lead to Prospect Ratio</label>
											<input type="text" class="form-control" id="lead_to_prospect_ratio" name="lead_to_prospect_ratio"  value=""  >
										  </div>
									</div>
									
									<!--<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<label for="exampleInputFile">Required Document Uploads</label>
											<div class="input-group">
											   <input type="file" name="idFiles[]" class="myfrm form-control">
											 
											</div>
											
										  </div>
									</div>-->
								  </div>
								 
								 <!--First-->
									<div class="row increment">
								  		<div class="col-md-6 col-lg-6">
												<div class="form-group">
													<div class="input-group realprocode control-group lst" >
								  				<!--<input type="text" name="filetitle[]" class="myfrm form-control" placeholder="Document Name">-->
								  				<select class="form-control" id="" name="filetitle[]">
														<option value="Producer-Agreement">Producer Agreement</option>
														<option value="Direct-Deposit-Authorization">Direct Deposit Authorization</option>
														<option value="Producer-Compensation-Schedule">Producer Compensation Schedule</option>
														<option value="Explanation-of-Information-found-on-MIB">Explanation of Information found on MIB</option>
														<option value="Other-Supporting-Documentation">Other Supporting Documentation</option>
													</select>
											      
											    </div>
												</div>
											</div>

											<div class="col-md-6 col-lg-6">
												<div class="form-group">
													<div class="input-group realprocode control-group lst " >
								  				
											      <input type="file" accept=".gif,.jpg,.jpeg,.png,.doc,.docx,.pdf" name="filenames[]" class="myfrm form-control">
											      <div class="input-group-btn"> 
											        <button class="btn btn-success addFile" type="button"> <i class="fldemo glyphicon glyphicon-plus"></i>Add</button>
											      </div>
											    </div>
												</div>
											</div>
								  </div>

								<!--Second-->
							<div class="clone d-none " >
								<div class="row ">
									<div class="realprocode col-md-12 col-lg-12">
								  		<div class="col-md-6 col-lg-6" style="float: left;">
												<div class="form-group">
													<div class=" control-group lst input-group" style="margin-top:10px">
											      	<!--<input type="text" name="filetitle[]" class="myfrm form-control" placeholder="Document Name">-->
											      	<select class="form-control" id="" name="filetitle[]">
														<option value="Driver-License">State-Issued Driver's License</option>
														<option value="Producer-Agreement">Producer Agreement</option>
														<option value="Direct-Deposit-Authorization">Direct Deposit Authorization</option>
														<option value="Producer-Compensation-Schedule">Producer Compensation Schedule</option>
														<option value="Explanation-of-Information-found-on-MIB">Explanation of Information found on MIB</option>
														<option value="Other-Supporting-Documentation">Other Supporting Documentation</option>
													</select>
											        
											      </div>
												</div>
											</div>

											<div class="col-md-6 col-lg-6" style="float: left;">
												<div class="form-group">
													<div class=" control-group lst input-group" style="margin-top:10px">
											      	
											        <input type="file" accept=".gif,.jpg,.jpeg,.png,.doc,.docx,.pdf" name="filenames[]" class="myfrm form-control">
											        <div class="input-group-btn"> 
											          <button class="btn btn-danger" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>
											        </div>
											      </div>
												</div>
											</div>
										</div>
								  </div>

								</div>
								</div>
								<button type="submit" class="mt-4 btn btn-lg btn-primary ">ADD DETAILS</button>
								<span  class="error finalErr"></span>
							 
							
						  </div>
						  <!-- /.card-body -->
						  
						</div>
						<!-- /.card -->
					  </div>
					</div>
				</form>
				<!-- register form stepper -->
				
			</div>
		
	</div>
	
	@include('include.admin_footer')

<script type="text/javascript">
    $(document).ready(function() {
    	
      $(".addFile").click(function(){ 
          var lsthmtl = $(".clone").html();
          $(".increment").after(lsthmtl);
      });
      $("body").on("click",".btn-danger",function(){ 
          $(this).parents(".realprocode").remove();
      });
    });
</script>